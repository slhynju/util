package name.slhynju.util.swt;

import name.slhynju.util.I18NResource;
import org.jetbrains.annotations.NotNull;

public interface I18NEnabled {

    public void setText(@NotNull String text);

    public default void setI18NText(@NotNull String family, @NotNull String key) {
        setText(I18NResource.getDefaultText(family, key));
    }
}
