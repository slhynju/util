package name.slhynju.util.swt.menu;

import org.eclipse.swt.widgets.MenuItem;
import org.jetbrains.annotations.NotNull;

public class SubMenuSupport extends DefaultMenuSupport {

    public SubMenuSupport(@NotNull MenuItem item) {
        super(item.getParent());
        item.setMenu(widget);
    }

}
