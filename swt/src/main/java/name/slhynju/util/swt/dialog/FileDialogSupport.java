package name.slhynju.util.swt.dialog;

import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static org.eclipse.swt.SWT.getPlatform;

public class FileDialogSupport {

    protected final FileDialog dialog;

    public FileDialogSupport(@NotNull Shell shell, @Nullable String fileName, int mode) {
        dialog = new FileDialog(shell, mode);
        dialog.setFilterPath(getFilterPath());
        dialog.setFileName(fileName);
    }

    public void setFilterNames(@Nullable String... filterNames) {
        dialog.setFilterNames(filterNames);
    }

    public void setFilterExtensions(@Nullable String... extensions) {
        dialog.setFilterExtensions(extensions);
    }

    @Nullable
    public String open() {
        return dialog.open();
    }

    @NotNull
    public static final String getFilterPath() {
        String platform = getPlatform();
        if ("win32".equals(platform) || "wpf".equals(platform)) {
            return "c:\\";
        }
        return "/";
    }

}
