package name.slhynju.util.swt.table;

import name.slhynju.ApplicationException;
import name.slhynju.UnexpectedApplicationFlowException;
import name.slhynju.util.swt.listener.SelectionListenerSupport;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.TableColumn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

import static org.eclipse.swt.SWT.*;

public final class SortListener extends SelectionListenerSupport {

    private final TableSupport table;

    private TextComparator comparator;

    public SortListener(@NotNull TableSupport table) {
        this.table = table;
        comparator = null;
    }

    @SuppressWarnings("null")
    @Override
    public void widgetSelected(@NotNull SelectionEvent event) {
        if (comparator == null) {
            throw new ApplicationException("TextComparator not initialized.");
        }
        // determine new sort column and direction
        TableColumn oldSortColumn = table.getSortColumn();
        TableColumn newSortColumn = (TableColumn) event.widget;
        processSortColumn(oldSortColumn, newSortColumn);
        int direction = processSortDirection(oldSortColumn, newSortColumn);
        // sort the data based on column and direction
        String[][] itemTexts = sortItemTexts(newSortColumn, direction);
        // display sorted result
        displaySortedItems(itemTexts);
    }

    @Nullable
    public TextComparator getComparator() {
        return comparator;
    }

    public void setComparator(@NotNull TextComparator comparator) {
        this.comparator = comparator;
    }

    private void processSortColumn(@NotNull TableColumn oldSortColumn, @NotNull TableColumn newSortColumn) {
        if (oldSortColumn != newSortColumn) {
            table.setSortColumn(newSortColumn);
        }
    }

    private int processSortDirection(@NotNull TableColumn oldSortColumn, @NotNull TableColumn newSortColumn) {
        int direction = table.getSortDirection();
        if (oldSortColumn == newSortColumn) {
            direction = swapDirection(direction);
        } else {
            direction = UP;
        }
        table.setSortDirection(direction);
        return direction;
    }

    private static int swapDirection(int direction) {
        switch (direction) {
            case UP:
                return DOWN;
            case DOWN:
                return UP;
            case NONE:
                return NONE;
            default:
                throw new UnexpectedApplicationFlowException();
        }
    }

    @NotNull
    private String[][] sortItemTexts(@NotNull TableColumn newSortColumn, int direction) {
        String[][] itemTexts = table.getItemTexts();
        int index = table.indexOf(newSortColumn);
        comparator.setIndex(index);
        comparator.setDirection(direction);
        Arrays.sort(itemTexts, comparator);
        return itemTexts;
    }

    private void displaySortedItems(@NotNull String[][] itemTexts) {
        table.removeAll();
        for (String[] itemText : itemTexts) {
            table.addItem(itemText);
        }
    }

}
