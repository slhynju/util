package name.slhynju.util.swt.table;

public enum TextComparisonStrategy {

    TEXT, LOCALIZED_TEXT, INTEGER, CUSTOMIZED;
}
