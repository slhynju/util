package name.slhynju.util.swt.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

public class CloseShellListener extends SelectionListenerSupport {

    protected final Shell shell;

    public CloseShellListener(@NotNull Shell shell) {
        this.shell = shell;
    }

    @Override
    public void widgetSelected(SelectionEvent event) {
        shell.close();
    }

}
