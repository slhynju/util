package name.slhynju.util.swt.graphics;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.jetbrains.annotations.NotNull;

public final class GraphicUtil {

    @NotNull
    public static Image captureScreen(@NotNull Display display) {
        GC gc = new GC(display);
        Image image = new Image(display, display.getBounds());
        gc.copyArea(image, 0, 0);
        gc.dispose();
        return image;
    }

}
