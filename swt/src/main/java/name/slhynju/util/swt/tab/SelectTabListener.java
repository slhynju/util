package name.slhynju.util.swt.tab;

import name.slhynju.util.swt.listener.SelectionListenerSupport;
import org.eclipse.swt.events.SelectionEvent;
import org.jetbrains.annotations.NotNull;

public class SelectTabListener extends SelectionListenerSupport {

    private final CTabFolderSupport tabFolder;

    private final int index;

    public SelectTabListener(@NotNull CTabFolderSupport tabFolder, int index) {
        this.tabFolder = tabFolder;
        this.index = index;
    }

    @Override
    public void widgetSelected(@NotNull SelectionEvent event) {
        tabFolder.setSelection(index);
    }

}
