package name.slhynju.util.swt.menu;

import name.slhynju.util.I18NResource;
import name.slhynju.util.swt.ShellSupport;
import name.slhynju.util.swt.WidgetSupport;
import name.slhynju.util.swt.listener.OpenShellListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;

import static org.eclipse.swt.SWT.*;

public class DefaultMenuSupport extends WidgetSupport<Menu> {

    public DefaultMenuSupport(@NotNull Shell parent, int style) {
        widget = new Menu(parent, style);
    }

    public DefaultMenuSupport(@NotNull Menu menu) {
        widget = new Menu(menu);
    }

    public void disposeMenuItems() {
        MenuItem[] items = widget.getItems();
        for (MenuItem item : items) {
            item.dispose();
        }
    }

    public void addSeparator() {
        new MenuItem(widget, SEPARATOR);
    }

    @NotNull
    public Shell getShell() {
        return widget.getShell();
    }

    @NotNull
    public MenuItem addOpenShellItem(@NotNull I18NResource resource, @NotNull String key, @NotNull Function<Shell, ShellSupport> action) {
        String text = resource.get(key);
        return addOpenShellItem(text, action);
    }

    @NotNull
    public MenuItem addOpenShellItem(@NotNull String text, @NotNull Function<Shell, ShellSupport> action) {
        SelectionListener listener = new OpenShellListener(widget.getShell(), action);
        return addPushItem(text, listener);
    }

    @NotNull
    public MenuItem addPushItem(@NotNull I18NResource resource, @NotNull String key, @Nullable SelectionListener listener) {
        String text = resource.get(key);
        return addPushItem(text, listener);
    }

    @NotNull
    public MenuItem addPushItem(@NotNull String text, @Nullable SelectionListener listener) {
        return addItem(text, PUSH, listener);
    }

    @NotNull
    public MenuItem addCascadeItem(@NotNull I18NResource resource, @NotNull String key) {
        String text = resource.get(key);
        return addCascadeItem(text);
    }

    @NotNull
    public MenuItem addCascadeItem(@NotNull String text) {
        return addItem(text, CASCADE, null);
    }

    @NotNull
    public MenuItem addItem(@NotNull String text, int style, @Nullable SelectionListener listener) {
        MenuItem item = new MenuItem(widget, style);
        item.setText(text);
        if (listener != null) {
            item.addSelectionListener(listener);
        }
        return item;
    }

    public void setEnabled(boolean enabled) {
        widget.setEnabled(enabled);
    }

}
