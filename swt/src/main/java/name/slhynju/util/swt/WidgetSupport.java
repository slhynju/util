package name.slhynju.util.swt;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Widget;
import org.jetbrains.annotations.NotNull;

public class WidgetSupport<T extends Widget> {

    protected T widget;

    @NotNull
    public T getWidget() {
        return widget;
    }

    public void dispose() {
        widget.dispose();
    }

    @NotNull
    public Display getDisplay() {
        return widget.getDisplay();
    }

    public boolean isDisposed() {
        return widget.isDisposed();
    }

}
