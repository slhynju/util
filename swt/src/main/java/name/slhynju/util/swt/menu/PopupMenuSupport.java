package name.slhynju.util.swt.menu;

import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.POP_UP;

public class PopupMenuSupport extends DefaultMenuSupport {

    public PopupMenuSupport(@NotNull Shell parent) {
        super(parent, POP_UP);
    }

}
