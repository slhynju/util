package name.slhynju.util.swt;

import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ControlSupport<T extends Control> {

    protected T control;

    @NotNull
    public T getControl() {
        return control;
    }

    // methods inherited from Widget

    public void dispose() {
        control.dispose();
    }

    @NotNull
    public Display getDisplay() {
        return control.getDisplay();
    }

    public boolean isDisposed() {
        return control.isDisposed();
    }

    // methods of Control

    public void addMouseListener(@NotNull MouseListener listener) {
        control.addMouseListener(listener);
    }

    @Nullable
    public Composite getParent() {
        return control.getParent();
    }

    @NotNull
    public Shell getShell() {
        return control.getShell();
    }

    public void pack() {
        control.pack();
    }

    public void redraw() {
        control.redraw();
    }

    public void setEnabled(boolean enabled) {
        control.setEnabled(enabled);
    }

    public void setLayoutData(@NotNull Object data) {
        control.setLayoutData(data);
    }

    public void setMenu(@NotNull Menu menu) {
        control.setMenu(menu);
    }

    public void setSize(int width, int height) {
        control.setSize(width, height);
    }

}
