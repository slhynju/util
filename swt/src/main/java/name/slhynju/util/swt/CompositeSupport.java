package name.slhynju.util.swt;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.jetbrains.annotations.NotNull;

public class CompositeSupport<T extends Composite> extends ControlSupport<T> {

    public void setLayout(@NotNull Layout layout) {
        control.setLayout(layout);
    }

    @NotNull
    public Composite getComposite() {
        return control;
    }
}
