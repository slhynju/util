package name.slhynju.util.swt.combo;

import name.slhynju.util.swt.LabeledControlSupport;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.jetbrains.annotations.NotNull;

public class LabeledComboSupport extends LabeledControlSupport<Combo> {

    public LabeledComboSupport(@NotNull Composite parent, int style) {
        super(parent);
        control = new Combo(parent, style);
    }

    public void add(@NotNull String text) {
        control.add(text);
    }

    public void select(int index) {
        control.select(index);
    }

    @NotNull
    public String getText() {
        return control.getText();
    }

}
