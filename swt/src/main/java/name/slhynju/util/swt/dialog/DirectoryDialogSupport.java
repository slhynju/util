package name.slhynju.util.swt.dialog;

import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DirectoryDialogSupport {

    protected final DirectoryDialog dialog;

    public DirectoryDialogSupport(@NotNull Shell parent) {
        dialog = new DirectoryDialog(parent);
        dialog.setFilterPath(FileDialogSupport.getFilterPath());
    }

    @Nullable
    public String open() {
        return dialog.open();
    }

}
