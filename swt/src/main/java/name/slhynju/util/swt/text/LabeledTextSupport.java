package name.slhynju.util.swt.text;

import name.slhynju.util.swt.I18NEnabled;
import name.slhynju.util.swt.LabeledControlSupport;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.BORDER;

public class LabeledTextSupport extends LabeledControlSupport<Text> implements I18NEnabled {

    public LabeledTextSupport(@NotNull Composite parent) {
        super(parent);
        control = new Text(parent, BORDER);
    }

    @Override
    public void setText(@NotNull String text) {
        control.setText(text);
    }

    @NotNull
    public String getText() {
        return control.getText();
    }

}
