package name.slhynju.util.swt.table;

import name.slhynju.UnexpectedApplicationFlowException;
import name.slhynju.util.CompareToBuilder;
import name.slhynju.util.NumberUtil;
import org.jetbrains.annotations.NotNull;

import java.text.Collator;
import java.util.HashMap;
import java.util.Map;

import static org.eclipse.swt.SWT.UP;

public class DefaultTextComparator implements TextComparator {

    protected int index;

    protected int direction;

    protected final Collator collator;

    protected final Map<Integer, TextComparisonStrategy> strategies;

    public DefaultTextComparator() {
        index = 0;
        direction = UP;
        collator = Collator.getInstance();
        strategies = new HashMap<>();
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public void setDirection(int direction) {
        this.direction = direction;
    }

    @Override
    public void addStrategy(@NotNull TextComparisonStrategy strategy, @NotNull int... columnIndexes) {
        for (int columnIndex : columnIndexes) {
            strategies.put(Integer.valueOf(columnIndex), strategy);
        }
    }

    @Override
    public int compare(@NotNull String[] a, @NotNull String[] b) {
        String s1 = null;
        String s2 = null;
        if (direction == UP) {
            s1 = a[index];
            s2 = b[index];
        } else {
            s1 = b[index];
            s2 = a[index];
        }
        TextComparisonStrategy strategy = getStrategy();
        switch (strategy) {
            case TEXT:
                return compareText(s1, s2);
            case LOCALIZED_TEXT:
                return compareLocalizedText(s1, s2);
            case INTEGER:
                return compareInteger(s1, s2);
            case CUSTOMIZED:
                return compareCustomized(s1, s2);
            default:
                throw new UnexpectedApplicationFlowException();
        }
    }

    @NotNull
    private TextComparisonStrategy getStrategy() {
        TextComparisonStrategy strategy = strategies.get(Integer.valueOf(index));
        if (strategy == null) {
            return TextComparisonStrategy.TEXT;
        }
        return strategy;
    }

    protected static final int compareText(@NotNull String s1, @NotNull String s2) {
        return new CompareToBuilder().append(s1, s2).toValue();
    }

    protected final int compareLocalizedText(@NotNull String s1, @NotNull String s2) {
        return collator.compare(s1, s2);
    }

    @SuppressWarnings("static-method")
    protected int compareInteger(@NotNull String s1, @NotNull String s2) {
        int n1 = NumberUtil.toInt(s1, -1);
        int n2 = NumberUtil.toInt(s2, -1);
        return new CompareToBuilder().append(n1, n2).toValue();
    }

    @SuppressWarnings("static-method")
    protected int compareCustomized(@NotNull String s1, @NotNull String s2) {
        return compareText(s1, s2);
    }
}
