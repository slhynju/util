package name.slhynju.util.swt.group;

import name.slhynju.util.swt.CompositeSupport;
import name.slhynju.util.swt.I18NEnabled;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.NONE;

public class GroupSupport extends CompositeSupport<Group> implements I18NEnabled {

    public GroupSupport(@NotNull Composite parent) {
        control = new Group(parent, NONE);
    }

    @Override
    public void setText(@NotNull String text) {
        control.setText(text);
    }
}
