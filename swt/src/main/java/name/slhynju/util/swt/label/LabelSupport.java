package name.slhynju.util.swt.label;

import name.slhynju.util.swt.ControlSupport;
import name.slhynju.util.swt.I18NEnabled;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.NONE;

public class LabelSupport extends ControlSupport<Label> implements I18NEnabled {

    public LabelSupport(@NotNull Composite parent) {
        control = new Label(parent, NONE);
    }

    @Override
    public void setText(@NotNull String text) {
        control.setText(text);
    }
}
