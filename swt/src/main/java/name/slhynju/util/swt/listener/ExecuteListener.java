package name.slhynju.util.swt.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.jetbrains.annotations.NotNull;

public class ExecuteListener extends SelectionListenerSupport {

    private final Runnable task;

    public ExecuteListener(@NotNull Runnable task) {
        this.task = task;
    }

    @Override
    public void widgetSelected(SelectionEvent event) {
        task.run();
    }

}
