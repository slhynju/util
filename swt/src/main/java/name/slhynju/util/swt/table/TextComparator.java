package name.slhynju.util.swt.table;

import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public interface TextComparator extends Comparator<String[]> {

    public void setIndex(int index);

    public void setDirection(int direction);

    public void addStrategy(@NotNull TextComparisonStrategy strategy,
                            @NotNull int... columnIndexes);
}
