package name.slhynju.util.swt.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

public abstract class SelectionListenerSupport implements SelectionListener {

    @Override
    public void widgetDefaultSelected(SelectionEvent event) {
        widgetSelected(event);
    }

}
