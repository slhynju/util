package name.slhynju.util.swt.dialog;

import name.slhynju.util.swt.ShellSupport;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.APPLICATION_MODAL;
import static org.eclipse.swt.SWT.DIALOG_TRIM;

public class DialogSupport extends ShellSupport {

    public DialogSupport(@NotNull Shell parent, int width, int height) {
        control = new Shell(parent, DIALOG_TRIM | APPLICATION_MODAL);
        control.setSize(width, height);
    }

}
