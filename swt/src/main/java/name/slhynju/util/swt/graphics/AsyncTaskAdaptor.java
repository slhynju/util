package name.slhynju.util.swt.graphics;

import org.eclipse.swt.widgets.Display;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncTaskAdaptor implements Runnable {

    private final Display display;

    private final Runnable task;

    private final Logger log;

    public AsyncTaskAdaptor(@NotNull Display display, @NotNull Runnable task) {
        this.display = display;
        this.task = task;
        this.log = LoggerFactory.getLogger(AsyncTaskAdaptor.class);
    }

    @Override
    public void run() {
        try {
            display.asyncExec(task);
        } catch (Exception e) {
            log.error("Failed to execute async task.", e);
        }
    }

}
