package name.slhynju.util.swt.menu;

import name.slhynju.util.swt.WidgetSupport;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.BAR;

public class MenuBarSupport extends WidgetSupport<Menu> {

    public MenuBarSupport(@NotNull Shell shell) {
        widget = new Menu(shell, BAR);
        shell.setMenuBar(widget);
    }

    @NotNull
    public Shell getShell() {
        return widget.getShell();
    }

}
