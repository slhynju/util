package name.slhynju.util.swt.tab;

import name.slhynju.util.swt.CompositeSupport;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.widgets.Composite;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.BORDER;

public class CTabFolderSupport extends CompositeSupport<CTabFolder> {

    public CTabFolderSupport(@NotNull Composite parent) {
        control = new CTabFolder(parent, BORDER);
        control.setSimple(false);
        control.setUnselectedCloseVisible(true);
    }

    public void setSelection(@NotNull CTabItemSupport item) {
        control.setSelection(item.getWidget());
    }

    public void setSelection(int index) {
        control.setSelection(index);
    }

}
