package name.slhynju.util.swt;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.NONE;
import static org.eclipse.swt.SWT.SHELL_TRIM;

public class ApplicationSupport extends ShellSupport {

    protected final Display display;

    public ApplicationSupport(int width, int height) {
        this(width, height, NONE);
    }

    public ApplicationSupport(int width, int height, int style) {
        display = new Display();
        control = new Shell(display, SHELL_TRIM | style);
        control.setSize(width, height);
    }

    public void run() {
        control.open();
        while (!control.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        display.dispose();
    }

    @Override
    @NotNull
    public Display getDisplay() {
        return display;
    }

}
