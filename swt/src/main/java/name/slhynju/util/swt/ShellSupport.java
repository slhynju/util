package name.slhynju.util.swt;

import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

public class ShellSupport extends CompositeSupport<Shell> implements I18NEnabled {

    public void close() {
        control.close();
    }

    @Override
    @NotNull
    public Shell getShell() {
        return control;
    }

    public void open() {
        control.open();
    }

    public boolean isMinimized() {
        return control.getMinimized();
    }

    public void setMinimized(boolean minimized) {
        control.setMinimized(minimized);
    }

    public boolean isMaximized() {
        return control.getMaximized();
    }

    public void setMaximized(boolean maximized) {
        control.setMaximized(maximized);
    }

    @Override
    public void setText(@NotNull String text) {
        control.setText(text);
    }
}
