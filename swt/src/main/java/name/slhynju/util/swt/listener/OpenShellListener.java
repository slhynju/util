package name.slhynju.util.swt.listener;

import name.slhynju.util.swt.ShellSupport;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

public class OpenShellListener extends SelectionListenerSupport {

    private final Shell parent;

    private ShellSupport shell;

    private final Function<Shell, ShellSupport> shellCreator;

    public OpenShellListener(@NotNull Shell parent, @NotNull Function<Shell, ShellSupport> shellCreator) {
        this.parent = parent;
        shell = null;
        this.shellCreator = shellCreator;
    }

    @Override
    public void widgetSelected(SelectionEvent event) {
        if (shell == null || shell.isDisposed()) {
            shell = shellCreator.apply(parent);
        }
        shell.open();
        if (shell.isMinimized()) {
            shell.setMinimized(false);
        }
    }

}
