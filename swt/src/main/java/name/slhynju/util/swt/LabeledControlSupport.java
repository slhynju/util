package name.slhynju.util.swt;

import name.slhynju.util.I18NResource;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.NONE;

public class LabeledControlSupport<T extends Control> extends ControlSupport<T> {

    protected final Label label;

    public LabeledControlSupport(@NotNull Composite parent) {
        label = new Label(parent, NONE);
    }

    @NotNull
    public Label getLabel() {
        return label;
    }

    public void setLabelText(@NotNull String labelText) {
        label.setText(labelText);
    }

    public void setI18NLabelText(@NotNull String family, @NotNull String key) {
        String text = I18NResource.getDefaultText(family, key);
        setLabelText(text);
    }

}
