package name.slhynju.util.swt.menu;

import name.slhynju.util.swt.I18NEnabled;
import org.eclipse.swt.widgets.MenuItem;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.CASCADE;
import static org.eclipse.swt.SWT.DROP_DOWN;

public class DropdownMenuSupport extends DefaultMenuSupport implements I18NEnabled {

    protected final MenuItem barItem;

    public DropdownMenuSupport(@NotNull MenuBarSupport menuBar) {
        super(menuBar.getShell(), DROP_DOWN);
        barItem = new MenuItem(menuBar.getWidget(), CASCADE);
        barItem.setMenu(widget);
    }

    @NotNull
    public MenuItem getBarItem() {
        return barItem;
    }

    @Override
    public void setText(@NotNull String text) {
        barItem.setText(text);
    }

}
