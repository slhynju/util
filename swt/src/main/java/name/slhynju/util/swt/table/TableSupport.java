package name.slhynju.util.swt.table;

import name.slhynju.util.I18NResource;
import name.slhynju.util.collection.CollectionUtil;
import name.slhynju.util.swt.CompositeSupport;
import name.slhynju.util.swt.menu.PopupMenuSupport;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static org.eclipse.swt.SWT.NONE;

public class TableSupport extends CompositeSupport<Table> {

    protected final SortListener sortListener;

    public TableSupport(@NotNull Composite parent, int style) {
        control = new Table(parent, style);
        control.setLinesVisible(true);
        control.setHeaderVisible(true);
        sortListener = new SortListener(this);
    }

    public void setTextComparator(@NotNull TextComparator comparator) {
        sortListener.setComparator(comparator);
    }

    @Nullable
    public TextComparator getTextComparator() {
        return sortListener.getComparator();
    }

    public void setHeaders(@NotNull String family, @NotNull String... headerKeys) {
        I18NResource resource = I18NResource.getDefaultInstance(family);
        for (String headerKey : headerKeys) {
            TableColumn column = new TableColumn(control, NONE);
            column.setText(resource.get(headerKey));
            column.addSelectionListener(sortListener);
        }
    }

    public void setColumnWidths(@NotNull int... columnWidths) {
        for (int I = 0; I < columnWidths.length; I++) {
            TableColumn column = control.getColumn(I);
            column.setWidth(columnWidths[I]);
        }
    }

    @NotNull
    public TableItem addItem(@NotNull String... values) {
        TableItem item = new TableItem(control, NONE);
        for (int I = 0; I < values.length; I++) {
            item.setText(I, values[I]);
        }
        return item;
    }

    public void removeAll() {
        control.removeAll();
    }

    @Nullable
    public TableItem[] getSelection() {
        return control.getSelection();
    }

    @Nullable
    public TableItem getFirstSelection() {
        return CollectionUtil.getFirst(control.getSelection());
    }

    @Nullable
    public String getFirstSelectionText() {
        TableItem item = getFirstSelection();
        if (item == null) {
            return null;
        }
        return item.getText(0);
    }

    @NotNull
    public TableColumn getColumn(int index) {
        return control.getColumn(index);
    }

    public int indexOf(@NotNull TableColumn column) {
        return control.indexOf(column);
    }

    @NotNull
    public TableColumn getSortColumn() {
        return control.getSortColumn();
    }

    public void setSortColumn(@NotNull TableColumn column) {
        control.setSortColumn(column);
    }

    public void setSortColumn(int index) {
        control.setSortColumn(control.getColumn(index));
    }

    public int getSortDirection() {
        return control.getSortDirection();
    }

    public void setSortDirection(int direction) {
        control.setSortDirection(direction);
    }

    @NotNull
    public String[][] getItemTexts() {
        TableItem[] items = control.getItems();
        int columnCount = control.getColumnCount();
        String[][] itemTexts = new String[items.length][columnCount];
        for (int I = 0; I < items.length; I++) {
            TableItem item = items[I];
            String[] a = new String[columnCount];
            for (int J = 0; J < columnCount; J++) {
                a[J] = item.getText(J);
            }
            itemTexts[I] = a;
        }
        return itemTexts;
    }

    public void setMenu(@NotNull PopupMenuSupport menu) {
        control.setMenu(menu.getWidget());
    }

}
