package name.slhynju.util.swt.tab;

import name.slhynju.util.swt.I18NEnabled;
import name.slhynju.util.swt.WidgetSupport;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.NONE;

public class CTabItemSupport extends WidgetSupport<CTabItem> implements I18NEnabled {

    protected final Composite composite;

    protected final CTabFolderSupport parent;

    public CTabItemSupport(@NotNull CTabFolderSupport parent, int style) {
        this.parent = parent;
        composite = new Composite(parent.getControl(), NONE);
        widget = new CTabItem(parent.getControl(), style);
        widget.setControl(composite);
    }

    @NotNull
    public Composite getComposite() {
        return composite;
    }

    @NotNull
    public CTabFolderSupport getParent() {
        return parent;
    }

    @NotNull
    public Shell getShell() {
        return parent.getShell();
    }

    @Override
    public void setText(@NotNull String text) {
        widget.setText(text);
    }

}
