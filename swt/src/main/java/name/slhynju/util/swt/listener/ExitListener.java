package name.slhynju.util.swt.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Shell;
import org.jetbrains.annotations.NotNull;

public class ExitListener extends SelectionListenerSupport {

    protected final Shell shell;

    public ExitListener(@NotNull Shell shell) {
        this.shell = shell;
    }

    @Override
    public void widgetSelected(SelectionEvent event) {
        shell.dispose();
    }

}
