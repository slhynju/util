package name.slhynju.util.swt.dnd;

import org.eclipse.swt.dnd.*;
import org.eclipse.swt.widgets.Control;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.io.*;

public abstract class CustomTransfer<T> extends ByteArrayTransfer {

    protected final Class<T> type;

    protected final int id;

    protected final Logger log;

    public CustomTransfer(@NotNull Class<T> type, int id, @NotNull Logger log) {
        this.type = type;
        this.id = id;
        this.log = log;
    }

    @Override
    protected void javaToNative(@Nullable Object object, @Nullable TransferData data) {
        if (!type.isInstance(object)) {
            return;
        }
        if (!isSupportedType(data)) {
            return;
        }
        try {
            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(byteOut);
            out.writeObject(object);
            out.flush();
            byte[] buffer = byteOut.toByteArray();
            out.close();
            super.javaToNative(buffer, data);
        } catch (IOException e) {
            log.error("Failed to transform Java Object to native data.", e);
        }
    }

    @Override
    protected @Nullable Object nativeToJava(@Nullable TransferData data) {
        if (!isSupportedType(data)) {
            return null;
        }
        byte[] buffer = (byte[]) super.nativeToJava(data);
        if (buffer == null) {
            return null;
        }
        try {
            ByteArrayInputStream byteIn = new ByteArrayInputStream(buffer);
            ObjectInputStream in = new ObjectInputStream(byteIn);
            Object view = in.readObject();
            in.close();
            if (type.isInstance(view)) {
                return view;
            }
        } catch (IOException | ClassNotFoundException e) {
            log.error("Failed to transform native data to Java Object.", e);
        }
        return null;
    }

    @Override
    @NotNull
    protected int[] getTypeIds() {
        return new int[]{id};
    }

    @Override
    @NotNull
    protected String[] getTypeNames() {
        return new String[]{type.getName()};
    }

    public static void applyDragSource(@NotNull Transfer instance, @NotNull Control sourceControl, int style,
                                       @NotNull DragSourceListener listener) {
        Transfer[] transferTypes = new Transfer[]{instance};
        DragSource source = new DragSource(sourceControl, style);
        source.setTransfer(transferTypes);
        source.addDragListener(listener);
    }

    public static void applyDropTarget(@NotNull Transfer instance, @NotNull Control targetControl, int style,
                                       @NotNull DropTargetListener listener) {
        Transfer[] transferTypes = new Transfer[]{instance};
        DropTarget target = new DropTarget(targetControl, style);
        target.setTransfer(transferTypes);
        target.addDropListener(listener);
    }

}
