package name.slhynju.util.swt.styledtext;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.jetbrains.annotations.NotNull;

import static org.eclipse.swt.SWT.*;

public class LogsSupport extends StyledTextSupport {

    public LogsSupport(@NotNull Composite parent, @NotNull Display display) {
        super(parent, MULTI | READ_ONLY | WRAP | H_SCROLL | V_SCROLL);
        control.setAlignment(LEFT);
        control.setBackground(display.getSystemColor(COLOR_WHITE));
    }

    @Override
    public void append(@NotNull String message) {
        control.append(message);
        int height = control.getClientArea().y;
        int lineCount = height / control.getLineHeight();
        int newTopIndex = control.getLineCount() - lineCount + 1;
        if (newTopIndex > control.getTopIndex()) {
            control.setTopIndex(newTopIndex);
        }
        control.setHorizontalIndex(0);
    }

}
