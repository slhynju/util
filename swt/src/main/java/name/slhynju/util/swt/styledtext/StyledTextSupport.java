package name.slhynju.util.swt.styledtext;

import name.slhynju.util.swt.CompositeSupport;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.jetbrains.annotations.NotNull;

public class StyledTextSupport extends CompositeSupport<StyledText> {

    public StyledTextSupport(@NotNull Composite parent, int style) {
        control = new StyledText(parent, style);
    }

    public void setAlignment(int alignment) {
        control.setAlignment(alignment);
    }

    public void setBackground(@NotNull Color color) {
        control.setBackground(color);
    }

    public void append(@NotNull String message) {
        control.append(message);
    }

    public void append(@NotNull CharSequence message) {
        append(message.toString());
    }

}
