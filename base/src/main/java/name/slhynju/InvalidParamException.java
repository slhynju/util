package name.slhynju;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * An exception that indicates an invalid parameter. This class extends from {@code java.lang.IllegalArgumentException} instead of {@code ApplicationException}.
 *
 * @author slhynju
 */
public class InvalidParamException extends IllegalArgumentException {

    private static final long serialVersionUID = 7628878271349121283L;

    @NotNull
    private final String paramName;

    @Nullable
    private final Object paramValue;

    /**
     * Builds an exception with an invalid parameter name.
     *
     * @param paramName an invalid parameter name. It shall not be {@code null}.
     */
    public InvalidParamException(@NotNull String paramName) {
        super(buildMessage(paramName));
        this.paramName = paramName;
        paramValue = null;
    }

    /**
     * Builds an exception with an invalid parameter name and cause.
     *
     * @param paramName an invalid parameter name. It shall not be {@code null}.
     * @param cause     an exception cause. It shall not be {@code null}.
     */
    public InvalidParamException(@NotNull String paramName, @NotNull Throwable cause) {
        super(buildMessage(paramName), cause);
        this.paramName = paramName;
        paramValue = null;
    }

    /**
     * Builds an exception with an invalid parameter name and value.
     *
     * @param paramName  an invalid parameter name. It shall not be {@code null}.
     * @param paramValue an invalid parameter value. It can be {@code null}.
     */
    public InvalidParamException(@NotNull String paramName, @Nullable Object paramValue) {
        super(buildMessage(paramName, paramValue));
        this.paramName = paramName;
        this.paramValue = paramValue;
    }

    /**
     * Builds an exception with an invalid parameter name, value and cause.
     *
     * @param paramName  an invalid parameter name. It shall not be {@code null}.
     * @param paramValue an invalid parameter value. It can be {@code null}.
     * @param cause      an exception cause. It shall not be @{code null}.
     */
    public InvalidParamException(@NotNull String paramName, @Nullable Object paramValue, @NotNull Throwable cause) {
        super(buildMessage(paramName, paramValue), cause);
        this.paramName = paramName;
        this.paramValue = paramValue;
    }

    /**
     * Returns an invalid param name.
     *
     * @return an invalid param name. It shall not be {@code null}.
     */
    @NotNull
    public String getParamName() {
        return paramName;
    }

    /**
     * Returns an invalid param value.
     *
     * @return an invalid param value. It can be {@code null}.
     */
    @Nullable
    public Object getParamValue() {
        return paramValue;
    }

    /**
     * Builds an exception message with an invalid parameter name.
     *
     * @param paramName an invalid parameter name. It shall not be {@code null}.
     * @return an exception message. It shall not be {@code null}.
     */
    @NotNull
    private static String buildMessage(@NotNull String paramName) {
        return new StringBuilder("Invalid param ").append(paramName).append('.').toString();
    }

    /**
     * Builds an exception message with an invalid parameter name and value
     *
     * @param paramName  an invalid parameter name. It shall not be {@code null}.
     * @param paramValue an invalid parameter value. It can be {@code null}.
     * @return an exception message. It shall not be {@code null}.
     */
    @NotNull
    private static String buildMessage(@NotNull String paramName, @Nullable Object paramValue) {
        return new StringBuilder("Invalid param ").append(paramName).append(" with value ").append(paramValue).append('.').toString();
    }
}
