package name.slhynju;

import org.jetbrains.annotations.NotNull;

/**
 * A root exception of this library.
 *
 * @author slhynju
 */
public class ApplicationException extends RuntimeException {

    private static final long serialVersionUID = -8737890225967405787L;

    /**
     * Builds a general exception with specified message.
     *
     * @param message an exception message. It shall not be {@code null}.
     */
    public ApplicationException(@NotNull String message) {
        super(message);
    }

    /**
     * Builds a general exception with specified message.
     *
     * @param message an exception message. It shall not be {@code null}.
     * @throws NullPointerException if {@code message} is {@code null}.
     */
    public ApplicationException(@NotNull CharSequence message) {
        super(message.toString());
    }

    /**
     * Builds a general exception with specified message and cause.
     *
     * @param message an exception message. It shall not be {@code null}.
     * @param cause   an exception cause. It shall not be {@code null}.
     */
    public ApplicationException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    /**
     * Builds a general exception with specified message and cause.
     *
     * @param message an exception message. It shall not be {@code null}.
     * @param cause   an exception cause. It shall not be {@code null}.
     * @throws NullPointerException if {@code message} is {@code null}.
     */
    public ApplicationException(@NotNull CharSequence message, @NotNull Throwable cause) {
        super(message.toString(), cause);
    }

}
