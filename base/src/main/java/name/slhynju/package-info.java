/**
 * Exceptions that can be reused across applications.
 *
 * @author slhynju
 */
package name.slhynju;