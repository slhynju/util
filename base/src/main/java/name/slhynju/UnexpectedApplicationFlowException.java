package name.slhynju;

import org.jetbrains.annotations.NotNull;

/**
 * An exception that indicates an unexpected application flow.
 *
 * @author slhynju
 */
public class UnexpectedApplicationFlowException extends ApplicationException {

    private static final long serialVersionUID = -5784582195587019763L;

    /**
     * Builds an exception with default message.
     */
    public UnexpectedApplicationFlowException() {
        super("Unexpected application flow.");
    }

    /**
     * Builds an exception with specified message.
     *
     * @param message an exception message. It shall not be {@code null}.
     */
    public UnexpectedApplicationFlowException(@NotNull String message) {
        super(message);
    }

    /**
     * Builds an exception with specified message.
     *
     * @param message an exception message. It shall not be {@code null}.
     * @throws NullPointerException if {@code message} is {@code null}.
     */
    public UnexpectedApplicationFlowException(@NotNull CharSequence message) {
        super(message);
    }

    /**
     * Builds an exception with default message and an error cause.
     *
     * @param cause an exception cause. It shall not be {@code null}.
     */
    public UnexpectedApplicationFlowException(@NotNull Throwable cause) {
        super("Unexpected application flow.", cause);
    }

    /**
     * Builds an exception with specified message and cause.
     *
     * @param message an exception message. It shall not be {@code null}.
     * @param cause   an exception cause. It shall not be {@code null}.
     */
    public UnexpectedApplicationFlowException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    /**
     * Builds an exception with specified message and cause.
     *
     * @param message an exception message. It shall not be {@code null}.
     * @param cause   an exception cause. It shall not be {@code null}.
     * @throws NullPointerException if {@code message} is {@code null}.
     */
    public UnexpectedApplicationFlowException(@NotNull CharSequence message, @NotNull Throwable cause) {
        super(message, cause);
    }
}
