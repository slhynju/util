/*
 * This source file is derived from Apache Commons Lang project. An Apache 2.0 License is attached in the code repository.
 */
package name.slhynju.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Utility class to support {@code hashCode()} method. This class is not thread-safe.
 * <p>This class shall have no dependency on other utility classes.</p>
 *
 * @author slhynju
 */
public final class HashCodeBuilder {

    private int value = 7;

    private static final int MULTIPLIER = 3;

    /**
     * Appends an {@code Object} property.
     *
     * @param obj an {@code Object} property value. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull HashCodeBuilder append(@Nullable Object obj) {
        value *= MULTIPLIER;
        if (obj != null) {
            value += obj.hashCode();
        }
        return this;
    }

    /**
     * Appends an {@code int} property value.
     *
     * @param i an {@code int} property value.
     * @return this builder.
     */
    public @NotNull HashCodeBuilder append(int i) {
        value *= MULTIPLIER;
        value += i;
        return this;
    }

    /**
     * Appends a {@code boolean} property value.
     *
     * @param booleanValue a {@code boolean} property value.
     * @return this builder.
     */
    public @NotNull HashCodeBuilder append(boolean booleanValue) {
        value *= MULTIPLIER;
        if (booleanValue) {
            value++;
        }
        return this;
    }

    /**
     * Appends a {@code long} property value.
     *
     * @param l a {@code long} property value.
     * @return this builder.
     */
    @SuppressWarnings({"MagicNumber", "NumericCastThatLosesPrecision"})
    public @NotNull HashCodeBuilder append(long l) {
        value *= MULTIPLIER;
        value = (int) (value + l ^ (l >>> 32));
        return this;
    }

    /**
     * Appends a {@code double} property value.
     *
     * @param d a {@code double} property value.
     * @return this builder.
     */
    public @NotNull HashCodeBuilder append(double d) {
        append(Double.doubleToLongBits(d));
        return this;
    }

    /**
     * Appends a {@code char} property value.
     *
     * @param c a {@code char} property value.
     * @return this builder.
     */
    public @NotNull HashCodeBuilder append(char c) {
        value *= MULTIPLIER;
        value += c;
        return this;
    }

    /**
     * Returns the result hash code.
     *
     * @return the result hash code.
     */
    public int toValue() {
        return value;
    }
}
