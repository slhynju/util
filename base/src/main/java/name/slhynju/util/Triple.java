package name.slhynju.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A simple Data Transfer Object with three properties.
 *
 * @author slhynju
 */
@SuppressWarnings("StandardVariableNames")
public final class Triple<A, B, C> {

    @Nullable
    public A a;

    @Nullable
    public B b;

    @Nullable
    public C c;

    /**
     * Creates a new instance.
     */
    public Triple() {
        a = null;
        b = null;
        c = null;
    }

    /**
     * Creates a new instance.
     *
     * @param a the first property value. It can be {@code null}.
     * @param b the second property value. It can be {@code null}.
     * @param c the third property value. It can be {@code null}.
     */
    public Triple(@Nullable A a, @Nullable B b, @Nullable C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Return the three properties as an {@code Object array}.
     *
     * @return a length-3 {@code Object array} of three properties.
     */
    @NotNull
    @Contract(" -> new")
    public Object[] toArray() {
        //noinspection ConstantConditions
        return new Object[]{a, b, c};
    }

    @SuppressWarnings("rawtypes")
    @Override
    @Contract("null -> false")
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Triple) {
            Triple other = (Triple) obj;
            return EqualsUtil.isEquals(a, other.a) && EqualsUtil.isEquals(b, other.b) && EqualsUtil.isEquals(c, other.c);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(a).append(b).append(c).toValue();
    }

    @Override
    @NotNull
    @Contract(" -> new")
    public String toString() {
        return new BeanStringBuilder(Triple.class).append("a", a).append("b", b).append("c", c).toS();
    }


}
