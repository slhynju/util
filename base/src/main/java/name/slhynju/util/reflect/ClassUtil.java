package name.slhynju.util.reflect;

import name.slhynju.ApplicationException;
import name.slhynju.UnexpectedApplicationFlowException;
import name.slhynju.util.FreeStringBuilder;
import name.slhynju.util.StringUtil;
import name.slhynju.util.collection.CollectionUtil;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * {@code Class}, {@code Method} utility methods.
 *
 * @author slhynju
 */
@SuppressWarnings("OverlyComplexClass")
public final class ClassUtil {

    /**
     * Returns all the super {@code Classes} and {@code Interfaces} of a {@code Class}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return all the super {@code Classes} and {@code Interfaces}.
     */
    public static @NotNull Set<Class<?>> getSuperClasses(@Nullable Class<?> aClass) {
        if (aClass == null) {
            return new HashSet<>(0);
        }
        Set<Class<?>> superClazz = new HashSet<>(8);
        CollectionUtil.addAll(superClazz, aClass.getInterfaces());
        Class<?> superClass = aClass.getSuperclass();
        while (superClass != null) {
            superClazz.add(superClass);
            CollectionUtil.addAll(superClazz, superClass.getInterfaces());
            superClass = superClass.getSuperclass();
        }
        return superClazz;
    }

    /**
     * Returns all the {@code Interfaces} of a {@code Class}, including the ones implemented by its super {@code Classes}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return all the implemented {@code Interfaces}.
     */
    public static @NotNull Set<Class<?>> getInterfaces(@Nullable Class<?> aClass) {
        if (aClass == null) {
            return new HashSet<>(0);
        }
        Set<Class<?>> interfaces = new HashSet<>(8);
        CollectionUtil.addAll(interfaces, aClass.getInterfaces());
        Class<?> superClass = aClass.getSuperclass();
        while (superClass != null) {
            CollectionUtil.addAll(interfaces, superClass.getInterfaces());
            superClass = superClass.getSuperclass();
        }
        return interfaces;
    }

    /**
     * Checks whether a {@code Class} is {@code abstract}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return {@code true} -- is {@code abstract}; {@code false} -- not {@code abstract}.
     */
    public static boolean isAbstract(@Nullable Class<?> aClass) {
        return aClass != null && Modifier.isAbstract(aClass.getModifiers());
    }

    /**
     * Checks whether a {@code Class} is an {@code abstract Class} and not {@code Interface}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return {@code true} -- is an {@code abstract Class} and not {@code Interface}; {@code false} -- otherwise.
     */
    public static boolean isAbstractClass(@Nullable Class<?> aClass) {
        return aClass != null && Modifier.isAbstract(aClass.getModifiers()) && !aClass.isInterface();
    }

    /**
     * Checks whether a {@code Class} is a {@code public}, concrete {@code Class} that can be initialized.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return {@code true} -- is a {@code public}, concrete {@code Class} that can be initialized; {@code false} -- otherwise.
     */
    public static boolean isImpl(@Nullable Class<?> aClass) {
        if (aClass == null) {
            return false;
        }
        int modifier = aClass.getModifiers();
        return !Modifier.isAbstract(modifier) && Modifier.isPublic(modifier) && aClass.getConstructors().length > 0;
    }

    /**
     * Checks whether a {@code Class} is an implementation {@code Class} of another {@code Class}.
     *
     * @param implClass  a {@code Class}. It can be {@code null}.
     * @param superClass another {@code Class}. It can be {@code null}.
     * @return {@code true} -- {@code implClass} is an implementation {@code Class} of {@code superClass}; {@code false} -- otherwise.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean isImplOf(@Nullable Class<?> implClass, @Nullable Class<?> superClass) {
        if (implClass == null || superClass == null) {
            return false;
        }
        return isImpl(implClass) && superClass.isAssignableFrom(implClass);
    }

    /**
     * Returns the {@code Package} name of a {@code Class}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return the {@code Package} name if available. Returns empty {@code String} if not available.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @NotNull String getPackageName(@Nullable Class<?> aClass) {
        if (aClass == null) {
            return "";
        }
        Package p = aClass.getPackage();
        if (p == null) {
            return "";
        }
        // Package.getName() returns null in some cases.
        return StringUtil.nullToEmpty(p.getName());
    }

    /**
     * Returns the {@code Module} name of a {@code Class}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return the {@code Module} name if available. Returns empty {@code String} if not available.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @NotNull String getModuleName(@Nullable Class<?> aClass) {
        if (aClass == null) {
            return "";
        }
        Module aModule = aClass.getModule();
        if (aModule == null) {
            return "";
        }
        // Module.getName() returns null in some cases.
        return StringUtil.nullToEmpty(aModule.getName());
    }

    /**
     * Checks whether a {@code Method} is {@code static}.
     *
     * @param method a {@code Method}. It can be {@code null}.
     * @return {@code true} -- is {@code static}; {@code false} -- not static.
     */
    public static boolean isStatic(@Nullable Method method) {
        return method != null && Modifier.isStatic(method.getModifiers());
    }

    /**
     * Checks whether the return type of a {@code Method} is {@code void} type.
     *
     * @param method a {@code Method}. It can be {@code null}.
     * @return {@code true} -- is void return type; {@code false} -- otherwise.
     */
    public static boolean isVoidReturnType(@Nullable Method method) {
        return method != null && Void.TYPE == method.getReturnType();
    }

    /**
     * Checks whether a {@code Method} is a {@code public} getter {@code Method}.
     *
     * @param method a {@code Method}. It can be {@code null}.
     * @return {@code true} -- is a {@code public} getter {@code Method}; {@code false} -- otherwise.
     */
    @SuppressWarnings("OverlyComplexBooleanExpression")
    public static boolean isGetter(@Nullable Method method) {
        if (method == null) {
            return false;
        }
        String name = method.getName();
        int modifier = method.getModifiers();
        return (name.startsWith("get") || name.startsWith("is")) && Modifier.isPublic(modifier)
                && !Modifier.isStatic(modifier) && method.getParameterTypes().length == 0 && !isVoidReturnType(method);
    }

    /**
     * Checks whether a {@code Method} is a {@code public} setter {@code Method}.
     *
     * @param method a {@code Method}. It can be {@code null}.
     * @return {@code true} -- is a {@code public} setter {@code Method}; {@code false} -- otherwise.
     */
    @SuppressWarnings("OverlyComplexBooleanExpression")
    public static boolean isSetter(@Nullable Method method) {
        if (method == null) {
            return false;
        }
        int modifier = method.getModifiers();
        return method.getName().startsWith("set") && Modifier.isPublic(modifier) && !Modifier.isStatic(modifier)
                && method.getParameterTypes().length == 1 && isVoidReturnType(method);
    }

    /**
     * Returns all the {@code public} getter {@code Methods} of a {@code Class}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return all the {@code public} getter {@code Methods}.
     */
    public static @NotNull List<Method> getGetters(@Nullable Class<?> aClass) {
        return aClass == null ? new ArrayList<>(0) : CollectionUtil.sub(aClass.getMethods(), ClassUtil::isGetter);
    }

    /**
     * Returns all the {@code public} setter {@code Methods} of a {@code Class}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return all the {@code public} setter {@code Methods}.
     */
    public static @NotNull List<Method> getSetters(@Nullable Class<?> aClass) {
        return aClass == null ? new ArrayList<>(0) : CollectionUtil.sub(aClass.getMethods(), ClassUtil::isSetter);
    }

    /**
     * Returns the parameter {@code Class} of a setter {@code Method}.
     *
     * @param setter a setter {@code Method}. It can be {@code null}.
     * @return the parameter {@code Class}. Returns {@code null} if {@code setter} is {@code null}.
     */
    public static @Nullable Class<?> getSetterParamClass(@Nullable Method setter) {
        return setter == null ? null : setter.getParameterTypes()[0];
    }

    /**
     * Returns the parameter {@code Type} of a setter {@code Method}.
     *
     * @param setter a setter {@code Method}. It can be {@code null}.
     * @return the parameter {@code Type}. Returns {@code null} if {@code setter} is {@code null}.
     */
    public static @Nullable Type getSetterParamType(@Nullable Method setter) {
        return setter == null ? null : setter.getGenericParameterTypes()[0];
    }

    /**
     * Finds all the {@code Methods} by name.
     *
     * @param aClass     a {@code Class}. It can be {@code null}.
     * @param methodName a {@code Method} name. It can be {@code null}.
     * @return all the {@code Methods} that have this name.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @NotNull List<Method> getMethods(@Nullable Class<?> aClass, @Nullable String methodName) {
        if (aClass == null || StringUtil.isEmpty(methodName)) {
            return new ArrayList<>(0);
        }
        return CollectionUtil.sub(aClass.getMethods(), method -> methodName.equals(method.getName()));
    }

    /**
     * Finds a {@code Method} by name and parameter {@code Classes}.
     *
     * @param aClass     a {@code Class}. It can be {@code null}.
     * @param methodName a method name. It can be {@code null}.
     * @param paramTypes the parameter {@code Classes}. It can be {@code null}.
     * @return the {@code Method} if found. Returns {@code null} if not found.
     * @throws ApplicationException if {@code SecurityException} occurs.
     */
    public static @Nullable Method getMethod(@Nullable Class<?> aClass, @Nullable String methodName, @Nullable Class<?> @Nullable ... paramTypes) {
        if (aClass == null || StringUtil.isEmpty(methodName)) {
            return null;
        }
        if (paramTypes == null) {
            //noinspection AssignmentToMethodParameter
            paramTypes = CollectionUtil.EMPTY_CLASS_ARRAY;
        }
        try {
            return aClass.getMethod(methodName, paramTypes);
        } catch (NoSuchMethodException e) {
            return null;
        } catch (SecurityException e) {
            @NonNls FreeStringBuilder message = new FreeStringBuilder("Cannot find method ");
            message.append(methodName).append(' ').append(paramTypes, "(", ", ", ")")
                    .append(" in Class ").append(aClass).append(" due to SecurityException.");
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Finds a {@code static Method} by name and parameter classes.
     *
     * @param aClass     a {@code Class}. It can be {@code null}.
     * @param methodName a method name. It can be {@code null}.
     * @param paramTypes the parameter {@code Classes}. It can be {@code null}.
     * @return the {@code static Method} if found. Returns {@code null} if not found.
     * @throws ApplicationException if {@code SecurityException} occurs.
     */
    public static @Nullable Method getStaticMethod(@Nullable Class<?> aClass, @Nullable String methodName, @Nullable Class<?>... paramTypes) {
        Method method = getMethod(aClass, methodName, paramTypes);
        return isStatic(method) ? method : null;
    }

    /**
     * Finds a {@code Constructor} by parameter {@code Classes}.
     *
     * @param aClass     a {@code Class}. It can be {@code null}.
     * @param paramTypes the parameter {@code Classes}. It can be {@code null}.
     * @return the {@code Constructor} if found. Returns {@code null} if not found.
     * @throws ApplicationException if {@code SecurityException} occurs.
     */
    public static @Nullable Constructor<?> getConstructor(@Nullable Class<?> aClass, @Nullable Class<?> @Nullable ... paramTypes) {
        if (aClass == null) {
            return null;
        }
        if (paramTypes == null) {
            //noinspection AssignmentToMethodParameter
            paramTypes = CollectionUtil.EMPTY_CLASS_ARRAY;
        }
        try {
            return aClass.getConstructor(paramTypes);
        } catch (NoSuchMethodException e) {
            return null;
        } catch (SecurityException e) {
            FreeStringBuilder message = new FreeStringBuilder("Cannot find constructor ");
            message.append(paramTypes, "(", ", ", ")").append(" in Class ")
                    .append(aClass).append(" due to SecurityException.");
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Finds a {@code Field} by name.
     *
     * @param aClass    a {@code Class}. It can be {@code null}.
     * @param fieldName a {@code Field} name. It can be {@code null}.
     * @return the {@code Field} if found. Returns null if not found.
     * @throws ApplicationException if {@code SecurityException} occurs.
     */
    public static @Nullable Field getField(@Nullable Class<?> aClass, @Nullable String fieldName) {
        if (aClass == null || StringUtil.isEmpty(fieldName)) {
            return null;
        }
        try {
            return aClass.getField(fieldName);
        } catch (NoSuchFieldException e) {
            return null;
        } catch (SecurityException e) {
            FreeStringBuilder message = new FreeStringBuilder("Cannot find field ");
            message.append(fieldName).append(" in Class ").append(aClass).append(" due to SecurityException.");
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Returns the simple name of a {@code Class}.
     *
     * @param aClass a {@code Class}. It can be {@code null}.
     * @return the simple name of the {@code Class}.
     */
    public static @NotNull String toS(@Nullable Class<?> aClass) {
        //noinspection ConstantConditions
        return aClass == null ? "" : aClass.getSimpleName();
    }

    /**
     * Returns the simple name of a {@code Type}.
     *
     * @param type a {@code Type}. It can be {@code null}.
     * @return the simple name of the {@code Type}.
     */
    @SuppressWarnings("ChainOfInstanceofChecks")
    public static @NotNull String toS(@Nullable Type type) {
        if (type == null) {
            return "";
        }
        if (type instanceof Class<?>) {
            return toS((Class<?>) type);
        }
        if (type instanceof ParameterizedType) {
            return toS((ParameterizedType) type);
        }
        if (type instanceof GenericArrayType) {
            return toS((GenericArrayType) type);
        }
        if (type instanceof TypeVariable<?>) {
            return toS((TypeVariable<?>) type);
        }
        if (type instanceof WildcardType) {
            return toS((WildcardType) type);
        }
        // shall never happen
        @NonNls String message = "I do not understand type " + type + '.';
        throw new UnexpectedApplicationFlowException(message);
    }

    /**
     * Returns the simple name of a {@code ParameterizedType}.
     *
     * @param type a {@code ParameterizedType}. It can be {@code null}.
     * @return the simple name of the {@code ParameterizedType}.
     */
    public static @NotNull String toS(@Nullable ParameterizedType type) {
        if (type == null) {
            return "";
        }
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(toS(type.getRawType()));
        sb.append(type.getActualTypeArguments(), "<", ", ", ">");
        return sb.toS();
    }

    /**
     * Returns the simple name of a {@code GenericArrayType}.
     *
     * @param type a {@code GenericArrayType}. It can be {@code null}.
     * @return the simple name of the {@code GenericArrayType}.
     */
    @SuppressWarnings("StringConcatenation")
    public static @NotNull String toS(@Nullable GenericArrayType type) {
        return type == null ? "" : toS(type.getGenericComponentType()) + "[]";
    }


    /**
     * Returns the simple name of a {@code TypeVariable}.
     *
     * @param type a {@code TypeVariable}. It can be {@code null}.
     * @return the simple name of the {@code TypeVariable}.
     */
    public static @NotNull String toS(@Nullable TypeVariable<?> type) {
        //noinspection ConstantConditions
        return type == null ? "" : type.getName();
    }

    /**
     * Returns the simple name of a {@code WildcardType}.
     *
     * @param type a {@code WildcardType}. It can be {@code null}.
     * @return the simple name of the {@code WildcardType}.
     */
    @SuppressWarnings("VariableNotUsedInsideIf")
    public static @NotNull String toS(@Nullable WildcardType type) {
        // the upper bounds and lower bounds are omitted.
        return type == null ? "" : "?";
    }

    private ClassUtil() {
        //DO NOTHING.
    }
}
