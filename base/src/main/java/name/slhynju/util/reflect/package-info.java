/**
 * Utility classes of reflection.
 *
 * @author slhynju
 */
package name.slhynju.util.reflect;