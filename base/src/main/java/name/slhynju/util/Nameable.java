package name.slhynju.util;

import org.jetbrains.annotations.NotNull;

/**
 * Marker interface which has a name property.
 *
 * @author slhynju
 */
public interface Nameable {

    /**
     * Returns the name.
     *
     * @return the name.
     */
    @NotNull String getName();

    /**
     * Sets the name.
     *
     * @param name the name. It shall not be {@code null}.
     */
    void setName(@NotNull String name);

}
