package name.slhynju.util.nio;

import name.slhynju.ApplicationException;
import name.slhynju.util.StringUtil;
import name.slhynju.util.io.IOUtil;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.*;

/**
 * Utility class of various NIO operations.
 * <p>This class shall only depend on other utility classes in package name.slhynju.util and package name.slhynju.util.io (subpackages excluded).</p>
 *
 * @author slhynju
 */
public final class PathUtil {

    /**
     * Lists all file {@code Paths} that match a path pattern under a given directory.
     *
     * @param dir  a directory to search the files. It can be {@code null}.
     * @param glob a path pattern. Example: **&#47;*.java. It can be {@code null}.
     * @param log  a {@code Logger}. It shall not be {@code null}.
     * @return a {@code List} of matching files. Returns empty list if {@code dir} is {@code null}.
     * @throws ApplicationException if {@code IOException} occurs.
     * @see FileSystem#getPathMatcher(String)
     */
    public static @NotNull List<Path> listAllFiles(@Nullable Path dir, @Nullable String glob, @NotNull Logger log) {
        if (dir == null || StringUtil.isEmpty(glob)) {
            return Collections.emptyList();
        }
        FileCollector collector = new FileCollector(dir.getFileSystem(), glob, log);
        try {
            Files.walkFileTree(dir, collector);
        } catch (IOException e) {
            @NonNls String message = "Unexpected IOException occurred when walking file tree under " + dir + '.';
            throw new ApplicationException(message, e);
        }
        return collector.getFiles();
    }

    /**
     * Converts a {@code Path} into a {@code String}.
     *
     * @param path a {@code Path}. It can be {@code null}.
     * @return the converted {@code String}.
     */
    public static @NotNull String toName(@Nullable Path path) {
        return path == null ? "" : path.toString();
    }

    /**
     * Converts a {@code List} of {@code Path} into a {@code List} of {@code String}.
     *
     * @param paths a {@code List} of {@code Path}. It can be {@code null}.
     * @return the converted {@code List} of {@code String}.
     */
    public static @NotNull List<String> toNames(@Nullable List<? extends Path> paths) {
        // this class shall not depend on CollectionUtil therefore it does not use the CollectionUtil.transformToList() method.
        if (isEmpty(paths)) {
            return new ArrayList<>(0);
        }
        List<String> list = new ArrayList<>(paths.size());
        for (Path path : paths) {
            list.add(toName(path));
        }
        return list;
    }

    /**
     * Converts a {@code String} of path name into a {@code Path}.
     *
     * @param pathName a path name. It can be {@code null}.
     * @return the converted {@code Path}.
     */
    public static @Nullable Path toPath(@Nullable String pathName) {
        return StringUtil.isEmpty(pathName) ? null : Paths.get(pathName);
    }

    /**
     * Converts a {@code List} of path names into a {@code List} of {@code Path}.
     *
     * @param pathNames a {@code List} of path names. It can be {@code null}.
     * @return the converted {@code List} of {@code Path}.
     */
    public static @NotNull List<Path> toPaths(@Nullable List<String> pathNames) {
        // this class shall not depend on CollectionUtil therefore it does not use the CollectionUtil.transformToList() method.
        if (isEmpty(pathNames)) {
            return new ArrayList<>(0);
        }
        List<Path> list = new ArrayList<>(pathNames.size());
        for (String pathName : pathNames) {
            list.add(toPath(pathName));
        }
        return list;
    }

    /**
     * Finds a file in context {@code ClassLoader}.
     *
     * @param pathName a path name. It can be {@code null}.
     * @return the {@code Path} found. Returns null if not found.
     * @throws ApplicationException if unknown {@code URISyntaxException} occurs.
     */
    @SuppressWarnings("MagicCharacter")
    public static @Nullable Path findResource(@Nullable String pathName) {
        if (StringUtil.isEmpty(pathName)) {
            return null;
        }
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        //noinspection ConstantConditions
        URL url = classLoader.getResource(pathName);
        if (url == null) {
            return null;
        }
        try {
            return Paths.get(url.toURI());
        } catch (URISyntaxException e) {
            @NonNls StringBuilder message = new StringBuilder("Cannot find file ");
            message.append(pathName).append(". Unknown URL syntax ").append(url).append('.');
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Checks whether a given {@code Path} is an empty, readable directory.
     *
     * @param path a {@code Path}. It can be {@code null}.
     * @return {@code true} -- is an empty, readable directory; {@code false} -- otherwise.
     */
    @SuppressWarnings("ConstantConditions")
    public static boolean isDirectoryEmpty(@Nullable Path path) {
        return isDirectoryReadable(path) && path.toFile().list().length == 0;
    }

    /**
     * Checks whether a given {@code Path} is a readable directory.
     *
     * @param path a {@code Path}. It can be {@code null}.
     * @return {@code true} -- is a readable directory; {@code false} -- otherwise.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean isDirectoryReadable(@Nullable Path path) {
        if (path == null || !Files.exists(path)) {
            return false;
        }
        return Files.isDirectory(path) && Files.isReadable(path);
    }

    /**
     * Checks whether a given {@code Path} is a writable directory.
     *
     * @param path a {@code Path}. It can be {@code null}.
     * @return {@code true} -- is a writable directory; {@code false} -- otherwise.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean isDirectoryWritable(@Nullable Path path) {
        if (path == null || !Files.exists(path)) {
            return false;
        }
        return Files.isDirectory(path) && Files.isWritable(path);
    }

    /**
     * Checks whether a given {@code Path} is a readable file.
     *
     * @param path a {@code Path}. It can be {@code null}.
     * @return {@code true} -- is a readable file; {@code false} -- otherwise.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean isFileReadable(@Nullable Path path) {
        if (path == null || !Files.exists(path)) {
            return false;
        }
        return !Files.isDirectory(path) && Files.isReadable(path);
    }

    /**
     * Checks whether a given {@code Path} is a writable file.
     *
     * @param path a {@code Path}. It can be {@code null}.
     * @return {@code true} -- is a writable file; {@code false} -- otherwise.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean isFileWritable(@Nullable Path path) {
        if (path == null || !Files.exists(path)) {
            return false;
        }
        return !Files.isDirectory(path) && Files.isWritable(path);
    }

    /**
     * Opens a text file with UTF-8 encoding.
     *
     * @param path a {@code Path}. It shall be a readable file. It shall not be {@code null}.
     * @return a {@code BufferedReader}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull BufferedReader readTextFile(@NotNull Path path) {
        //noinspection ConstantConditions
        return readTextFile(path, UTF_8);
    }

    /**
     * Opens a text file with specified encoding.
     *
     * @param path    a {@code Path}. It shall be a readable file. It shall not be {@code null}.
     * @param charset the encoding to use. It shall not be {@code null}.
     * @return a {@code BufferedReader}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull BufferedReader readTextFile(@NotNull Path path, @NotNull Charset charset) {
        try {
            return Files.newBufferedReader(path, charset);
        } catch (IOException e) {
            @NonNls StringBuilder message = new StringBuilder("Cannot open File ");
            message.append(path).append(" with Charset ").append(charset.name()).append('.');
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Writes to a text file with UTF-8 encoding.
     *
     * @param path a {@code Path}. It shall be writable. It shall not be {@code null}.
     * @return a {@code PrintWriter}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull PrintWriter writeTextFile(@NotNull Path path) {
        //noinspection ConstantConditions
        return writeTextFile(path, UTF_8);
    }

    /**
     * Writes to a text file with specified encoding.
     *
     * @param path    a {@code Path}. It shall be writable. It shall not be {@code null}.
     * @param charset the encoding to use. It shall not be {@code null}.
     * @return a {@code PrintWriter}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull PrintWriter writeTextFile(@NotNull Path path, @NotNull Charset charset) {
        try {
            return new PrintWriter(Files.newBufferedWriter(path, charset));
        } catch (IOException e) {
            @NonNls StringBuilder message = new StringBuilder("Cannot write to File ");
            message.append(path).append(" with Charset ").append(charset.name()).append('.');
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Appends to a text file with UTF-8 encoding.
     *
     * @param path a {@code Path}. It shall be writable. It shall not be {@code null}.
     * @return a {@code PrintWriter}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull PrintWriter appendTextFile(@NotNull Path path) {
        //noinspection ConstantConditions
        return appendTextFile(path, UTF_8);
    }

    /**
     * Appends to a text file with specified encoding.
     *
     * @param path    a {@code Path}. It shall be writable. It shall not be {@code null}.
     * @param charset the encoding to use. It shall not be {@code null}.
     * @return a {@code PrintWriter}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull PrintWriter appendTextFile(@NotNull Path path, @NotNull Charset charset) {
        try {
            return new PrintWriter(Files.newBufferedWriter(path, charset, CREATE, WRITE, APPEND));
        } catch (IOException e) {
            @NonNls StringBuilder message = new StringBuilder("Cannot write to File ");
            message.append(path).append(" with Charset ").append(charset.name()).append(" and APPEND mode.");
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Reads all lines of a small text file. It uses UTF-8 encoding.
     *
     * @param path a {@code Path}. It shall be a readable text file. It shall not be {@code null}.
     * @return all lines of the text file.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull List<String> readSmallTextFile(@NotNull Path path) {
        //noinspection ConstantConditions
        return readSmallTextFile(path, UTF_8);
    }

    /**
     * Reads all lines of a small text file. It uses specified encoding.
     *
     * @param path    a {@code Path}. It shall be a readable text file. It shall not be {@code null}.
     * @param charset the encoding to use. It shall not be {@code null}.
     * @return all lines of the text file.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull List<String> readSmallTextFile(@NotNull Path path, @NotNull Charset charset) {
        List<String> rawLines = readAllRawLines(path, charset);
        List<String> lines = new ArrayList<>(rawLines.size());
        for (String rawLine : rawLines) {
            lines.add(StringUtil.trim(rawLine));
        }
        return lines;
    }

    /**
     * Reads the properties in a property file. It uses UTF-8 encoding.
     *
     * @param path a {@code Path}. It shall be a readable text file. It shall not be {@code null}.
     * @return the property {@code Map}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull Map<String, String> readSmallPropertiesFile(@NotNull Path path) {
        //noinspection ConstantConditions
        return readSmallPropertiesFile(path, UTF_8);
    }

    /**
     * Reads the properties in a property file. It uses specified encoding.
     *
     * @param path    a {@code Path}. It shall be a readable text file. It shall not be {@code null}.
     * @param charset the encoding to use. It shall not be {@code null}.
     * @return the property {@code Map}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull Map<String, String> readSmallPropertiesFile(@NotNull Path path, @NotNull Charset charset) {
        List<String> rawLines = readAllRawLines(path, charset);
        return IOUtil.buildProperties(rawLines);
    }

    /**
     * Writes the properties into a property file. It uses UTF-8 encoding.
     *
     * @param properties a property {@code Map}. It shall not be {@code null}.
     * @param path       a file {@code Path}. It shall be writable. It shall not be {@code null}.
     * @param log        a {@code Logger}. It shall not be {@code null}.
     */
    public static void writeSmallPropertiesFile(@NotNull Map<String, String> properties, @NotNull Path path, @NotNull Logger log) {
        //noinspection ConstantConditions
        writeSmallPropertiesFile(properties, path, UTF_8, log);
    }

    /**
     * Writes the properties into a property file. It uses specified encoding.
     *
     * @param properties a property {@code Map}. It shall not be {@code null}.
     * @param path       a file {@code Path}. It shall be writable. It shall not be {@code null}.
     * @param charset    the encoding to use. It shall not be {@code null}.
     * @param log        a {@code Logger}. It shall not be {@code null}.
     */
    public static void writeSmallPropertiesFile(@NotNull Map<String, String> properties, @NotNull Path path, @NotNull Charset charset,
                                                @NotNull Logger log) {
        PrintWriter writer = writeTextFile(path, charset);
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            writer.print(key);
            writer.print(" = ");
            writer.println(value);
        }
        IOUtil.close(writer, log);
    }

    /**
     * Reads all raw lines of a small text file. It uses specified encoding.
     *
     * @param path    a {@code Path}. It shall be a readable text file. It shall not be {@code null}.
     * @param charset the encoding to use. It shall not be {@code null}.
     * @return all the raw lines.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    private static @NotNull List<String> readAllRawLines(@NotNull Path path, @NotNull Charset charset) {
        try {
            return Files.readAllLines(path, charset);
        } catch (IOException e) {
            @NonNls StringBuilder message = new StringBuilder("IOException when reading File ");
            message.append(path).append(" with Charset ").append(charset.name()).append('.');
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Checks whether a {@code Collection} is {@code null} or empty.
     * <p>Defines this method so that this class does not depend on {@code CollectionUtil}.</p>
     *
     * @param collection a {@code Collection}. It can be {@code null}.
     * @return {@code true} -- is null or empty; {@code false} -- not null or empty.
     */
    private static boolean isEmpty(@Nullable Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    private PathUtil() {
        //DO NOTHING.
    }
}
