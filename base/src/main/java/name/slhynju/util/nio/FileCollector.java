package name.slhynju.util.nio;

import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Collects file {@code Paths} that match a glob pattern. It is to support
 * {@code PathUtil.listAllFiles()} method.
 *
 * @author slhynju
 * @see PathUtil#listAllFiles(Path, String, Logger)
 */
public final class FileCollector implements FileVisitor<Path>, Consumer<Path> {

    /**
     * A file {@code Path} pattern.
     */
    private final @NotNull PathMatcher pathMatcher;

    /**
     * The matching files.
     */
    private final @NotNull List<Path> files;

    /**
     * A custom {@code Logger}.
     */
    @NonNls
    @SuppressWarnings("NonConstantLogger")
    private final @NotNull Logger log;

    /**
     * Creates a new instance.
     *
     * @param fileSystem a file system. It shall not be {@code null}.
     * @param glob       a file path pattern in glob format. Example: glob:**&#47;*.java. It shall not be {@code null}.
     * @param log        a custom {@code Logger}. It shall not be {@code null}.
     * @see FileSystem#getPathMatcher(String)
     */
    public FileCollector(@NotNull FileSystem fileSystem, @NotNull String glob, @NotNull Logger log) {
        //noinspection ConstantConditions
        pathMatcher = fileSystem.getPathMatcher(glob);
        //noinspection CollectionWithoutInitialCapacity
        files = new ArrayList<>();
        this.log = log;
    }

    /**
     * Collects file {@code Paths} that match the glob pattern.
     *
     * @param path a file {@code Path}. It can be {@code null}.
     */
    @SuppressWarnings("ParameterNameDiffersFromOverriddenParameter")
    @Override
    public void accept(@Nullable Path path) {
        if (path == null) {
            return;
        }
        if (pathMatcher.matches(path)) {
            files.add(path);
        }
    }

    @Override
    public @NotNull FileVisitResult visitFile(@Nullable Path file,
                                              @Nullable BasicFileAttributes attrs) {
        accept(file);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public @NotNull FileVisitResult visitFileFailed(@NotNull Path file, @Nullable IOException exc) {
        if (exc != null && log.isWarnEnabled()) {
            @NonNls String message = "Failed to access file " + file + ". Continue with other files.";
            log.warn(message, exc);
        }
        return FileVisitResult.CONTINUE;
    }

    /**
     * Returns the collected file {@code Paths}.
     *
     * @return the collected file {@code Paths}.
     */
    public @NotNull List<Path> getFiles() {
        return files;
    }

    @Override
    public @NotNull FileVisitResult preVisitDirectory(@Nullable Path dir, @Nullable BasicFileAttributes attrs) {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public @NotNull FileVisitResult postVisitDirectory(@NotNull Path dir, @Nullable IOException exc) {
        if (exc != null && log.isWarnEnabled()) {
            @NonNls String message = "Exception when access dir " + dir + ". Continue with other files.";
            log.warn(message, exc);
        }
        return FileVisitResult.CONTINUE;
    }

}
