package name.slhynju.util;

import org.jetbrains.annotations.Nullable;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * Utility class to support {@code equals()} method.
 * <p>This class shall only depend on {@code NumberUtil} and {@code DateUtil}.</p>
 *
 * @author slhynju
 */
public final class EqualsUtil {

    /**
     * Checks whether two {@code Objects} are equivalent.
     *
     * @param o1 one {@code Object}. It can be {@code null}.
     * @param o2 another {@code Object}. It can be {@code null}.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEquals(@Nullable final Object o1, @Nullable final Object o2) {
        return Objects.equals(o1, o2);
    }

    /**
     * Checks whether two {@code Objects} are not equivalent.
     *
     * @param o1 one {@code Object}. It can be {@code null}.
     * @param o2 another {@code Object}. It can be {@code null}.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEquals(@Nullable final Object o1, @Nullable final Object o2) {
        return !isEquals(o1, o2);
    }

    /**
     * Checks whether two {@code Object arrays} are equivalent.
     *
     * @param a1 one {@code Object array}. It can be {@code null}.
     * @param a2 another {@code Object array}. It can be {@code null}.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEquals(@Nullable final Object[] a1, @Nullable final Object[] a2) {
        return Arrays.equals(a1, a2);
    }

    /**
     * Checks whether two {@code Object arrays} are not equivalent.
     *
     * @param a1 one {@code Object array}. It can be {@code null}.
     * @param a2 another {@code Object array}. It can be {@code null}.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEquals(@Nullable final Object[] a1, @Nullable final Object[] a2) {
        return !isEquals(a1, a2);
    }

    /**
     * Checks whether two {@code int} numbers are same value.
     *
     * @param i1 one {@code int} number.
     * @param i2 another {@code int} number.
     * @return {@code true} -- same value; {@code false} -- different value.
     */
    public static boolean isEquals(final int i1, final int i2) {
        return i1 == i2;
    }

    /**
     * Checks whether two {@code int} numbers are different value.
     *
     * @param i1 one {@code int} number.
     * @param i2 another {@code int} number.
     * @return {@code true} -- different value; {@code false} -- same value.
     */
    public static boolean notEquals(final int i1, final int i2) {
        return i1 != i2;
    }

    /**
     * Checks whether two {@code int arrays} are equivalent.
     *
     * @param a1 one {@code int array}. It can be {@code null}.
     * @param a2 another {@code int array}. It can be {@code null}.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEquals(@Nullable final int[] a1, @Nullable final int[] a2) {
        return Arrays.equals(a1, a2);
    }

    /**
     * Checks whether two {@code int arrays} are not equivalent.
     *
     * @param a1 one {@code int array}. It can be {@code null}.
     * @param a2 another {@code int array}. It can be {@code null}.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEquals(@Nullable final int[] a1, @Nullable final int[] a2) {
        return !isEquals(a1, a2);
    }

    /**
     * Checks whether two {@code long} numbers are same value.
     *
     * @param l1 one {@code long} number.
     * @param l2 another {@code long} number.
     * @return {@code true} -- same value; {@code false} -- different value.
     */
    public static boolean isEquals(final long l1, final long l2) {
        return l1 == l2;
    }

    /**
     * Checks whether two {@code long} numbers are different value.
     *
     * @param l1 one {@code long} number.
     * @param l2 another {@code long} number.
     * @return {@code true} -- different value; {@code false} -- same value.
     */
    public static boolean notEquals(final long l1, final long l2) {
        return l1 != l2;
    }

    /**
     * Checks whether two {@code long arrays} are equivalent.
     *
     * @param a1 one {@code long array}. It can be {@code null}.
     * @param a2 another {@code long array}. It can be {@code null}.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEquals(@Nullable final long[] a1, @Nullable final long[] a2) {
        return Arrays.equals(a1, a2);
    }

    /**
     * Checks whether two {@code long arrays} are not equivalent.
     *
     * @param a1 one {@code long array}. It can be {@code null}.
     * @param a2 another {@code long array}. It can be {@code null}.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEquals(@Nullable final long[] a1, @Nullable final long[] a2) {
        return !isEquals(a1, a2);
    }

    /**
     * Checks whether two {@code boolean} values are same value.
     *
     * @param b1 one {@code boolean} value.
     * @param b2 another {@code boolean} value.
     * @return {@code true} -- same value; {@code false} -- different value.
     */
    @SuppressWarnings("BooleanParameter")
    public static boolean isEquals(final boolean b1, final boolean b2) {
        return b1 == b2;
    }

    /**
     * Checks whether two {@code boolean} values are different value.
     *
     * @param b1 one {@code boolean} value.
     * @param b2 another {@code boolean} value.
     * @return {@code true} -- different value; {@code false} -- same value.
     */
    @SuppressWarnings("BooleanParameter")
    public static boolean notEquals(final boolean b1, final boolean b2) {
        return b1 != b2;
    }

    /**
     * Checks whether two {@code boolean arrays} are equivalent.
     *
     * @param a1 one {@code boolean array}. It can be {@code null}.
     * @param a2 another {@code boolean array}. It can be {@code null}.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEquals(@Nullable final boolean[] a1, @Nullable final boolean[] a2) {
        return Arrays.equals(a1, a2);
    }

    /**
     * Checks whether two {@code boolean arrays} are not equivalent.
     *
     * @param a1 one {@code boolean array}. It can be {@code null}.
     * @param a2 another {@code boolean array}. It can be {@code null}.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEquals(@Nullable final boolean[] a1, @Nullable final boolean[] a2) {
        return !isEquals(a1, a2);
    }

    /**
     * Checks whether two {@code double} numbers are equivalent within certain precision.
     * <p>Example: 3.609 and 3.613 are equivalent with precision of 2 fraction digits.</p>
     *
     * @param d1             one {@code double} number.
     * @param d2             another {@code double} number.
     * @param fractionDigits precision digits.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEquals(final double d1, final double d2, final int fractionDigits) {
        final NumberFormat format = NumberUtil.newFormat(fractionDigits);
        final String s1 = format.format(d1);
        final String s2 = format.format(d2);
        //noinspection ConstantConditions
        return s1.equals(s2);
    }

    /**
     * Checks whether two {@code double} number are not equivalent within certain precision.
     *
     * @param d1             one {@code double} number.
     * @param d2             another {@code double} number.
     * @param fractionDigits precision digits.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEquals(final double d1, final double d2, final int fractionDigits) {
        return !isEquals(d1, d2, fractionDigits);
    }

    /**
     * Checks whether two {@code double arrays} are equivalent within certain precision.
     *
     * @param a1             one {@code double array}. It can be {@code null}.
     * @param a2             another {@code double array}. It can be {@code null}.
     * @param fractionDigits precision digits.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEquals(@Nullable final double @Nullable [] a1, @Nullable final double @Nullable [] a2, final int fractionDigits) {
        //noinspection ArrayEquality
        if (a1 == a2) {
            return true;
        }
        if (a1 == null || a2 == null) {
            return false;
        }
        if (a1.length != a2.length) {
            return false;
        }
        for (int I = 0; I < a1.length; I++) {
            if (notEquals(a1[I], a2[I], fractionDigits)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks whether two {@code double arrays} are not equivalent within certain precision.
     *
     * @param a1             one {@code double array}. It can be {@code null}.
     * @param a2             another {@code double array}. It can be {@code null}.
     * @param fractionDigits precision digits.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEquals(@Nullable final double[] a1, @Nullable final double[] a2, final int fractionDigits) {
        return !isEquals(a1, a2, fractionDigits);
    }

    /**
     * Checks whether two {@code chars} are same value.
     *
     * @param c1 one {@code char}.
     * @param c2 another {@code char}.
     * @return {@code true} -- same value; {@code false} -- different value.
     */
    public static boolean isEquals(final char c1, final char c2) {
        return c1 == c2;
    }

    /**
     * Checks whether two {@code chars} are different value.
     *
     * @param c1 one {@code char}.
     * @param c2 another {@code char}.
     * @return {@code true} -- different value; {@code false} -- same value.
     */
    public static boolean notEquals(final char c1, final char c2) {
        return c1 != c2;
    }

    /**
     * Checks whether two {@code Strings} are equivalent ignoring case.
     *
     * @param s1 one {@code String}. It can be {@code null}.
     * @param s2 another {@code String}. It can be {@code null}.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEqualsIgnoreCase(@Nullable final String s1, @Nullable final String s2) {
        //noinspection StringEquality
        return s1 == s2 || (s1 != null && s1.equalsIgnoreCase(s2));
    }

    /**
     * Checks whether two {@code Strings} are not equivalent ignoring case.
     *
     * @param s1 one {@code String}. It can be {@code null}.
     * @param s2 another {@code String}. It can be {@code null}.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEqualsIgnoreCase(@Nullable final String s1, @Nullable final String s2) {
        return !isEqualsIgnoreCase(s1, s2);
    }

    /**
     * Checks whether two {@code String arrays} are equivalent ignoring case.
     *
     * @param a1 one {@code String array}. It can be {@code null}.
     * @param a2 another {@code String array}. It can be {@code null}.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     */
    public static boolean isEqualsIgnoreCase(@Nullable final String @Nullable [] a1, @Nullable final String @Nullable [] a2) {
        //noinspection ArrayEquality
        if (a1 == a2) {
            return true;
        }
        if (a1 == null || a2 == null) {
            return false;
        }
        if (a1.length != a2.length) {
            return false;
        }
        for (int I = 0; I < a1.length; I++) {
            if (notEqualsIgnoreCase(a1[I], a2[I])) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks whether two {@code String arrays} are not equivalent ignoring case.
     *
     * @param a1 one {@code String array}. It can be {@code null}.
     * @param a2 another {@code String array}. It can be {@code null}.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     */
    public static boolean notEqualsIgnoreCase(@Nullable final String[] a1, @Nullable final String[] a2) {
        return !isEqualsIgnoreCase(a1, a2);
    }

    /**
     * Checks whether two {@code Dates} are equivalent within certain precision.
     *
     * @param d1   one {@code Date}. It can be {@code null}.
     * @param d2   another {@code Date}. It can be {@code null}.
     * @param unit a precision time unit.
     * @return {@code true} -- equivalent; {@code false} -- not equivalent.
     * @throws IllegalArgumentException if {@code unit} is invalid.
     * @see java.util.Calendar
     */
    @SuppressWarnings({"ConstantConditions", "UseOfObsoleteDateTimeApi"})
    public static boolean isEquals(@Nullable final Date d1, @Nullable final Date d2, final int unit) {
        //noinspection ObjectEquality
        if (d1 == d2) {
            return true;
        }
        if (d1 == null || d2 == null) {
            return false;
        }
        final Date t1 = DateUtil.truncate(d1, unit);
        final Date t2 = DateUtil.truncate(d2, unit);
        return t1.equals(t2);
    }

    /**
     * Checks whether two {@code Dates} are not equivalent within certain precision.
     *
     * @param d1   one {@code Date}. It can be {@code null}.
     * @param d2   another {@code Date}. It can be {@code null}.
     * @param unit a precision time unit.
     * @return {@code true} -- not equivalent; {@code false} -- equivalent.
     * @throws IllegalArgumentException if {@code unit} is invalid.
     * @see java.util.Calendar
     */
    @SuppressWarnings("UseOfObsoleteDateTimeApi")
    public static boolean notEquals(@Nullable final Date d1, @Nullable final Date d2, final int unit) {
        return !isEquals(d1, d2, unit);
    }

    private EqualsUtil() {
        //DO NOTHING.
    }

}
