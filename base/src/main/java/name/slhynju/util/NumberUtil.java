package name.slhynju.util;

import name.slhynju.UnexpectedApplicationFlowException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigInteger;
import java.text.NumberFormat;

/**
 * Number utility methods.
 * <p>This class shall have no dependency on other utility classes.</p>
 *
 * @author slhynju
 */
public final class NumberUtil {

    // defines this constant so that this class does not depend on StringUtil.
    @NotNull
    private static final String NULL = "null";

    /**
     * Creates a new {@code NumberFormat} instance for decimal numbers.
     *
     * @param minFractionDigits minimum number of fraction digits.
     * @param maxFractionDigits maximum number of fraction digits.
     * @param groupingUsed      whether to use grouping marks.
     * @return a new {@code NumberFormat} instance.
     */
    @SuppressWarnings("ConstantConditions")
    @NotNull
    @Contract("_, _, _ -> new")
    public static NumberFormat newFormat(int minFractionDigits, int maxFractionDigits, boolean groupingUsed) {
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(minFractionDigits);
        format.setMaximumFractionDigits(maxFractionDigits);
        format.setGroupingUsed(groupingUsed);
        return format;
    }

    /**
     * Creates a new {@code NumberFormat} instance for decimal numbers. This method uses fixed number of fraction digits and no grouping marks.
     *
     * @param fractionDigits fixed number of fraction digits.
     * @return a new {@code NumberFormat} instance.
     */
    @NotNull
    @Contract("_ -> new")
    public static NumberFormat newFormat(int fractionDigits) {
        return newFormat(fractionDigits, fractionDigits, false);
    }

    /**
     * Parses a {@code String} text into an {@code int} number.
     *
     * @param s            a {@code String} text. It can be {@code null}.
     * @param defaultValue the default value to use if the {@code String} is {@code null}, empty or {@code "null"}.
     * @return the parsed {@code int} number.
     * @throws NumberFormatException if {@code s} is invalid.
     */
    @Contract("null, _ -> param2")
    public static int toInt(@Nullable String s, int defaultValue) {
        return isNull(s) ? defaultValue : Integer.parseInt(s);
    }

    /**
     * Parses a {@code String} text into an {@code Integer} object.
     *
     * @param s            a {@code String} text. It can be {@code null}.
     * @param defaultValue the default value to use if the {@code String} is {@code null}, empty or {@code "null"}.
     * @return the parsed {@code Integer} object.
     * @throws NumberFormatException if {@code s} is invalid.
     */
    @NotNull
    @Contract("null, _ -> !null")
    public static Integer toInteger(@Nullable String s, int defaultValue) {
        return isNull(s) ? Integer.valueOf(defaultValue) : Integer.valueOf(s);
    }

    /**
     * Converts an {@code Integer} object into a {@code String}.
     *
     * @param n an {@code Integer} object. It can be {@code null}.
     * @return the converted {@code String}.
     */
    @NotNull
    @Contract("_ -> !null")
    public static String toS(@Nullable Integer n) {
        return n == null ? NULL : n.toString();
    }

    /**
     * Parses a {@code String} text into a {@code long} number.
     *
     * @param s            a {@code String} text. It can be {@code null}.
     * @param defaultValue the default value to use if the {@code String} is {@code null}, empty or {@code "null"}.
     * @return the parsed {@code long} number.
     * @throws NumberFormatException if {@code s} is invalid.
     */
    @Contract("null, _ -> param2")
    public static long toLong(@Nullable String s, long defaultValue) {
        return isNull(s) ? defaultValue : Long.parseLong(s);
    }

    /**
     * Parses a {@code String} text into a {@code Long} object.
     *
     * @param s            a {@code String} text. It can be {@code null}.
     * @param defaultValue the default value to use if the {@code String} is {@code null}, empty or {@code "null"}.
     * @return the parsed {@code Long} object.
     * @throws NumberFormatException if {@code s} is invalid.
     */
    @NotNull
    @Contract("null, _ -> !null")
    public static Long toLongObject(@Nullable String s, long defaultValue) {
        return isNull(s) ? Long.valueOf(defaultValue) : Long.valueOf(s);
    }

    /**
     * Converts a {@code Long} object into a {@code String}.
     *
     * @param l a {@code Long} object. It can be {@code null}.
     * @return the converted {@code String}.
     */
    @NotNull
    @Contract("_ -> !null")
    public static String toS(@Nullable Long l) {
        return l == null ? NULL : l.toString();
    }

    /**
     * Formats a {@code Long} object into a {@code String}.
     *
     * @param l      a {@code Long} object. It can be {@code null}.
     * @param format a number format. It shall not be {@code null}.
     * @return the formatted {@code String}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    @Contract("null, _ -> !null; !null, null -> fail")
    public static String toS(@Nullable Long l, @NotNull NumberFormat format) {
        return l == null ? NULL : format.format(l.longValue());
    }

    /**
     * Parses a {@code String} text into a {@code double} number.
     *
     * @param s            a {@code String} text. It can be {@code null}.
     * @param defaultValue the default value to use if the {@code String} is {@code null}, empty or {@code "null"}.
     * @return the parsed {@code double} number.
     */
    @Contract("null, _ -> param2")
    public static double toDouble(@Nullable String s, double defaultValue) {
        return isNull(s) ? defaultValue : Double.parseDouble(s);
    }

    /**
     * Parses a {@code String} text into a {@code Double} object.
     *
     * @param s            a {@code String} text. It can be {@code null}.
     * @param defaultValue the default value to use if the {@code String} is {@code null}, empty or {@code "null"}.
     * @return the parsed {@code Double} object.
     * @throws NumberFormatException if {@code s} is invalid.
     */
    @NotNull
    @Contract("null, _ -> !null")
    public static Double toDoubleObject(@Nullable String s, double defaultValue) {
        return isNull(s) ? Double.valueOf(defaultValue) : Double.valueOf(s);
    }

    /**
     * Converts a {@code Double} object into a {@code String}.
     *
     * @param d a {@code Double} object. It can be {@code null}.
     * @return the converted {@code String}.
     */
    @NotNull
    @Contract("_ -> !null")
    public static String toS(@Nullable Double d) {
        return d == null ? NULL : d.toString();
    }

    /**
     * Formats a {@code Double} object into a {@code String}.
     *
     * @param d      a {@code Double} object. It can be {@code null}.
     * @param format a number format. It shall not be {@code null}.
     * @return the formatted {@code String}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    @Contract("null, _ -> !null; !null, null -> fail")
    public static String toS(@Nullable Double d, @NotNull NumberFormat format) {
        //noinspection ConstantConditions
        return d == null ? NULL : format.format(d.doubleValue());
    }

    /**
     * Formats a {@code double} number into a {@code String}.
     *
     * @param d                 a {@code double} number.
     * @param minFractionDigits minimum number of fraction digits.
     * @param maxFractionDigits maximum number of fraction digits.
     * @param groupingUsed      whether to use grouping marks.
     * @return the formatted {@code String}.
     */
    @NotNull
    @Contract("_, _, _, _ -> new")
    public static String toS(double d, int minFractionDigits, int maxFractionDigits, boolean groupingUsed) {
        NumberFormat format = newFormat(minFractionDigits, maxFractionDigits, groupingUsed);
        //noinspection ConstantConditions
        return format.format(d);
    }

    /**
     * Formats a {@code double} number into a {@code String}. It uses fixed fraction digits and no grouping marks.
     *
     * @param d              a {@code double} number.
     * @param fractionDigits fixed number of fraction digits.
     * @return the formatted {@code String}.
     */
    @NotNull
    @Contract("_, _ -> new")
    public static String toS(double d, int fractionDigits) {
        return toS(d, fractionDigits, fractionDigits, false);
    }

    /**
     * Parses a {@code String} text into a {@code BigInteger} object.
     *
     * @param s            a {@code String} text. It can be {@code null}.
     * @param defaultValue the default value to use if the {@code String} is {@code null}, empty or {@code "null"}.
     * @return the parsed {@code BigInteger} object.
     * @throws NumberFormatException if {@code s} is invalid.
     */
    @NotNull
    @Contract("null, _ -> param2")
    public static BigInteger toBigInteger(@Nullable String s, @NotNull BigInteger defaultValue) {
        return isNull(s) ? defaultValue : new BigInteger(s);
    }

    /**
     * Converts a {@code BigInteger} object into a {@code String}.
     *
     * @param value a {@code BigInteger} object. It can be {@code null}.
     * @return the converted {@code String}.
     */
    @NotNull
    @Contract("_ -> !null")
    public static String toS(@Nullable BigInteger value) {
        return value == null ? NULL : value.toString();
    }

    /**
     * Checks whether a {@code String} text represents an integer number with radix of 10.
     *
     * @param s a {@code String} text. It can be {@code null}.
     * @return {@code true} -- is an integer number; {@code false} -- not an integer number.
     */
    @Contract("null -> false")
    public static boolean isIntegerString(@Nullable String s) {
        return isIntegerString(s, 10);
    }

    /**
     * Checks whether a {@code String} text represents an integer number with specified radix.
     *
     * @param s     a {@code String} text. It can be {@code null}.
     * @param radix the radix. It shall be a valid one.
     * @return {@code true} -- is an integer number; {@code false} -- not an integer number.
     * @throws IllegalArgumentException if {@code radix} is invalid.
     */
    @Contract("null, _ -> false")
    public static boolean isIntegerString(@Nullable String s, int radix) {
        if (isNull(s)) {
            return false;
        }
        guardRadix(radix);
        INTEGER_CODE_POINT_STATE expected = INTEGER_CODE_POINT_STATE.SIGN_OR_DIGIT;
        if (s.length() == 1) {
            expected = INTEGER_CODE_POINT_STATE.DIGIT;
        }
        for (int I = 0; I < s.length(); I++) {
            int codePoint = s.codePointAt(I);
            switch (expected) {
                case SIGN_OR_DIGIT:
                    if (isValidDigit(codePoint, radix) || isSign(codePoint)) {
                        expected = INTEGER_CODE_POINT_STATE.DIGIT;
                        continue;
                    }
                    return false;
                case DIGIT:
                    if (isValidDigit(codePoint, radix)) {
                        continue;
                    }
                    return false;
                default:
                    // this shall never happen
                    throw new UnexpectedApplicationFlowException();
            }
        }
        return true;
    }

    /**
     * Ensure the radix is a valid one.
     *
     * @param radix the radix.
     * @throws IllegalArgumentException if {@code radix} is invalid.
     */
    private static void guardRadix(int radix) {
        if (radix < Character.MIN_RADIX || radix > Character.MAX_RADIX) {
            // do not use FreeStringBuilder.format() here, otherwise it will introduce circular dependency.
            //noinspection StringBufferReplaceableByString
            @NonNls StringBuilder message = new StringBuilder("Invalid radix ");
            message.append(radix).append(". It shall be between ").append(Character.MIN_RADIX).append(" and ")
                    .append(Character.MAX_RADIX).append('.');
            throw new IllegalArgumentException(message.toString());
        }
    }

    /**
     * Checks whether a code point is a valid digit with specified radix.
     *
     * @param codePoint a code point.
     * @param radix     the radix.
     * @return {@code true} -- is a digit; {@code false} -- not a digit.
     */
    public static boolean isValidDigit(int codePoint, int radix) {
        return Character.digit(codePoint, radix) != -1;
    }

    /**
     * Checks whether the code point is the plus mark or minus mark.
     *
     * @param codePoint a code point.
     * @return {@code true} -- is plus or minus mark; {@code false} -- not plus or minus mark.
     */
    @SuppressWarnings("MagicCharacter")
    public static boolean isSign(int codePoint) {
        return codePoint == '+' || codePoint == '-';
    }

    private enum INTEGER_CODE_POINT_STATE {
        SIGN_OR_DIGIT, DIGIT
    }

    /**
     * Checks whether a {@code String} text represents a decimal number.
     *
     * @param s a {@code String} text. It can be {@code null}.
     * @return {@code true} -- is a decimal number; {@code false} -- not a decimal number.
     */
    @Contract("null -> false")
    public static boolean isNumberString(@Nullable String s) {
        return isNumberString(s, 10);
    }

    /**
     * Checks whether a String text represents a number with specified radix.
     *
     * @param s     a {@code String} text. It can be {@code null}.
     * @param radix the radix. It shall be a valid one.
     * @return {@code true} -- is a number; {@code false} -- not a number.
     * @throws IllegalArgumentException if {@code radix} is invalid.
     */
    @SuppressWarnings({"SwitchStatementDensity", "MagicCharacter", "OverlyComplexMethod", "OverlyLongMethod"})
    @Contract("null, _ -> false")
    public static boolean isNumberString(@Nullable String s, int radix) {
        if (isNull(s)) {
            return false;
        }
        guardRadix(radix);
        NUMBER_CODE_POINT_STATE expected = NUMBER_CODE_POINT_STATE.SIGN_OR_DIGIT;
        if (s.length() == 1) {
            expected = NUMBER_CODE_POINT_STATE.DIGIT_NO_SEPARATOR;
        }
        for (int I = 0; I < s.length(); I++) {
            int codePoint = s.codePointAt(I);
            switch (expected) {
                case SIGN_OR_DIGIT:
                    if (isValidDigit(codePoint, radix)) {
                        expected = NUMBER_CODE_POINT_STATE.DIGIT_OR_SEPARATOR;
                        continue;
                    }
                    if (isSign(codePoint)) {
                        expected = NUMBER_CODE_POINT_STATE.DIGIT_REQUIRED_BEFORE_SEPARATOR;
                        continue;
                    }
                    return false;
                case DIGIT_REQUIRED_BEFORE_SEPARATOR:
                    if (isValidDigit(codePoint, radix)) {
                        expected = NUMBER_CODE_POINT_STATE.DIGIT_OR_SEPARATOR;
                        continue;
                    }
                    return false;
                case DIGIT_OR_SEPARATOR:
                    if (isValidDigit(codePoint, radix)) {
                        continue;
                    }
                    if (codePoint == '.') {
                        expected = NUMBER_CODE_POINT_STATE.DIGIT_REQUIRED_AFTER_SEPARATOR;
                        continue;
                    }
                    return false;
                case DIGIT_REQUIRED_AFTER_SEPARATOR:
                    if (isValidDigit(codePoint, radix)) {
                        expected = NUMBER_CODE_POINT_STATE.DIGIT_NO_SEPARATOR;
                        continue;
                    }
                    return false;
                case DIGIT_NO_SEPARATOR:
                    if (isValidDigit(codePoint, radix)) {
                        continue;
                    }
                    return false;
                default:
                    // this shall never happen
                    throw new UnexpectedApplicationFlowException();
            }
        }
        // other states are invalid ones.
        return expected == NUMBER_CODE_POINT_STATE.DIGIT_OR_SEPARATOR || expected == NUMBER_CODE_POINT_STATE.DIGIT_NO_SEPARATOR;
    }

    private enum NUMBER_CODE_POINT_STATE {
        SIGN_OR_DIGIT, DIGIT_REQUIRED_BEFORE_SEPARATOR, DIGIT_OR_SEPARATOR, DIGIT_REQUIRED_AFTER_SEPARATOR, DIGIT_NO_SEPARATOR
    }

    /**
     * Defines this method so that this class does not depend on {@code StringUtil}.
     */
    @Contract("null -> true")
    private static boolean isNull(@Nullable String s) {
        return s == null || s.isEmpty() || NULL.equals(s);
    }

    private NumberUtil() {
        //DO NOTHING.
    }

}
