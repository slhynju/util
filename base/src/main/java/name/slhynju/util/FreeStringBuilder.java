package name.slhynju.util;

import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.math.BigInteger;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.function.Function;

import static name.slhynju.util.StringUtil.NULL;

/**
 * Utility class to build a {@code String}. This class is not thread-safe.
 * <p>This class shall only depend on other utility classes in same package (subpackages excluded).</p>
 *
 * @author slhynju
 */
public final class FreeStringBuilder implements Serializable, Appendable, CharSequence {

    private static final long serialVersionUID = 5622464332073959812L;

    /**
     * Single quote to wrap a {@code char}.
     */
    public static final char CHAR_QUOTE = '\'';

    /**
     * Double quote to wrap a {@code String} or {@code CharSequence}.
     */
    public static final char STRING_QUOTE = '"';

    /**
     * Opening mark of {@code arrays} and {@code Collections}.
     */
    public static final @NotNull String LIST_OPENING_MARK = "[";

    /**
     * Element separator of {@code arrays} and {@code Collections}.
     */
    public static final @NotNull String LIST_ELEMENT_SEPARATOR = ", ";

    /**
     * Closing mark of {@code arrays} and {@code Collections}.
     */
    public static final @NotNull String LIST_CLOSING_MARK = "]";

    /**
     * Opening mark of {@code Maps}.
     */
    public static final @NotNull String MAP_OPENING_MARK = "{";

    /**
     * Key-value separator of {@code Maps}.
     */
    public static final @NotNull String MAP_KEY_VALUE_SEPARATOR = ": ";

    /**
     * Entry separator of {@code Maps}.
     */
    public static final @NotNull String MAP_ENTRY_SEPARATOR = ", ";

    /**
     * Closing mark of {@code Maps}.
     */
    public static final @NotNull String MAP_CLOSING_MARK = "}";

    /**
     * Opening mark of parameter placeholders in {@code String} templates.
     */
    private static final char PARAMETER_OPENING_MARK = '{';

    /**
     * Closing mark of parameter placeholders in {@code String} templates.
     */
    private static final char PARAMETER_CLOSING_MARK = '}';

    @SuppressWarnings("StringBufferField")
    private final @NotNull StringBuilder sb;

    /**
     * Concatenates a {@code String array} into a single {@code String} with custom separator.
     *
     * @param strings   a {@code String array}. It can be {@code null}.
     * @param separator an array element separator. It shall not be {@code null}.
     * @return the concatenated {@code String}.
     * @throws NullPointerException if {@code separator} is {@code null}.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @NotNull String toS(@Nullable final String @Nullable [] strings, @NotNull final String separator) {
        if (strings == null) {
            return NULL;
        }
        return new FreeStringBuilder().append(strings, "", separator, "").toS();
    }

    /**
     * Concatenates an {@code Object array} into a single {@code String} with custom separator.
     *
     * @param objects   an {@code Object array}. It can be {@code null}.
     * @param separator an array element separator. It shall not be {@code null}.
     * @return the concatenated {@code String}.
     * @throws NullPointerException if {@code separator} is {@code null}.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @NotNull String toS(@Nullable final Object @Nullable [] objects, @NotNull final String separator) {
        if (objects == null) {
            return NULL;
        }
        return new FreeStringBuilder().append(objects, "", separator, "").toS();
    }

    /**
     * Concatenates a {@code Collection} into a single {@code String} with custom separator.
     *
     * @param collection a {@code Collection}. It can be {@code null}.
     * @param separator  a {@code Collection} element separator. It shall not be {@code null}.
     * @return the concatenated {@code String}.
     * @throws NullPointerException if {@code separator} is {@code null}.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @NotNull String toS(@Nullable final Collection<?> collection, @NotNull final String separator) {
        if (collection == null) {
            return NULL;
        }
        return new FreeStringBuilder().append(collection, "", separator, "").toS();
    }

    /**
     * Concatenates a {@code Map} into a single {@code String} with custom separators.
     *
     * @param map               a {@code Map}. It can be {@code null}.
     * @param keyValueSeparator a key-value separator. It shall not be {@code null}.
     * @param entrySeparator    a {@code Map} entry separator. It shall not be {@code null}.
     * @return the concatenated {@code String}.
     * @throws NullPointerException if {@code separator} is {@code null}.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @NotNull String toS(@Nullable final Map<?, ?> map, @NotNull final String keyValueSeparator, @NotNull final String entrySeparator) {
        if (map == null) {
            return NULL;
        }
        return new FreeStringBuilder().append(map, "", keyValueSeparator, entrySeparator, "").toS();
    }

    /**
     * Formats a {@code String} template into a concrete {@code String} with provided parameters.
     *
     * @param format a {@code String} template. It can be {@code null}.
     * @param values an array of parameter values. It shall not be {@code null}.
     * @return the result {@code String}.
     * @throws IllegalArgumentException       if {@code format} is invalid.
     * @throws ArrayIndexOutOfBoundsException if a parameter index in {@code format} is invalid.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @NotNull String format(@Nullable final String format, @NotNull final Object... values) {
        if (format == null) {
            return NULL;
        }
        return new FreeStringBuilder().appendFormat(format, values).toS();
    }

    // constructors

    /**
     * Initializes this builder.
     */
    public FreeStringBuilder() {
        sb = new StringBuilder();
    }

    /**
     * Initializes this builder with specified capacity.
     *
     * @param capacity the initial capacity.
     * @throws NegativeArraySizeException if {@code capacity} is negative.
     */
    public FreeStringBuilder(final int capacity) {
        sb = new StringBuilder(capacity);
    }

    /**
     * Initializes this builder with a {@code String} text.
     *
     * @param s an initial {@code String} text. It shall not be {@code null}.
     * @throws NullPointerException if {@code s} is {@code null}.
     */
    public FreeStringBuilder(@NotNull final String s) {
        sb = new StringBuilder(s);
    }

    // primitive types

    /**
     * Appends a {@code boolean} value.
     *
     * @param value a {@code boolean} value.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(final boolean value) {
        sb.append(value);
        return this;
    }

    /**
     * Appends a {@code char} value.
     *
     * @param c a {@code char} value.
     * @return this builder.
     */
    @Override
    public @NotNull FreeStringBuilder append(final char c) {
        sb.append(c);
        return this;
    }

    /**
     * Appends a {@code char} value wrapped with single quotes.
     *
     * @param c a {@code char} value.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder appendQuoted(final char c) {
        sb.append(CHAR_QUOTE).append(c).append(CHAR_QUOTE);
        return this;
    }

    // append(byte) is covered by append(int)

    /**
     * Appends an {@code int} value.
     *
     * @param i an {@code int} value.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(final int i) {
        sb.append(i);
        return this;
    }

    /**
     * Appends a {@code long} value.
     *
     * @param l a {@code long} value.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(final long l) {
        sb.append(l);
        return this;
    }

    /**
     * Appends a {@code long} value with custom format.
     *
     * @param l      a {@code long} value.
     * @param format a number format to format {@code long} numbers. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(final long l, @NotNull final NumberFormat format) {
        sb.append(format.format(l));
        return this;
    }

    /**
     * Appends a {@code float} value.
     *
     * @param f a {@code float} value.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(final float f) {
        sb.append(f);
        return this;
    }

    // append(float, int, int, boolean) is covered by append(double, int, int, boolean)

    // append(float, NumberFormat) is covered by append(double, NumberFormat)

    /**
     * Appends a {@code double} value.
     *
     * @param d a {@code double} value.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(final double d) {
        sb.append(d);
        return this;
    }

    /**
     * Appends a {@code double} value with custom format.
     *
     * @param d                 a {@code double} value.
     * @param minFractionDigits minimum fraction digits.
     * @param maxFractionDigits maximum fraction digits.
     * @param groupingUsed      whether to use grouping separator.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(final double d, final int minFractionDigits, final int maxFractionDigits, final boolean groupingUsed) {
        final String s = NumberUtil.toS(d, minFractionDigits, maxFractionDigits, groupingUsed);
        sb.append(s);
        return this;
    }

    /**
     * Appends a {@code double} value with custom format.
     *
     * @param d      a {@code double} value.
     * @param format a number format to format {@code double} numbers. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(final double d, @NotNull final NumberFormat format) {
        final String s = format.format(d);
        sb.append(s);
        return this;
    }

    // Big Integer

    /**
     * Appends a {@code BigInteger} object.
     *
     * @param value a {@code BigInteger} object. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(@Nullable final BigInteger value) {
        sb.append(value);
        return this;
    }

    // primitive arrays

    /**
     * Appends a {@code boolean array} with marks and element separator.
     *
     * @param array            a {@code boolean array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final boolean @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final boolean value : array) {
                sb.append(value).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code byte array} with marks and element separator.
     *
     * @param array            a {@code byte array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final byte @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final byte b : array) {
                sb.append(b).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code char array} with marks and element separator.
     *
     * @param array            a {@code char array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final char @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final char c : array) {
                sb.append(c).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code char array} with marks and element separator. Each char is wrapped with single quotes.
     *
     * @param array            a {@code char array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder appendQuoted(@Nullable final char @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                                   @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final char c : array) {
                appendQuoted(c);
                sb.append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code float array} with marks, element separator and a custom number format.
     *
     * @param array            a {@code float array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @param format           a number format to format {@code float} numbers. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} or {@code format} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final float @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark, @NotNull final NumberFormat format) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final float f : array) {
                append(f, format);
                sb.append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code double array} with marks, element separator and a custom number format.
     *
     * @param array            a {@code double array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @param format           a number format to format {@code double} numbers. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} or {@code format} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final double @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark, @NotNull final NumberFormat format) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final double d : array) {
                append(d, format);
                sb.append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends an {@code int array} with marks and element separator.
     *
     * @param array            an {@code int array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final int @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final int i : array) {
                sb.append(i).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code long array} with marks and element separator.
     *
     * @param array            a {@code long array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final long @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final long l : array) {
                sb.append(l).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code long array} with marks, element separator and a custom number format.
     *
     * @param array            a {@code long array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @param format           a number format to format {@code long} numbers. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} or {@code format} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final long @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark, @NotNull final NumberFormat format) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final long l : array) {
                append(l, format);
                sb.append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    // String

    /**
     * Appends a {@code String}.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(@Nullable final String s) {
        sb.append(s);
        return this;
    }

    /**
     * Appends a {@code String} wrapped with double quotes.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder appendQuoted(@Nullable final String s) {
        if (s == null) {
            sb.append(NULL);
        } else {
            sb.append(STRING_QUOTE).append(s).append(STRING_QUOTE);
        }
        return this;
    }

    // CharSequence

    /**
     * Appends a {@code CharSequence}.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ParameterNameDiffersFromOverriddenParameter")
    @Override
    public @NotNull FreeStringBuilder append(@Nullable final CharSequence sequence) {
        sb.append(sequence);
        return this;
    }

    /**
     * Appends a subsequence of a {@code CharSequence}.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @param start    the start index of subsequence (included).
     * @param end      the end index of subsequence (excluded).
     * @return this builder.
     * @throws IndexOutOfBoundsException if {@code start} or {@code end} is an invalid position.
     */
    @SuppressWarnings("ParameterNameDiffersFromOverriddenParameter")
    @Override
    public @NotNull FreeStringBuilder append(@Nullable final CharSequence sequence, final int start, final int end) {
        if (sequence == null) {
            sb.append(NULL);
        } else {
            sb.append(sequence, start, end);
        }
        return this;
    }

    /**
     * Appends a {@code CharSequence} wrapped with double quotes.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder appendQuoted(@Nullable final CharSequence sequence) {
        if (sequence == null) {
            sb.append(NULL);
        } else {
            sb.append(STRING_QUOTE).append(sequence).append(STRING_QUOTE);
        }
        return this;
    }

    /**
     * Appends a subsequence of a {@code CharSequence}, wrapped with double quotes.
     *
     * @param chars a {@code CharSequence}. It can be {@code null}.
     * @param start the start index of subsequence (included).
     * @param end   the end index of subsequence (excluded).
     * @return this builder.
     * @throws IndexOutOfBoundsException if {@code start} or {@code end} is an invalid position.
     */
    public @NotNull FreeStringBuilder appendQuoted(@Nullable final CharSequence chars, final int start, final int end) {
        if (chars == null) {
            sb.append(NULL);
        } else {
            sb.append(STRING_QUOTE).append(chars, start, end).append(STRING_QUOTE);
        }
        return this;
    }

    // String array

    /**
     * Appends a {@code String array} with default marks and element separator.
     *
     * @param array a {@code String array}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(@Nullable final String... array) {
        return append(array, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
    }

    /**
     * Appends a {@code String array} with default element separator only.
     *
     * @param array a {@code String array}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder appendWithoutMarks(@Nullable final String... array) {
        return append(array, "", LIST_ELEMENT_SEPARATOR, "");
    }

    /**
     * Appends a {@code String array} with marks and element separator.
     *
     * @param array            a {@code String array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final String @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final String s : array) {
                sb.append(s).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }


    /**
     * Appends a {@code String array} with marks and element separator. Each {@code String} element is wrapped with double quotes.
     *
     * @param array            a {@code String array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder appendQuoted(@Nullable final String @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                                   @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final String o : array) {
                appendQuoted(o);
                sb.append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    // common types

    /**
     * Appends an {@code Enum} object.
     *
     * @param e an {@code Enum} object. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(@Nullable final Enum<?> e) {
        sb.append(e);
        return this;
    }

    /**
     * Appends a {@code Date} with custom format.
     *
     * @param date   a {@code Date}. It can be {@code null}.
     * @param format a {@code Date} format. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @SuppressWarnings("UseOfObsoleteDateTimeApi")
    public @NotNull FreeStringBuilder append(@Nullable final Date date, @NotNull final String format) {
        final String s = DateUtil.toS(date, format);
        sb.append(s);
        return this;
    }

    /**
     * Appends a {@code Date} with custom format.
     *
     * @param date   a {@code Date}. It can be {@code null}.
     * @param format a {@code Date} format. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @SuppressWarnings("UseOfObsoleteDateTimeApi")
    public @NotNull FreeStringBuilder append(@Nullable final Date date, @NotNull final DateFormat format) {
        final String s = DateUtil.toS(date, format);
        sb.append(s);
        return this;
    }

    /**
     * Appends a Temporal object with custom formatter.
     *
     * @param temporal  a Temporal object. It can be {@code null}.
     * @param formatter a Temporal formatter. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code formatter} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final TemporalAccessor temporal, @NotNull final DateTimeFormatter formatter) {
        if (temporal == null) {
            sb.append(NULL);
        } else {
            formatter.formatTo(temporal, sb);
        }
        return this;
    }

    /**
     * Appends a {@code Path}.
     *
     * @param path a {@code Path}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(@Nullable final Path path) {
        sb.append(path);
        return this;
    }

    // Object array

    /**
     * Appends an {@code Object array} with default marks and element separator.
     *
     * @param array an {@code Object array}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(@Nullable final Object[] array) {
        return append(array, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
    }


    /**
     * Appends an {@code Object array} with default element separator only.
     *
     * @param array an {@code Object array}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder appendWithoutMarks(@Nullable final Object[] array) {
        return append(array, "", LIST_ELEMENT_SEPARATOR, "");
    }

    /**
     * Appends an {@code Object array} with marks and element separator.
     *
     * @param array            an {@code Object array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final Object @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final Object o : array) {
                append(o);
                sb.append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends an {@code Object array} with marks, element separator and a custom element formatter.
     *
     * @param array            an {@code Object array}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator an array element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @param elementFormatter a custom formatter to format array elements. It shall not be {@code null}.
     * @param <T>              the element type.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} or {@code elementFormatter} is {@code null}.
     */
    public @NotNull <T> FreeStringBuilder append(@Nullable final T @Nullable [] array, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                                 @NotNull final String closingMark, @NotNull final Function<? super T, String> elementFormatter) {
        if (array == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(array)) {
            for (final T o : array) {
                sb.append(elementFormatter.apply(o)).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    // Collections

    /**
     * Appends a {@code Collection} with default marks and element separator.
     *
     * @param objects a {@code Collection}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(@Nullable final Collection<?> objects) {
        return append(objects, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
    }

    /**
     * Appends a {@code Collection} with default element separator only.
     *
     * @param objects a {@code Collection}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder appendWithoutMarks(@Nullable final Collection<?> objects) {
        return append(objects, "", LIST_ELEMENT_SEPARATOR, "");
    }

    /**
     * Appends a {@code Collection} with marks and element separator.
     *
     * @param objects          a {@code Collection}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator a {@code Collection} element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final Collection<?> objects, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                             @NotNull final String closingMark) {
        if (objects == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(objects)) {
            for (final Object o : objects) {
                append(o);
                sb.append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code Collection} with marks, element separator and a custom element formatter.
     *
     * @param objects          a {@code Collection}. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator a {@code Collection} element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @param elementFormatter a custom formatter to format elements. It shall not be {@code null}.
     * @param <T>              the element type.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} or {@code elementFormatter} is {@code null}.
     */
    public @NotNull <T> FreeStringBuilder append(@Nullable final Collection<T> objects, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                                 @NotNull final String closingMark, @NotNull final Function<? super T, String> elementFormatter) {
        if (objects == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(objects)) {
            for (final T o : objects) {
                sb.append(elementFormatter.apply(o)).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    // Map

    /**
     * Appends a {@code Map} with default marks and separators.
     *
     * @param map a {@code Map}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder append(@Nullable final Map<?, ?> map) {
        return append(map, MAP_OPENING_MARK, MAP_KEY_VALUE_SEPARATOR, MAP_ENTRY_SEPARATOR, MAP_CLOSING_MARK);
    }

    /**
     * Appends a {@code Map} with default separators only.
     *
     * @param map a {@code Map}. It can be {@code null}.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder appendWithoutMarks(@Nullable final Map<?, ?> map) {
        return append(map, "", MAP_KEY_VALUE_SEPARATOR, MAP_ENTRY_SEPARATOR, "");
    }

    /**
     * Appends a {@code Map} with marks and separators.
     *
     * @param map               a {@code Map}. It can be {@code null}.
     * @param openingMark       an opening mark. It shall not be {@code null}.
     * @param keyValueSeparator a key-value separator. It shall not be {@code null}.
     * @param entrySeparator    a {@code Map} entry separator. It shall not be {@code null}.
     * @param closingMark       a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code entrySeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder append(@Nullable final Map<?, ?> map, @NotNull final String openingMark, @NotNull final String keyValueSeparator,
                                             @NotNull final String entrySeparator, @NotNull final String closingMark) {
        if (map == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(map)) {
            for (final Map.Entry<?, ?> entry : map.entrySet()) {
                append(entry.getKey());
                sb.append(keyValueSeparator);
                append(entry.getValue());
                sb.append(entrySeparator);
            }
            deleteEnd(entrySeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a {@code Map} with marks, separators and custom formatters.
     *
     * @param map               a {@code Map}. It can be {@code null}.
     * @param openingMark       an opening mark. It shall not be {@code null}.
     * @param keyValueSeparator a key-value separator. It shall not be {@code null}.
     * @param entrySeparator    a {@code Map} entry separator. It shall not be {@code null}.
     * @param closingMark       a closing mark. It shall not be {@code null}.
     * @param keyFormatter      a custom formatter to format {@code Map} keys. It shall not be {@code null}.
     * @param valueFormatter    a custom formatter to format {@code Map} values. It shall not be {@code null}.
     * @param <K>               the key type.
     * @param <V>               the value type.
     * @return this builder.
     * @throws NullPointerException if {@code entrySeparator}, {@code keyFormatter} or {@code valueFormatter} is {@code null}.
     */
    @SuppressWarnings("MethodWithTooManyParameters")
    public @NotNull <K, V> FreeStringBuilder append(@Nullable final Map<K, V> map, @NotNull final String openingMark, @NotNull final String keyValueSeparator,
                                                    @NotNull final String entrySeparator, @NotNull final String closingMark, @NotNull final Function<? super K, String> keyFormatter,
                                                    @NotNull final Function<? super V, String> valueFormatter) {
        if (map == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(map)) {
            for (final Map.Entry<K, V> entry : map.entrySet()) {
                sb.append(keyFormatter.apply(entry.getKey())).append(keyValueSeparator)
                        .append(valueFormatter.apply(entry.getValue())).append(entrySeparator);
            }
            deleteEnd(entrySeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    // Stack

    /**
     * Appends a stack with marks and element separator in reversed order.
     *
     * @param stack            a stack. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator a stack element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} is {@code null}.
     */
    public @NotNull FreeStringBuilder appendStack(@Nullable final Deque<?> stack, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                                  @NotNull final String closingMark) {
        if (stack == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(stack)) {
            final Iterator<?> it = stack.descendingIterator();
            while (it.hasNext()) {
                append(it.next());
                sb.append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    /**
     * Appends a stack with marks, element separator and a custom element formatter in reversed order.
     *
     * @param stack            a stack. It can be {@code null}.
     * @param openingMark      an opening mark. It shall not be {@code null}.
     * @param elementSeparator a stack element separator. It shall not be {@code null}.
     * @param closingMark      a closing mark. It shall not be {@code null}.
     * @param elementFormatter a custom formatter to format elements. It shall not be {@code null}.
     * @param <T>              the element type.
     * @return this builder.
     * @throws NullPointerException if {@code elementSeparator} or {@code elementFormatter} is {@code null}.
     */
    public @NotNull <T> FreeStringBuilder appendStack(@Nullable final Deque<T> stack, @NotNull final String openingMark, @NotNull final String elementSeparator,
                                                      @NotNull final String closingMark, @NotNull final Function<? super T, String> elementFormatter) {
        if (stack == null) {
            sb.append(NULL);
            return this;
        }
        sb.append(openingMark);
        if (notEmpty(stack)) {
            final Iterator<T> it = stack.descendingIterator();
            while (it.hasNext()) {
                sb.append(elementFormatter.apply(it.next())).append(elementSeparator);
            }
            deleteEnd(elementSeparator.length());
        }
        sb.append(closingMark);
        return this;
    }

    // Object

    /**
     * Appends an {@code Object}.
     *
     * @param o an {@code Object}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings({"ChainOfInstanceofChecks", "OverlyStrongTypeCast", "rawtypes", "OverlyComplexMethod", "UseOfObsoleteDateTimeApi"})
    public @NotNull FreeStringBuilder append(@Nullable final Object o) {
        if (o == null) {
            sb.append(NULL);
            return this;
        }
        if (o instanceof String) {
            return append((String) o);
        }
        if (o instanceof CharSequence) {
            return append((CharSequence) o);
        }
        if (o instanceof Collection) {
            return append((Collection) o);
        }
        if (o instanceof Map) {
            return append((Map) o);
        }
        if (o instanceof Date) {
            return append((Date) o, DateUtil.SIMPLE_FORMAT);
        }
        if (o instanceof LocalDate) {
            return append((LocalDate) o, DateTimeFormatter.ISO_LOCAL_DATE);
        }
        if (o instanceof LocalTime) {
            return append((LocalTime) o, DateTimeFormatter.ISO_LOCAL_TIME);
        }
        if (o instanceof LocalDateTime) {
            return append((LocalDateTime) o, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        }
        if (o instanceof ZonedDateTime) {
            return append((ZonedDateTime) o, DateTimeFormatter.ISO_ZONED_DATE_TIME);
        }
        if (o instanceof String[]) {
            return append((String[]) o);
        }
        if (o instanceof Object[]) {
            return append((Object[]) o);
        }
        sb.append(o);
        return this;
    }

    // format

    /**
     * Appends a {@code String} template and parameter values.
     * <p>Examples:</p>
     * <ul>
     *     <li>appendFormat("The city {} is part of country {}.", "Shanghai", "China") = "The city Shanghai is part of country China."</li>
     *     <li>appendFormat("{2} = {0} + {1}", 1, 2, 3) = "3 = 1 + 2"</li>
     * </ul>
     *
     * @param format a {@code String} template. It can be {@code null}.
     * @param values the parameter values. The number of values shall match the parameter placeholders in template.
     * @return this builder.
     * @throws IllegalArgumentException       if {@code format} is invalid.
     * @throws ArrayIndexOutOfBoundsException if a parameter index in {@code format} is an invalid position.
     */
    @SuppressWarnings({"LocalVariableUsedAndDeclaredInDifferentSwitchBranches", "SwitchStatementDensity", "ReuseOfLocalVariable", "OverlyLongMethod"})
    public @NotNull FreeStringBuilder appendFormat(@Nullable final String format, @NotNull final Object... values) {
        if (format == null) {
            sb.append(NULL);
            return this;
        }
        FORMAT_STATE state = FORMAT_STATE.TEMPLATE;
        int current = 0;
        final int size = format.length();
        int paramCount = 0;
        while (current < size) {
            switch (state) {
                case TEMPLATE:
                    int next = format.indexOf(PARAMETER_OPENING_MARK, current);
                    if (next == -1) {
                        sb.append(format.substring(current));
                        return this;
                    }
                    if (next > current) {
                        sb.append(format, current, next);
                        current = next;
                    }
                    state = FORMAT_STATE.PARAMETER;
                    break;
                case PARAMETER:
                    next = format.indexOf(PARAMETER_CLOSING_MARK, current);
                    if (next == -1) {
                        @NonNls final String message = "Invalid format " + format + '.';
                        throw new IllegalArgumentException(message);
                    }
                    int paramIndex = paramCount;
                    if (next == current + 1) {
                        paramCount++;
                    } else {
                        final String paramIndexStr = format.substring(current + 1, next);
                        paramIndex = NumberUtil.toInt(paramIndexStr, 0);
                    }
                    append(values[paramIndex]);
                    current = next + 1;
                    state = FORMAT_STATE.TEMPLATE;
                    break;
            }
        }
        return this;
    }

    private enum FORMAT_STATE {
        TEMPLATE, PARAMETER
    }

    // Other utility methods.

    /**
     * Appends a number of tab characters.
     *
     * @param indentation the number of tab characters. It shall be a positive integer.
     * @return this builder.
     */
    public @NotNull FreeStringBuilder indent(final int indentation) {
        if (indentation > 0) {
            sb.append("\t".repeat(indentation));
        }
        return this;
    }

    /**
     * Checks whether the result {@code String} starts with a prefix.
     *
     * @param prefix a prefix to check. It shall not be {@code null}.
     * @return true -- starting with prefix; false -- not starting with prefix.
     * @throws NullPointerException if {@code prefix} is {@code null}.
     */
    public boolean startsWith(@NotNull final String prefix) {
        final String sub = StringUtil.subPrefix(sb, prefix.length());
        return prefix.equals(sub);
    }

    /**
     * Deletes a number of prefix characters.
     *
     * @param prefixLength the number of prefix characters to delete. It shall be a positive integer.
     * @return this builder.
     * @throws StringIndexOutOfBoundsException if {@code prefixLength} is negative.
     */
    public @NotNull FreeStringBuilder deletePrefix(final int prefixLength) {
        StringUtil.deletePrefix(sb, prefixLength);
        return this;
    }

    /**
     * Deletes a prefix substring if found.
     *
     * @param prefix a prefix substring to delete. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code prefix} is {@code null}.
     */
    public @NotNull FreeStringBuilder deletePrefixIf(@NotNull final String prefix) {
        if (startsWith(prefix)) {
            deletePrefix(prefix.length());
        }
        return this;
    }

    /**
     * Checks whether the result {@code String} ends with a suffix.
     *
     * @param suffix a suffix to check. It shall not be {@code null}.
     * @return true -- ending with the suffix; false -- not ending with the suffix.
     * @throws NullPointerException if {@code suffix} is {@code null}.
     */
    public boolean endsWith(@NotNull final String suffix) {
        final String sub = StringUtil.subSuffix(sb, suffix.length());
        return suffix.equals(sub);
    }

    /**
     * Deletes a number of suffix characters.
     *
     * @param suffixLength the number of suffix characters to delete. It shall be a positive number.
     * @return this builder.
     * @throws StringIndexOutOfBoundsException if {@code suffixLength} is negative.
     */
    public @NotNull FreeStringBuilder deleteEnd(final int suffixLength) {
        StringUtil.deleteSuffix(sb, suffixLength);
        return this;
    }

    /**
     * Deletes a suffix substring if found.
     *
     * @param suffix a suffix substring to delete. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code suffix} is {@code null}.
     */
    public @NotNull FreeStringBuilder deleteEndIf(@NotNull final String suffix) {
        if (endsWith(suffix)) {
            deleteEnd(suffix.length());
        }
        return this;
    }

    /**
     * Deletes a subsequence.
     *
     * @param start the starting index (included).
     * @param end   the ending index (excluded).
     * @return this builder.
     * @throws StringIndexOutOfBoundsException if {@code start} or {@code end} is an invalid position.
     */
    public @NotNull FreeStringBuilder delete(final int start, final int end) {
        sb.delete(start, end);
        return this;
    }

    /**
     * Inserts a subsequence at specified index.
     *
     * @param s      a subsequence to insert. {@code null} is treated as {@code String "null"}.
     * @param offset the index to insert. It shall be a valid one.
     * @return this builder.
     * @throws StringIndexOutOfBoundsException if {@code offset} is an invalid position.
     */
    public @NotNull FreeStringBuilder insert(@Nullable final String s, final int offset) {
        sb.insert(offset, s);
        return this;
    }

    /**
     * Looks up a subsequence.
     *
     * @param s a subsequence to look up. It shall not be {@code null}.
     * @return the starting index of the first matching subsequence if found; {@code -1} if not found.
     * @throws NullPointerException if {@code s} is {@code null}.
     */
    public int indexOf(@NotNull final String s) {
        return indexOf(s, 0);
    }

    /**
     * Looks up a subsequence, starting from specified index.
     *
     * @param s         a subsequence to look up. It shall not be {@code null}.
     * @param fromIndex the starting index to look up. negative value is treated as {@code 0}.
     * @return the starting index of the first matching subsequence if found; {@code -1} if not found.
     * @throws NullPointerException if {@code s} is {@code null}.
     */
    public int indexOf(@NotNull final String s, final int fromIndex) {
        return sb.indexOf(s, fromIndex);
    }

    /**
     * Forward looks up a subsequence.
     *
     * @param s a subsequence to look up. It shall not be {@code null}.
     * @return the starting index of the last matching subsequence if found; {@code -1} if not found.
     * @throws NullPointerException if {@code s} is {@code null}.
     */
    public int lastIndexOf(@NotNull final String s) {
        return lastIndexOf(s, sb.length() - 1);
    }

    /**
     * Forward looks up a subsequence, starting from specified index.
     *
     * @param s         a subsequence to look up. It shall not be {@code null}.
     * @param fromIndex the starting index to forward look up.
     * @return the starting index of the last matching subsequence if found; {@code -1} if not found. If {@code fromIndex} is negative, this method always returns {@code -1}.
     * @throws NullPointerException if {@code s} is {@code null}.
     */
    public int lastIndexOf(@NotNull final String s, final int fromIndex) {
        return sb.lastIndexOf(s, fromIndex);
    }

    /**
     * Changes first {@code char} to upper case.
     *
     * @return this builder.
     */
    public @NotNull FreeStringBuilder firstCharToUpperCase() {
        StringUtil.firstCharToUpperCase(sb);
        return this;
    }

    /**
     * Changes first {@code char} to lower case.
     *
     * @return this builder.
     */
    public @NotNull FreeStringBuilder firstCharToLowerCase() {
        StringUtil.firstCharToLowerCase(sb);
        return this;
    }

    /**
     * Removes the whole {@code String}.
     *
     * @return this builder.
     */
    public @NotNull FreeStringBuilder clear() {
        sb.setLength(0);
        return this;
    }

    /**
     * Checks whether the result {@code String} is empty.
     *
     * @return true -- is empty; false -- not empty.
     */
    public boolean isEmpty() {
        return sb.length() == 0;
    }

    @Override
    public int length() {
        return sb.length();
    }

    @Override
    public char charAt(final int index) {
        return sb.charAt(index);
    }

    /**
     * Sets the {@code char} at specified index.
     *
     * @param index an index. It shall be a valid one.
     * @param ch    a {@code char} value.
     * @return this builder.
     * @throws StringIndexOutOfBoundsException if {@code index} is an invalid position.
     */
    public @NotNull FreeStringBuilder setCharAt(final int index, final char ch) {
        sb.setCharAt(index, ch);
        return this;
    }

    @Override
    public @NotNull CharSequence subSequence(final int start, final int end) {
        return sb.subSequence(start, end);
    }

    /**
     * Returns the result {@code String}.
     *
     * @return the result {@code String}.
     */
    public @NotNull String toS() {
        return sb.toString();
    }

    /**
     * Returns the result {@code String}. Same as {@code toS()} method.
     *
     * @return the result {@code String}.
     */
    @Override
    public @NotNull String toString() {
        return sb.toString();
    }

    // define these methods so that this class does not depend on CollectionUtil.

    private static boolean notEmpty(@Nullable final boolean @Nullable [] a) {
        return a != null && a.length > 0;
    }

    private static boolean notEmpty(@Nullable final byte @Nullable [] a) {
        return a != null && a.length > 0;
    }

    private static boolean notEmpty(@Nullable final char @Nullable [] a) {
        return a != null && a.length > 0;
    }

    private static boolean notEmpty(@Nullable final double @Nullable [] a) {
        return a != null && a.length > 0;
    }

    private static boolean notEmpty(@Nullable final float @Nullable [] a) {
        return a != null && a.length > 0;
    }

    private static boolean notEmpty(@Nullable final int @Nullable [] a) {
        return a != null && a.length > 0;
    }

    private static boolean notEmpty(@Nullable final long @Nullable [] a) {
        return a != null && a.length > 0;
    }

    private static boolean notEmpty(@Nullable final Object @Nullable [] a) {
        return a != null && a.length > 0;
    }

    private static boolean notEmpty(@Nullable final Collection<?> collection) {
        return collection != null && !collection.isEmpty();
    }

    private static boolean notEmpty(@Nullable final Map<?, ?> map) {
        return map != null && !map.isEmpty();
    }

}
