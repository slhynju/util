package name.slhynju.util;

import name.slhynju.UnexpectedApplicationFlowException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Collection;
import java.util.Date;
import java.util.Deque;
import java.util.Map;
import java.util.function.Function;

import static name.slhynju.util.FreeStringBuilder.*;
import static name.slhynju.util.StringUtil.NULL;

/**
 * Utility class to support {@code toString()} method in bean classes. This class is not thread-safe.
 * <p>This class shall only depend on other utility classes in same package (subpackages excluded).</p>
 *
 * @author slhynju
 */
public final class BeanStringBuilder implements Serializable, CharSequence {

    private static final long serialVersionUID = -7420097406605565120L;

    private static final char BEAN_OPENING_MARK = '{';

    private static final char BEAN_CLOSING_MARK = '}';

    @NotNull
    private static final String PROPERTY_SEPARATOR = ", ";

    @NotNull
    private static final String PROPERTY_KEY_VALUE_SEPARATOR = ": ";

    @NotNull
    private final FreeStringBuilder sb;

    @NotNull
    private STATE state;

    /**
     * Initializes a builder with bean class name.
     *
     * @param beanClass a bean class. It shall not be {@code null}.
     * @throws NullPointerException if {@code beanClass} is {@code null}.
     */
    public BeanStringBuilder(@NotNull final Class<?> beanClass) {
        sb = new FreeStringBuilder();
        sb.append(beanClass.getName()).append(BEAN_OPENING_MARK);
        state = STATE.STARTED;
    }

    // primitive types

    // append(String, byte) is covered by append(String, int)

    /**
     * Appends a {@code Byte} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code Byte} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final Byte value) {
        return value == null ? appendNullProperty(name) : append(name, value.intValue());
    }

    /**
     * Appends a {@code byte array} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code byte array} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final byte @Nullable [] value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
        return this;
    }

    /**
     * Appends an {@code int} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value an {@code int} property value.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, final int value) {
        appendPropertyName(name);
        sb.append(value);
        return this;
    }

    /**
     * Appends an {@code Integer} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value an {@code Integer} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final Integer value) {
        return value == null ? appendNullProperty(name) : append(name, value.intValue());
    }

    /**
     * Appends an {@code int array} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value an {@code int array} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final int @Nullable [] value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
        return this;
    }

    /**
     * Appends a {@code long} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code long} property value.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, final long value) {
        appendPropertyName(name);
        sb.append(value);
        return this;
    }

    /**
     * Appends a {@code long} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code long} property value.
     * @param format a number format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, final long value, @NotNull final NumberFormat format) {
        appendPropertyName(name);
        sb.append(value, format);
        return this;
    }

    /**
     * Appends a {@code Long} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code Long} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final Long value) {
        return value == null ? appendNullProperty(name) : append(name, value.longValue());
    }

    /**
     * Appends a {@code Long} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code Long} property value. It can be {@code null}.
     * @param format a number format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final Long value, @NotNull final NumberFormat format) {
        return value == null ? appendNullProperty(name) : append(name, value.longValue(), format);
    }

    /**
     * Appends a {@code long array} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code long array} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final long @Nullable [] value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
        return this;
    }

    /**
     * Appends a {@code long array} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code long array} property value. It can be {@code null}.
     * @param format a number format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final long @Nullable [] value, @NotNull final NumberFormat format) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK, format);
        return this;
    }

    // append(String, float, NumberFormat) is covered by append(String, double, NumberFormat)

    /**
     * Appends a {@code Float} property value with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code Float} property value. It can be {@code null}.
     * @param format a number format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final Float value, @NotNull final NumberFormat format) {
        return value == null ? appendNullProperty(name) : append(name, value.doubleValue(), format);
    }

    /**
     * Appends a {@code float array} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code float array} property value. It can be {@code null}.
     * @param format a number format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final float @Nullable [] value, @NotNull final NumberFormat format) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK, format);
        return this;
    }

    /**
     * Appends a {@code double} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code double} property value.
     * @param format a number format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, final double value, @NotNull final NumberFormat format) {
        appendPropertyName(name);
        sb.append(value, format);
        return this;
    }

    /**
     * Appends a {@code Double} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code Double} property value. It can be {@code null}.
     * @param format a number format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final Double value, @NotNull final NumberFormat format) {
        return value == null ? appendNullProperty(name) : append(name, value.doubleValue(), format);
    }

    /**
     * Appends a {@code double array} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code double array} property value. It can be {@code null}.
     * @param format a number format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final double @Nullable [] value, @NotNull final NumberFormat format) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK, format);
        return this;
    }

    /**
     * Appends a {@code boolean} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code boolean} property value.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, final boolean value) {
        appendPropertyName(name);
        sb.append(value);
        return this;
    }

    /**
     * Appends a {@code Boolean} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code Boolean} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final Boolean value) {
        return value == null ? appendNullProperty(name) : append(name, value.booleanValue());
    }

    /**
     * Appends a {@code boolean array} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code boolean array} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final boolean @Nullable [] value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
        return this;
    }

    /**
     * Appends a {@code char} property with single quotes.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code char property} value.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder appendQuoted(@NotNull final String name, final char value) {
        appendPropertyName(name);
        sb.appendQuoted(value);
        return this;
    }

    /**
     * Appends a {@code Character} property with single quotes.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code Character} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder appendQuoted(@NotNull final String name, @Nullable final Character value) {
        return value == null ? appendNullProperty(name) : appendQuoted(name, value.charValue());
    }

    /**
     * Appends a {@code char array} property with single quotes.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code char array} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder appendQuoted(@NotNull final String name, @Nullable final char @Nullable [] value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.appendQuoted(value, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
        return this;
    }

    // common object types

    /**
     * Appends a {@code BigInteger} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code BigInteger} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder append(@NotNull final String name, @Nullable final BigInteger value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value);
        return this;
    }

    /**
     * Appends a {@code String} property with double quotes.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code String} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder appendQuoted(@NotNull final String name, @Nullable final String value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.appendQuoted(value);
        return this;
    }

    /**
     * Appends a {@code CharSequence} property with double quotes.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code CharSequence} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    @NotNull
    public BeanStringBuilder appendQuoted(@NotNull final String name, @Nullable final CharSequence value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.appendQuoted(value);
        return this;
    }

    /**
     * Appends an {@code Enum} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value an {@code Enum} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final Enum<?> value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value);
        return this;
    }

    /**
     * Appends a {@code Class} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code String} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final Class<?> value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value.getName());
        return this;
    }

    /**
     * Appends a general {@code Object} property.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value an {@code Object} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final Object value) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value);
        return this;
    }

    // date and temporal types

    /**
     * Appends a {@code Date} property with format of {@code yyyy-MM-dd}.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code Date} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final Date value) {
        return append(name, value, DateUtil.SIMPLE_FORMAT);
    }

    /**
     * Appends a {@code Date} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code Date} property value. It can be {@code null}.
     * @param format a date format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final Date value, @NotNull final String format) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, format);
        return this;
    }

    /**
     * Appends a {@code Date} property with format.
     *
     * @param name   a property name. It shall not be {@code null}.
     * @param value  a {@code Date} property value. It can be {@code null}.
     * @param format a date format. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    public @NotNull BeanStringBuilder append(
            @NotNull final String name, @Nullable final Date value, @NotNull final DateFormat format) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, format);
        return this;
    }

    /**
     * Appends a Temporal property with formatter.
     *
     * @param name      a property name. It shall not be {@code null}.
     * @param value     a Temporal property value. It can be {@code null}.
     * @param formatter a Temporal formatter. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code formatter} is {@code null}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final TemporalAccessor value, @NotNull final DateTimeFormatter formatter) {
        if (value == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(value, formatter);
        return this;
    }

    /**
     * Appends a {@code LocalDate} property with standard formatter.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code LocalDate} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @see DateTimeFormatter#ISO_LOCAL_DATE
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final LocalDate value) {
        return append(name, value, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    /**
     * Appends a {@code LocalDateTime} property with standard formatter.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code LocalDateTime} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @see DateTimeFormatter#ISO_LOCAL_DATE_TIME
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final LocalDateTime value) {
        return append(name, value, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    /**
     * Appends a {@code LocalTime} property with standard formatter.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code LocalTime} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @see DateTimeFormatter#ISO_LOCAL_TIME
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final LocalTime value) {
        return append(name, value, DateTimeFormatter.ISO_LOCAL_TIME);
    }

    /**
     * Appends a {@code ZonedDateTime} property with standard formatter.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param value a {@code ZonedDateTime} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     * @see DateTimeFormatter#ISO_ZONED_DATE_TIME
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final ZonedDateTime value) {
        return append(name, value, DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }

    // collection types

    /**
     * Appends a {@code Collection} property.
     *
     * @param name       a property name. It shall not be {@code null}.
     * @param collection a {@code Collection} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final Collection<?> collection) {
        if (collection == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(collection, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
        return this;
    }

    /**
     * Appends a {@code Collection} property with custom element formatter.
     *
     * @param name             a property name. It shall not be {@code null}.
     * @param collection       a {@code Collection} property value. It can be {@code null}.
     * @param elementFormatter a custom formatter to format a {@code Collection} element into a {@code String}. It shall support the case of {@code null} element.
     * @param <T>              the {@code Collection} element type.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code elementFormatter} is {@code null}.
     */
    public @NotNull <T> BeanStringBuilder append(@NotNull final String name, @Nullable final Collection<T> collection, @NotNull final Function<? super T, String> elementFormatter) {
        if (collection == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(collection, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK, elementFormatter);
        return this;
    }

    /**
     * Appends an {@code Object array} property.
     *
     * @param name    a property name. It shall not be {@code null}.
     * @param objects an {@code Object array} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final Object @Nullable [] objects) {
        if (objects == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(objects, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
        return this;
    }

    /**
     * Appends an {@code Object array} property with custom element formatter.
     *
     * @param name             a property name. It shall not be {@code null}.
     * @param objects          an {@code Object array} property value. It can be {@code null}.
     * @param elementFormatter a custom formatter to format an array element into a {@code String}. It shall support the case of {@code null} element.
     * @param <T>              the {@code Object array} element type.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code elementFormatter} is {@code null}.
     */
    public @NotNull <T> BeanStringBuilder append(@NotNull final String name, @Nullable final T @Nullable [] objects, @NotNull final Function<? super T, String> elementFormatter) {
        if (objects == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(objects, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK, elementFormatter);
        return this;
    }

    /**
     * Appends a {@code Map} property.
     *
     * @param name a property name. It shall not be {@code null}.
     * @param map  a {@code Map} property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder append(@NotNull final String name, @Nullable final Map<?, ?> map) {
        if (map == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(map, MAP_OPENING_MARK, MAP_KEY_VALUE_SEPARATOR, MAP_ENTRY_SEPARATOR, MAP_CLOSING_MARK);
        return this;
    }

    /**
     * Appends a {@code Map} property with custom key and value formatters.
     *
     * @param name           a property name. It shall not be {@code null}.
     * @param map            a {@code Map} property value. It can be {@code null}.
     * @param keyFormatter   a custom formatter to format a {@code Map} key into a {@code String}. It shall support the case of {@code null} key.
     * @param valueFormatter a custom formatter to format a {@code Map} value into a {@code String}. It shall support the case of {@code null} value.
     * @param <K>            the {@code Map} key type.
     * @param <V>            the {@code Map} value type.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code keyFormatter} or {@code valueFormatter} is {@code null}.
     */
    public @NotNull <K, V> BeanStringBuilder append(@NotNull final String name, @Nullable final Map<K, V> map, @NotNull final Function<? super K, String> keyFormatter, @NotNull final Function<? super V, String> valueFormatter) {
        if (map == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.append(map, MAP_OPENING_MARK, MAP_KEY_VALUE_SEPARATOR, MAP_ENTRY_SEPARATOR, MAP_CLOSING_MARK, keyFormatter, valueFormatter);
        return this;
    }

    /**
     * Appends a stack property in reversed order.
     *
     * @param name  a property name. It shall not be {@code null}.
     * @param stack a stack property value. It can be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder appendStack(@NotNull final String name, @Nullable final Deque<?> stack) {
        if (stack == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.appendStack(stack, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK);
        return this;
    }

    /**
     * Appends a stack property with custom element formatter in reversed order.
     *
     * @param name             a property name. It shall not be {@code null}.
     * @param stack            a stack property value. It can be {@code null}.
     * @param elementFormatter a custom formatter to format a stack element into a {@code String}. It shall support the case of {@code null} element.
     * @param <T>              the stack element type.
     * @return this {@code BeanStringBuilder}.
     * @throws NullPointerException if {@code elementFormatter} is {@code null}.
     */
    public @NotNull <T> BeanStringBuilder appendStack(@NotNull final String name, @Nullable final Deque<T> stack, @NotNull final Function<? super T, String> elementFormatter) {
        if (stack == null) {
            return appendNullProperty(name);
        }
        appendPropertyName(name);
        sb.appendStack(stack, LIST_OPENING_MARK, LIST_ELEMENT_SEPARATOR, LIST_CLOSING_MARK, elementFormatter);
        return this;
    }

    // null value

    /**
     * Appends a property with {@code null} value.
     *
     * @param name a property name. It shall not be {@code null}.
     * @return this {@code BeanStringBuilder}.
     */
    public @NotNull BeanStringBuilder appendNullProperty(@NotNull final String name) {
        appendPropertyName(name);
        sb.append(NULL);
        return this;
    }

    // other methods

    /**
     * Closes this builder and returns the result.
     *
     * @return the result {@code String}.
     */
    public @NotNull String toS() {
        if (state != STATE.CLOSED) {
            sb.append(BEAN_CLOSING_MARK);
            state = STATE.CLOSED;
        }
        return sb.toS();
    }

    /**
     * Closes this builder and returns the result. Same as {@code toS()} method.
     *
     * @return the result {@code String}.
     */
    @Override
    public @NotNull String toString() {
        return toS();
    }

    @Override
    public int length() {
        return sb.length();
    }

    @Override
    public char charAt(final int index) {
        return sb.charAt(index);
    }

    @Override
    public @NotNull CharSequence subSequence(final int start, final int end) {
        return sb.subSequence(start, end);
    }

    /**
     * Appends a new property name.
     *
     * @param name a property name. It shall not be {@code null}.
     * @throws UnexpectedApplicationFlowException if {@code toS()} or {@code toString()} method is already invoked.
     */
    private void appendPropertyName(@NotNull final String name) {
        switch (state) {
            case STARTED:
                state = STATE.PROPERTY_APPENDED;
                break;
            case PROPERTY_APPENDED:
                sb.append(PROPERTY_SEPARATOR);
                break;
            default:
                throw new UnexpectedApplicationFlowException("BeanStringBuilder is already closed.");
        }
        sb.append(name).append(PROPERTY_KEY_VALUE_SEPARATOR);
    }

    /**
     * Internal state control.
     */
    private enum STATE {
        STARTED, PROPERTY_APPENDED, CLOSED
    }

}
