/**
 * Directed graph classes.
 *
 * @author slhynju
 */
package name.slhynju.util.collection.graph.directed;