/**
 * Undirected graph classes.
 *
 * @author slhynju
 */
package name.slhynju.util.collection.graph.undirected;