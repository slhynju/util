package name.slhynju.util.collection.tree.walker;

import name.slhynju.util.collection.tree.Tree;
import name.slhynju.util.collection.tree.TreeNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

/**
 * Default implementation of {@code TreeWalker}.
 *
 * @param <N> the tree node type.
 * @author slhynju
 */
@SuppressWarnings({"rawtypes", "FrequentlyUsedInheritorInspection"})
public abstract class TreeWalkerSupport<N extends TreeNode> implements TreeWalker<N> {

    @Override
    public final void walk(@Nullable Tree<N> tree, @NotNull Consumer<? super N> action) {
        if (tree == null) {
            return;
        }
        N next = getStartNode(tree);
        while (next != null) {
            process(next, action);
            next = getNextNode(next);
        }
    }

    /**
     * Returns the first node to process.
     *
     * @param tree a tree. It shall not be {@code null}.
     * @return the first node to process.
     */
    protected abstract @Nullable @Nullable N getStartNode(@NotNull Tree<N> tree);

    /**
     * Returns the next node to process.
     *
     * @param node a visited node. It shall not be {@code null}.
     * @return the next node to process.
     */
    protected abstract @Nullable @Nullable N getNextNode(@NotNull N node);

    /**
     * Process the current node.
     *
     * @param node   the current node to process. It shall not be {@code null}.
     * @param action a custom action. It shall not be {@code null}.
     * @throws NullPointerException if {@code action} is {@code null}.
     */
    protected void process(@NotNull N node, @NotNull Consumer<? super N> action) {
        action.accept(node);
    }
}
