package name.slhynju.util.collection.tree;

import name.slhynju.util.EqualsUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@code BinaryTreeNode}.
 *
 * @param <T> the node type that extends this type.
 * @author slhynju
 */
@SuppressWarnings({"rawtypes", "FrequentlyUsedInheritorInspection", "EqualsAndHashcode"})
public class BinaryTreeNodeSupport<T extends BinaryTreeNodeSupport> extends TreeNodeSupport<T>
        implements BinaryTreeNode<T> {

    /**
     * The left child.
     */
    protected @Nullable T leftChild = null;

    /**
     * The right child.
     */
    protected @Nullable T rightChild = null;

    /**
     * Creates a new binary tree node.
     *
     * @param name a node name. It shall not be {@code null}.
     */
    public BinaryTreeNodeSupport(@NotNull String name) {
        super(name);
    }

    @Override
    public final @Nullable T getLeftChild() {
        return leftChild;
    }

    @Override
    public final void setLeftChild(@Nullable T leftChild) {
        this.leftChild = leftChild;
    }

    @Override
    public final @Nullable T getRightChild() {
        return rightChild;
    }

    @Override
    public final void setRightChild(@Nullable T rightChild) {
        this.rightChild = rightChild;
    }

    @Override
    public final @NotNull List<T> getChildren() {
        List<T> children = new ArrayList<>(2);
        if (leftChild != null) {
            children.add(leftChild);
        }
        if (rightChild != null) {
            children.add(rightChild);
        }
        return children;
    }

    @SuppressWarnings({"SimplifiableIfStatement", "ObjectEquality", "VariableNotUsedInsideIf"})
    @Override
    public int indexOf(@Nullable T child) {
        if (child == null) {
            return -1;
        }
        if (leftChild == child) {
            return 0;
        }
        if (rightChild == child) {
            return leftChild == null ? 0 : 1;
        }
        return -1;
    }

    @Override
    public final boolean isLeaf() {
        return leftChild == null && rightChild == null;
    }

    @Override
    public final boolean hasChildren() {
        return leftChild != null || rightChild != null;
    }

    @Override
    public final @Nullable T getFirstChild() {
        return leftChild == null ? rightChild : leftChild;
    }

    @Override
    public final @Nullable T getLastChild() {
        return rightChild == null ? leftChild : rightChild;
    }

    @SuppressWarnings("ObjectEquality")
    @Override
    public final boolean isLeftChild() {
        return parent != null && this == parent.getLeftChild();
    }

    @SuppressWarnings("ObjectEquality")
    @Override
    public final boolean isRightChild() {
        return parent != null && this == parent.getRightChild();
    }

    @SuppressWarnings("ObjectEquality")
    @Override
    public @Nullable T getPrevSibling() {
        if (parent == null) {
            return null;
        }
        T parentLeftChild = (T) parent.getLeftChild();
        return this == parentLeftChild ? null : parentLeftChild;
    }

    @SuppressWarnings("ObjectEquality")
    @Override
    public @Nullable T getNextSibling() {
        if (parent == null) {
            return null;
        }
        T parentRightChild = (T) parent.getRightChild();
        return this == parentRightChild ? null : parentRightChild;
    }

    @SuppressWarnings({"rawtypes", "CanBeFinal"})
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BinaryTreeNodeSupport) {
            BinaryTreeNodeSupport other = (BinaryTreeNodeSupport) obj;
            return EqualsUtil.isEquals(name, other.name);
        }
        return false;
    }
}
