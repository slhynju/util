package name.slhynju.util.collection.graph.undirected;

import name.slhynju.util.HashCodeBuilder;
import name.slhynju.util.collection.graph.GraphEdgeSupport;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@code UndirectedGraphEdge}.
 *
 * @param <N> the node type.
 * @param <E> the edge type that extends this type.
 * @author slhynju
 */
@SuppressWarnings("CanBeFinal")
public class UndirectedGraphEdgeSupport<N extends UndirectedGraphNode<N, E>, E extends UndirectedGraphEdgeSupport<N, E>>
        extends GraphEdgeSupport implements UndirectedGraphEdge<N, E> {

    /**
     * One end of this edge.
     */
    protected final @NotNull N end1;

    /**
     * The other end of this edge.
     */
    protected final @NotNull N end2;

    /**
     * Creates a new edge with two nodes.
     *
     * @param end1 one node. It shall not be {@code null}.
     * @param end2 another node. It shall not be {@code null}.
     */
    public UndirectedGraphEdgeSupport(@NotNull N end1, @NotNull N end2) {
        this.end1 = end1;
        this.end2 = end2;
    }

    @SuppressWarnings("ObjectEquality")
    @Override
    public @NotNull List<N> getNodes() {
        List<N> list = new ArrayList<>(2);
        list.add(end1);
        if (end1 != end2) {
            list.add(end2);
        }
        return list;
    }

    @SuppressWarnings({"rawtypes", "ObjectEquality"})
    @Override
    public @NotNull UndirectedGraphNode @NotNull [] getNodeArray() {
        return end1 == end2 ? new UndirectedGraphNode[]{end1} : new UndirectedGraphNode[]{end1, end2};
    }

    @SuppressWarnings("ObjectEquality")
    @Override
    public @NotNull N getOtherEnd(@NotNull N oneEnd) {
        if (end1 == oneEnd) {
            return end2;
        }
        if (end2 == oneEnd) {
            return end1;
        }
        @NonNls
        String message = oneEnd + " is not an end of edge " + this + '.';
        throw new IllegalArgumentException(message);
    }

    @SuppressWarnings("ObjectEquality")
    @Override
    public boolean containsEnd(@Nullable N oneEnd) {
        return end1 == oneEnd || end2 == oneEnd;
    }

    @SuppressWarnings({"OverlyComplexBooleanExpression", "ObjectEquality", "ParameterHidesMemberVariable"})
    @Override
    public final boolean containsEnds(@Nullable N end1, @Nullable N end2) {
        return (this.end1 == end1 && this.end2 == end2) || (this.end2 == end1 && this.end1 == end2);
    }

    @SuppressWarnings("CanBeFinal")
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof UndirectedGraphEdgeSupport) {
            UndirectedGraphEdgeSupport<N, E> other = (UndirectedGraphEdgeSupport<N, E>) obj;
            return containsEnds(other.end1, other.end2);
        }
        return false;
    }

    @SuppressWarnings({"ObjectEquality", "CanBeFinal"})
    @Override
    public int hashCode() {
        int hash1 = new HashCodeBuilder().append(end1).append(end2).toValue();
        if (end1 == end2) {
            return hash1;
        }
        int hash2 = new HashCodeBuilder().append(end2).append(end1).toValue();
        return Math.min(hash1, hash2);
    }

    @SuppressWarnings("CanBeFinal")
    @NonNls
    @Override
    public @NotNull String toString() {
        return end1 + " -- " + end2;
    }

}
