package name.slhynju.util.collection;

import org.jetbrains.annotations.Nullable;

/**
 * A {@code Predicate} that checks both index and value.
 *
 * @param <T> the {@code array} or {@code List} element type.
 * @author slhynju
 */
@FunctionalInterface
public interface IndexPredicate<T> {

    /**
     * Checks a pair of index and element value.
     *
     * @param index the index of the element.
     * @param value the element value. It can be {@code null}.
     * @return {@code true} -- matches; {@code false} -- does not match.
     */
    @SuppressWarnings("BooleanMethodNameMustStartWithQuestion")
    boolean test(int index, @Nullable T value);

}
