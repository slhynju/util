package name.slhynju.util.collection.graph.directed;

import name.slhynju.util.collection.graph.GraphNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * A node of directed graph.
 *
 * @param <N> the node type that extends this type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings({"CyclicClassDependency", "FrequentlyUsedInheritorInspection"})
public interface DirectedGraphNode<N extends DirectedGraphNode<N, E>, E extends DirectedGraphEdge<N, E>>
        extends GraphNode {

    /**
     * Returns all the incoming edges.
     *
     * @return a {@code List} of incoming edges.
     */
    @NotNull List<E> getInEdges();

    /**
     * Adds an incoming edge.
     *
     * @param edge an incoming edge. It shall not be {@code null}.
     */
    void addInEdge(@NotNull E edge);

    /**
     * Removes an incoming edge.
     *
     * @param edge an incoming edge. It can be {@code null}.
     */
    void removeInEdge(@Nullable E edge);

    /**
     * Returns all the direct predecessor nodes.
     *
     * @return a {@code List} of direct predecessor nodes.
     */
    @NotNull List<N> getDirectPredecessors();

    /**
     * Returns the direct predecessor node by name.
     *
     * @param nodeName a node name. It shall not be {@code null}.
     * @return the direct predecessor node. Returns null if not found.
     */
    @Nullable @Nullable N getDirectPredecessor(@NotNull String nodeName);

    /**
     * Returns the in-degree.
     *
     * @return the in-degree.
     */
    int getInDegree();

    /**
     * Returns all the outgoing edges.
     *
     * @return a {@code List} of outgoing edges.
     */
    @NotNull List<E> getOutEdges();

    /**
     * Adds a outgoing edge.
     *
     * @param edge a outgoing edge. It shall not be {@code null}.
     */
    void addOutEdge(@NotNull E edge);

    /**
     * Removes a outgoing edge.
     *
     * @param edge a outgoing edge. It can be {@code null}.
     */
    void removeOutEdge(@Nullable E edge);

    /**
     * Returns all the direct successor nodes.
     *
     * @return a {@code List} of direct successor nodes.
     */
    @NotNull List<N> getDirectSuccessors();

    /**
     * Returns the direct successor node by name.
     *
     * @param nodeName a node name. It shall not be {@code null}.
     * @return the direct successor node. Returns null if not found.
     */
    @Nullable @Nullable N getDirectSuccessor(@NotNull String nodeName);

    /**
     * Returns the out-degree.
     *
     * @return the out-degree.
     */
    int getOutDegree();

}
