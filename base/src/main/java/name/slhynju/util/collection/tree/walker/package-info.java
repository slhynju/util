/**
 * Different strategies to walk a Tree.
 *
 * @author slhynju
 */
package name.slhynju.util.collection.tree.walker;