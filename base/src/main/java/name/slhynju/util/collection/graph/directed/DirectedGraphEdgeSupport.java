package name.slhynju.util.collection.graph.directed;

import name.slhynju.util.HashCodeBuilder;
import name.slhynju.util.collection.graph.GraphEdgeSupport;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Default implementation of {@code DirectedGraphEdge}.
 *
 * @param <N> the node type.
 * @param <E> the edge type that extends this type.
 * @author slhynju
 */
@SuppressWarnings("CanBeFinal")
public class DirectedGraphEdgeSupport<N extends DirectedGraphNode<N, E>, E extends DirectedGraphEdgeSupport<N, E>>
        extends GraphEdgeSupport implements DirectedGraphEdge<N, E> {

    /**
     * The tail node.
     */
    protected final @NotNull N tail;

    /**
     * The head node.
     */
    protected final @NotNull N head;

    /**
     * Creates a new directed edge with tail and head.
     *
     * @param tail a tail node. It shall not be {@code null}.
     * @param head a head node. It shall not be {@code null}.
     */
    public DirectedGraphEdgeSupport(@NotNull N tail, @NotNull N head) {
        this.tail = tail;
        this.head = head;
    }

    @Override
    public final @NotNull N getHead() {
        return head;
    }

    @Override
    public final @NotNull N getTail() {
        return tail;
    }

    @SuppressWarnings({"rawtypes", "ObjectEquality", "CanBeFinal"})
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof DirectedGraphEdgeSupport) {
            DirectedGraphEdgeSupport other = (DirectedGraphEdgeSupport) obj;
            return tail == other.tail && head == other.head;
        }
        return false;
    }

    @SuppressWarnings("CanBeFinal")
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(tail).append(head).toValue();
    }

    @SuppressWarnings("CanBeFinal")
    @NonNls
    @Override
    public @NotNull String toString() {
        return tail + " -> " + head;
    }

}
