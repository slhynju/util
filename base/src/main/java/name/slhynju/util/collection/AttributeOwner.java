package name.slhynju.util.collection;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A general container class to store all kinds of properties.
 * <p>Any implementation class shall overwrite 4 methods:</p>
 * <ul>
 *     <li>{@code boolean containsAttribute(String key)}</li>
 *     <li>{@code Object getAttribute(String key)}</li>
 *     <li>{@code void setAttribute(String key, Object value)}</li>
 *     <li>{@code void removeAttribute(String key)}</li>
 * </ul>
 *
 * @author slhynju
 */
public interface AttributeOwner {

    /**
     * Checks whether it contains a property.
     *
     * @param key a property key. It shall not be {@code null}.
     * @return {@code true} -- contains the property; {@code false} -- does not contain the property.
     */
    boolean containsAttribute(@NotNull String key);

    /**
     * Returns a property value.
     *
     * @param key a property key. It shall not be {@code null}.
     * @return the property value.
     */
    @Nullable @Nullable Object getAttribute(@NotNull String key);

    /**
     * Sets a property value.
     *
     * @param key   a property key. It shall not be {@code null}.
     * @param value the property value. It can be {@code null}.
     */
    void setAttribute(@NotNull String key, @Nullable Object value);

    /**
     * Removes a property.
     *
     * @param key a property key. It shall not be {@code null}.
     */
    void removeAttribute(@NotNull String key);

    // default methods

    /**
     * Returns a property value.
     *
     * @param key    a property key. It shall not be {@code null}.
     * @param aClass a property type. It shall not be {@code null}.
     * @param <T>    the property type.
     * @return the property value.
     */
    default @Nullable <T> T getAttribute(@NotNull String key, @NotNull Class<T> aClass) {
        return (T) getAttribute(key);
    }

    /**
     * Returns a property value.
     *
     * @param key          a property key. It shall not be {@code null}.
     * @param defaultValue the default value to use if not found. It shall not be {@code null}.
     * @return the property value.
     */
    default @NotNull Object getAttribute(@NotNull String key, @NotNull Object defaultValue) {
        Object v = getAttribute(key);
        return v == null ? defaultValue : v;
    }

    /**
     * Returns a property value.
     *
     * @param key          a property key. It shall not be {@code null}.
     * @param aClass       a property type. It shall not be {@code null}.
     * @param defaultValue the default value to use if not found. It shall not be {@code null}.
     * @param <T>          the property type.
     * @return the property value.
     */
    default @NotNull <T> T getAttribute(@NotNull String key, @NotNull Class<T> aClass, @NotNull T defaultValue) {
        Object v = getAttribute(key);
        return v == null ? defaultValue : (T) v;
    }

    /**
     * Returns a {@code boolean} property value.
     *
     * @param key          a {@code boolean} property key. It shall not be {@code null}.
     * @param defaultValue the default value to use if not found.
     * @return the {@code boolean} property value.
     */
    @SuppressWarnings("BooleanMethodNameMustStartWithQuestion")
    default boolean getBooleanAttribute(@NotNull String key, boolean defaultValue) {
        Boolean o = (Boolean) getAttribute(key);
        return o == null ? defaultValue : o.booleanValue();
    }

    /**
     * Sets a {@code boolean} property value.
     *
     * @param key   a {@code boolean} property key. It shall not be {@code null}.
     * @param value the {@code boolean} property value.
     */
    default void setBooleanAttribute(@NotNull String key, boolean value) {
        setAttribute(key, Boolean.valueOf(value));
    }

    /**
     * Returns an {@code int} property value.
     *
     * @param key          an {@code int} property key. It shall not be {@code null}.
     * @param defaultValue the default value to use if not found.
     * @return the {@code int} property value.
     */
    default int getIntAttribute(@NotNull String key, int defaultValue) {
        Integer o = (Integer) getAttribute(key);
        return o == null ? defaultValue : o.intValue();
    }

    /**
     * Sets an {@code int} property value.
     *
     * @param key   a property key. It shall not be {@code null}.
     * @param value the {@code int} property value.
     */
    default void setIntAttribute(@NotNull String key, int value) {
        setAttribute(key, Integer.valueOf(value));
    }

    /**
     * Returns a {@code long} property value.
     *
     * @param key          a {@code long} property key. It shall not be {@code null}.
     * @param defaultValue the default value to use if not found.
     * @return the {@code long} property value.
     */
    default long getLongAttribute(@NotNull String key, long defaultValue) {
        Long o = (Long) getAttribute(key);
        return o == null ? defaultValue : o.longValue();
    }

    /**
     * Sets a {@code long} property value.
     *
     * @param key   a {@code long} property key. It shall not be {@code null}.
     * @param value the {@code long} property value.
     */
    default void setLongAttribute(@NotNull String key, long value) {
        setAttribute(key, Long.valueOf(value));
    }

    /**
     * Returns a {@code double} property value.
     *
     * @param key          a {@code double} property key. It shall not be {@code null}.
     * @param defaultValue the default value to use if not found.
     * @return the {@code double} property value.
     */
    default double getDoubleAttribute(@NotNull String key, double defaultValue) {
        Double d = (Double) getAttribute(key);
        return d == null ? defaultValue : d.doubleValue();
    }

    /**
     * Sets a {@code double} property value.
     *
     * @param key   a {@code double} property key. It shall not be {@code null}.
     * @param value the {@code double} property value.
     */
    default void setDoubleAttribute(@NotNull String key, double value) {
        setAttribute(key, Double.valueOf(value));
    }

    /**
     * Returns a {@code String} property value.
     *
     * @param key a {@code String} property key. It shall not be {@code null}.
     * @return the {@code String} property value.
     */
    default @Nullable String getStringAttribute(@NotNull String key) {
        return (String) getAttribute(key);
    }

    /**
     * Returns a {@code String} property value.
     *
     * @param key          a {@code String} property key. It shall not be {@code null}.
     * @param defaultValue the default value to use if not found.
     * @return the {@code String} property value.
     */
    default @NotNull String getStringAttribute(@NotNull String key, @NotNull String defaultValue) {
        return (String) getAttribute(key, defaultValue);
    }

    /**
     * Sets a {@code String} property value.
     *
     * @param key   a {@code String} property key. It shall not be {@code null}.
     * @param value the {@code String} property value. It can be {@code null}.
     */
    default void setStringAttribute(@NotNull String key, @Nullable String value) {
        setAttribute(key, value);
    }
}
