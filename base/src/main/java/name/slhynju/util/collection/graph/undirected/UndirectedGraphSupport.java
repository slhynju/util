package name.slhynju.util.collection.graph.undirected;

import name.slhynju.util.collection.graph.GraphSupport;
import org.jetbrains.annotations.Nullable;

/**
 * Default implementation of {@code UndirectedGraph}.
 *
 * @param <N> the node type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings("CanBeFinal")
public class UndirectedGraphSupport<N extends UndirectedGraphNode<N, E>, E extends UndirectedGraphEdge<N, E>>
        extends GraphSupport<N, E> implements UndirectedGraph<N, E> {

    @Override
    public void removeNode(@Nullable N node) {
        if (node == null) {
            return;
        }
        super.removeNode(node);
        for (E edge : node.getEdges()) {
            N otherEnd = edge.getOtherEnd(node);
            // this check is necessary to avoid ConcurrentModificationException.
            //noinspection ObjectEquality
            if (otherEnd != node) {
                otherEnd.removeEdge(edge);
            }
            removeEdge(edge);
        }
    }
}
