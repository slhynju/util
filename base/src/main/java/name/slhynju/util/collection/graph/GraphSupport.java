package name.slhynju.util.collection.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Function;

/**
 * Default implementation of {@code Graph}.
 *
 * @param <N> the node type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings({"FrequentlyUsedInheritorInspection", "CanBeFinal"})
public class GraphSupport<N extends GraphNode, E extends GraphEdge> implements Graph<N, E> {

    /**
     * The nodes. The keys are node names and the values are nodes.
     */
    @SuppressWarnings("CollectionWithoutInitialCapacity")
    protected final @NotNull Map<String, N> nodes = new HashMap<>();

    /**
     * The edges.
     */
    @SuppressWarnings("CollectionWithoutInitialCapacity")
    protected final @NotNull Set<E> edges = new HashSet<>();

    @Override
    public @Nullable N getNode(@NotNull String name) {
        return nodes.get(name);
    }

    @Override
    public boolean containsNode(@NotNull String name) {
        return nodes.containsKey(name);
    }

    @Override
    public @NotNull Collection<N> getNodes() {
        return Collections.unmodifiableCollection(nodes.values());
    }

    @Override
    public void addNode(@NotNull N node) {
        nodes.put(node.getName(), node);
    }

    @Override
    @SuppressWarnings({"BoundedWildcard", "ReuseOfLocalVariable"})
    public @NotNull N addNodeIfAbsent(@NotNull String name, @NotNull Function<String, ? extends N> nodeCreator) {
        N node = nodes.get(name);
        if (node != null) {
            return node;
        }
        node = nodeCreator.apply(name);
        nodes.put(name, node);
        //noinspection ConstantConditions
        return node;
    }

    @Override
    public void removeNode(@Nullable N node) {
        if (node == null) {
            return;
        }
        nodes.remove(node.getName());
    }

    @Override
    public @NotNull Set<E> getEdges() {
        return Collections.unmodifiableSet(edges);
    }

    @Override
    public void addEdge(@NotNull E edge) {
        edges.add(edge);
    }

    @Override
    public void removeEdge(@Nullable E edge) {
        if (edge == null) {
            return;
        }
        edges.remove(edge);
    }

}
