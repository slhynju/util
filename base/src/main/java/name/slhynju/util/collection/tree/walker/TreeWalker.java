package name.slhynju.util.collection.tree.walker;

import name.slhynju.util.collection.tree.Tree;
import name.slhynju.util.collection.tree.TreeNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

/**
 * A strategy to walk a tree.
 *
 * @param <N> the tree node type.
 * @author slhynju
 */
@SuppressWarnings({"InterfaceMayBeAnnotatedFunctional", "rawtypes"})
public interface TreeWalker<N extends TreeNode> {

    /**
     * Walk a tree.
     *
     * @param tree   a tree. It can be {@code null}.
     * @param action an action to perform for each node. It shall not be {@code null}.
     * @throws NullPointerException if {@code action} is {@code null}.
     */
    void walk(@Nullable Tree<N> tree, @NotNull Consumer<? super N> action);
}
