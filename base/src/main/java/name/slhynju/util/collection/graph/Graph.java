package name.slhynju.util.collection.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Set;
import java.util.function.Function;

/**
 * A general graph.
 *
 * @param <N> the node type.
 * @param <E> the edge type.
 * @author slhynju
 */
public interface Graph<N extends GraphNode, E extends GraphEdge> {

    /**
     * Returns the node by name.
     *
     * @param name a node name. It shall not be {@code null}.
     * @return the node.
     */
    @Nullable @Nullable N getNode(@NotNull String name);

    /**
     * Checks whether this graph contains a named node.
     *
     * @param name a node name. It shall not be {@code null}.
     * @return {@code true} -- contains the node; {@code false} -- does not contain the node.
     */
    boolean containsNode(@NotNull String name);

    /**
     * Returns all the nodes.
     *
     * @return a {@code Collection} of all nodes.
     */
    @NotNull Collection<N> getNodes();

    /**
     * Adds a node.
     *
     * @param node a node to add. It shall not be {@code null}.
     */
    void addNode(@NotNull N node);

    /**
     * Adds a node if it does not exist.
     *
     * @param name        a node name. It shall not be {@code null}.
     * @param nodeCreator a custom node creator to create a new node by name. It shall not be {@code null}.
     * @return the node.
     * @throws NullPointerException if {@code nodeCreator} is {@code null}.
     */
    @NotNull N addNodeIfAbsent(@NotNull String name, @NotNull Function<String, ? extends N> nodeCreator);

    /**
     * Removes a node.
     *
     * @param node a node. It can be {@code null}.
     */
    void removeNode(@Nullable N node);

    /**
     * Returns all edges.
     *
     * @return a {@code Set} of all edges.
     */
    @NotNull Set<E> getEdges();

    /**
     * Adds an edge.
     *
     * @param edge an edge. It shall not be {@code null}.
     */
    void addEdge(@NotNull E edge);

    /**
     * Removes an edge.
     *
     * @param edge an edge. It can be {@code null}.
     */
    void removeEdge(@Nullable E edge);

}
