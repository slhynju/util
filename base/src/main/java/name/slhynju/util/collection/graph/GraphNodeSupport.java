package name.slhynju.util.collection.graph;

import name.slhynju.util.EqualsUtil;
import name.slhynju.util.HashCodeBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of {@code GraphNode}.
 *
 * @author slhynju
 */
@SuppressWarnings("FrequentlyUsedInheritorInspection")
public class GraphNodeSupport implements GraphNode {

    /**
     * The node name.
     */
    protected @NotNull String name = "";

    /**
     * The node attributes.
     */
    @SuppressWarnings("FieldNotUsedInToString")
    protected final @NotNull Map<String, Object> attributes = new HashMap<>(4);

    /**
     * Creates a new node.
     *
     * @param name a node name. It shall not be {@code null}.
     */
    public GraphNodeSupport(@NotNull String name) {
        this.name = name;
    }

    @Override
    public final @NotNull String getName() {
        return name;
    }

    @Override
    public final void setName(@NotNull String name) {
        this.name = name;
    }

    @Override
    public final boolean containsAttribute(@NotNull String key) {
        return attributes.containsKey(key);
    }

    @Override
    public final @Nullable Object getAttribute(@NotNull String key) {
        return attributes.get(key);
    }

    @Override
    public final void setAttribute(@NotNull String key, @Nullable Object value) {
        attributes.put(key, value);
    }

    @Override
    public final void removeAttribute(@NotNull String key) {
        attributes.remove(key);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof GraphNodeSupport) {
            GraphNodeSupport other = (GraphNodeSupport) obj;
            return EqualsUtil.isEquals(name, other.name);
        }
        return false;
    }

    @SuppressWarnings("CanBeFinal")
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).toValue();
    }

    @SuppressWarnings("CanBeFinal")
    @Override
    public @NotNull String toString() {
        return name;
    }

}
