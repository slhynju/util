package name.slhynju.util.collection.tree;

import name.slhynju.util.EqualsUtil;
import name.slhynju.util.HashCodeBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of {@code TreeNode}.
 *
 * @param <T> the node type that extends this type.
 * @author slhynju
 */
@SuppressWarnings({"rawtypes", "FrequentlyUsedInheritorInspection"})
public abstract class TreeNodeSupport<T extends TreeNodeSupport> implements TreeNode<T> {

    /**
     * The node name.
     */
    protected @NotNull String name = "";

    /**
     * The parent node.
     */
    @SuppressWarnings("FieldNotUsedInToString")
    protected @Nullable T parent = null;

    /**
     * The node attributes.
     */
    @SuppressWarnings("FieldNotUsedInToString")
    protected final @NotNull Map<String, Object> attributes = new HashMap<>(4);

    /**
     * Creates a new tree node.
     *
     * @param name a node name. It shall not be {@code null}.
     */
    public TreeNodeSupport(@NotNull String name) {
        this.name = name;
    }

    @Override
    public final @NotNull String getName() {
        return name;
    }

    @Override
    public final void setName(@NotNull String name) {
        this.name = name;
    }

    @Override
    public final boolean containsAttribute(@NotNull String key) {
        return attributes.containsKey(key);
    }

    @Override
    public final @Nullable Object getAttribute(@NotNull String key) {
        return attributes.get(key);
    }

    @Override
    public final void setAttribute(@NotNull String key, @Nullable Object value) {
        attributes.put(key, value);
    }

    @Override
    public final void removeAttribute(@NotNull String key) {
        attributes.remove(key);
    }

    @Override
    public final @Nullable T getParent() {
        return parent;
    }

    @Override
    public final void setParent(@Nullable T parent) {
        this.parent = parent;
    }

    @Override
    public final boolean isRoot() {
        return parent == null;
    }

    // getChildren() is implemented by child classes.

    // indexOf(T node) is implemented by child classes.

    @SuppressWarnings("ObjectEquality")
    @Override
    public boolean isFirstChild() {
        return parent != null && this == parent.getFirstChild();
    }

    @SuppressWarnings("ObjectEquality")
    @Override
    public boolean isLastChild() {
        return parent != null && this == parent.getLastChild();
    }

    // getPrevSibling() and getNextSibling() are implemented by child classes.

    @SuppressWarnings("CanBeFinal")
    @Override
    public @Nullable String toString() {
        return name;
    }

    @SuppressWarnings("CanBeFinal")
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).toValue();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof TreeNodeSupport) {
            TreeNodeSupport other = (TreeNodeSupport) obj;
            return EqualsUtil.isEquals(name, other.name);
        }
        return false;
    }

}
