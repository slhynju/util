package name.slhynju.util.collection.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of {@code GraphEdge}.
 *
 * @author slhynju
 */
@SuppressWarnings("FrequentlyUsedInheritorInspection")
public class GraphEdgeSupport implements GraphEdge {

    /**
     * The edge attributes.
     */
    protected final @NotNull Map<String, Object> attributes = new HashMap<>(4);

    @Override
    public final boolean containsAttribute(@NotNull String key) {
        return attributes.containsKey(key);
    }

    @Override
    public final @Nullable Object getAttribute(@NotNull String key) {
        return attributes.get(key);
    }

    @Override
    public final void setAttribute(@NotNull String key, @Nullable Object value) {
        attributes.put(key, value);
    }

    @Override
    public final void removeAttribute(@NotNull String key) {
        attributes.remove(key);
    }
}
