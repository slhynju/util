package name.slhynju.util.collection.tree.walker;

import name.slhynju.util.collection.tree.Tree;
import name.slhynju.util.collection.tree.TreeNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Walk a general tree in pre-order.
 *
 * @param <N> the tree node type.
 * @author slhynju
 */
public final class PreOrderTreeWalker<N extends TreeNode<N>> extends TreeWalkerSupport<N> {

    @SuppressWarnings("BoundedWildcard")
    @Override
    protected @Nullable N getStartNode(@NotNull Tree<N> tree) {
        return tree.getRoot();
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    @Override
    protected @Nullable N getNextNode(@NotNull N node) {
        if (node.isLeaf()) {
            //noinspection ConstantConditions
            while (node.isLastChild()) {
                node = node.getParent();
            }
            // if node has next sibling, the next node is the sibling.
            // if node is the root node, the next node is null, which is same to the getNextSibling() result.
            return node.getNextSibling();
        }
        return node.getFirstChild();
    }
}
