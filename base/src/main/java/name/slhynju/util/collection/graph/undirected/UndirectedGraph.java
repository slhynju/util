package name.slhynju.util.collection.graph.undirected;

import name.slhynju.util.collection.graph.Graph;
import org.jetbrains.annotations.NotNull;

import java.util.function.BiFunction;

/**
 * A undirected graph.
 *
 * @param <N> the node type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings("FrequentlyUsedInheritorInspection")
public interface UndirectedGraph<N extends UndirectedGraphNode<N, E>, E extends UndirectedGraphEdge<N, E>>
        extends Graph<N, E> {

    /**
     * Adds a undirected edge if not available.
     *
     * @param end1        a node. It shall not be {@code null}.
     * @param end2        another node. It shall not be {@code null}.
     * @param edgeCreator a custom edge creator to create a new edge with two nodes. It shall not be {@code null}.
     * @return the undirected edge.
     * @throws NullPointerException if {@code end1}, {@code end2} or {@code edgeCreator} is {@code null}.
     */
    default @NotNull E linkIfAbsent(@NotNull N end1, @NotNull N end2, @NotNull BiFunction<? super N, ? super N, ? extends E> edgeCreator) {
        for (E edge : end1.getEdges()) {
            if (edge.containsEnds(end1, end2)) {
                return edge;
            }
        }
        E edge = edgeCreator.apply(end1, end2);
        //noinspection ConstantConditions
        end1.addEdge(edge);
        // do not add duplicated edge if end1 and end2 are same node.
        //noinspection ObjectEquality
        if (end1 != end2) {
            end2.addEdge(edge);
        }
        addEdge(edge);
        return edge;
    }
}
