package name.slhynju.util.collection.tree;

import name.slhynju.util.EqualsUtil;
import name.slhynju.util.HashCodeBuilder;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Default implementation of {@code Tree}.
 *
 * @param <N> the node type.
 * @author slhynju
 */
@SuppressWarnings({"rawtypes", "CanBeFinal"})
public class TreeSupport<N extends TreeNode> implements Tree<N> {

    /**
     * The root node.
     */
    protected @Nullable N root = null;

    /**
     * Creates an empty tree.
     */
    public TreeSupport() {
        // DO NOTHING
    }

    /**
     * Creates a new tree.
     *
     * @param root the root node. It shall not be {@code null}.
     */
    public TreeSupport(@NotNull N root) {
        this.root = root;
    }

    @Override
    public final @Nullable N getRoot() {
        return root;
    }

    @Override
    public final void setRoot(@Nullable N root) {
        this.root = root;
    }

    @Override
    public final boolean isEmpty() {
        return root == null;
    }

    @SuppressWarnings("CanBeFinal")
    @NonNls
    @Override
    public @NotNull String toString() {
        return root == null ? "Tree null" : "Tree " + root.getName();
    }

    @SuppressWarnings("CanBeFinal")
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(root).toValue();
    }

    @SuppressWarnings({"rawtypes", "CanBeFinal"})
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof TreeSupport) {
            TreeSupport other = (TreeSupport) obj;
            return EqualsUtil.isEquals(root, other.root);
        }
        return false;
    }

}
