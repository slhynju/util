package name.slhynju.util.collection.tree;


import org.jetbrains.annotations.Nullable;

/**
 * A binary tree node.
 *
 * @param <T> the node type that extends this type.
 * @author slhynju
 */
@SuppressWarnings({"rawtypes", "FrequentlyUsedInheritorInspection"})
public interface BinaryTreeNode<T extends BinaryTreeNode> extends TreeNode<T> {

    /**
     * Returns the left child.
     *
     * @return the left child.
     */
    @Nullable @Nullable T getLeftChild();

    /**
     * Sets the left child.
     *
     * @param leftChild the left child. It can be {@code null}.
     */
    void setLeftChild(@Nullable T leftChild);

    /**
     * Returns the right child.
     *
     * @return the right child.
     */
    @Nullable @Nullable T getRightChild();

    /**
     * Sets the right child.
     *
     * @param rightChild the right child. It can be {@code null}.
     */
    void setRightChild(@Nullable T rightChild);

    /**
     * Checks whether this node is a left child of some other node.
     *
     * @return {@code true} -- is left child; {@code false} -- not left child.
     */
    boolean isLeftChild();

    /**
     * Checks whether this node is a right child of some other node.
     *
     * @return {@code true} -- is right child; {@code false} -- not right child.
     */
    boolean isRightChild();

}
