package name.slhynju.util.collection;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.*;
import java.util.function.*;

/**
 * {@code Array}, {@code Collection} and {@code Map} utility methods.
 *
 * @author slhynju
 */
@SuppressWarnings("OverlyComplexClass")
public final class CollectionUtil {

    // empty array constants.

    /**
     * A constant of empty {@code boolean array}.
     */
    public static final @NotNull boolean[] EMPTY_BOOLEAN_ARRAY = new boolean[0];

    /**
     * A constant of empty {@code byte array}.
     */
    public static final @NotNull byte[] EMPTY_BYTE_ARRAY = new byte[0];

    /**
     * A constant of empty {@code char array}.
     */
    public static final @NotNull char[] EMPTY_CHAR_ARRAY = new char[0];

    /**
     * A constant of empty {@code double array}.
     */
    public static final @NotNull double[] EMPTY_DOUBLE_ARRAY = new double[0];

    /**
     * A constant of empty {@code float array}.
     */
    public static final @NotNull float[] EMPTY_FLOAT_ARRAY = new float[0];

    /**
     * A constant of empty {@code int array}.
     */
    public static final @NotNull int[] EMPTY_INT_ARRAY = new int[0];

    /**
     * A constant of empty {@code long array}.
     */
    public static final @NotNull long[] EMPTY_LONG_ARRAY = new long[0];

    /**
     * A constant of empty {@code short array}.
     */
    public static final @NotNull short[] EMPTY_SHORT_ARRAY = new short[0];

    /**
     * A constant of empty {@code String array}.
     */
    public static final @NotNull String[] EMPTY_STRING_ARRAY = new String[0];

    /**
     * A constant of empty {@code Date array}.
     */
    public static final @NotNull Date[] EMPTY_DATE_ARRAY = new Date[0];

    /**
     * A constant of empty {@code BigInteger array}.
     */
    public static final @NotNull BigInteger[] EMPTY_BIG_INTEGER_ARRAY = new BigInteger[0];

    /**
     * A constant of empty {@code Class array}.
     */
    @SuppressWarnings("rawtypes")
    public static final @NotNull Class[] EMPTY_CLASS_ARRAY = new Class[0];

    /**
     * A constant of empty {@code Method array}.
     */
    public static final @NotNull Method[] EMPTY_METHOD_ARRAY = new Method[0];

    // isEmpty methods

    /**
     * Checks whether a {@code boolean array} is {@code null} or empty.
     *
     * @param array a {@code boolean array}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable boolean @Nullable [] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks whether a {@code boolean array} is not {@code null} or empty.
     *
     * @param array a {@code boolean array}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable boolean[] array) {
        return !isEmpty(array);
    }

    /**
     * Checks whether a {@code byte array} is {@code null} or empty.
     *
     * @param array a {@code byte array}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable byte @Nullable [] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks whether a {@code byte array} is not {@code null} or empty.
     *
     * @param array a {@code byte array}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable byte[] array) {
        return !isEmpty(array);
    }

    /**
     * Checks whether a {@code char array} is {@code null} or empty.
     *
     * @param array a {@code char array}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable char @Nullable [] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks whether a {@code char array} is not {@code null} or empty.
     *
     * @param array a {@code char array}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable char[] array) {
        return !isEmpty(array);
    }

    /**
     * Checks whether an {@code int array} is {@code null} or empty.
     *
     * @param array an {@code int array}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable int @Nullable [] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks whether an {@code int array} is not {@code null} or empty.
     *
     * @param array an {@code int array}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable int[] array) {
        return !isEmpty(array);
    }

    /**
     * Checks whether a {@code long array} is {@code null} or empty.
     *
     * @param array a {@code long array}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable long @Nullable [] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks whether a {@code long array} is not {@code null} or empty.
     *
     * @param array a {@code long array}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable long[] array) {
        return !isEmpty(array);
    }

    /**
     * Checks whether a {@code float array} is {@code null} or empty.
     *
     * @param array a {@code float array}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable float @Nullable [] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks whether a {@code float array} is not {@code null} or empty.
     *
     * @param array a {@code float array}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable float[] array) {
        return !isEmpty(array);
    }

    /**
     * Checks whether a {@code double array} is {@code null} or empty.
     *
     * @param array a {@code double array}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable double @Nullable [] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks whether a {@code double array} is not {@code null} or empty.
     *
     * @param array a {@code double array}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable double[] array) {
        return !isEmpty(array);
    }

    /**
     * Checks whether a {@code Collection} is {@code null} or empty.
     *
     * @param collection a {@code Collection}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * Checks whether a {@code Collection} is not {@code null} or empty.
     *
     * @param collection a {@code Collection}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable Collection<?> collection) {
        return !isEmpty(collection);
    }

    /**
     * Checks whether a {@code Map} is {@code null} or empty.
     *
     * @param map a {@code Map}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    /**
     * Checks whether a {@code Map} is not {@code null} or empty.
     *
     * @param map a {@code Map}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable Map<?, ?> map) {
        return !isEmpty(map);
    }

    /**
     * Checks whether an {@code Object array} is {@code null} or empty.
     *
     * @param array an {@code Object array}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    public static boolean isEmpty(@Nullable Object @Nullable [] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks whether an {@code Object array} is not {@code null} or empty.
     *
     * @param array an {@code Object array}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    public static boolean notEmpty(@Nullable Object[] array) {
        return !isEmpty(array);
    }

    /**
     * Calculates the required capacity of {@code HashSets} and {@code HashMaps}.
     *
     * @param size the known number of elements or entries.
     * @return the calculated capacity.
     */
    @SuppressWarnings({"MagicNumber", "NumericCastThatLosesPrecision"})
    public static int calculateHashCapacity(int size) {
        return Math.max((int) (size / 0.75) + 1, 16);
    }

    /**
     * Counts the number of {@code Enum} constants.
     *
     * @param enumClass an {@code Enum} class. It shall not be {@code null}.
     * @param <T>       the {@code Enum} type.
     * @return the number of {@code Enum} constants.
     * @throws NullPointerException if {@code enumClass} is {@code null}.
     */
    @SuppressWarnings("rawtypes")
    public static <T extends Enum> int countEnum(@NotNull Class<T> enumClass) {
        //noinspection ConstantConditions
        return enumClass.getEnumConstants().length;
    }

    // toList methods

    /**
     * Converts an {@code Object array} into a {@code List}.
     *
     * @param from an {@code Object array}. It can be {@code null}.
     * @param <T>  the element type.
     * @return a new {@code List} with same elements.
     */
    @SafeVarargs
    public static @NotNull <T> List<T> toList(@Nullable T... from) {
        // make a copy, otherwise any modification to underlying array will impact the returned list.
        return isEmpty(from) ? new ArrayList<>(0) : new ArrayList<>(Arrays.asList(from));
    }

    /**
     * Converts a {@code Collection} into a {@code List}.
     *
     * @param from a {@code Collection}. It can be {@code null}.
     * @param <T>  the element type.
     * @return a new {@code List} with same elements.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <T> List<T> toList(@Nullable Collection<T> from) {
        return isEmpty(from) ? new ArrayList<>(0) : new ArrayList<>(from);
    }

    /**
     * Converts a {@code StringTokenizer} into a {@code List} of {@code String} tokens.
     *
     * @param tokenizer a {@code StringTokenizer}. It can be {@code null}.
     * @return a {@code List} of {@code String} tokens.
     */
    public static @NotNull List<String> toList(@Nullable StringTokenizer tokenizer) {
        if (tokenizer == null) {
            return new ArrayList<>(0);
        }
        List<String> list = new ArrayList<>(4);
        while (tokenizer.hasMoreTokens()) {
            list.add(tokenizer.nextToken());
        }
        return list;
    }

    /**
     * Converts a {@code boolean array} into a {@code List} of {@code Boolean} objects.
     *
     * @param array a {@code boolean array}. It can be {@code null}.
     * @return a {@code List} of {@code Boolean} objects.
     */
    public static @NotNull List<Boolean> toList(boolean @NotNull ... array) {
        if (isEmpty(array)) {
            return new ArrayList<>(0);
        }
        List<Boolean> list = new ArrayList<>(array.length);
        for (boolean value : array) {
            list.add(Boolean.valueOf(value));
        }
        return list;
    }

    /**
     * Converts an {@code int array} into a {@code List} of {@code Integer} objects.
     *
     * @param array an {@code int array}. It can be {@code null}.
     * @return a {@code List} of {@code Integer} objects.
     */
    public static @NotNull List<Integer> toList(int @NotNull ... array) {
        if (isEmpty(array)) {
            return new ArrayList<>(0);
        }
        List<Integer> list = new ArrayList<>(array.length);
        for (int I : array) {
            list.add(Integer.valueOf(I));
        }
        return list;
    }

    /**
     * Converts a {@code long array} into a {@code List} of {@code Long} objects.
     *
     * @param array a {@code long array}. It can be {@code null}.
     * @return a {@code List} of {@code Long} objects.
     */
    public static @NotNull List<Long> toList(long @NotNull ... array) {
        if (isEmpty(array)) {
            return new ArrayList<>(0);
        }
        List<Long> list = new ArrayList<>(array.length);
        for (long l : array) {
            list.add(Long.valueOf(l));
        }
        return list;
    }

    /**
     * Converts a {@code double array} into a {@code List} of {@code Double} objects.
     *
     * @param array a {@code double array}. It can be {@code null}.
     * @return a {@code List} of {@code Double} objects.
     */
    @SuppressWarnings("ObjectAllocationInLoop")
    public static @NotNull List<Double> toList(double @NotNull ... array) {
        if (isEmpty(array)) {
            return new ArrayList<>(0);
        }
        List<Double> list = new ArrayList<>(array.length);
        for (double d : array) {
            list.add(Double.valueOf(d));
        }
        return list;
    }

    /**
     * Converts an {@code Enumeration} into a {@code List} of objects.
     *
     * @param enumeration an {@code Enumeration}. It can be {@code null}.
     * @param <T>         the element type.
     * @return a {@code List} of objects.
     */
    public static @NotNull <T> List<T> toList(@Nullable Enumeration<T> enumeration) {
        return enumeration == null ? new ArrayList<>(0) : Collections.list(enumeration);
    }

    /**
     * Transforms a {@code Collection} into a {@code List} with custom mapper.
     *
     * @param <F>    the source element type.
     * @param <T>    the target element type.
     * @param from   the source {@code Collection}. It can be {@code null}.
     * @param mapper a custom mapper. It shall not be {@code null}.
     * @return the transformed {@code List}.
     * @throws NullPointerException if {@code mapper} is {@code null}.
     */
    public static @NotNull <F, T> List<T> transformToList(@Nullable Collection<F> from, @NotNull Function<? super F, ? extends T> mapper) {
        if (isEmpty(from)) {
            return new ArrayList<>(0);
        }
        List<T> list = new ArrayList<>(from.size());
        for (F obj : from) {
            list.add(mapper.apply(obj));
        }
        return list;
    }

    /**
     * Transforms an {@code Object array} into a {@code List} with custom mapper.
     *
     * @param <F>    the source element type.
     * @param <T>    the target element type.
     * @param from   the source {@code Object array}. It can be {@code null}.
     * @param mapper a custom mapper. It shall not be {@code null}.
     * @return the transformed {@code List}.
     * @throws NullPointerException if {@code mapper} is {@code null}.
     */
    public static @NotNull <F, T> List<T> transformToList(F @NotNull [] from, @NotNull Function<? super F, ? extends T> mapper) {
        if (isEmpty(from)) {
            return new ArrayList<>(0);
        }
        List<T> list = new ArrayList<>(from.length);
        for (F obj : from) {
            list.add(mapper.apply(obj));
        }
        return list;
    }

    // toSet methods

    /**
     * Converts an {@code Object array} into a {@code Set}.
     *
     * @param array an {@code Object array}. It can be {@code null}.
     * @param <T>   the element type.
     * @return a {@code Set} with same elements.
     */
    @SuppressWarnings({"UseBulkOperation", "ManualArrayToCollectionCopy"})
    @SafeVarargs
    public static @NotNull <T> Set<T> toSet(T @NotNull ... array) {
        if (isEmpty(array)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<T> set = new HashSet<>();
        for (T obj : array) {
            set.add(obj);
        }
        return set;
    }

    /**
     * Converts a {@code Collection} into a {@code Set}.
     *
     * @param from a {@code Collection}. It can be {@code null}.
     * @param <T>  the element type.
     * @return a {@code Set} with same elements.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <T> Set<T> toSet(@Nullable Collection<T> from) {
        return isEmpty(from) ? new HashSet<>(0) : new HashSet<>(from);
    }

    /**
     * Converts a {@code boolean array} into a {@code Set} of {@code Boolean} objects.
     *
     * @param array a {@code boolean array}. It can be {@code null}.
     * @return a {@code Set} of {@code Boolean} objects.
     */
    public static @NotNull Set<Boolean> toSet(boolean @NotNull ... array) {
        if (isEmpty(array)) {
            return new HashSet<>(0);
        }
        // Uses capacity of 4 as a minor performance tuning in order to avoid the resizing in underlying HashMap.
        Set<Boolean> set = new HashSet<>(4);
        for (boolean value : array) {
            set.add(Boolean.valueOf(value));
        }
        return set;
    }

    /**
     * Converts an {@code int array} into a {@code Set} of {@code Integer} objects.
     *
     * @param array an {@code int array}. It can be {@code null}.
     * @return a {@code Set} of {@code Integer} objects.
     */
    public static @NotNull Set<Integer> toSet(int @NotNull ... array) {
        if (isEmpty(array)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<Integer> set = new HashSet<>();
        for (int I : array) {
            set.add(Integer.valueOf(I));
        }
        return set;
    }

    /**
     * Converts a {@code long array} into a {@code Set} of {@code Long} objects.
     *
     * @param array a {@code long array}. It can be {@code null}.
     * @return a {@code Set} of {@code Long} objects.
     */
    public static @NotNull Set<Long> toSet(long @NotNull ... array) {
        if (isEmpty(array)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<Long> set = new HashSet<>();
        for (long l : array) {
            set.add(Long.valueOf(l));
        }
        return set;
    }

    /**
     * Converts a {@code double array} into a {@code Set} of {@code Double} objects.
     *
     * @param array a {@code double array}. It can be {@code null}.
     * @return a {@code Set} of {@code Double} objects.
     */
    @SuppressWarnings("ObjectAllocationInLoop")
    public static @NotNull Set<Double> toSet(double @NotNull ... array) {
        if (isEmpty(array)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<Double> set = new HashSet<>();
        for (double d : array) {
            set.add(Double.valueOf(d));
        }
        return set;
    }

    /**
     * Transforms a {@code Collection} into a {@code Set} with a custom mapper.
     *
     * @param <F>    the source element type.
     * @param <T>    the target element type.
     * @param from   the source {@code Collection}. It can be {@code null}.
     * @param mapper a custom mapper. It shall not be {@code null}.
     * @return the transformed {@code Set}.
     * @throws NullPointerException if {@code mapper} is {@code null}.
     */
    public static @NotNull <F, T> Set<T> transformToSet(@Nullable Collection<F> from, @NotNull Function<? super F, ? extends T> mapper) {
        if (isEmpty(from)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<T> set = new HashSet<>();
        for (F obj : from) {
            set.add(mapper.apply(obj));
        }
        return set;
    }

    /**
     * Transforms an {@code Object array} into a {@code Set} with a custom mapper.
     *
     * @param <F>    the source element type.
     * @param <T>    the target element type.
     * @param from   the source {@code Object array}. It can be {@code null}.
     * @param mapper a custom mapper. It shall not be {@code null}.
     * @return the transformed {@code Set}.
     * @throws NullPointerException if {@code mapper} is {@code null}.
     */
    public static @NotNull <F, T> Set<T> transformToSet(F @NotNull [] from, @NotNull Function<? super F, ? extends T> mapper) {
        if (isEmpty(from)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<T> set = new HashSet<>();
        for (F obj : from) {
            set.add(mapper.apply(obj));
        }
        return set;
    }

    // toMap methods

    /**
     * Copy a {@code Map}.
     *
     * @param map a {@code Map}. It can be {@code null}.
     * @param <K> the key type.
     * @param <V> the value type.
     * @return a new {@code Map} with same entries.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <K, V> Map<K, V> toMap(@Nullable Map<K, V> map) {
        return isEmpty(map) ? new HashMap<>(0) : new HashMap<>(map);
    }

    /**
     * Converts an {@code Object array} into a {@code Map}. The array is in format of key1, value1, key2, value2...
     *
     * @param keyValuePairs an {@code Object array} of keys and values. Its length shall be an even number.
     * @return a {@code Map} of same entries.
     * @throws IllegalArgumentException if {@code keyValuePairs} length is an odd number.
     */
    @SuppressWarnings("rawtypes")
    public static @NotNull Map toMap(Object @NotNull ... keyValuePairs) {
        if (isEmpty(keyValuePairs)) {
            return new HashMap(0);
        }
        if (keyValuePairs.length % 2 != 0) {
            throw new IllegalArgumentException("Illegal key value pairs. Its length shall be an even number.");
        }
        Map map = new HashMap(calculateHashCapacity(keyValuePairs.length / 2));
        for (int I = 0; I < keyValuePairs.length; I += 2) {
            map.put(keyValuePairs[I], keyValuePairs[I + 1]);
        }
        return map;
    }

    /**
     * Transforms a {@code Collection} into a {@code Map} with a custom key mapper. The {@code Collection} elements are treated as {@code Map}values.
     *
     * @param <K>       the key type.
     * @param <V>       the value type, also the {@code Collection} element type.
     * @param values    a {@code Collection}. It can be {@code null}.
     * @param keyMapper a custom mapper to generate the key from the value. It shall not be {@code null}.
     * @return the transformed {@code Map}.
     * @throws NullPointerException if {@code keyMapper} is {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <K, V> Map<K, V> transformToMap(@Nullable Collection<V> values, @NotNull Function<? super V, ? extends K> keyMapper) {
        if (isEmpty(values)) {
            return new HashMap<>(0);
        }
        Map<K, V> map = new HashMap<>(calculateHashCapacity(values.size()));
        for (V value : values) {
            K key = keyMapper.apply(value);
            map.put(key, value);
        }
        return map;
    }

    /**
     * Transforms an {@code Object array} into a {@code Map} with a custom key mapper. The array elements are treated as {@code Map} values.
     *
     * @param <K>       the key type.
     * @param <V>       the value type, also the array element type.
     * @param values    an {@code Object array}. It can be {@code null}.
     * @param keyMapper a custom mapper to generate the key from the value. It shall not be {@code null}.
     * @return the transformed {@code Map}.
     * @throws NullPointerException if {@code keyMapper} is {@code null}.
     */
    public static @NotNull <K, V> Map<K, V> transformToMap(V @NotNull [] values, @NotNull Function<? super V, ? extends K> keyMapper) {
        if (isEmpty(values)) {
            return new HashMap<>(0);
        }
        Map<K, V> map = new HashMap<>(calculateHashCapacity(values.length));
        for (V value : values) {
            K key = keyMapper.apply(value);
            map.put(key, value);
        }
        return map;
    }

    /**
     * Transforms a {@code Map} into another {@code Map} with custom key and value mappers.
     *
     * @param <K>         the target key type.
     * @param <V>         the target value type.
     * @param <K2>        the source key type.
     * @param <V2>        the source value type.
     * @param fromMap     the source {@code Map}. It can be {@code null}.
     * @param keyMapper   a custom mapper to generate the target key from the source key. It shall not be {@code null}.
     * @param valueMapper a custom mapper to generate the target value from the source value. It shall not be {@code null}.
     * @return the transformed {@code Map}.
     * @throws NullPointerException if {@code keyMapper} or {@code valueMapper} is {@code null}.
     */
    public static @NotNull <K, V, K2, V2> Map<K, V> transformToMap(@Nullable Map<K2, V2> fromMap, @NotNull Function<? super K2, ? extends K> keyMapper,
                                                                   @NotNull Function<? super V2, ? extends V> valueMapper) {
        if (isEmpty(fromMap)) {
            return new HashMap<>(0);
        }
        Map<K, V> map = new HashMap<>(calculateHashCapacity(fromMap.size()));
        for (Map.Entry<K2, V2> entry : fromMap.entrySet()) {
            K2 fromKey = entry.getKey();
            V2 fromValue = entry.getValue();
            map.put(keyMapper.apply(fromKey), valueMapper.apply(fromValue));
        }
        return map;
    }

    /**
     * Transforms a {@code Map} into another {@code Map} with a custom key mapper. It retains same values.
     *
     * @param <K>       the target key type.
     * @param <V>       the value type.
     * @param <K2>      the source key type.
     * @param fromMap   the source {@code Map}. It can be {@code null}.
     * @param keyMapper a custom mapper to generate the target key from the source key. It shall not be {@code null}.
     * @return the transformed {@code Map}.
     * @throws NullPointerException if {@code keyMapper} is {@code null}.
     */
    public static @NotNull <K, K2, V> Map<K, V> transformKeys(@Nullable Map<K2, V> fromMap,
                                                              @NotNull Function<? super K2, ? extends K> keyMapper) {
        if (isEmpty(fromMap)) {
            return new HashMap<>(0);
        }
        Map<K, V> map = new HashMap<>(calculateHashCapacity(fromMap.size()));
        for (Map.Entry<K2, V> entry : fromMap.entrySet()) {
            K newKey = keyMapper.apply(entry.getKey());
            map.put(newKey, entry.getValue());
        }
        return map;
    }

    /**
     * Transforms a {@code Map} into another {@code Map} with a custom value mapper. It retains same keys.
     *
     * @param <K>         the key type.
     * @param <V>         the target value type.
     * @param <V2>        the source value type.
     * @param fromMap     the source {@code Map}. It can be {@code null}.
     * @param valueMapper a custom mapper to generate the target value from the source value. It shall not be {@code null}.
     * @return the transformed {@code Map}.
     * @throws NullPointerException if {@code valueMapper} is {@code null}.
     */
    public static @NotNull <K, V, V2> Map<K, V> transformValues(@Nullable Map<K, V2> fromMap,
                                                                @NotNull Function<? super V2, ? extends V> valueMapper) {
        if (isEmpty(fromMap)) {
            return new HashMap<>(0);
        }
        Map<K, V> map = new HashMap<>(calculateHashCapacity(fromMap.size()));
        for (Map.Entry<K, V2> entry : fromMap.entrySet()) {
            V newValue = valueMapper.apply(entry.getValue());
            map.put(entry.getKey(), newValue);
        }
        return map;
    }

    // toArray methods

    /**
     * Converts a {@code List} of {@code Boolean} objects into a {@code boolean array}.
     *
     * @param list         a {@code List} of {@code Boolean} objects. It can be {@code null}.
     * @param defaultValue the default value to use if an array element is {@code null}.
     * @return a {@code boolean array} with same values.
     */
    public static @NotNull boolean @NotNull [] toBooleanArray(@Nullable List<Boolean> list, boolean defaultValue) {
        if (isEmpty(list)) {
            return EMPTY_BOOLEAN_ARRAY;
        }
        boolean[] array = new boolean[list.size()];
        for (int I = 0; I < list.size(); I++) {
            Boolean v = list.get(I);
            array[I] = v == null ? defaultValue : v.booleanValue();
        }
        return array;
    }

    /**
     * Converts a {@code List} of {@code Integer} objects into an {@code int array}.
     *
     * @param list         a {@code List} of {@code Integer} objects. It can be {@code null}.
     * @param defaultValue the default value to use if an array element is {@code null}.
     * @return an {@code int array} with same values.
     */
    public static @NotNull int @NotNull [] toIntArray(@Nullable List<Integer> list, int defaultValue) {
        if (isEmpty(list)) {
            return EMPTY_INT_ARRAY;
        }
        int[] array = new int[list.size()];
        for (int I = 0; I < list.size(); I++) {
            Integer v = list.get(I);
            array[I] = v == null ? defaultValue : v.intValue();
        }
        return array;
    }

    /**
     * Converts a {@code List} of {@code Long} objects into a {@code long array}.
     *
     * @param list         a {@code List} of {@code Long} objects. It can be {@code null}.
     * @param defaultValue the default value to use if an array element is {@code null}.
     * @return a {@code long array} with same values.
     */
    public static @NotNull long @NotNull [] toLongArray(@Nullable List<Long> list, long defaultValue) {
        if (isEmpty(list)) {
            return EMPTY_LONG_ARRAY;
        }
        long[] array = new long[list.size()];
        for (int I = 0; I < list.size(); I++) {
            Long v = list.get(I);
            array[I] = v == null ? defaultValue : v.longValue();
        }
        return array;
    }

    /**
     * Converts a {@code List} of {@code Double} objects into a {@code double array}.
     *
     * @param list         a {@code List} of {@code Double} objects. It can be {@code null}.
     * @param defaultValue the default value to use if an array element is {@code null}.
     * @return a {@code double array} with same values.
     */
    public static @NotNull double @NotNull [] toDoubleArray(@Nullable List<Double> list, double defaultValue) {
        if (isEmpty(list)) {
            return EMPTY_DOUBLE_ARRAY;
        }
        double[] array = new double[list.size()];
        for (int I = 0; I < list.size(); I++) {
            Double v = list.get(I);
            array[I] = v == null ? defaultValue : v.doubleValue();
        }
        return array;
    }

    /**
     * Converts a {@code List} of {@code String} into a {@code String array}.
     *
     * @param list a {@code List} of {@code String}. It can be {@code null}.
     * @return a {@code String array} with same elements.
     */
    public static @NotNull String[] toStringArray(@Nullable List<String> list) {
        if (isEmpty(list)) {
            return EMPTY_STRING_ARRAY;
        }
        String[] array = new String[list.size()];
        return list.toArray(array);
    }

    /**
     * Converts a {@code List} of {@code Date} into a {@code Date array}.
     *
     * @param list a {@code List} of {@code Date}. It can be {@code null}.
     * @return a {@code Date array} with same elements.
     */
    public static @NotNull Date[] toDateArray(@Nullable List<Date> list) {
        if (isEmpty(list)) {
            return EMPTY_DATE_ARRAY;
        }
        Date[] array = new Date[list.size()];
        return list.toArray(array);
    }

    /**
     * Converts a {@code List} of {@code Class} into a {@code Class array}.
     *
     * @param list a {@code List} of {@code Class}. It can be {@code null}.
     * @return a {@code Class array} with same elements.
     */
    public static @NotNull Class<?>[] toClassArray(@Nullable List<Class<?>> list) {
        if (isEmpty(list)) {
            return EMPTY_CLASS_ARRAY;
        }
        Class<?>[] array = new Class<?>[list.size()];
        return list.toArray(array);
    }

    /**
     * Converts a {@code List} of {@code Method} into a {@code Method array}.
     *
     * @param list a {@code List} of {@code Method}. It can be {@code null}.
     * @return a {@code Method array} with same elements.
     */
    public static @NotNull Method[] toMethodArray(@Nullable List<Method> list) {
        if (isEmpty(list)) {
            return EMPTY_METHOD_ARRAY;
        }
        Method[] array = new Method[list.size()];
        return list.toArray(array);
    }

    // getFirst methods

    /**
     * Returns the first element of a {@code List}.
     *
     * @param list a {@code List}. It can be {@code null}.
     * @param <T>  the element type.
     * @return the first element if available. Returns {@code null} if not found.
     */
    public static @Nullable <T> T getFirst(@Nullable List<T> list) {
        return isEmpty(list) ? null : list.get(0);
    }

    /**
     * Returns the first element of an {@code Object array}.
     *
     * @param array an {@code Object array}. It can be {@code null}.
     * @param <T>   the element type.
     * @return the first element if available. Returns {@code null} if not found.
     */
    public static @Nullable <T> T getFirst(@Nullable T[] array) {
        return isEmpty(array) ? null : array[0];
    }

    /**
     * Returns the first matching element of a {@code List}.
     *
     * @param list    a {@code List}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the first matching element if found. Returns {@code null} if not found.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @Nullable <T> T getFirst(@Nullable List<T> list, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(list)) {
            return null;
        }
        for (T obj : list) {
            if (matcher.test(obj)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * Returns the first matching element of a {@code List}.
     *
     * @param list    a {@code List}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the first matching element if found. Returns {@code null} if not found.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @Nullable <T> T getFirst(@Nullable List<T> list,
                                           @NotNull IndexPredicate<? super T> matcher) {
        if (isEmpty(list)) {
            return null;
        }
        for (int I = 0; I < list.size(); I++) {
            T obj = list.get(I);
            if (matcher.test(I, obj)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * Returns the first matching element of an {@code Object array}.
     *
     * @param array   an {@code Object array}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the first matching element if found. Returns {@code null} if not found.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @Nullable <T> T getFirst(T @NotNull [] array, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(array)) {
            return null;
        }
        for (T obj : array) {
            if (matcher.test(obj)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * Returns the first matching element of an {@code Object array}.
     *
     * @param array   an {@code Object array}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the first matching element if found. Returns {@code null} if not found.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @Nullable <T> T getFirst(T @NotNull [] array, @NotNull IndexPredicate<? super T> matcher) {
        if (isEmpty(array)) {
            return null;
        }
        for (int I = 0; I < array.length; I++) {
            T obj = array[I];
            if (matcher.test(I, obj)) {
                return obj;
            }
        }
        return null;
    }

    // getLast methods

    /**
     * Returns the last element of a {@code List}.
     *
     * @param list a {@code List}. It can be {@code null}.
     * @param <T>  the element type.
     * @return the last element if available. Returns {@code null} if not found.
     */
    public static @Nullable <T> T getLast(@Nullable List<T> list) {
        return isEmpty(list) ? null : list.get(list.size() - 1);
    }

    /**
     * Returns the last element of an {@code Object array}.
     *
     * @param array an {@code Object array}. It can be {@code null}.
     * @param <T>   the element type.
     * @return the last element if available. Returns {@code null} if not found.
     */
    public static @Nullable <T> T getLast(T @NotNull [] array) {
        return isEmpty(array) ? null : array[array.length - 1];
    }

    /**
     * Returns the last matching element of a {@code List}.
     *
     * @param list    a {@code List}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the last matching element if found. Returns {@code null} if not found.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @Nullable <T> T getLast(@Nullable List<T> list, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(list)) {
            return null;
        }
        ListIterator<T> it = list.listIterator(list.size());
        while (it.hasPrevious()) {
            T obj = it.previous();
            if (matcher.test(obj)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * Returns the last matching element of a {@code List}.
     *
     * @param list    a {@code List}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the last matching element if found. Returns {@code null} if not found.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @Nullable <T> T getLast(@Nullable List<T> list, @NotNull IndexPredicate<? super T> matcher) {
        if (isEmpty(list)) {
            return null;
        }
        for (int I = list.size() - 1; I >= 0; I--) {
            T obj = list.get(I);
            if (matcher.test(I, obj)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * Returns the last matching element of an {@code Object array}.
     *
     * @param array   an {@code Object array}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the last matching element if found. Returns {@code null} if not found.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @Nullable <T> T getLast(T @NotNull [] array, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(array)) {
            return null;
        }
        for (int I = array.length - 1; I >= 0; I--) {
            T obj = array[I];
            if (matcher.test(obj)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * Returns the last matching element of an {@code Object array}.
     *
     * @param array   an {@code Object array}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the last matching element if found. Returns {@code null} if not found.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @Nullable <T> T getLast(T @NotNull [] array, @NotNull IndexPredicate<? super T> matcher) {
        if (isEmpty(array)) {
            return null;
        }
        for (int I = array.length - 1; I >= 0; I--) {
            T obj = array[I];
            if (matcher.test(I, obj)) {
                return obj;
            }
        }
        return null;
    }

    // getElement methods

    /**
     * Returns the element at the specified index.
     *
     * @param list  a {@code List}. It can be {@code null}.
     * @param index the element index.
     * @param <T>   the element type.
     * @return the element if available. Returns null if not found.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @Nullable <T> T getElement(@Nullable List<T> list, int index) {
        if (isEmpty(list)) {
            return null;
        }
        return index < 0 || index >= list.size() ? null : list.get(index);
    }

    /**
     * Returns the element at the specified index.
     *
     * @param array an {@code Object array}. It can be {@code null}.
     * @param index the element index.
     * @param <T>   the element type.
     * @return the element if available. Returns null if not found.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static @Nullable <T> T getElement(T @NotNull [] array, int index) {
        if (isEmpty(array)) {
            return null;
        }
        return index < 0 || index >= array.length ? null : array[index];
    }

    /**
     * Returns the boolean element at the specified index.
     *
     * @param array        a {@code boolean array}. It can be {@code null}.
     * @param index        the element index.
     * @param defaultValue the default value to use if not found.
     * @return the boolean element if available. Returns {@code defaultValue} if not found.
     */
    @SuppressWarnings({"SimplifiableIfStatement", "BooleanMethodNameMustStartWithQuestion"})
    public static boolean getElement(boolean @NotNull [] array, int index, boolean defaultValue) {
        if (isEmpty(array)) {
            return defaultValue;
        }
        return index < 0 || index >= array.length ? defaultValue : array[index];
    }

    /**
     * Returns the int element at the specified index.
     *
     * @param array        an {@code int array}. It can be {@code null}.
     * @param index        the element index.
     * @param defaultValue the default value to use if not found.
     * @return the int element if available. Returns {@code defaultValue} if not found.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static int getElement(int @NotNull [] array, int index, int defaultValue) {
        if (isEmpty(array)) {
            return defaultValue;
        }
        return index < 0 || index >= array.length ? defaultValue : array[index];
    }

    /**
     * Returns the long element at the specified index.
     *
     * @param array        a {@code long array}. It can be {@code null}.
     * @param index        the element index.
     * @param defaultValue the default value to use if not found.
     * @return the long element if available. Returns {@code defaultValue} if not found.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static long getElement(long @NotNull [] array, int index, long defaultValue) {
        if (isEmpty(array)) {
            return defaultValue;
        }
        return index < 0 || index >= array.length ? defaultValue : array[index];
    }

    /**
     * Returns the double element at the specified index.
     *
     * @param array        a {@code double array}. It can be {@code null}.
     * @param index        the element index.
     * @param defaultValue the default value to use if not found.
     * @return the double element if available. Returns {@code defaultValue} if not found.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static double getElement(double @NotNull [] array, int index, double defaultValue) {
        if (isEmpty(array)) {
            return defaultValue;
        }
        return index < 0 || index >= array.length ? defaultValue : array[index];
    }

    // addAll methods

    /**
     * Adds all elements of a {@code Collection} to another {@code Collection}.
     *
     * @param to   the target {@code Collection}. It can be {@code null}.
     * @param from the source {@code Collection}. It can be {@code null}.
     * @param <T>  the element type of target {@code Collection}.
     */
    public static <T> void addAll(@Nullable Collection<T> to, @Nullable Collection<? extends T> from) {
        if (to == null || isEmpty(from)) {
            return;
        }
        to.addAll(from);
    }

    /**
     * Adds all elements of an {@code Object array} to a {@code Collection}.
     *
     * @param to   the target {@code Collection}. It can be {@code null}.
     * @param from the source {@code Object array}. It can be {@code null}.
     * @param <T>  the element type of source {@code Object array}.
     */
    public static <T> void addAll(@Nullable Collection<? super T> to, @Nullable T[] from) {
        if (to == null || isEmpty(from)) {
            return;
        }
        to.addAll(Arrays.asList(from));
    }

    /**
     * Adds all elements of an {@code Enumeration} to a {@code Collection}.
     *
     * @param to   the target {@code Collection}. It can be {@code null}.
     * @param from the source {@code Enumeration}. It can be {@code null}.
     * @param <T>  the element type of target {@code Collection}.
     */
    public static <T> void addAll(@Nullable Collection<T> to, @Nullable Enumeration<? extends T> from) {
        if (to == null || from == null) {
            return;
        }
        while (from.hasMoreElements()) {
            to.add(from.nextElement());
        }
    }

    /**
     * Adds all tokens of a {@code StringTokenizer} to a {@code Collection}.
     *
     * @param to   the target {@code Collection}. It can be {@code null}.
     * @param from the source {@code StringTokenizer}. It can be {@code null}.
     */
    public static void addAll(@Nullable Collection<? super String> to, @Nullable StringTokenizer from) {
        if (to == null || from == null) {
            return;
        }
        while (from.hasMoreTokens()) {
            to.add(from.nextToken());
        }
    }

    /**
     * Adds all keys of a {@code Map} to a {@code Collection}.
     *
     * @param to   the target {@code Collection}. It can be {@code null}.
     * @param from the source {@code Map}. It can be {@code null}.
     * @param <T>  the element type of target {@code Collection}.
     */
    public static <T> void addAllKeys(@Nullable Collection<T> to, @Nullable Map<? extends T, ?> from) {
        if (to == null || isEmpty(from)) {
            return;
        }
        to.addAll(from.keySet());
    }

    /**
     * Adds all values of a {@code Map} to a {@code Collection}.
     *
     * @param to   the target {@code Collection}. It can be {@code null}.
     * @param from the source {@code Map}. It can be {@code null}.
     * @param <T>  the element type of target {@code Collection}.
     */
    public static <T> void addAllValues(@Nullable Collection<T> to, @Nullable Map<?, ? extends T> from) {
        if (to == null || isEmpty(from)) {
            return;
        }
        to.addAll(from.values());
    }

    // addAllMatches methods

    /**
     * Adds all matching elements of a {@code Collection} to another {@code Collection} with a custom matcher.
     *
     * @param to      the target {@code Collection}. It can be {@code null}.
     * @param from    the source {@code Collection}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type of target {@code Collection}.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <T> void addAllMatches(@Nullable Collection<T> to, @Nullable Collection<? extends T> from, @NotNull Predicate<? super T> matcher) {
        if (to == null || isEmpty(from)) {
            return;
        }
        for (T obj : from) {
            if (matcher.test(obj)) {
                to.add(obj);
            }
        }
    }

    /**
     * Adds all matching elements of a {@code List} to a {@code Collection} with a custom matcher.
     *
     * @param to      the target {@code Collection}. It can be {@code null}.
     * @param from    the source {@code List}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type of target {@code Collection}.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <T> void addAllMatches(@Nullable Collection<T> to, @Nullable List<? extends T> from, @NotNull IndexPredicate<? super T> matcher) {
        if (to == null || isEmpty(from)) {
            return;
        }
        for (int I = 0; I < from.size(); I++) {
            T obj = from.get(I);
            if (matcher.test(I, obj)) {
                to.add(obj);
            }
        }
    }

    /**
     * Adds all matching elements of an {@code Object array} to a {@code Collection} with a custom matcher.
     *
     * @param to      the target {@code Collection}. It can be {@code null}.
     * @param from    the source {@code Object array}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type of target {@code Collection}.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <T> void addAllMatches(@Nullable Collection<? super T> to, T @NotNull [] from, @NotNull Predicate<? super T> matcher) {
        if (to == null || isEmpty(from)) {
            return;
        }
        for (T obj : from) {
            if (matcher.test(obj)) {
                to.add(obj);
            }
        }
    }

    /**
     * Adds all matching elements of an {@code Object array} to a {@code Collection} with a custom matcher.
     *
     * @param to      the target {@code Collection}. It can be {@code null}.
     * @param from    the source {@code Object array}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type of target {@code Collection}.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <T> void addAllMatches(@Nullable Collection<? super T> to, T @NotNull [] from, @NotNull IndexPredicate<? super T> matcher) {
        if (to == null || isEmpty(from)) {
            return;
        }
        for (int I = 0; I < from.length; I++) {
            T obj = from[I];
            if (matcher.test(I, obj)) {
                to.add(obj);
            }
        }
    }

    /**
     * Adds all matching elements of a {@code Object Map} to another {@code Map} with a custom matcher.
     *
     * @param to      the target {@code Map}. It can be {@code null}.
     * @param from    the source {@code Map}. It can be {@code null}.
     * @param matcher a custom matcher that checks both key and value. It shall not be {@code null}.
     * @param <K>     the key type of target {@code Map}.
     * @param <V>     the value type of target {@code Map}.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <K, V> void addAllMatches(@Nullable Map<K, V> to, @Nullable Map<? extends K, ? extends V> from, @NotNull BiPredicate<? super K, ? super V> matcher) {
        if (to == null || isEmpty(from)) {
            return;
        }
        for (Map.Entry<? extends K, ? extends V> entry : from.entrySet()) {
            K key = entry.getKey();
            V value = entry.getValue();
            if (matcher.test(key, value)) {
                to.put(key, value);
            }
        }
    }

    // removeAllMatches methods

    /**
     * Removes all matching elements from a {@code Collection} with a custom matcher.
     *
     * @param collection a {@code Collection}. It can be {@code null}.
     * @param matcher    a custom matcher. It shall not be {@code null}.
     * @param <T>        the element type.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <T> void removeAllMatches(@Nullable Collection<T> collection, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(collection)) {
            return;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            T obj = it.next();
            if (matcher.test(obj)) {
                it.remove();
            }
        }
    }

    /**
     * Removes all matching elements from a {@code List} with a custom matcher.
     *
     * @param list    a {@code List}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <T> void removeAllMatches(@Nullable List<T> list, @NotNull IndexPredicate<? super T> matcher) {
        if (isEmpty(list)) {
            return;
        }
        for (int I = list.size() - 1; I >= 0; I--) {
            T obj = list.get(I);
            if (matcher.test(I, obj)) {
                list.remove(I);
            }
        }
    }

    /**
     * Removes all matching elements from a {@code Map} with a custom matcher.
     *
     * @param map     a {@code Map}. It can be {@code null}.
     * @param matcher a custom matcher that checks both key and value. It shall not be {@code null}.
     * @param <K>     the key type.
     * @param <V>     the value type.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    @SuppressWarnings("ConstantConditions")
    public static <K, V> void removeAllMatches(@Nullable Map<K, V> map, @NotNull BiPredicate<? super K, ? super V> matcher) {
        if (isEmpty(map)) {
            return;
        }
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> entry = it.next();
            K key = entry.getKey();
            V value = entry.getValue();
            if (matcher.test(key, value)) {
                it.remove();
            }
        }
    }

    // retainAllMatches methods

    /**
     * Removes all mismatching elements from a {@code Collection} with a custom matcher.
     *
     * @param collection a {@code Collection}. It can be {@code null}.
     * @param matcher    a custom matcher. It shall not be {@code null}.
     * @param <T>        the element type.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <T> void retainAllMatches(@Nullable Collection<T> collection, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(collection)) {
            return;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            T obj = it.next();
            if (!matcher.test(obj)) {
                it.remove();
            }
        }
    }

    /**
     * Removes all mismatching elements from a {@code List} with a custom matcher.
     *
     * @param list    a {@code List}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static <T> void retainAllMatches(@Nullable List<T> list, @NotNull IndexPredicate<? super T> matcher) {
        if (isEmpty(list)) {
            return;
        }
        for (int I = list.size() - 1; I >= 0; I--) {
            T obj = list.get(I);
            if (!matcher.test(I, obj)) {
                list.remove(I);
            }
        }
    }

    /**
     * Removes all mismatching elements from a {@code Map} with a custom matcher.
     *
     * @param map     a {@code Map}. It can be {@code null}.
     * @param matcher a custom matcher that checks both key and value. It shall not be {@code null}.
     * @param <K>     the key type.
     * @param <V>     the value type.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    @SuppressWarnings("ConstantConditions")
    public static <K, V> void retainAllMatches(@Nullable Map<K, V> map, @NotNull BiPredicate<? super K, ? super V> matcher) {
        if (isEmpty(map)) {
            return;
        }
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> entry = it.next();
            K key = entry.getKey();
            V value = entry.getValue();
            if (!matcher.test(key, value)) {
                it.remove();
            }
        }
    }

    // sub methods

    /**
     * Returns a sublist of matching elements.
     *
     * @param list    a {@code List}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the sublist of matching elements.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <T> List<T> sub(@Nullable List<T> list, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(list)) {
            return new ArrayList<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        List<T> subList = new ArrayList<>();
        addAllMatches(subList, list, matcher);
        return subList;
    }

    /**
     * Returns a sublist of matching elements.
     *
     * @param list    a {@code List}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the sublist of matching elements.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <T> List<T> sub(@Nullable List<T> list, @NotNull IndexPredicate<? super T> matcher) {
        if (isEmpty(list)) {
            return new ArrayList<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        List<T> subList = new ArrayList<>();
        addAllMatches(subList, list, matcher);
        return subList;
    }

    /**
     * Returns a sublist of matching elements.
     *
     * @param array   an {@code Object array}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the sublist of matching elements.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @NotNull <T> List<T> sub(T @NotNull [] array, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(array)) {
            return new ArrayList<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        List<T> subList = new ArrayList<>();
        addAllMatches(subList, array, matcher);
        return subList;
    }

    /**
     * Returns a sublist of matching elements.
     *
     * @param array   an {@code Object array}. It can be {@code null}.
     * @param matcher a custom matcher that checks both index and element. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the sublist of matching elements.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    public static @NotNull <T> List<T> sub(T @NotNull [] array, @NotNull IndexPredicate<? super T> matcher) {
        if (isEmpty(array)) {
            return new ArrayList<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        List<T> subList = new ArrayList<>();
        addAllMatches(subList, array, matcher);
        return subList;
    }

    /**
     * Returns a subset of matching elements.
     *
     * @param set     a {@code Set}. It can be {@code null}.
     * @param matcher a custom matcher. It shall not be {@code null}.
     * @param <T>     the element type.
     * @return the subset of matching elements.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <T> Set<T> sub(@Nullable Set<T> set, @NotNull Predicate<? super T> matcher) {
        if (isEmpty(set)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<T> subSet = new HashSet<>();
        addAllMatches(subSet, set, matcher);
        return subSet;
    }

    /**
     * Returns a sub {@code Map} of matching elements.
     *
     * @param map     a {@code Map}. It can be {@code null}.
     * @param matcher a custom matcher that checks both key and value. It shall not be {@code null}.
     * @param <K>     the key type.
     * @param <V>     the value type.
     * @return the sub {@code Map} of matching elements.
     * @throws NullPointerException if {@code matcher} is {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <K, V> Map<K, V> sub(@Nullable Map<K, V> map, @NotNull BiPredicate<? super K, ? super V> matcher) {
        if (isEmpty(map)) {
            return new HashMap<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Map<K, V> subMap = new HashMap<>();
        addAllMatches(subMap, map, matcher);
        return subMap;
    }

    /**
     * Returns a sublist of matching keys.
     *
     * @param map        a {@code Map}. It can be {@code null}.
     * @param keyMatcher a custom matcher that checks keys. It shall not be {@code null}.
     * @param <K>        the key type.
     * @param <V>        the value type.
     * @return the sublist of matching keys.
     * @throws NullPointerException if {@code keyMatcher} is {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <K, V> List<K> subKeys(@Nullable Map<K, V> map, @NotNull Predicate<? super K> keyMatcher) {
        if (isEmpty(map)) {
            return new ArrayList<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        List<K> subList = new ArrayList<>();
        addAllMatches(subList, map.keySet(), keyMatcher);
        return subList;
    }

    /**
     * Returns a sublist of matching values.
     *
     * @param map          a {@code Map}. It can be {@code null}.
     * @param valueMatcher a custom matcher that checks values. It shall not be {@code null}.
     * @param <K>          the key type.
     * @param <V>          the value type.
     * @return the sublist of matching values.
     * @throws NullPointerException if {@code valueMatcher} is {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <K, V> List<V> subValues(@Nullable Map<K, V> map, @NotNull Predicate<? super V> valueMatcher) {
        if (isEmpty(map)) {
            return new ArrayList<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        List<V> subList = new ArrayList<>();
        addAllMatches(subList, map.values(), valueMatcher);
        return subList;
    }

    // forEach methods

    /**
     * Performs an action for each {@code Collection} element.
     *
     * @param collection a {@code Collection}. It can be {@code null}.
     * @param action     an action to perform for each element. It shall not be {@code null}.
     * @param <T>        the element type.
     * @throws NullPointerException if {@code action} is {@code null}.
     */
    public static <T> void forEach(@Nullable Collection<T> collection, @NotNull Consumer<? super T> action) {
        if (isEmpty(collection)) {
            return;
        }
        for (T obj : collection) {
            action.accept(obj);
        }
    }

    /**
     * Performs an action for each {@code Object array} element.
     *
     * @param array  an {@code Object array}. It can be {@code null}.
     * @param action an action to perform for each element. It shall not be {@code null}.
     * @param <T>    the element type.
     * @throws NullPointerException if {@code action} is {@code null}.
     */
    public static <T> void forEach(T @NotNull [] array, @NotNull Consumer<? super T> action) {
        if (isEmpty(array)) {
            return;
        }
        for (T obj : array) {
            action.accept(obj);
        }
    }

    /**
     * Performs an action for each {@code List} element.
     *
     * @param list   a {@code List}. It can be {@code null}.
     * @param action an action to perform for each element. It shall not be {@code null}.
     * @param <T>    the element type.
     * @throws NullPointerException if {@code action} is {@code null}.
     */
    public static <T> void forEach(@Nullable List<T> list, @NotNull IndexConsumer<? super T> action) {
        if (isEmpty(list)) {
            return;
        }
        for (int I = 0; I < list.size(); I++) {
            T o = list.get(I);
            action.accept(I, o);
        }
    }

    /**
     * Performs an action for each {@code Object array} element.
     *
     * @param array  an {@code Object array}. It can be {@code null}.
     * @param action an action to perform for each element. It shall not be {@code null}.
     * @param <T>    the element type.
     * @throws NullPointerException if {@code action} is {@code null}.
     */
    public static <T> void forEach(T @NotNull [] array, @NotNull IndexConsumer<? super T> action) {
        if (isEmpty(array)) {
            return;
        }
        for (int I = 0; I < array.length; I++) {
            T o = array[I];
            action.accept(I, o);
        }
    }

    /**
     * Performs an action for each {@code Map} entry.
     *
     * @param map    an {@code Map}. It can be {@code null}.
     * @param action an action to perform for each {@code Map} entry. It shall not be {@code null}.
     * @param <K>    the key type.
     * @param <V>    the value type.
     * @throws NullPointerException if {@code action} is {@code null}.
     */
    public static <K, V> void forEach(@Nullable Map<K, V> map, @NotNull BiConsumer<? super K, ? super V> action) {
        if (isEmpty(map)) {
            return;
        }
        for (Map.Entry<K, V> entry : map.entrySet()) {
            action.accept(entry.getKey(), entry.getValue());
        }
    }

    private CollectionUtil() {
        //DO NOTHING.
    }
}
