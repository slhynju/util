package name.slhynju.util.collection.graph.directed;

import name.slhynju.util.collection.graph.GraphSupport;
import org.jetbrains.annotations.Nullable;

/**
 * Default implementation of {@code DirectedGraph}.
 *
 * @param <N> the node type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings("CanBeFinal")
public class DirectedGraphSupport<N extends DirectedGraphNode<N, E>, E extends DirectedGraphEdge<N, E>>
        extends GraphSupport<N, E> implements DirectedGraph<N, E> {

    @SuppressWarnings("MethodWithMultipleLoops")
    @Override
    public void removeNode(@Nullable N node) {
        if (node == null) {
            return;
        }
        super.removeNode(node);
        for (E in : node.getInEdges()) {
            in.getTail().removeOutEdge(in);
            removeEdge(in);
        }
        for (E out : node.getOutEdges()) {
            out.getHead().removeInEdge(out);
            removeEdge(out);
        }
    }

}
