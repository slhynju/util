package name.slhynju.util.collection.graph.directed;

import name.slhynju.util.EqualsUtil;
import name.slhynju.util.collection.CollectionUtil;
import name.slhynju.util.collection.graph.GraphNodeSupport;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Default implementation of {@code DirectedGraphNode}.
 *
 * @param <N> the node type that extends this type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings({"EqualsAndHashcode", "CanBeFinal"})
public class DirectedGraphNodeSupport<N extends DirectedGraphNodeSupport<N, E>, E extends DirectedGraphEdge<N, E>>
        extends GraphNodeSupport implements DirectedGraphNode<N, E> {

    /**
     * The incoming edges.
     */
    protected final @NotNull List<E> inEdges = new ArrayList<>(4);

    /**
     * The outgoing edges.
     */
    protected final @NotNull List<E> outEdges = new ArrayList<>(4);

    /**
     * Creates a new node.
     *
     * @param name a node name. It shall not be {@code null}.
     */
    public DirectedGraphNodeSupport(@NotNull String name) {
        super(name);
    }

    // incoming edge methods

    @Override
    public @NotNull List<E> getInEdges() {
        return Collections.unmodifiableList(inEdges);
    }

    @Override
    public void addInEdge(@NotNull E edge) {
        inEdges.add(edge);
    }

    @Override
    public void removeInEdge(@Nullable E edge) {
        inEdges.remove(edge);
    }

    @Override
    public @NotNull List<N> getDirectPredecessors() {
        return CollectionUtil.transformToList(inEdges, DirectedGraphEdge::getTail);
    }

    @Override
    public @Nullable N getDirectPredecessor(@NotNull String nodeName) {
        for (E in : inEdges) {
            N tail = in.getTail();
            //noinspection ObjectsEqualsCanBeSimplified
            if (Objects.equals(nodeName, tail.getName())) {
                return tail;
            }
        }
        return null;
    }

    @Override
    public int getInDegree() {
        return inEdges.size();
    }

    // outgoing edge methods

    @Override
    public @NotNull List<E> getOutEdges() {
        return Collections.unmodifiableList(outEdges);
    }

    @Override
    public void addOutEdge(@NotNull E edge) {
        outEdges.add(edge);
    }

    @Override
    public void removeOutEdge(@Nullable E edge) {
        outEdges.remove(edge);
    }

    @Override
    public @NotNull List<N> getDirectSuccessors() {
        return CollectionUtil.transformToList(outEdges, DirectedGraphEdge::getHead);
    }

    @Override
    public @Nullable N getDirectSuccessor(@NotNull String nodeName) {
        for (E out : outEdges) {
            N head = out.getHead();
            //noinspection ObjectsEqualsCanBeSimplified
            if (Objects.equals(nodeName, head.getName())) {
                return head;
            }
        }
        return null;
    }

    @Override
    public int getOutDegree() {
        return outEdges.size();
    }

    @SuppressWarnings({"rawtypes", "CanBeFinal"})
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof DirectedGraphNodeSupport) {
            DirectedGraphNodeSupport other = (DirectedGraphNodeSupport) obj;
            return EqualsUtil.isEquals(name, other.name);
        }
        return false;
    }

}
