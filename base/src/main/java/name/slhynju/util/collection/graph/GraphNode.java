package name.slhynju.util.collection.graph;

import name.slhynju.util.Nameable;
import name.slhynju.util.collection.AttributeOwner;

/**
 * A marker interface of graph node.
 *
 * @author slhynju
 */
public interface GraphNode extends AttributeOwner, Nameable {

    // DO NOTHING
}
