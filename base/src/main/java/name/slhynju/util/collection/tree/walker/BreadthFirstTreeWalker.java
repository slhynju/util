package name.slhynju.util.collection.tree.walker;

import name.slhynju.util.collection.tree.Tree;
import name.slhynju.util.collection.tree.TreeNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Walk a general tree with breadth first strategy.
 *
 * @param <N> the tree node type
 * @author slhynju
 */
@SuppressWarnings("rawtypes")
public final class BreadthFirstTreeWalker<N extends TreeNode> extends TreeWalkerSupport<N> {

    /**
     * The nodes that are pending processing.
     */
    @SuppressWarnings("CollectionWithoutInitialCapacity")
    private final @NotNull Queue<N> pendingNodes = new ArrayDeque<>();

    /**
     * The nodes that are already visited.
     */
    @SuppressWarnings("CollectionWithoutInitialCapacity")
    private final @NotNull Set<N> visitedNodes = new HashSet<>();

    @Override
    @SuppressWarnings("BoundedWildcard")
    protected @Nullable N getStartNode(@NotNull Tree<N> tree) {
        return tree.getRoot();
    }

    @Override
    protected @Nullable N getNextNode(@NotNull N node) {
        return pendingNodes.poll();
    }

    @Override
    protected void process(@NotNull N node, @NotNull Consumer<? super N> action) {
        if (visitedNodes.contains(node)) {
            return;
        }
        action.accept(node);
        visitedNodes.add(node);
        pendingNodes.addAll(node.getChildren());
    }

    /**
     * Process a state tree or graph in breadth-first strategy.
     *
     * @param startState the initial state. It can be {@code null}.
     * @param expander   a custom factory to generate further states from current state. It shall not be {@code null}.
     * @param action     a custom action to perform for each state. It shall not be {@code null}.
     * @param <T>        the state type.
     * @throws NullPointerException if {@code expander} or {@code action} is {@code null}.
     */
    @SuppressWarnings({"BoundedWildcard", "CollectionWithoutInitialCapacity"})
    public static <T> void walk(@Nullable T startState, @NotNull Function<? super T, List<T>> expander, @NotNull Consumer<? super T> action) {
        Queue<T> pendingNodes = new ArrayDeque<>();
        Set<T> visitedNodes = new HashSet<>();
        T next = startState;
        while (next != null) {
            if (!visitedNodes.contains(next)) {
                action.accept(next);
                visitedNodes.add(next);
                //noinspection ConstantConditions
                pendingNodes.addAll(expander.apply(next));
            }
            next = pendingNodes.poll();
        }
    }
}
