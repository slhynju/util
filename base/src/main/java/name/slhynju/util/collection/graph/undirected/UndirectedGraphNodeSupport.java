package name.slhynju.util.collection.graph.undirected;

import name.slhynju.util.EqualsUtil;
import name.slhynju.util.collection.CollectionUtil;
import name.slhynju.util.collection.graph.GraphNodeSupport;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Default implementation of {@code UndirectedGraphNode}.
 *
 * @param <N> the node type that extends this type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings({"EqualsAndHashcode", "CanBeFinal"})
public class UndirectedGraphNodeSupport<N extends UndirectedGraphNodeSupport<N, E>, E extends UndirectedGraphEdge<N, E>>
        extends GraphNodeSupport implements UndirectedGraphNode<N, E> {

    /**
     * The edges.
     */
    protected final @NotNull List<E> edges = new ArrayList<>(4);

    /**
     * Creates a new node.
     *
     * @param name a node name. It shall not be {@code null}.
     */
    public UndirectedGraphNodeSupport(@NotNull String name) {
        super(name);
    }

    @Override
    public @NotNull List<E> getEdges() {
        return Collections.unmodifiableList(edges);
    }

    @Override
    public void addEdge(@NotNull E edge) {
        edges.add(edge);
    }

    @Override
    public void removeEdge(@Nullable E edge) {
        if (edge == null) {
            return;
        }
        edges.remove(edge);
    }

    @Override
    public @NotNull List<N> getAdjacentNodes() {
        return CollectionUtil.transformToList(edges, edge -> edge.getOtherEnd((N) this));
    }

    @Override
    public int getDegree() {
        return edges.size();
    }

    @SuppressWarnings({"rawtypes", "CanBeFinal"})
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof UndirectedGraphNodeSupport) {
            UndirectedGraphNodeSupport other = (UndirectedGraphNodeSupport) obj;
            return EqualsUtil.isEquals(name, other.name);
        }
        return false;
    }
}
