package name.slhynju.util.collection;

import org.jetbrains.annotations.Nullable;

/**
 * A {@code Consumer} that accepts {@code array} or {@code List} index.
 *
 * @param <T> the {@code array} or {@code List} element type.
 * @author slhynju
 */
@FunctionalInterface
public interface IndexConsumer<T> {

    /**
     * Accepts a pair of index and element value.
     *
     * @param index the index of the element.
     * @param value the element value. It can be {@code null}.
     */
    void accept(int index, @Nullable T value);

}
