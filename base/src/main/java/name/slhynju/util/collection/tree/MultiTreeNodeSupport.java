package name.slhynju.util.collection.tree;

import name.slhynju.util.EqualsUtil;
import name.slhynju.util.collection.CollectionUtil;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@code MultiTreeNode}.
 *
 * @param <T> the node type that extends this type.
 * @author slhynju
 */
@SuppressWarnings({"rawtypes", "FrequentlyUsedInheritorInspection", "EqualsAndHashcode", "CanBeFinal"})
public class MultiTreeNodeSupport<T extends MultiTreeNodeSupport> extends TreeNodeSupport<T>
        implements MultiTreeNode<T> {

    /**
     * The children nodes.
     */
    protected final @NotNull List<T> children = new ArrayList<>(4);

    /**
     * Creates a new multi-tree node.
     *
     * @param name a node name. It shall not be {@code null}.
     */
    public MultiTreeNodeSupport(@NotNull String name) {
        super(name);
    }

    @Override
    public @NotNull List<T> getChildren() {
        return children;
    }

    @Override
    public int indexOf(@Nullable T child) {
        return child == null ? -1 : children.indexOf(child);
    }

    @Override
    public @Nullable T getChild(int index) {
        return CollectionUtil.getElement(children, index);
    }

    @Override
    public void setChild(int index, @NotNull T child) {
        if (index < 0 || index > children.size()) {
            @NonNls String message = "Illegal index " + index + '.';
            throw new IllegalArgumentException(message);
        }
        if (index == children.size()) {
            children.add(child);
            return;
        }
        children.set(index, child);
    }

    @Override
    public @Nullable T removeChild(int index) {
        return children.remove(index);
    }

    @Override
    public void addChild(@NotNull T child) {
        children.add(child);
    }

    @Override
    public @Nullable T getPrevSibling() {
        if (parent == null) {
            return null;
        }
        //noinspection AccessingNonPublicFieldOfAnotherObject
        List<T> siblings = parent.children;
        //noinspection SuspiciousMethodCalls
        int index = siblings.indexOf(this);
        return index == 0 ? null : siblings.get(index - 1);
    }

    @Override
    public @Nullable T getNextSibling() {
        if (parent == null) {
            return null;
        }
        //noinspection AccessingNonPublicFieldOfAnotherObject
        List<T> siblings = parent.children;
        //noinspection SuspiciousMethodCalls
        int index = siblings.indexOf(this);
        return index == siblings.size() - 1 ? null : siblings.get(index + 1);
    }

    @SuppressWarnings({"rawtypes", "CanBeFinal"})
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MultiTreeNodeSupport) {
            MultiTreeNodeSupport other = (MultiTreeNodeSupport) obj;
            return EqualsUtil.isEquals(name, other.name);
        }
        return false;
    }


}
