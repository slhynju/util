package name.slhynju.util.collection.graph;

import name.slhynju.util.collection.AttributeOwner;

/**
 * A marker interface of graph edge.
 *
 * @author slhynju
 */
@SuppressWarnings("FrequentlyUsedInheritorInspection")
public interface GraphEdge extends AttributeOwner {

    // DO NOTHING
}
