package name.slhynju.util.collection.tree;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A multi-tree node.
 *
 * @param <T> the node type that extends this type.
 * @author slhynju
 */
@SuppressWarnings({"rawtypes", "FrequentlyUsedInheritorInspection"})
public interface MultiTreeNode<T extends MultiTreeNode> extends TreeNode<T> {

    /**
     * Returns the child at the specified index.
     *
     * @param index an index.
     * @return the child at the specified index. Returns null if not found.
     */
    @Nullable @Nullable T getChild(int index);

    /**
     * Sets the child at specified index.
     *
     * @param index an index. It shall be a valid one, or equals to the current children count.
     * @param child a tree node as a child. It shall not be {@code null}.
     * @throws IllegalArgumentException if {@code index} is invalid.
     */
    void setChild(int index, @NotNull T child);

    /**
     * Appends a child.
     *
     * @param child a tree node to be appended as a child. It shall not be {@code null}.
     */
    void addChild(@NotNull T child);

    /**
     * Removes a child.
     *
     * @param index an index. It shall be a valid one.
     * @return the previous child at the specified index.
     * @throws IndexOutOfBoundsException if {@code index} is invalid.
     */
    @Nullable @Nullable T removeChild(int index);

}
