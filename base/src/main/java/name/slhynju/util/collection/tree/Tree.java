package name.slhynju.util.collection.tree;

import org.jetbrains.annotations.Nullable;

/**
 * A general tree. It could be a binary tree, or a multi-tree, depending on the node type.
 *
 * @param <N> the node type.
 * @author slhynju
 */
@SuppressWarnings("rawtypes")
public interface Tree<N extends TreeNode> {

    /**
     * Returns the root node.
     *
     * @return the root node.
     */
    @Nullable @Nullable N getRoot();

    /**
     * Sets the root node.
     *
     * @param root the root node. It can be {@code null}.
     */
    void setRoot(@Nullable N root);

    /**
     * Checks whether this tree is empty.
     *
     * @return {@code true} -- is empty; {@code false} -- not empty.
     */
    boolean isEmpty();
}
