/**
 * Utility classes for {@code arrays}, {@code Collections} and {@code Maps}.
 *
 * @author slhynju
 */
package name.slhynju.util.collection;