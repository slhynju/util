package name.slhynju.util.collection.graph.directed;

import name.slhynju.util.collection.graph.Graph;
import org.jetbrains.annotations.NotNull;

import java.util.function.BiFunction;

/**
 * A directed graph.
 *
 * @param <N> the node type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings("FrequentlyUsedInheritorInspection")
public interface DirectedGraph<N extends DirectedGraphNode<N, E>, E extends DirectedGraphEdge<N, E>>
        extends Graph<N, E> {

    /**
     * Adds a directed edge if not available.
     *
     * @param tail        a tail node. It shall not be {@code null}.
     * @param head        a head node. It shall not be {@code null}.
     * @param edgeCreator a custom edge creator to create a new edge with two nodes. It shall not be {@code null}.
     * @return the directed edge.
     * @throws NullPointerException if {@code tail}, {@code head} or {@code edgeCreator} is null.
     */
    @SuppressWarnings("ConstantConditions")
    default @NotNull E linkIfAbsent(@NotNull N tail, @NotNull N head, @NotNull BiFunction<? super N, ? super N, ? extends E> edgeCreator) {
        for (E out : tail.getOutEdges()) {
            //noinspection ObjectEquality
            if (head == out.getHead()) {
                return out;
            }
        }
        E edge = edgeCreator.apply(tail, head);
        tail.addOutEdge(edge);
        head.addInEdge(edge);
        addEdge(edge);
        return edge;
    }
}

