package name.slhynju.util.collection.tree;

import name.slhynju.util.Nameable;
import name.slhynju.util.collection.AttributeOwner;
import name.slhynju.util.collection.CollectionUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * A general tree node.
 *
 * @param <T> the node type that extends this type.
 * @author slhynju
 */
@SuppressWarnings({"ClassWithTooManyDependents", "rawtypes"})
public interface TreeNode<T extends TreeNode> extends AttributeOwner, Nameable {

    /**
     * Returns the parent node if available.
     *
     * @return the parent node. Returns null if not found.
     */
    @Nullable @Nullable T getParent();

    /**
     * Sets the parent node.
     *
     * @param parent the parent node. It can be {@code null}.
     */
    void setParent(@Nullable T parent);

    /**
     * Checks whether this node is a root node.
     *
     * @return {@code true} -- is root; {@code false} -- not root.
     */
    boolean isRoot();

    /**
     * Returns all the children nodes.
     *
     * @return a {@code List} of children nodes.
     */
    @NotNull List<T> getChildren();

    /**
     * Finds the index of a child.
     *
     * @param child a child node. It can be {@code null}.
     * @return the index of given node. Returns -1 if {@code child} is null or not found.
     */
    int indexOf(@Nullable T child);

    /**
     * Checks whether this node is a leaf node.
     *
     * @return {@code true} -- is leaf; {@code false} -- not leaf.
     */
    default boolean isLeaf() {
        return CollectionUtil.isEmpty(getChildren());
    }

    /**
     * Checks whether this node has children nodes.
     *
     * @return {@code true} -- has children; {@code false} -- no children.
     */
    default boolean hasChildren() {
        return !isLeaf();
    }

    /**
     * Checks whether this node is the first child of its parent.
     *
     * @return {@code true} -- is the first child; {@code false} -- not the first child.
     */
    boolean isFirstChild();

    /**
     * Checks whether this node is the last child of its parent.
     *
     * @return {@code true} -- is the last child; {@code false} -- not the last child.
     */
    boolean isLastChild();

    /**
     * Returns the first child.
     *
     * @return the first child. Returns null if not found.
     */
    default @Nullable @Nullable T getFirstChild() {
        return CollectionUtil.getFirst(getChildren());
    }

    /**
     * Returns the last child.
     *
     * @return the last child. Returns null if not found.
     */
    default @Nullable @Nullable T getLastChild() {
        return CollectionUtil.getLast(getChildren());
    }

    /**
     * Returns the previous sibling node.
     *
     * @return the previous sibling node. Returns null if not found.
     */
    @Nullable @Nullable T getPrevSibling();

    /**
     * Returns the next sibling node.
     *
     * @return the next sibling ndoe. Returns null if not found.
     */
    @Nullable @Nullable T getNextSibling();

}
