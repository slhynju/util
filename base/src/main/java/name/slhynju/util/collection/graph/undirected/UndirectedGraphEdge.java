package name.slhynju.util.collection.graph.undirected;

import name.slhynju.util.collection.graph.GraphEdge;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * A undirected graph edge.
 *
 * @param <N> the node type.
 * @param <E> the edge type that extends this type.
 * @author slhynju
 */
@SuppressWarnings({"CyclicClassDependency", "FrequentlyUsedInheritorInspection"})
public interface UndirectedGraphEdge<N extends UndirectedGraphNode<N, E>, E extends UndirectedGraphEdge<N, E>>
        extends GraphEdge {

    /**
     * Returns the nodes.
     *
     * @return a length-1 or length2 {@code List} of nodes.
     */
    @NotNull List<N> getNodes();

    /**
     * Returns the nodes.
     *
     * @return a length-1 or length-2 array of nodes.
     */
    @SuppressWarnings("rawtypes")
    @NotNull UndirectedGraphNode[] getNodeArray();

    /**
     * Returns the other end node.
     *
     * @param oneEnd a node. It shall be one end of this edge.
     * @return the other end.
     * @throws IllegalArgumentException if {@code oneEnd} is not an end of this edge.
     */
    @NotNull N getOtherEnd(@NotNull N oneEnd);

    /**
     * Checks whether this edge contains a node.
     *
     * @param oneEnd a node. It can be {@code null}.
     * @return {@code true} -- contains the node; {@code false} does not contain the node.
     */
    boolean containsEnd(@Nullable N oneEnd);

    /**
     * Checks whether this edge contains given nodes.
     *
     * @param end1 a node. It can be {@code null}.
     * @param end2 another node. It can be {@code null}.
     * @return {@code true} -- contains these two nodes; {@code false} -- does not contain these two nodes.
     */
    boolean containsEnds(@Nullable N end1, @Nullable N end2);
}
