package name.slhynju.util.collection.graph.undirected;

import name.slhynju.util.collection.graph.GraphNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * A undirected graph node.
 *
 * @param <N> the node type that extends this type.
 * @param <E> the edge type.
 * @author slhynju
 */
@SuppressWarnings({"CyclicClassDependency", "FrequentlyUsedInheritorInspection"})
public interface UndirectedGraphNode<N extends UndirectedGraphNode<N, E>, E extends UndirectedGraphEdge<N, E>>
        extends GraphNode {

    /**
     * Returns all the edges.
     *
     * @return a {@code List} of edges.
     */
    @NotNull List<E> getEdges();

    /**
     * Adds an edge.
     *
     * @param edge an edge. It shall not be {@code null}.
     */
    void addEdge(@NotNull E edge);

    /**
     * Removes an edge.
     *
     * @param edge an edge. It can be {@code null}.
     */
    void removeEdge(@Nullable E edge);

    /**
     * Returns all the adjacent nodes.
     *
     * @return a {@code List} of adjacent nodes.
     */
    @NotNull List<N> getAdjacentNodes();

    /**
     * Returns the degree.
     *
     * @return the degree.
     */
    int getDegree();

}
