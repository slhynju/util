package name.slhynju.util.collection.tree.walker;

import name.slhynju.util.collection.tree.BinaryTreeNode;
import name.slhynju.util.collection.tree.Tree;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Walk a binary tree in in-order.
 *
 * @param <N> the binary tree node type.
 * @author slhynju
 */
public final class InOrderBinaryTreeWalker<N extends BinaryTreeNode<N>> extends TreeWalkerSupport<N> {

    @Override
    @SuppressWarnings("BoundedWildcard")
    protected @Nullable N getStartNode(@NotNull Tree<N> tree) {
        N p = tree.getRoot();
        if (p == null) {
            return null;
        }
        while (p.getLeftChild() != null) {
            p = p.getLeftChild();
        }
        return p;
    }

    @SuppressWarnings({"MethodWithMultipleLoops", "AssignmentToMethodParameter"})
    @Override
    protected @Nullable N getNextNode(@NotNull N node) {
        if (node.getRightChild() != null) {
            node = node.getRightChild();
            while (node.getLeftChild() != null) {
                node = node.getLeftChild();
            }
            return node;
        }
        //noinspection ConstantConditions
        while (node.isRightChild()) {
            node = node.getParent();
        }
        // if node is a left child, the next node is its parent.
        // if node is the root node, the next node is null, which is same to the getParent() result.
        return node.getParent();
    }
}
