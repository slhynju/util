package name.slhynju.util.collection.tree.walker;

import name.slhynju.util.collection.tree.Tree;
import name.slhynju.util.collection.tree.TreeNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Walk a general tree in post order.
 *
 * @param <N> the tree node type.
 * @author slhynju
 */
public final class PostOrderTreeWalker<N extends TreeNode<N>> extends TreeWalkerSupport<N> {

    @Override
    @SuppressWarnings("BoundedWildcard")
    protected @Nullable N getStartNode(@NotNull Tree<N> tree) {
        N p = tree.getRoot();
        if (p == null) {
            return null;
        }
        //noinspection ConstantConditions
        while (p.hasChildren()) {
            p = p.getFirstChild();
        }
        return p;
    }

    @Override
    @SuppressWarnings("AssignmentToMethodParameter")
    protected @Nullable N getNextNode(@NotNull N node) {
        if (node.isLastChild()) {
            return node.getParent();
        }
        if (node.isRoot()) {
            return null;
        }
        node = node.getNextSibling();
        //noinspection ConstantConditions
        while (node.hasChildren()) {
            node = node.getFirstChild();
        }
        return node;
    }
}
