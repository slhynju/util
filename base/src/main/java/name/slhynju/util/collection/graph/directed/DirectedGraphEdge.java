package name.slhynju.util.collection.graph.directed;

import name.slhynju.util.collection.graph.GraphEdge;
import org.jetbrains.annotations.NotNull;

/**
 * A directed edge.
 *
 * @param <N> the node type.
 * @param <E> the edge type that extends this type.
 * @author slhynju
 */
@SuppressWarnings({"CyclicClassDependency", "FrequentlyUsedInheritorInspection"})
public interface DirectedGraphEdge<N extends DirectedGraphNode<N, E>, E extends DirectedGraphEdge<N, E>> extends GraphEdge {

    /**
     * Returns the edge head.
     *
     * @return the node of edge head.
     */
    @NotNull N getHead();

    /**
     * Returns the edge tail.
     *
     * @return the node of edge tail.
     */
    @NotNull N getTail();

}
