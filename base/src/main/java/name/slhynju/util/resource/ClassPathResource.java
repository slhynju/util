package name.slhynju.util.resource;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author slhynju
 */
public class ClassPathResource extends ResourceSupport {

    public ClassPathResource(@NotNull String path) {
        super(path);
    }

    @Override
    public @Nullable InputStream open() throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader.getResourceAsStream(path);
    }

}
