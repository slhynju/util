package name.slhynju.util.resource;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * @author slhynju
 */
public abstract class ByteArrayResource implements Resource {

    private ByteBuffer buffer;

    public ByteArrayResource(ByteBuffer buffer) {
        this.buffer = buffer;
    }

    public @NotNull InputStream open() {
        return new ByteArrayInputStream(buffer.array(), 0, buffer.limit());
    }
}
