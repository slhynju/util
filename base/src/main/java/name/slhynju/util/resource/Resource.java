package name.slhynju.util.resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * @author slhynju
 */
public interface Resource {

    public String getPath();

    public InputStream open() throws IOException;

    public BufferedReader openReader() throws IOException;

    public BufferedReader openReader(Charset charset) throws IOException;

}
