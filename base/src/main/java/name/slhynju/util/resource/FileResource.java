package name.slhynju.util.resource;

import name.slhynju.util.nio.PathUtil;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * @author slhynju
 */
public class FileResource extends ResourceSupport {

    public FileResource(@NotNull String path) {
        super(path);
    }

    @Override
    public @NotNull InputStream open() throws IOException {
        return new BufferedInputStream(new FileInputStream(path));
    }

    @Override
    public @NotNull BufferedReader openReader(@NotNull Charset charset) throws IOException {
        return new BufferedReader(new InputStreamReader(new FileInputStream(path), charset));
    }

    public FileChannel openChannel() throws IOException {
        Path path = PathUtil.toPath(this.path);
        return FileChannel.open(path, StandardOpenOption.READ);
    }
}
