package name.slhynju.util.resource;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author slhynju
 */
public abstract class ResourceSupport implements Resource {

    protected final @NotNull String path;

    public ResourceSupport(@NotNull String path) {
        this.path = path;
    }

    @Override
    public @NotNull String getPath() {
        return path;
    }

    @Override
    public abstract InputStream open() throws IOException;

    @Override
    public @NotNull BufferedReader openReader() throws IOException {
        return openReader(StandardCharsets.UTF_8);
    }

    @Override
    public @NotNull BufferedReader openReader(@NotNull Charset charset) throws IOException {
        return new BufferedReader(new InputStreamReader(open(), charset));
    }

}
