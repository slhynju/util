package name.slhynju.util.io;

import name.slhynju.util.EqualsUtil;
import name.slhynju.util.FreeStringBuilder;
import name.slhynju.util.HashCodeBuilder;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * A container that stores internationalization texts in key value pairs.
 *
 * @author slhynju
 */
public final class I18NResource {

    /**
     * The key-value pairs of texts.
     */
    private final @NotNull Map<String, String> texts;

    /**
     * Creates a new I18NResource instance.
     * <p>This constructor is reserved by {@code I18NResourceFactory}.</p>
     *
     * @param texts internationalization texts in key value pairs. It shall not be {@code null}.
     */
    I18NResource(@NotNull Map<String, String> texts) {
        //no copy for performance reason.
        this.texts = texts;
    }

    /**
     * Returns the text associated with the key.
     *
     * @param key a key {@code String}. It shall not be {@code null}.
     * @return the text. Returns the key itself if not found.
     */
    public @NotNull String get(@NonNls @NotNull String key) {
        //noinspection AssignmentToMethodParameter
        key = key.toLowerCase();
        String value = texts.get(key);
        return value == null ? key : value;
    }

    /**
     * Finds a template associated with the key and fills it with parameter values.
     *
     * @param key    a key {@code String} which is associated with a {@code String} template. It shall not be {@code null}.
     * @param values the parameter values.
     * @return the formatted text.
     * @throws IllegalArgumentException if the template associated with the key is invalid.
     * @see FreeStringBuilder#format(String, Object...)
     */
    public @NotNull String format(@NotNull String key, @NotNull Object... values) {
        String format = get(key);
        return FreeStringBuilder.format(format, values);
    }

    /**
     * Returns the text associated with multiple parts of key.
     *
     * @param keys multiple parts of key. It shall not be {@code null}.
     * @return the text. Returns the full key itself if not found.
     */
    public @NotNull String get(@NotNull String... keys) {
        String fullKey = FreeStringBuilder.toS(keys, ".");
        return get(fullKey);
    }

    /**
     * Returns the text associated with an {@code Enum}. The {@code Enum} object is treated as a two-part key of its class and its value.
     *
     * @param key an {@code Enum}. It shall not be {@code null}.
     * @param <T> the {@code Enum} type.
     * @return the text.
     */
    public @NotNull <T extends Enum<T>> String get(@NotNull T key) {
        //noinspection ConstantConditions
        @NonNls String fullKey = key.getClass().getSimpleName() + '.' + key.name();
        return get(fullKey);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof I18NResource) {
            I18NResource other = (I18NResource) obj;
            return EqualsUtil.isEquals(texts, other.texts);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(texts).toValue();
    }

}
