/**
 * Utility classes of IO operations.
 *
 * @author slhynju
 */
package name.slhynju.util.io;