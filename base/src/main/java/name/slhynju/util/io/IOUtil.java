package name.slhynju.util.io;

import name.slhynju.ApplicationException;
import name.slhynju.util.StringUtil;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Utility class of various IO operations.
 * <p>This class shall only depend on other utility classes in package name.slhynju.util (subpackages excluded).</p>
 *
 * @author slhynju
 */
public final class IOUtil {

    /**
     * An empty byte array.
     * <p>define this constant so that this class does not depend on {@code CollectionUtil}.</p>
     */
    private static final @NotNull byte[] EMPTY_BYTE_ARRAY = new byte[0];

    /**
     * Opens an InputStream with specified resource path.
     *
     * @param pathName a path. It shall not be {@code null}.
     * @return the resource {@code InputStream} if found.
     * @throws ApplicationException if the resource is not found.
     */
    public static @NotNull InputStream openResourceAsStream(@NotNull String pathName) {
        InputStream in = getResourceAsStream(pathName);
        if (in == null) {
            @NonNls String message = "Cannot find resource " + pathName + " in context ClassLoader.";
            throw new ApplicationException(message);
        }
        return in;
    }

    /**
     * Opens an InputStream with specified resource path. This method does not throw exceptions if the resource is not found.
     *
     * @param pathName a path. It shall not be {@code null}.
     * @return the resource {@code InputStream} if found. Returns null if not found.
     */
    public static @Nullable InputStream getResourceAsStream(@NotNull String pathName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        //noinspection ConstantConditions
        return classLoader.getResourceAsStream(pathName);
    }

    /**
     * Safely closes a {@code Reader}.
     *
     * @param reader a {@code Reader}. It can be {@code null}.
     * @param log    a {@code Logger}. It shall not be {@code null}.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static void close(@Nullable Reader reader, @NotNull Logger log) {
        close(reader, "Failed to close Reader.", log);
    }

    /**
     * Safely closes a {@code Writer}.
     *
     * @param writer a {@code Writer}. It can be {@code null}.
     * @param log    a {@code Logger}. It shall not be {@code null}.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static void close(@Nullable Writer writer, @NotNull Logger log) {
        close(writer, "Failed to close Writer.", log);
    }

    /**
     * Safely closes an {@code InputStream}.
     *
     * @param inputStream an {@code InputStream}. It can be {@code null}.
     * @param log         a {@code Logger}. It shall not be {@code null}.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static void close(@Nullable InputStream inputStream, @NotNull Logger log) {
        close(inputStream, "Failed to close InputStream.", log);
    }

    /**
     * Safely closes a {@code Closeable} resource.
     *
     * @param closeable    a {@code Closeable} resource. It can be {@code null}.
     * @param errorMessage an error message to use in the logs if failed. The log level is set to WARN. It shall not be {@code null}.
     * @param log          a {@code Logger}. It shall not be {@code null}.
     */
    public static void close(@Nullable Closeable closeable, @NotNull String errorMessage, @NotNull Logger log) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (IOException e) {
            if (log.isWarnEnabled()) {
                log.warn(errorMessage, e);
            }
        }
    }

    /**
     * Loads a small property file with UTF-8 encoding, and auto closes it after reading.
     *
     * @param in an {@code InputStream}. It can be {@code null}.
     * @return the parsed properties.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull Map<String, String> readSmallPropertiesFile(@Nullable InputStream in) {
        //noinspection ConstantConditions
        return readSmallPropertiesFile(in, StandardCharsets.UTF_8);
    }

    /**
     * Loads a small property file with specified encoding, and auto closes it after reading.
     *
     * @param in      an {@code InputStream}. It can be {@code null}.
     * @param charset the encoding. It shall not be {@code null}.
     * @return the parsed properties.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull Map<String, String> readSmallPropertiesFile(@Nullable InputStream in, @NotNull Charset charset) {
        List<String> rawLines = readAllRawLines(in, charset);
        return buildProperties(rawLines);
    }

    /**
     * Reads all lines of a small property file with specified encoding, and auto closes it after reading.
     *
     * @param in      an {@code InputStream}. It can be {@code null}.
     * @param charset the encoding. It shall not be {@code null}.
     * @return all the lines, including comments and empty ones.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    private static @NotNull List<String> readAllRawLines(@Nullable InputStream in, @NotNull Charset charset) {
        if (in == null) {
            return Collections.emptyList();
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, charset))) {
            //noinspection CollectionWithoutInitialCapacity
            List<String> lines = new ArrayList<>();
            String line = reader.readLine();
            while (line != null) {
                lines.add(line);
                line = reader.readLine();
            }
            return lines;
        } catch (IOException e) {
            @NonNls String message = "IOException when reading InputStream with Charset " + charset.name() + '.';
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Parses the raw lines into a property {@code Map}.
     *
     * @param rawLines the raw lines. It can be {@code null}.
     * @return the parsed properties.
     */
    public static @NotNull Map<String, String> buildProperties(@Nullable List<String> rawLines) {
        if (isEmpty(rawLines)) {
            return Collections.emptyMap();
        }
        //noinspection CollectionWithoutInitialCapacity
        Map<String, String> map = new HashMap<>();
        for (String rawLine : rawLines) {
            String line = StringUtil.trim(rawLine);
            if (line.isEmpty()) {
                continue;
            }
            if (line.startsWith("#")) {
                continue;
            }
            String[] pair = StringUtil.splitPair(line, "=");
            map.put(pair[0], pair[1]);
        }
        return map;
    }

    /**
     * Reads all bytes of a small file, and auto closes it after reading.
     *
     * @param in     an {@code InputStream}. It can be {@code null}.
     * @param buffer a buffer large enough to store all the bytes. It shall not be {@code null}.
     * @param log    a {@code Logger}. It shall not be {@code null}.
     * @return all the bytes in a {@code byte array}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    @SuppressWarnings("TooBroadScope")
    public static @NotNull byte[] readAllBytes(@Nullable InputStream in, @NotNull byte @NotNull [] buffer, @NotNull Logger log) {
        if (in == null) {
            return EMPTY_BYTE_ARRAY;
        }
        int position = 0;
        int length = 0;
        try {
            while (length != -1) {
                length = in.read(buffer, position, buffer.length - position);
                if (length > -1) {
                    position += length;
                }
            }
        } catch (IOException e) {
            throw new ApplicationException("Failed to read InputStream.", e);
        } finally {
            close(in, log);
        }
        return Arrays.copyOf(buffer, position);
    }

    /**
     * Reads all bytes of a small file, and auto closes it after reading.
     *
     * @param in     an {@code InputStream}. It can be {@code null}.
     * @param sizeKB a buffer size in KB which is large enough to store all the bytes.
     * @param log    a {@code Logger}. It shall not be {@code null}.
     * @return all the bytes in a {@code byte array}.
     * @throws ApplicationException if {@code IOException} occurs.
     */
    public static @NotNull byte[] readAllBytes(@Nullable InputStream in, int sizeKB, @NotNull Logger log) {
        //noinspection MagicNumber
        byte[] buffer = new byte[sizeKB * 1024];
        return readAllBytes(in, buffer, log);
    }

    /**
     * Checks whether a {@code Collection} is {@code null} or empty.
     * <p>Defines this method so that this class does not depend on {@code CollectionUtil}.</p>
     *
     * @param collection a {@code Collection}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    private static boolean isEmpty(@Nullable Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    private IOUtil() {
        //DO NOTHING.
    }

}
