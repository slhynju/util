package name.slhynju.util.io;

import name.slhynju.ApplicationException;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * A factory to build {@code I18NResource} from resource files.
 *
 * @author slhynju
 */
public final class I18NResourceFactory {

    /**
     * Fallback to en_US if no resource files can be found for other locales.
     */
    @SuppressWarnings("ConstantConditions")
    private static final @NotNull Locale FALLBACK_LOCALE = Locale.US;

    /**
     * Default locale to use. Application is responsible to set this in initialization code.
     */
    @SuppressWarnings("ConstantConditions")
    private static volatile @NotNull Locale defaultLocale = FALLBACK_LOCALE;

    /**
     * The i18n resources of different families and locales.
     */
    private static final @NotNull ConcurrentMap<String, I18NResource> RESOURCES = new ConcurrentHashMap<>(4);

    /**
     * Internal logger.
     */
    @SuppressWarnings("ConstantConditions")
    private static final @NotNull Logger LOG = LoggerFactory.getLogger(I18NResourceFactory.class);

    /**
     * Returns the default locale.
     *
     * @return the default locale.
     */
    public static @NotNull Locale getDefaultLocale() {
        return defaultLocale;
    }

    /**
     * Sets the default locale.
     * Application is responsible to call this method once in initialization code.
     *
     * @param locale the default locale.
     */
    public static void setDefaultLocale(@NotNull Locale locale) {
        defaultLocale = locale;
    }

    /**
     * Returns the i18n resource associated with the family and default locale.
     *
     * @param family a family name. It shall not be {@code null}.
     * @return the i18n resource.
     * @throws ApplicationException if no resource file is found or IOException occurs.
     */
    public static @NotNull I18NResource getDefaultInstance(@NotNull String family) {
        return getInstance(family, defaultLocale);
    }

    /**
     * Returns the i18n resource associated with the family and locale.
     *
     * @param family a family name. It shall not be {@code null}.
     * @param locale a {@code Locale}. It shall not be {@code null}.
     * @return the i18n resource.
     * @throws NullPointerException if locale is null.
     * @throws ApplicationException if no resource file is found or IOException occurs.
     */
    @SuppressWarnings("ReuseOfLocalVariable")
    public static @NotNull I18NResource getInstance(@NotNull String family, @NotNull Locale locale) {
        String key = buildResourceKey(family, locale);
        I18NResource resource = RESOURCES.get(key);
        if (resource == null) {
            resource = create(family, locale);
            RESOURCES.putIfAbsent(key, resource);
            //noinspection ConstantConditions
            return RESOURCES.get(key);
        }
        return resource;
    }

    /**
     * Builds a resource key from a family and a locale.
     *
     * @param family a family name. It shall not be {@code null}.
     * @param locale a {@code Locale}. It shall not be {@code null}.
     * @return the resource key.
     * @throws NullPointerException if {@code locale} is {@code null}.
     */
    @NonNls
    private static @NotNull String buildResourceKey(@NotNull String family, @NotNull Locale locale) {
        return family + '_' + locale.getLanguage() + '_' + locale.getCountry();
    }

    /**
     * Finds the associated resource file based on family and locale, and creates an {@code I18NResource} instance.
     *
     * @param family a family name. It shall not be {@code null}.
     * @param locale a {@code Locale}. It shall not be {@code null}.
     * @return the created {@code I18NResource} instance.
     * @throws NullPointerException if {@code locale} is {@code null}.
     * @throws ApplicationException if no resource file is found or IOException occurs.
     */
    private static @NotNull I18NResource create(@NotNull String family, @NotNull Locale locale) {
        String[] resourceNames = buildResourceNames(family, locale);
        InputStream in = findResourceStream(family, resourceNames);
        Map<String, String> texts = IOUtil.readSmallPropertiesFile(in);
        IOUtil.close(in, LOG);
        return new I18NResource(texts);
    }

    /**
     * Builds possible resource file names to check.
     *
     * @param family a family name. It shall not be {@code null}.
     * @param locale a {@code Locale}. It shall not be {@code null}.
     * @return a {@code List} of prioritized file names.
     * @throws NullPointerException if {@code locale} is {@code null}.
     */
    private static @NotNull String @NotNull [] buildResourceNames(@NotNull String family, @NotNull Locale locale) {
        String fullName = buildResourceName(family, locale, true);
        String shortName = buildResourceName(family, locale, false);
        String fallbackName = buildResourceName(family, FALLBACK_LOCALE, false);
        // the order is prioritized.
        return new String[]{fullName, shortName, fallbackName};
    }

    /**
     * Builds a possible resource file name.
     *
     * @param family      a family name. It shall not be {@code null}.
     * @param locale      a {@code Locale}. It shall not be {@code null}.
     * @param withCountry whether to use country name.
     * @return a possible file name.
     * @throws NullPointerException if {@code locale} is {@code null}.
     */
    private static @NotNull String buildResourceName(@NotNull String family, @NotNull Locale locale, boolean withCountry) {
        @NonNls StringBuilder sb = new StringBuilder();
        sb.append(family).append("_locale_").append(locale.getLanguage());
        if (withCountry) {
            sb.append('_').append(locale.getCountry());
        }
        sb.append(".properties");
        return sb.toString();
    }

    /**
     * Finds the resource file and opens it as an {@code InputStream}.
     *
     * @param family        a family name. It shall not be {@code null}.
     * @param resourceNames possible resource file names. It shall not be {@code null}.
     * @return the resource {@code InputStream} if found.
     * @throws ApplicationException if no resource file is found.
     */
    @SuppressWarnings("MethodCanBeVariableArityMethod")
    private static @NotNull InputStream findResourceStream(@NotNull String family, @NotNull String @NotNull [] resourceNames) {
        for (String resourceName : resourceNames) {
            InputStream in = IOUtil.getResourceAsStream(resourceName);
            if (in != null) {
                return in;
            }
        }
        @NonNls String message = "Cannot find I18N resource file with prefix " + family + " in classpath.";
        throw new ApplicationException(message);
    }

}
