package name.slhynju.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Method;
import java.text.Collator;

/**
 * Utility class to support {@code compareTo()} method and {@code Comparators}. This class is not thread-safe.
 * <p>This class shall have no dependency on other utility classes.</p>
 *
 * @author slhynju
 */
public final class CompareToBuilder {

    // this property shall have three values: -1, 0, 1.
    private int value = 0;

    /**
     * Appends two {@code Objects} for comparision.
     *
     * @param o1  an {@code Object}. It can be {@code null}.
     * @param o2  an {@code Object}. It can be {@code null}.
     * @param <T> the type of {@code o2}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull <T> CompareToBuilder append(@Nullable final Comparable<T> o1, @Nullable final T o2) {
        if (isBypassed(o1, o2)) {
            return this;
        }
        value = o1.compareTo(o2);
        return this;
    }

    /**
     * Appends two {@code Object arrays} for comparision.
     *
     * @param objects1 an {@code Object array}. It can be {@code null}.
     * @param objects2 an {@code Object array}. It can be {@code null}.
     * @param <T>      the {@code Object array} element type.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull <T> CompareToBuilder append(final Comparable<T> @NotNull [] objects1, final T @NotNull [] objects2) {
        if (isBypassed(objects1, objects2)) {
            return this;
        }
        final int elementCountToCompare = Math.min(objects1.length, objects2.length);
        for (int I = 0; I < elementCountToCompare; I++) {
            append(objects1[I], objects2[I]);
            if (value != 0) {
                return this;
            }
        }
        compareInt(objects1.length, objects2.length);
        return this;
    }

    /**
     * Appends two {@code int} numbers for comparision.
     *
     * @param i1 an {@code int} number.
     * @param i2 an {@code int} number.
     * @return this builder.
     */
    public @NotNull CompareToBuilder append(final int i1, final int i2) {
        if (value != 0) {
            return this;
        }
        compareInt(i1, i2);
        return this;
    }

    /**
     * Appends two {@code int arrays} for comparision.
     *
     * @param a1 an {@code int array}. It can be {@code null}.
     * @param a2 an {@code int array}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull CompareToBuilder append(final int @NotNull [] a1, final int @NotNull [] a2) {
        if (isBypassed(a1, a2)) {
            return this;
        }
        final int elementCountToCompare = Math.min(a1.length, a2.length);
        for (int I = 0; I < elementCountToCompare; I++) {
            compareInt(a1[I], a2[I]);
            if (value != 0) {
                return this;
            }
        }
        compareInt(a1.length, a2.length);
        return this;
    }

    /**
     * Appends two {@code long} numbers for comparision.
     *
     * @param l1 a {@code long} number.
     * @param l2 a {@code long} number.
     * @return this builder.
     */
    public @NotNull CompareToBuilder append(final long l1, final long l2) {
        if (value != 0) {
            return this;
        }
        compareLong(l1, l2);
        return this;
    }

    /**
     * Appends two {@code long arrays} for comparision.
     *
     * @param a1 a {@code long array}. It can be {@code null}.
     * @param a2 a {@code long array}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull CompareToBuilder append(final long @NotNull [] a1, final long @NotNull [] a2) {
        if (isBypassed(a1, a2)) {
            return this;
        }
        final int elementCountToCompare = Math.min(a1.length, a2.length);
        for (int I = 0; I < elementCountToCompare; I++) {
            compareLong(a1[I], a2[I]);
            if (value != 0) {
                return this;
            }
        }
        compareInt(a1.length, a2.length);
        return this;
    }

    /**
     * Appends two {@code double} numbers for comparision.
     *
     * @param d1 a {@code double} number.
     * @param d2 a {@code double} number.
     * @return this builder.
     */
    public @NotNull CompareToBuilder append(final double d1, final double d2) {
        if (value != 0) {
            return this;
        }
        compareDouble(d1, d2);
        return this;
    }

    /**
     * Appends two {@code double arrays} for comparision.
     *
     * @param a1 a {@code double array}. It can be {@code null}.
     * @param a2 a {@code double array}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull CompareToBuilder append(final double @NotNull [] a1, final double @NotNull [] a2) {
        if (isBypassed(a1, a2)) {
            return this;
        }
        final int elementCountToCompare = Math.min(a1.length, a2.length);
        for (int I = 0; I < elementCountToCompare; I++) {
            compareDouble(a1[I], a2[I]);
            if (value != 0) {
                return this;
            }
        }
        compareInt(a1.length, a2.length);
        return this;
    }

    /**
     * Appends two {@code boolean} values for comparision.
     *
     * @param b1 a {@code boolean} value.
     * @param b2 a {@code boolean} value.
     * @return this builder.
     */
    public @NotNull CompareToBuilder append(final boolean b1, final boolean b2) {
        if (value != 0) {
            return this;
        }
        compareBoolean(b1, b2);
        return this;
    }

    /**
     * Appends two {@code boolean arrays} for comparision.
     *
     * @param a1 a {@code boolean array}. It can be {@code null}.
     * @param a2 a {@code boolean array}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull CompareToBuilder append(final boolean @NotNull [] a1, final boolean @NotNull [] a2) {
        if (isBypassed(a1, a2)) {
            return this;
        }
        final int elementCountToCompare = Math.min(a1.length, a2.length);
        for (int I = 0; I < elementCountToCompare; I++) {
            compareBoolean(a1[I], a2[I]);
            if (value != 0) {
                return this;
            }
        }
        compareInt(a1.length, a2.length);
        return this;
    }

    /**
     * Appends two {@code chars} for comparision.
     *
     * @param c1 a {@code char}.
     * @param c2 a {@code char}.
     * @return this builder.
     */
    public @NotNull CompareToBuilder append(final char c1, final char c2) {
        if (value != 0) {
            return this;
        }
        compareChar(c1, c2);
        return this;
    }

    /**
     * Appends two {@code Strings} for comparision with custom {@code Collator}.
     *
     * @param s1       a {@code String}. It can be {@code null}.
     * @param s2       a {@code String}. It can be {@code null}.
     * @param collator a custom {@code Collator}. It shall not be {@code null}.
     * @return this builder.
     * @throws NullPointerException if {@code collator} is {@code null}.
     */
    public @NotNull CompareToBuilder append(@Nullable final String s1, @Nullable final String s2, @NotNull final
    Collator collator) {
        if (value != 0) {
            return this;
        }
        value = collator.compare(s1, s2);
        return this;
    }

    /**
     * Appends two {@code Classes} for comparision.
     *
     * @param c1 a {@code Class}. It can be {@code null}.
     * @param c2 a {@code Class}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull CompareToBuilder append(@Nullable final Class<?> c1, @Nullable final Class<?> c2) {
        if (isBypassed(c1, c2)) {
            return this;
        }
        compareClass(c1, c2);
        return this;
    }

    /**
     * Appends two {@code Class arrays} for comparision.
     *
     * @param a1 a {@code Class array}. It can be {@code null}.
     * @param a2 a {@code Class array}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull CompareToBuilder append(final Class<?> @NotNull [] a1, final Class<?> @NotNull [] a2) {
        if (isBypassed(a1, a2)) {
            return this;
        }
        final int elementCountToCompare = Math.min(a1.length, a2.length);
        for (int I = 0; I < elementCountToCompare; I++) {
            append(a1[I], a2[I]);
            if (value != 0) {
                return this;
            }
        }
        compareInt(a1.length, a2.length);
        return this;
    }

    /**
     * Appends two {@code Methods} for comparision.
     *
     * @param m1 a {@code Method}. It can be {@code null}.
     * @param m2 a {@code Method}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull CompareToBuilder append(@Nullable final Method m1, @Nullable final Method m2) {
        if (isBypassed(m1, m2)) {
            return this;
        }
        compareMethod(m1, m2);
        return this;
    }

    /**
     * Appends two {@code Method arrays} for comparision.
     *
     * @param a1 a {@code Method array}. It can be {@code null}.
     * @param a2 a {@code Method array}. It can be {@code null}.
     * @return this builder.
     */
    @SuppressWarnings("ConstantConditions")
    public @NotNull CompareToBuilder append(final Method @NotNull [] a1, final Method @NotNull [] a2) {
        if (isBypassed(a1, a2)) {
            return this;
        }
        final int elementCountToCompare = Math.min(a1.length, a2.length);
        for (int I = 0; I < elementCountToCompare; I++) {
            append(a1[I], a2[I]);
            if (value != 0) {
                return this;
            }
        }
        compareInt(a1.length, a2.length);
        return this;
    }

    /**
     * Returns the comparision result.
     *
     * @return the comparision result.
     */
    public int toValue() {
        return value;
    }

    /**
     * Checks whether the comparision can be bypassed.
     *
     * @param o1 an {@code Object}. It can be {@code null}.
     * @param o2 an {@code Object}. It can be {@code null}.
     * @return {@code true} -- bypass the comparision of these two {@code Objects}; {@code false} -- continue the comparision.
     */
    private boolean isBypassed(@Nullable final Object o1, @Nullable final Object o2) {
        if (value != 0) {
            return true;
        }
        //noinspection ObjectEquality
        if (o1 == o2) {
            return true;
        }
        if (o1 == null) {
            value = -1;
            return true;
        }
        if (o2 == null) {
            value = 1;
            return true;
        }
        return false;
    }

    /**
     * Compares two {@code int} numbers.
     *
     * @param i1 an {@code int} number.
     * @param i2 an {@code int} number.
     */
    private void compareInt(final int i1, final int i2) {
        value = Integer.compare(i1, i2);
    }

    /**
     * Compares two {@code long} numbers.
     *
     * @param l1 a {@code long} number.
     * @param l2 a{@code long} number.
     */
    private void compareLong(final long l1, final long l2) {
        value = Long.compare(l1, l2);
    }

    /**
     * Compares two {@code double} numbers.
     *
     * @param d1 a {@code double} number.
     * @param d2 a {@code double} number.
     */
    private void compareDouble(final double d1, final double d2) {
        value = Double.compare(d1, d2);
    }

    /**
     * Compares two {@code boolean} values.
     *
     * @param b1 a {@code boolean} value.
     * @param b2 a {@code boolean} value.
     */
    private void compareBoolean(final boolean b1, final boolean b2) {
        value = Boolean.compare(b1, b2);
    }

    /**
     * Compares two {@code char} values.
     *
     * @param c1 a {@code char} value.
     * @param c2 a {@code char} value.
     */
    private void compareChar(final char c1, final char c2) {
        value = Character.compare(c1, c2);
    }

    /**
     * Compares two {@code Class} objects.
     *
     * @param c1 a {@code Class} object. It shall not be {@code null}.
     * @param c2 a {@code Class} object. It shall not be {@code null}.
     */
    private void compareClass(@NotNull final Class<?> c1, @NotNull final Class<?> c2) {
        final String name1 = c1.getSimpleName();
        final String name2 = c2.getSimpleName();
        append(name1, name2);
    }

    /**
     * Compares two {@code Method} objects.
     *
     * @param m1 a {@code Method} object. It shall not be {@code null}.
     * @param m2 a {@code Method} object. It shall not be {@code null}.
     */
    private void compareMethod(@NotNull final Method m1, @NotNull final Method m2) {
        append(m1.getName(), m2.getName());
        if (value != 0) {
            return;
        }
        append(m1.getParameterTypes(), m2.getParameterTypes());
    }
}
