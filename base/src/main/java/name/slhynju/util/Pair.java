package name.slhynju.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A simple Data Transfer Object with two properties.
 *
 * @author slhynju
 */
@SuppressWarnings("StandardVariableNames")
public final class Pair<A, B> {

    @Nullable
    public A a;

    @Nullable
    public B b;

    /**
     * Creates a new instance.
     */
    public Pair() {
        a = null;
        b = null;
    }

    /**
     * Creates a new instance.
     *
     * @param a first property value. It can be {@code null}.
     * @param b second property value. It can be {@code null}.
     */
    public Pair(@Nullable A a, @Nullable B b) {
        this.a = a;
        this.b = b;
    }

    /**
     * Returns the two properties as an {@code Object array}.
     *
     * @return a length-2 {@code Object array}.
     */
    @NotNull
    @Contract(" -> new")
    public Object[] toArray() {
        //noinspection ConstantConditions
        return new Object[]{a, b};
    }

    @SuppressWarnings("rawtypes")
    @Override
    @Contract("null -> false")
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Pair) {
            Pair other = (Pair) obj;
            return EqualsUtil.isEquals(a, other.a) && EqualsUtil.isEquals(b, other.b);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(a).append(b).toValue();
    }

    @Override
    @NotNull
    @Contract(" -> new")
    public String toString() {
        return new BeanStringBuilder(Pair.class).append("a", a).append("b", b).toS();
    }

}
