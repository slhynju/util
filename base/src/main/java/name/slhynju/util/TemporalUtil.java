package name.slhynju.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

/**
 * Temporal utility methods.
 * <p>
 * This class shall have no dependency on other utility classes.
 * </p>
 *
 * @author slhynju
 */
@SuppressWarnings("ClassIndependentOfModule")
public final class TemporalUtil {

    // define this constant so that this class does not depend on {@code StringUtil}.
    private static final @NotNull String NULL = "null";

    /**
     * Formats a Temporal object into a {@code String}.
     *
     * @param temporal  a Temporal object. It can be {@code null}.
     * @param formatter a Temporal formatter. It shall not be {@code null}.
     * @return the formatted {@code String}.
     * @throws NullPointerException if {@code formatter} is {@code null}.
     */
    public static @NotNull String toS(@Nullable TemporalAccessor temporal, @NotNull DateTimeFormatter formatter) {
        return temporal == null ? NULL : formatter.format(temporal);
    }

    /**
     * Parses a {@code CharSequence} into a {@code LocalDate} object.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return the parsed {@code LocalDate} object.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable LocalDate toDate(@Nullable CharSequence sequence) {
        return isNull(sequence) ? null : LocalDate.parse(sequence);
    }

    /**
     * Parses a {@code CharSequence} into a {@code LocalDate} object with custom formatter.
     *
     * @param sequence  a {@code CharSequence}. It can be {@code null}.
     * @param formatter a custom Temporal formatter. It shall not be {@code null}.
     * @return the parsed {@code LocalDate} object.
     * @throws NullPointerException                    if {@code formatter} is {@code null}.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable LocalDate toDate(@Nullable CharSequence sequence, @NotNull DateTimeFormatter formatter) {
        return isNull(sequence) ? null : LocalDate.parse(sequence, formatter);
    }

    /**
     * Parses a {@code CharSequence} into a {@code LocalDateTime} object.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return the parsed {@code LocalDateTime} object.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable LocalDateTime toDateTime(@Nullable CharSequence sequence) {
        return isNull(sequence) ? null : LocalDateTime.parse(sequence);
    }

    /**
     * Parses a {@code CharSequence} into a {@code LocalDateTime} object with custom formatter.
     *
     * @param sequence  a {@code CharSequence}. It can be {@code null}.
     * @param formatter a custom Temporal formatter. It shall not be {@code null}.
     * @return the parsed {@code LocalDateTime} object.
     * @throws NullPointerException                    if {@code formatter} is {@code null}.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable LocalDateTime toDateTime(@Nullable CharSequence sequence, @NotNull DateTimeFormatter formatter) {
        return isNull(sequence) ? null : LocalDateTime.parse(sequence, formatter);
    }

    /**
     * Parses a {@code CharSequence} into a {@code LocalTime} object.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return the parsed {@code LocalTime} object.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable LocalTime toTime(@Nullable CharSequence sequence) {
        return isNull(sequence) ? null : LocalTime.parse(sequence);
    }

    /**
     * Parses a {@code CharSequence} into a {@code LocalTime} object with custom formatter.
     *
     * @param sequence  a {@code CharSequence}. It can be {@code null}.
     * @param formatter a custom Temporal formatter. It shall not be {@code null}.
     * @return the parsed {@code LocalTime} object.
     * @throws NullPointerException                    if {@code formatter} is {@code null}.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable LocalTime toTime(@Nullable CharSequence sequence, @NotNull DateTimeFormatter formatter) {
        return isNull(sequence) ? null : LocalTime.parse(sequence, formatter);
    }

    /**
     * Parses a {@code CharSequence} into a {@code ZonedDateTime} object.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return the parsed {@code ZonedDateTime} object.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable ZonedDateTime toZonedDateTime(@Nullable CharSequence sequence) {
        return isNull(sequence) ? null : ZonedDateTime.parse(sequence);
    }

    /**
     * Parses a {@code CharSequence} into a {@code ZonedDateTime} object with custom formatter.
     *
     * @param sequence  a {@code CharSequence}. It can be {@code null}.
     * @param formatter a custom Temporal formatter. It shall not be {@code null}.
     * @return the parsed {@code ZonedDateTime} object.
     * @throws NullPointerException                    if {@code formatter} is {@code null}.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable ZonedDateTime toZonedDateTime(@Nullable CharSequence sequence, @NotNull DateTimeFormatter formatter) {
        return isNull(sequence) ? null : ZonedDateTime.parse(sequence, formatter);
    }

    /**
     * Parses a {@code CharSequence} into an {@code Instant} object.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return the parsed {@code Instant} object.
     * @throws java.time.format.DateTimeParseException if {@code sequence} is invalid.
     */
    public static @Nullable Instant toInstant(@Nullable CharSequence sequence) {
        return isNull(sequence) ? null : Instant.parse(sequence);
    }

    /**
     * Checks whether a {@code CharSequence} is {@code null}, empty or {@code "null"}.
     * Defines this method so that this class does not depend on {@code StringUtil}.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- is {@code null}, empty or {@code "null"}; {@code false} -- not {@code null}, empty or {@code "null"}.
     */
    @SuppressWarnings("OverlyComplexBooleanExpression")
    private static boolean isNull(@Nullable CharSequence sequence) {
        return sequence == null || sequence.length() == 0 || (sequence.length() == 4 && NULL.equals(sequence.toString()));
    }

    private TemporalUtil() {
        //DO NOTHING.
    }
}
