package name.slhynju.util;

import name.slhynju.ApplicationException;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * {@code Date} and {@code Calendar} utility methods.
 * <p>This class shall have no dependency on other utility classes.</p>
 *
 * @author slhynju
 */
@SuppressWarnings("UseOfObsoleteDateTimeApi")
public final class DateUtil {

    /**
     * Simple date format of {@code yyyy-MM-dd}.
     */
    public static final @NotNull String SIMPLE_FORMAT = "yyyy-MM-dd";

    /**
     * Long date format of {@code yyyy-MM-dd HH:mm:ss}.
     */
    public static final @NotNull String LONG_FORMAT = "yyyy-MM-dd HH:mm:ss";

    // defines this constant so that this class does not depend on {@code StringUtil}.
    @NonNls
    private static final @NotNull String NULL = "null";

    /**
     * Formats a {@code Date} into a {@code String}. It uses system default timezone.
     *
     * @param date   a {@code Date} to be formatted. It can be {@code null}.
     * @param format a {@code Date} format. It shall not be {@code null}.
     * @return the formatted {@code String}. A {@code null Date} value is formatted into {@code String "null"}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    @SuppressWarnings("SimpleDateFormatWithoutLocale")
    public static @NotNull String toS(@Nullable final Date date, @NotNull final String format) {
        if (date == null) {
            return NULL;
        }
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * Formats a {@code Date} into a {@code String}.
     *
     * @param date   a {@code Date} to be formatted. It can be {@code null}.
     * @param format a {@code Date} format. It shall not be {@code null}.
     * @return the formatted {@code String}. A {@code null Date} value is formatted into {@code String "null"}.
     * @throws NullPointerException if {@code format} is {@code null}.
     */
    public static @NotNull String toS(@Nullable final Date date, @NotNull final DateFormat format) {
        return date == null ? NULL : format.format(date);
    }

    /**
     * Parses a {@code String} into a {@code Date}. It uses system default timezone.
     *
     * @param text   a {@code String} text. It can be {@code null}.
     * @param format a {@code Date} format. It shall not be {@code null}.
     * @return the parsed result {@code Date}.
     * @throws NullPointerException if {@code format} is {@code null}.
     * @throws ApplicationException if failed to parse the {@code String} text with given format.
     */
    @SuppressWarnings("SimpleDateFormatWithoutLocale")
    public static @Nullable Date toDate(@Nullable final String text, @NotNull final String format) {
        if (isNull(text)) {
            return null;
        }
        final DateFormat dateFormat = new SimpleDateFormat(format);
        try {
            return dateFormat.parse(text);
        } catch (final @NotNull ParseException e) {
            @NonNls final String message = "Cannot parse Date from String " + text + " with format " + format + '.';
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Parses a {@code String} into a {@code Date}.
     *
     * @param text   a {@code String} text. It can be {@code null}.
     * @param format a {@code Date} format. It shall not be {@code null}.
     * @return the parsed result {@code Date}.
     * @throws NullPointerException if {@code format} is {@code null}.
     * @throws ApplicationException if failed to parse the {@code String} text with given format.
     */
    public static @Nullable Date toDate(@Nullable final String text, @NotNull final DateFormat format) {
        if (isNull(text)) {
            return null;
        }
        try {
            return format.parse(text);
        } catch (final @NotNull ParseException e) {
            @NonNls final String message = "Cannot parse Date from String " + text + " with format " + format + '.';
            throw new ApplicationException(message, e);
        }
    }

    /**
     * Transforms a {@code LocalDate} object to a {@code Date} object. It uses system default timezone.
     *
     * @param localDate a {@code LocalDate} object. It can be {@code null}.
     * @return the transformed {@code Date} object.
     */
    public static @Nullable Date toDate(@Nullable final LocalDate localDate) {
        //noinspection ConstantConditions
        return toDate(localDate, ZoneId.systemDefault());
    }

    /**
     * Transforms a {@code LocalDate} object to a {@code Date} object.
     *
     * @param localDate a {@code LocalDate} object. It can be {@code null}.
     * @param zoneId    a target {@code ZoneId}. It shall not be {@code null}.
     * @return the transformed {@code Date} object.
     * @throws NullPointerException if {@code zoneId} is {@code null}.
     */
    public static @Nullable Date toDate(@Nullable final LocalDate localDate, @NotNull final ZoneId zoneId) {
        //noinspection ConstantConditions
        return localDate == null ? null : Date.from(localDate.atStartOfDay(zoneId).toInstant());
    }

    /**
     * Transforms a {@code LocalDateTime} object to a {@code Date} object. It uses system default timezone.
     *
     * @param localDateTime a {@code LocalDateTime} object. It can be {@code null}.
     * @return the transformed {@code Date} object.
     */
    public static @Nullable Date toDate(@Nullable final LocalDateTime localDateTime) {
        //noinspection ConstantConditions
        return toDate(localDateTime, ZoneId.systemDefault());
    }

    /**
     * Transforms a {@code LocalDateTime} object to a {@code Date} object.
     *
     * @param localDateTime a {@code LocalDateTime} object. It can be {@code null}.
     * @param zoneId        a target {@code ZoneId}. It shall not be {@code null}.
     * @return the transformed {@code Date} object.
     * @throws NullPointerException if {@code zoneId} is {@code null}.
     */
    public static @Nullable Date toDate(@Nullable final LocalDateTime localDateTime, @NotNull final ZoneId zoneId) {
        //noinspection ConstantConditions
        return localDateTime == null ? null : Date.from(localDateTime.atZone(zoneId).toInstant());
    }

    /**
     * Transforms an {@code Instant} object to a {@code Date} object.
     *
     * @param t an {@code Instant} object. It can be {@code null}.
     * @return the transformed {@code Date} object.
     */
    public static @Nullable Date toDate(@Nullable final Instant t) {
        return t == null ? null : Date.from(t);
    }

    /**
     * Transforms a {@code ZonedDateTime} object to a {@code Date} object.
     *
     * @param zonedDateTime a {@code ZonedDateTime} object. It can be {@code null}.
     * @return the transformed {@code Date} object.
     */
    public static @Nullable Date toDate(@Nullable final ZonedDateTime zonedDateTime) {
        //noinspection ConstantConditions
        return zonedDateTime == null ? null : Date.from(zonedDateTime.toInstant());
    }

    /**
     * Transforms a {@code Date} object to a {@code LocalDate} object. It uses system default timezone.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @return the transformed {@code LocalDate} object.
     */
    public static @Nullable LocalDate toLocalDate(@Nullable final Date date) {
        //noinspection ConstantConditions
        return toLocalDate(date, ZoneId.systemDefault());
    }

    /**
     * Transforms a {@code Date} object to a {@code LocalDate} object.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @param zone a target {@code TimeZone}. It shall not be {@code null}.
     * @return the transformed {@code LocalDate} object.
     * @throws NullPointerException if {@code zone} is {@code null}.
     */
    public static @Nullable LocalDate toLocalDate(@Nullable final Date date, @NotNull final TimeZone zone) {
        //noinspection ConstantConditions
        return toLocalDate(date, zone.toZoneId());
    }

    /**
     * Transforms a {@code Date} object to a {@code LocalDate} object.
     *
     * @param date   a {@code Date} object. It can be {@code null}.
     * @param zoneId a target {@code ZoneId}. It shall not be {@code null}.
     * @return the transformed {@code LocalDate} object.
     * @throws NullPointerException if {@code zoneId} is {@code null}.
     */
    public static @Nullable LocalDate toLocalDate(@Nullable final Date date, @NotNull final ZoneId zoneId) {
        //noinspection ConstantConditions
        return date == null ? null : LocalDate.ofInstant(date.toInstant(), zoneId);
    }

    /**
     * Transforms a {@code Date} object to a {@code LocalDateTime} object. It uses system default timezone.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @return the transformed {@code LocalDateTime} object.
     */
    public static @Nullable LocalDateTime toLocalDateTime(@Nullable final Date date) {
        //noinspection ConstantConditions
        return toLocalDateTime(date, ZoneId.systemDefault());
    }

    /**
     * Transforms a {@code Date} object to a {@code LocalDateTime} object.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @param zone a target {@code TimeZone}. It shall not be {@code null}.
     * @return the transformed {@code LocalDateTime} object.
     * @throws NullPointerException if {@code zone} is {@code null}.
     */
    public static @Nullable LocalDateTime toLocalDateTime(@Nullable final Date date, @NotNull final TimeZone zone) {
        //noinspection ConstantConditions
        return toLocalDateTime(date, zone.toZoneId());
    }

    /**
     * Transforms a {@code Date} object to a {@code LocalDateTime} object.
     *
     * @param date   a {@code Date} object. It can be {@code null}.
     * @param zoneId a target {@code ZoneId}. It shall not be {@code null}.
     * @return the transformed {@code LocalDateTime} object.
     * @throws NullPointerException if {@code zoneId} is {@code null}.
     */
    public static @Nullable LocalDateTime toLocalDateTime(@Nullable final Date date, @NotNull final ZoneId zoneId) {
        //noinspection ConstantConditions
        return date == null ? null : LocalDateTime.ofInstant(date.toInstant(), zoneId);
    }

    /**
     * Transforms a {@code Date} object to an {@code Instant} object.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @return the transformed {@code Instant} object.
     */
    public static @Nullable Instant toInstant(@Nullable final Date date) {
        return date == null ? null : date.toInstant();
    }

    /**
     * Transforms a {@code Date} object to a {@code ZonedDateTime} object. It uses system default timezone.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @return the transformed {@code ZonedDateTime} object.
     */
    public static @Nullable ZonedDateTime toZonedDateTime(@Nullable final Date date) {
        //noinspection ConstantConditions
        return toZonedDateTime(date, ZoneId.systemDefault());
    }

    /**
     * Transforms a {@code Date} object to a {@code ZonedDateTime} object.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @param zone a target {@code TimeZone}. It shall not be {@code null}.
     * @return the transformed {@code ZonedDateTime} object.
     * @throws NullPointerException if {@code zone} is {@code null}.
     */
    public static @Nullable ZonedDateTime toZonedDateTime(@Nullable final Date date, @NotNull final TimeZone zone) {
        //noinspection ConstantConditions
        return toZonedDateTime(date, zone.toZoneId());
    }

    /**
     * Transforms a {@code Date} object to a {@code ZonedDateTime} object.
     *
     * @param date   a {@code Date} object. It can be {@code null}.
     * @param zoneId a target {@code ZoneId}. It shall not be {@code null}.
     * @return the transformed {@code ZonedDateTime} object.
     * @throws NullPointerException if {@code zoneId} is {@code null}.
     */
    public static @Nullable ZonedDateTime toZonedDateTime(@Nullable final Date date, @NotNull final ZoneId zoneId) {
        //noinspection ConstantConditions
        return date == null ? null : ZonedDateTime.ofInstant(date.toInstant(), zoneId);
    }

    /**
     * Truncates a {@code Date} object with specified precision unit, and returns the new result. It uses system default timezone.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @param unit a precision time unit.
     * @return the truncated result {@code Date}.
     * @throws IllegalArgumentException if {@code unit} is invalid.
     * @see Calendar
     */
    public static @Nullable Date truncate(@Nullable final Date date, final int unit) {
        //noinspection ConstantConditions
        return truncate(date, unit, TimeZone.getDefault());
    }

    /**
     * Truncates a {@code Date} object with specified precision unit, and returns the new result.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @param unit a precision time unit.
     * @param zone a target {@code TimeZone}. It shall not be {@code null}.
     * @return the truncated result {@code Date}.
     * @throws IllegalArgumentException if {@code unit} is invalid.
     * @see Calendar
     */
    @SuppressWarnings({"fallthrough", "ConstantConditions"})
    public static @Nullable Date truncate(@Nullable final Date date, final int unit, final @NotNull TimeZone zone) {
        if (date == null) {
            return null;
        }
        final Calendar calendar = toCalendar(date, zone);
        switch (unit) {
            case Calendar.YEAR:
                calendar.set(Calendar.MONTH, Calendar.JANUARY);
            case Calendar.MONTH:
                calendar.set(Calendar.DATE, 1);
            case Calendar.DATE:
                calendar.set(Calendar.HOUR_OF_DAY, 0);
            case Calendar.HOUR_OF_DAY:
                calendar.set(Calendar.MINUTE, 0);
            case Calendar.MINUTE:
                calendar.set(Calendar.SECOND, 0);
            case Calendar.SECOND:
                calendar.set(Calendar.MILLISECOND, 0);
                return calendar.getTime();
            case Calendar.DAY_OF_WEEK:
                calendar.set(Calendar.DAY_OF_WEEK, calendar.getActualMinimum(Calendar.DAY_OF_WEEK));
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                return calendar.getTime();
            default:
                @NonNls final String message = "Cannot truncate date with unit value " + unit + ". Only understands unit YEAR(1), MONTH(2), DATE(5), DAY_OF_WEEK(7), HOUR_OF_DAY(11), MINUTE(12), SECOND(13).";
                throw new IllegalArgumentException(message);
        }
    }

    /**
     * Adds or minuses a time period from a {@code Date} and returns the new result. It uses system default timezone.
     *
     * @param date   a {@code Date} object. It can be {@code null}.
     * @param unit   a time unit.
     * @param amount an amount of time unit.
     * @return the new result {@code Date}.
     * @throws IllegalArgumentException if {@code unit} is invalid.
     * @see Calendar
     */
    @SuppressWarnings("ConstantConditions")
    public static @Nullable Date add(@Nullable final Date date, final int unit, final int amount) {
        if (date == null) {
            return null;
        }
        final Calendar calendar = toCalendar(date);
        calendar.add(unit, amount);
        return calendar.getTime();
    }

    /**
     * Sets the unit data to maximum value and returns the new result. It uses system default timezone.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @param unit a time unit.
     * @return the new result {@code Date}.
     * @throws ArrayIndexOutOfBoundsException if {@code unit} is invalid.
     * @see Calendar
     */
    @SuppressWarnings("ConstantConditions")
    public static @Nullable Date max(@Nullable final Date date, final int unit) {
        if (date == null) {
            return null;
        }
        final Calendar calendar = toCalendar(date);
        max(calendar, unit);
        return calendar.getTime();
    }

    /**
     * Sets the unit data to maximum value.
     *
     * @param calendar a {@code Calendar} object. It can be {@code null}.
     * @param unit     a time unit.
     * @throws ArrayIndexOutOfBoundsException if {@code unit} is invalid.
     * @see Calendar
     */
    public static void max(@Nullable final Calendar calendar, final int unit) {
        if (calendar == null) {
            return;
        }
        calendar.set(unit, calendar.getActualMaximum(unit));
    }

    /**
     * Sets the unit data to minimum value and returns the new result. It uses system default timezone.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @param unit a time unit.
     * @return the new result {@code Date}.
     * @throws ArrayIndexOutOfBoundsException if {@code unit} is invalid.
     * @see Calendar
     */
    @SuppressWarnings("ConstantConditions")
    public static @Nullable Date min(@Nullable final Date date, final int unit) {
        if (date == null) {
            return null;
        }
        final Calendar calendar = toCalendar(date);
        min(calendar, unit);
        return calendar.getTime();
    }

    /**
     * Sets the unit data to minimum value.
     *
     * @param calendar a {@code Calendar} object. It can be {@code null}.
     * @param unit     a time unit.
     * @throws ArrayIndexOutOfBoundsException if {@code unit} is invalid.
     * @see Calendar
     */
    public static void min(@Nullable final Calendar calendar, final int unit) {
        if (calendar == null) {
            return;
        }
        calendar.set(unit, calendar.getActualMinimum(unit));
    }

    /**
     * Transforms a {@code Date} object to a {@code Calendar} object. It uses system default timezone.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @return the transformed {@code Calendar} object.
     */
    @SuppressWarnings("ConstantConditions")
    public static @Nullable Calendar toCalendar(@Nullable final Date date) {
        if (date == null) {
            return null;
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Transforms a {@code Date} object to a {@code Calendar} object.
     *
     * @param date a {@code Date} object. It can be {@code null}.
     * @param zone a target {@code TimeZone}. It shall not be {@code null}.
     * @return the transformed {@code Calendar} object.
     * @throws NullPointerException if {@code zone} is {@code null}.
     */
    @SuppressWarnings("ConstantConditions")
    public static @Nullable Calendar toCalendar(@Nullable final Date date, @NotNull final TimeZone zone) {
        if (date == null) {
            return null;
        }
        final Calendar calendar = Calendar.getInstance(zone);
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Returns the start time (midnight) of today. It uses system default timezone.
     *
     * @return the start time of today.
     */
    public static @NotNull Date today() {
        return toDate(LocalDate.now());
    }

    /**
     * Checks whether a {@code String} text represents a {@code null Date}.
     * <p>Define this method so that this class does not depend on {@code StringUtil}.</p>
     *
     * @param s a {@code String} text. It can be {@code null}.
     * @return {@code true} -- it represents a {@code null Date}; {@code false} -- not a {@code null Date}.
     */
    private static boolean isNull(@Nullable final String s) {
        return s == null || s.isEmpty() || NULL.equalsIgnoreCase(s);
    }

    private DateUtil() {
        //DO NOTHING.
    }

}
