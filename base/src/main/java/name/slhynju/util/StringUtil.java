package name.slhynju.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.IntPredicate;

/**
 * {@code String}, {@code StringBuilder} and {@code CharSequence} utility methods.
 * <p>This class shall have no dependency on other utility classes.</p>
 *
 * @author slhynju
 */
@SuppressWarnings("OverlyComplexClass")
public final class StringUtil {

    /**
     * {@code String} constant of {@code "null"}.
     */
    @NotNull
    public static final String NULL = "null";

    /**
     * Checks whether a {@code String} text is {@code null} or empty.
     *
     * @param s a {@code String} text. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    @Contract("null -> true")
    public static boolean isEmpty(@Nullable String s) {
        return s == null || s.isEmpty();
    }

    /**
     * Checks whether a {@code String} text is not {@code null} or empty.
     *
     * @param s a {@code String} text. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    @Contract("null -> false")
    public static boolean notEmpty(@Nullable String s) {
        return !isEmpty(s);
    }

    /**
     * Checks whether a {@code CharSequence} is {@code null} or empty.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- is {@code null} or empty; {@code false} -- not {@code null} or empty.
     */
    @Contract("null -> true")
    public static boolean isEmpty(@Nullable CharSequence sequence) {
        return sequence == null || sequence.length() == 0;
    }

    /**
     * Checks whether a {@code CharSequence} is not {@code null} or empty.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- not {@code null} or empty; {@code false} -- is {@code null} or empty.
     */
    @Contract("null -> false")
    public static boolean notEmpty(@Nullable CharSequence sequence) {
        return !isEmpty(sequence);
    }

    /**
     * Checks whether a {@code String} is {@code null}, empty or {@code "null"}. This method is case insensitive.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return {@code true} -- is {@code null}, empty or {@code "null"}; {@code false} -- not {@code null}, empty or {@code "null"}.
     */
    @Contract("null -> true")
    public static boolean isNull(@Nullable String s) {
        return s == null || s.isEmpty() || NULL.equalsIgnoreCase(s);
    }

    /**
     * Checks whether a {@code String} is not {@code null}, empty or {@code "null"}. This method is case insensitive.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return {@code true} -- is not {@code null}, empty or {@code "null"}; {@code false} -- is {@code null}, empty or {@code "null"}.
     */
    @Contract("null -> false")
    public static boolean notNull(@Nullable String s) {
        return !isNull(s);
    }

    /**
     * Converts an empty {@code String} to {@code null}.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return {@code null} if original {@code String} is empty; same value otherwise.
     */
    @Nullable
    @Contract("null -> null")
    public static String emptyToNull(@Nullable String s) {
        return isEmpty(s) ? null : s;
    }

    /**
     * Converts {@code null} to an empty {@code String}.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return empty {@code String} if original {@code String} is {@code null}; same value otherwise.
     */
    @NotNull
    @Contract("null -> !null")
    public static String nullToEmpty(@Nullable String s) {
        return s == null ? "" : s;
    }

    /**
     * Trims a {@code String}. Returns empty {@code String} if original {@code String} is {@code null}.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return a trimmed {@code String}. Returns empty {@code String} if original {@code String} is {@code null}.
     */
    @NotNull
    @Contract("null -> !null")
    public static String trim(@Nullable String s) {
        return isEmpty(s) ? "" : s.trim();
    }

    /**
     * Checks whether a {@code String} is {@code "y"}, {@code "yes"}, {@code "t"}, {@code "true"}, {@code "1"}. This method is case insensitive.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return {@code true} -- is {@code "y"}, {@code "yes"}, {@code "t"}, {@code "true"}, {@code "1"}; {@code false} -- otherwise.
     */
    @SuppressWarnings("FoldExpressionIntoStream")
    @Contract("null -> false")
    public static boolean isYes(@Nullable String s) {
        return "y".equalsIgnoreCase(s) || "yes".equalsIgnoreCase(s) || "t".equalsIgnoreCase(s)
                || "true".equalsIgnoreCase(s) || "1".equals(s);
    }

    /**
     * Checks whether a {@code String} is {@code "n"}, {@code "no"}, {@code "f"}, {@code "false"}, {@code "0"}. This method is case insensitive.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return {@code true} -- is {@code "n"}, {@code "no"}, {@code "f"}, {@code "false"}, {@code "0"}; {@code false} -- otherwise.
     */
    @SuppressWarnings("FoldExpressionIntoStream")
    @Contract("null -> false")
    public static boolean isNo(@Nullable String s) {
        return "n".equalsIgnoreCase(s) || "no".equalsIgnoreCase(s) || "f".equalsIgnoreCase(s)
                || "false".equalsIgnoreCase(s) || "0".equals(s);
    }

    /**
     * Checks whether all code points of a {@code CharSequence} are digits.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- all code points are digits; {@code false} -- empty or some code points are not digits.
     */
    @Contract("null -> false")
    public static boolean isDigits(@Nullable CharSequence sequence) {
        return allMatch(sequence, Character::isDigit);
    }

    /**
     * Checks whether some code points of a {@code CharSequence} are not digits.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- empty or some code points are not digits; {@code false} -- all code points are digits.
     */
    @Contract("null -> true")
    public static boolean notDigits(@Nullable CharSequence sequence) {
        return !isDigits(sequence);
    }

    /**
     * Checks whether all code points of a {@code CharSequence} are upper cases.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- all code points are upper cases; {@code false} -- empty or some code points are not upper cases.
     */
    @Contract("null -> false")
    public static boolean isUpperCases(@Nullable CharSequence sequence) {
        return allMatch(sequence, Character::isUpperCase);
    }

    /**
     * Checks whether some code points of a {@code CharSequence} are not upper cases.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- empty or some code points are not upper cases; {@code false} -- all code points are upper cases.
     */
    @Contract("null -> true")
    public static boolean notUpperCases(@Nullable CharSequence sequence) {
        return !isUpperCases(sequence);
    }

    /**
     * Checks whether all code points of a {@code CharSequence} are lower cases.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- all code points are lower cases; {@code false} -- empty or some code points are not lower cases.
     */
    @Contract("null -> false")
    public static boolean isLowerCases(@Nullable CharSequence sequence) {
        return allMatch(sequence, Character::isLowerCase);
    }

    /**
     * Checks whether some code points of a {@code CharSequence} are not lower cases.
     *
     * @param sequence a {@code CharSequence}. It can be {@code null}.
     * @return {@code true} -- empty or some code points are not lower cases; {@code false} -- all code points are lower cases.
     */
    @Contract("null -> true")
    public static boolean notLowerCases(@Nullable CharSequence sequence) {
        return !isLowerCases(sequence);
    }

    /**
     * Checks whether all code points of a {@code CharSequence} matches certain criteria.
     *
     * @param sequence         a {@code CharSequence}. It can be {@code null}.
     * @param codePointMatcher a matcher to check the code points. It shall not be {@code null}.
     * @return {@code true} -- all code points match the criteria; {@code false} -- empty or some code points do not match the criteria.
     * @throws NullPointerException if {@code codePointMatcher} is {@code null}.
     */
    @SuppressWarnings("BooleanMethodNameMustStartWithQuestion")
    @Contract("null, _ -> false")
    public static boolean allMatch(@Nullable CharSequence sequence, @NotNull IntPredicate codePointMatcher) {
        //noinspection ConstantConditions
        return notEmpty(sequence) && sequence.codePoints().allMatch(codePointMatcher);
    }

    /**
     * Finds the first occurrence of a code point.
     *
     * @param s                a {@code String}. It can be {@code null}.
     * @param codePointMatcher a matcher to check the code points. It shall not be {@code null}.
     * @return the first occurrence index. returns {@code -1} if not found.
     * @throws NullPointerException if {@code codePointMatcher} is {@code null}.
     */
    @Contract("!null, null -> fail")
    public static int indexOf(@Nullable String s, @NotNull IntPredicate codePointMatcher) {
        if (isEmpty(s)) {
            return -1;
        }
        for (int I = 0; I < s.length(); I++) {
            int codePoint = s.codePointAt(I);
            if (codePointMatcher.test(codePoint)) {
                return I;
            }
        }
        return -1;
    }

    /**
     * Finds the last occurrence of a code point.
     *
     * @param s                a {@code String}. It can be {@code null}.
     * @param codePointMatcher a matcher to check the code points. It shall not be {@code null}.
     * @return the last occurrence index. returns -1 if not found.
     * @throws NullPointerException if {@code codePointMatcher} is {@code null}.
     */
    @Contract("!null, null -> fail")
    public static int lastIndexOf(@Nullable String s, @NotNull IntPredicate codePointMatcher) {
        if (isEmpty(s)) {
            return -1;
        }
        for (int I = s.length() - 1; I >= 0; I--) {
            int codePoint = s.codePointAt(I);
            if (codePointMatcher.test(codePoint)) {
                return I;
            }
        }
        return -1;
    }

    /**
     * Returns a prefix substring with specified length.
     *
     * @param s            a {@code String}. It can be {@code null}.
     * @param prefixLength a prefix length. It shall be non-negative.
     * @return a prefix substring.
     * @throws StringIndexOutOfBoundsException if {@code prefixLength} is negative.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    @NotNull
    @Contract("null, _ -> !null")
    public static String subPrefix(@Nullable String s, int prefixLength) {
        if (s == null) {
            return "";
        }
        return prefixLength >= s.length() ? s : s.substring(0, prefixLength);
    }

    /**
     * Returns a prefix substring with specified length.
     *
     * @param sb           a {@code StringBuilder}. It can be {@code null}.
     * @param prefixLength a prefix length. It shall be non-negative.
     * @return a prefix substring.
     * @throws StringIndexOutOfBoundsException if {@code prefixLength} is negative.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    @NotNull
    @Contract("null, _ -> !null")
    public static String subPrefix(@Nullable StringBuilder sb, int prefixLength) {
        if (sb == null) {
            return "";
        }
        //noinspection ConstantConditions
        return prefixLength >= sb.length() ? sb.toString() : sb.substring(0, prefixLength);
    }

    /**
     * Deletes a prefix from a {@code String} and returns the new result {@code String}.
     *
     * @param s            a {@code String}. It can be {@code null}.
     * @param prefixLength a prefix length to delete. It shall be non-negative.
     * @return a new {@code String} without the prefix.
     * @throws StringIndexOutOfBoundsException if {@code prefixLength} is negative.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    @NotNull
    @Contract("null, _ -> !null")
    public static String deletePrefix(@Nullable String s, int prefixLength) {
        if (s == null) {
            return "";
        }
        return prefixLength >= s.length() ? "" : s.substring(prefixLength);
    }

    /**
     * Deletes a prefix from a {@code String} and returns the new result {@code String}.
     *
     * @param s      a {@code String}. It can be {@code null}.
     * @param prefix a prefix to delete. It shall not be {@code null}.
     * @return a new {@code String} without the prefix.
     * @throws NullPointerException if {@code prefix} is {@code null}.
     */
    @NotNull
    @Contract("null, _ -> !null; !null, null -> fail")
    public static String deletePrefixIf(@Nullable String s, @NotNull String prefix) {
        if (s == null) {
            return "";
        }
        if (s.startsWith(prefix)) {
            return s.substring(prefix.length());
        }
        return s;
    }

    /**
     * Deletes a prefix from a {@code StringBuilder}.
     *
     * @param sb           a {@code StringBuilder}. It can be {@code null}.
     * @param prefixLength a prefix length to delete. It shall be non-negative.
     * @throws StringIndexOutOfBoundsException if {@code prefixLength} is negative.
     */
    public static void deletePrefix(@Nullable StringBuilder sb, int prefixLength) {
        if (sb == null) {
            return;
        }
        if (prefixLength >= sb.length()) {
            sb.setLength(0);
            return;
        }
        sb.delete(0, prefixLength);
    }


    /**
     * Deletes a prefix from a {@code StringBuilder}.
     *
     * @param sb     a {@code StringBuilder}. It can be {@code null}.
     * @param prefix a prefix to delete. It shall not be {@code null}.
     * @throws NullPointerException if {@code prefix} is {@code null}.
     */
    @Contract("!null, null -> fail")
    public static void deletePrefixIf(@Nullable StringBuilder sb, @NotNull String prefix) {
        if (sb == null || sb.length() < prefix.length()) {
            return;
        }
        if (prefix.equals(sb.substring(0, prefix.length()))) {
            sb.delete(0, prefix.length());
        }
    }

    /**
     * Returns a suffix substring with specified length.
     *
     * @param s            a {@code String}. It can be {@code null}.
     * @param suffixLength a suffix length. It shall be non-negative.
     * @return a suffix substring.
     * @throws StringIndexOutOfBoundsException if {@code suffixLength} is negative.
     */
    @NotNull
    @Contract("null, _ -> !null")
    public static String subSuffix(@Nullable String s, int suffixLength) {
        if (s == null) {
            return "";
        }
        int length = s.length();
        return suffixLength >= length ? s : s.substring(length - suffixLength);
    }

    /**
     * Returns a suffix substring with specified length.
     *
     * @param sb           a {@code StringBuilder}. It can be {@code null}.
     * @param suffixLength a suffix length. It shall be non-negative.
     * @return a suffix substring.
     * @throws StringIndexOutOfBoundsException if {@code suffixLength} is negative.
     */
    @NotNull
    @Contract("null, _ -> !null")
    public static String subSuffix(@Nullable StringBuilder sb, int suffixLength) {
        if (sb == null) {
            return "";
        }
        int length = sb.length();
        //noinspection ConstantConditions
        return suffixLength >= length ? sb.toString() : sb.substring(length - suffixLength);
    }

    /**
     * Deletes a suffix from a {@code String} and returns the new result {@code String}.
     *
     * @param s            a {@code String}. It can be {@code null}.
     * @param suffixLength a suffix length to delete. It shall be non-negative.
     * @return a new {@code String} without the suffix.
     * @throws StringIndexOutOfBoundsException if {@code suffixLength} is negative.
     */
    @NotNull
    @Contract("null, _ -> !null")
    public static String deleteSuffix(@Nullable String s, int suffixLength) {
        if (s == null) {
            return "";
        }
        int end = s.length() - suffixLength;
        return end > 0 ? s.substring(0, end) : "";
    }

    /**
     * Deletes a suffix from a {@code String} and returns the new result {@code String}.
     *
     * @param s      a {@code String}. It can be {@code null}.
     * @param suffix a suffix to delete. It shall not be {@code null}.
     * @return a new {@code String} without the suffix.
     * @throws NullPointerException if {@code suffix} is {@code null}.
     */
    @NotNull
    @Contract("null, _ -> !null; !null, null -> fail")
    public static String deleteSuffixIf(@Nullable String s, @NotNull String suffix) {
        if (s == null) {
            return "";
        }
        if (s.endsWith(suffix)) {
            return s.substring(0, s.length() - suffix.length());
        }
        return s;
    }

    /**
     * Deletes a suffix from a {@code StringBuilder}.
     *
     * @param sb           a {@code StringBuilder}. It can be {@code null}.
     * @param suffixLength a suffix length to delete. It shall be non-negative.
     * @throws StringIndexOutOfBoundsException if {@code suffixLength} is negative.
     */
    public static void deleteSuffix(@Nullable StringBuilder sb, int suffixLength) {
        if (sb == null) {
            return;
        }
        int length = sb.length();
        if (suffixLength >= length) {
            sb.setLength(0);
            return;
        }
        sb.delete(length - suffixLength, length);
    }

    /**
     * Deletes a suffix from a {@code StringBuilder}.
     *
     * @param sb     a {@code StringBuilder}. It can be {@code null}.
     * @param suffix a suffix to delete. It shall be non-negative.
     * @throws NullPointerException if {@code suffix} is {@code null}.
     */
    @Contract("!null, null -> fail")
    public static void deleteSuffixIf(@Nullable StringBuilder sb, @NotNull String suffix) {
        if (sb == null || sb.length() < suffix.length()) {
            return;
        }
        int end = sb.length() - suffix.length();
        if (suffix.equals(sb.substring(end))) {
            sb.delete(end, sb.length());
        }
    }

    /**
     * Wraps a {@code String}.
     *
     * @param s       a {@code String}. It can be {@code null}.
     * @param wrapper a wrapper {@code String}. It shall not be {@code null}.
     * @return a wrapped {@code String}.
     * @throws NullPointerException if {@code wrapper} is {@code null}.
     */
    @NotNull
    @Contract("_, !null -> new; _, null -> fail")
    public static String wrap(@Nullable String s, @NotNull String wrapper) {
        StringBuilder sb = new StringBuilder(wrapper);
        if (s != null) {
            sb.append(s);
        }
        return sb.append(wrapper).toString();
    }

    /**
     * Unwraps a {@code String}.
     *
     * @param s             a {@code String}. It can be {@code null}.
     * @param wrapperLength a wrapper length to delete at both ends. It shall be non-negative.
     * @return a unwrapped {@code String}.
     * @throws StringIndexOutOfBoundsException if {@code wrapperLength} is negative.
     */
    @NotNull
    @Contract("null, _ -> !null")
    public static String unwrap(@Nullable String s, int wrapperLength) {
        if (s == null) {
            return "";
        }
        int length = s.length();
        return wrapperLength * 2 >= length ? "" : s.substring(wrapperLength, length - wrapperLength);
    }

    /**
     * Changes the first {@code char} to upper case and returns the new result {@code String}.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return a new {@code String} with first {@code char} changed to upper case.
     */
    @NotNull
    @Contract("_ -> !null")
    public static String firstCharToUpperCase(@Nullable String s) {
        if (isEmpty(s)) {
            return "";
        }
        char ch = s.charAt(0);
        if (Character.isUpperCase(ch)) {
            return s;
        }
        StringBuilder sb = new StringBuilder(s);
        sb.setCharAt(0, Character.toUpperCase(ch));
        return sb.toString();
    }

    /**
     * Changes the first {@code char} to lower case and returns the new result {@code String}.
     *
     * @param s a String. It can be {@code null}.
     * @return a new {@code String} with first {@code char} changed to lower case.
     */
    @NotNull
    @Contract("_ -> !null")
    public static String firstCharToLowerCase(@Nullable String s) {
        if (isEmpty(s)) {
            return "";
        }
        char ch = s.charAt(0);
        if (Character.isLowerCase(ch)) {
            return s;
        }
        StringBuilder sb = new StringBuilder(s);
        sb.setCharAt(0, Character.toLowerCase(ch));
        return sb.toString();
    }

    /**
     * Changes the first {@code char} of a {@code StringBuilder} to upper case.
     *
     * @param sb a {@code StringBuilder}. It can be {@code null}.
     */
    public static void firstCharToUpperCase(@Nullable StringBuilder sb) {
        if (isEmpty(sb)) {
            return;
        }
        char ch = sb.charAt(0);
        if (Character.isUpperCase(ch)) {
            return;
        }
        sb.setCharAt(0, Character.toUpperCase(ch));
    }

    /**
     * Changes the first {@code char} of a {@code StringBuilder} to lower case.
     *
     * @param sb a {@code StringBuilder}. It can be {@code null}.
     */
    public static void firstCharToLowerCase(@Nullable StringBuilder sb) {
        if (isEmpty(sb)) {
            return;
        }
        char ch = sb.charAt(0);
        if (Character.isLowerCase(ch)) {
            return;
        }
        sb.setCharAt(0, Character.toLowerCase(ch));
    }

    /**
     * Splits a {@code String} into a {@code Set} of {@code Characters}.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return a {@code Set} of {@code Characters}.
     */
    @NotNull
    @Contract("_ -> new")
    public static Set<Character> splitChars(@Nullable String s) {
        if (isEmpty(s)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<Character> set = new HashSet<>();
        for (int I = 0; I < s.length(); I++) {
            char ch = s.charAt(I);
            set.add(Character.valueOf(ch));
        }
        return set;
    }

    /**
     * Splits a {@code String} into a {@code Set} of code points.
     *
     * @param s a {@code String}. It can be {@code null}.
     * @return a {@code Set} of code points.
     */
    @NotNull
    @Contract("_ -> new")
    public static Set<Integer> splitCodePoints(@Nullable String s) {
        if (isEmpty(s)) {
            return new HashSet<>(0);
        }
        //noinspection CollectionWithoutInitialCapacity
        Set<Integer> set = new HashSet<>();
        for (int I = 0; I < s.length(); I++) {
            int codePoint = s.codePointAt(I);
            set.add(Integer.valueOf(codePoint));
        }
        return set;
    }

    /**
     * Splits a {@code String} into a key-value pair.
     *
     * @param s         a {@code String}. It can be {@code null}.
     * @param separator a key-value separator. It shall not be {@code null}.
     * @return a length-2 {@code String array} of key and value.
     * @throws NullPointerException if {@code separator} is {@code null}.
     */
    @NotNull
    @Contract("_, !null -> new; null, null -> new; !null, null -> fail")
    public static String[] splitPair(@Nullable String s, @NotNull String separator) {
        String[] pair = {"", ""};
        if (isEmpty(s)) {
            return pair;
        }
        int separatorIndex = s.indexOf(separator);
        if (separatorIndex == -1) {
            pair[0] = s.trim();
            return pair;
        }
        pair[0] = trim(s.substring(0, separatorIndex));
        int valueStartIndex = separatorIndex + separator.length();
        if (valueStartIndex >= s.length()) {
            return pair;
        }
        pair[1] = trim(s.substring(valueStartIndex));
        return pair;
    }

    /**
     * Splits a {@code String} into an array of {@code String} tokens. It uses a series of separators, and each separator is used exactly once.
     * <p>Examples:</p>
     * <ul>
     * <li>{@code splitTokens("hello world", "l", "or") = ["he", "lo w", "ld"].}</li>
     * <li>{@code splitTokens("fee = 3.45$", "=") = ["fee", "3.45$"].}</li>
     * <li>{@code splitTokens(null, ":", "#") = ["", "", ""].}</li>
     * </ul>
     *
     * @param s          a {@code String} to split. It can be {@code null}.
     * @param separators an array of separators. It shall not be {@code null}. It shall not contain any {@code null} element.
     * @return an array of {@code String} tokens.
     * @throws NullPointerException if {@code separators} is {@code null} or contains {@code null} element.
     */
    @NotNull
    @Contract("_, null -> fail; _, !null -> new")
    public static String[] splitTokens(@Nullable String s, @NotNull String @NotNull ... separators) {
        String[] tokens = new String[separators.length + 1];
        Arrays.fill(tokens, "");
        if (isEmpty(s)) {
            return tokens;
        }
        int current = 0;
        for (int I = 0; I < separators.length; I++) {
            String separator = separators[I];
            int separatorIndex = s.indexOf(separator, current);
            if (separatorIndex == -1) {
                tokens[I] = trim(s.substring(current));
                return tokens;
            }
            tokens[I] = trim(s.substring(current, separatorIndex));
            current = separatorIndex + separator.length();
            if (current >= s.length()) {
                return tokens;
            }
        }
        tokens[separators.length] = trim(s.substring(current));
        return tokens;
    }

    /**
     * Splits a {@code String} into a key-value {@code Map} by key-value separator and entry separator.
     *
     * @param s                 a {@code String}. It can be {@code null}.
     * @param keyValueSeparator a key-value separator. It shall not be {@code null}.
     * @param entrySeparator    an entry separator. It shall not be {@code null}.
     * @return a key-value {@code Map}. It shall not be {@code null}.
     * @throws NullPointerException if {@code keyValueSeparator} or {@code entrySeparator} is {@code null}.
     */
    @NotNull
    @Contract("null, _, _ -> new; !null, null, _ -> fail; !null, _, null -> fail")
    public static Map<String, String> splitKeyValues(@Nullable String s, @NotNull String keyValueSeparator, @NotNull String entrySeparator) {
        if (isEmpty(s)) {
            return new HashMap<>(0);
        }
        Map<String, String> result = new HashMap<>();
        int current = 0;
        while (current < s.length()) {
            int index = s.indexOf(entrySeparator, current);
            String keyValue = null;
            if (index == -1) {
                keyValue = s.substring(current);
                current = s.length();
            } else {
                keyValue = s.substring(current, index);
                current = index + entrySeparator.length();
            }
            String[] pair = splitPair(keyValue, keyValueSeparator);
            result.put(pair[0], pair[1]);
        }
        result.remove("");
        return result;
    }

    private StringUtil() {
        // DO NOTHING.
    }
}
