module name.slhynju.util {
    requires org.slf4j;
    requires org.jetbrains.annotations;

    exports name.slhynju;
    exports name.slhynju.util;
    exports name.slhynju.util.collection;
    exports name.slhynju.util.collection.graph;
    exports name.slhynju.util.collection.graph.directed;
    exports name.slhynju.util.collection.graph.undirected;
    exports name.slhynju.util.collection.tree;
    exports name.slhynju.util.collection.tree.walker;
    exports name.slhynju.util.io;
    exports name.slhynju.util.nio;
    exports name.slhynju.util.reflect;
    exports name.slhynju.util.resource;
}