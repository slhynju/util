package name.slhynju;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class UnexpectedApplicationFlowExceptionTest {

    @Test
    public void defaultConstructor() {
        var e = new UnexpectedApplicationFlowException();
        assertEquals("Unexpected application flow.", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void message() {
        var e = new UnexpectedApplicationFlowException("message2");
        assertEquals("message2", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void message_null() {
        var e = new UnexpectedApplicationFlowException((String) null);
        assertNull(e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void charSequence() {
        CharSequence sb = new StringBuilder("message3");
        var e = new UnexpectedApplicationFlowException(sb);
        assertEquals("message3", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void charSequence_null() {
        CharSequence sb = null;
        assertThrows(NullPointerException.class, () -> new UnexpectedApplicationFlowException(sb));
    }

    @Test
    public void cause() {
        Throwable cause = new RuntimeException("abc");
        var e = new UnexpectedApplicationFlowException(cause);
        assertEquals("Unexpected application flow.", e.getMessage());
        assertSame(cause, e.getCause());
    }

    @Test
    public void cause_null() {
        Throwable cause = null;
        var e = new UnexpectedApplicationFlowException(cause);
        assertEquals("Unexpected application flow.", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void messageAndCause() {
        Throwable cause = new RuntimeException("abc");
        var e = new UnexpectedApplicationFlowException("message4", cause);
        assertEquals("message4", e.getMessage());
        assertSame(cause, e.getCause());
    }

    @Test
    public void messageAndCause_messageNull() {
        Throwable cause = new RuntimeException("abc");
        var e = new UnexpectedApplicationFlowException(null, cause);
        assertNull(e.getMessage());
        assertSame(cause, e.getCause());
    }

    @Test
    public void messageAndCause_causeNull() {
        Throwable cause = null;
        var e = new UnexpectedApplicationFlowException("message4", cause);
        assertEquals("message4", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void messageAndCause_bothNull() {
        Throwable cause = null;
        var e = new UnexpectedApplicationFlowException(null, cause);
        assertNull(e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void charSequenceAndCause() {
        StringBuilder sb = new StringBuilder("message5");
        Throwable cause = new RuntimeException("abc");
        var e = new UnexpectedApplicationFlowException(sb, cause);
        assertEquals("message5", e.getMessage());
        assertSame(cause, e.getCause());
    }

    @Test
    public void charSequenceAndCause_messageNull() {
        StringBuilder sb = null;
        Throwable cause = new RuntimeException("abc");
        assertThrows(NullPointerException.class, () -> new UnexpectedApplicationFlowException(sb, cause));
    }

    @Test
    public void charSequenceAndCause_causeNull() {
        StringBuilder sb = new StringBuilder("message5");
        Throwable cause = null;
        var e = new UnexpectedApplicationFlowException(sb, cause);
        assertEquals("message5", e.getMessage());
        assertNull(cause);
    }

    @Test
    public void charSequenceAndCause_bothNull() {
        StringBuilder sb = null;
        Throwable cause = null;
        assertThrows(NullPointerException.class, () -> new UnexpectedApplicationFlowException(sb, cause));
    }

}
