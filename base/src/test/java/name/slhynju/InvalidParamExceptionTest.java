package name.slhynju;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class InvalidParamExceptionTest {

    @Test
    public void paramName() {
        var e = new InvalidParamException("address");
        assertEquals("Invalid param address.", e.getMessage());
        assertNull(e.getCause());
        assertEquals("address", e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramName_null() {
        var e = new InvalidParamException(null);
        assertEquals("Invalid param null.", e.getMessage());
        assertNull(e.getCause());
        assertNull(e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramNameAndCause() {
        Throwable cause = new RuntimeException("abc");
        var e = new InvalidParamException("address", cause);
        assertEquals("Invalid param address.", e.getMessage());
        assertSame(cause, e.getCause());
        assertEquals("address", e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramNameAndCause_paramNameNull() {
        Throwable cause = new RuntimeException("abc");
        var e = new InvalidParamException(null, cause);
        assertEquals("Invalid param null.", e.getMessage());
        assertSame(cause, e.getCause());
        assertNull(e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramNameAndCause_causeNull() {
        Throwable cause = null;
        var e = new InvalidParamException("address", cause);
        assertEquals("Invalid param address.", e.getMessage());
        assertNull(e.getCause());
        assertEquals("address", e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramNameAndCause_bothNull() {
        Throwable cause = null;
        var e = new InvalidParamException(null, cause);
        assertEquals("Invalid param null.", e.getMessage());
        assertNull(e.getCause());
        assertNull(e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramNameAndValue() {
        var e = new InvalidParamException("name", "some-invalid-name");
        assertEquals("Invalid param name with value some-invalid-name.", e.getMessage());
        assertNull(e.getCause());
        assertEquals("name", e.getParamName());
        assertEquals("some-invalid-name", e.getParamValue());
    }

    @Test
    public void paramNameAndValue_paramNameNull() {
        var e = new InvalidParamException(null, "some-invalid-name");
        assertEquals("Invalid param null with value some-invalid-name.", e.getMessage());
        assertNull(e.getCause());
        assertNull(e.getParamName());
        assertEquals("some-invalid-name", e.getParamValue());
    }

    @Test
    public void paramNameAndValue_paramValueNull() {
        Object paramValue = null;
        var e = new InvalidParamException("name", paramValue);
        assertEquals("Invalid param name with value null.", e.getMessage());
        assertNull(e.getCause());
        assertEquals("name", e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramNameAndValue_bothNull() {
        Object paramValue = null;
        var e = new InvalidParamException(null, paramValue);
        assertEquals("Invalid param null with value null.", e.getMessage());
        assertNull(e.getCause());
        assertNull(e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramNameValueAndCause() {
        Integer paramValue = Integer.valueOf(10);
        Throwable cause = new RuntimeException("abc");
        var e = new InvalidParamException("lowerBound", paramValue, cause);
        assertEquals("Invalid param lowerBound with value 10.", e.getMessage());
        assertSame(cause, e.getCause());
        assertEquals("lowerBound", e.getParamName());
        assertEquals(paramValue, e.getParamValue());
    }

    @Test
    public void paramNameValueAndCause_paramNameNull() {
        Integer paramValue = Integer.valueOf(10);
        Throwable cause = new RuntimeException("abc");
        var e = new InvalidParamException(null, paramValue, cause);
        assertEquals("Invalid param null with value 10.", e.getMessage());
        assertSame(cause, e.getCause());
        assertNull(e.getParamName());
        assertEquals(paramValue, e.getParamValue());
    }

    @Test
    public void paramNameValueAndCause_paramValueNull() {
        Integer paramValue = null;
        Throwable cause = new RuntimeException("abc");
        var e = new InvalidParamException("lowerBound", paramValue, cause);
        assertEquals("Invalid param lowerBound with value null.", e.getMessage());
        assertSame(cause, e.getCause());
        assertEquals("lowerBound", e.getParamName());
        assertNull(e.getParamValue());
    }

    @Test
    public void paramNameValueAndCause_causeNull() {
        Integer paramValue = Integer.valueOf(10);
        Throwable cause = null;
        var e = new InvalidParamException("lowerBound", paramValue, cause);
        assertEquals("Invalid param lowerBound with value 10.", e.getMessage());
        assertNull(e.getCause());
        assertEquals("lowerBound", e.getParamName());
        assertEquals(paramValue, e.getParamValue());
    }

    @Test
    public void paramNameValueAndCause_allNull() {
        Integer paramValue = null;
        Throwable cause = null;
        var e = new InvalidParamException(null, paramValue, cause);
        assertEquals("Invalid param null with value null.", e.getMessage());
        assertNull(e.getCause());
        assertNull(e.getParamName());
        assertNull(e.getParamValue());
    }

}
