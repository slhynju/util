package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtil_toInstant_Test {

    @Test
    public void toInstant() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        Instant t = DateUtil.toInstant(d);
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        LocalDateTime t2 = LocalDateTime.ofInstant(t, zoneId);
        assertEquals(2012, t2.getYear());
        assertEquals(2, t2.getMonthValue());
        assertEquals(4, t2.getDayOfMonth());
        assertEquals(15, t2.getHour());
        assertEquals(36, t2.getMinute());
        assertEquals(2, t2.getSecond());
    }

    @Test
    public void toInstant_null() {
        assertNull(DateUtil.toInstant(null));
    }

    @Test
    public void toZonedDateTime_date() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = format.parse("2012-02-04 15:36:02");
        ZonedDateTime t = DateUtil.toZonedDateTime(d);
        assertEquals(2012, t.getYear());
        assertEquals(2, t.getMonthValue());
        assertEquals(4, t.getDayOfMonth());
        assertEquals(15, t.getHour());
        assertEquals(36, t.getMinute());
        assertEquals(2, t.getSecond());
    }

    @Test
    public void toZonedDateTime_date_dateNull() {
        assertNull(DateUtil.toZonedDateTime((Date) null));
    }

    @Test
    public void toZonedDateTime_dateWithTimeZone() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        ZonedDateTime l = DateUtil.toZonedDateTime(d, zone);
        assertEquals(2012, l.getYear());
        assertEquals(2, l.getMonthValue());
        assertEquals(4, l.getDayOfMonth());
        assertEquals(15, l.getHour());
        assertEquals(36, l.getMinute());
        assertEquals(2, l.getSecond());
    }

    @Test
    public void toZonedDateTime_dateWithTimeZone_dateNull() {
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        assertNull(DateUtil.toZonedDateTime((Date) null, zone));
    }

    @Test
    public void toZonedDateTime_dateWithTimeZone_zoneNull() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        assertThrows(NullPointerException.class, () -> DateUtil.toZonedDateTime(d, (TimeZone) null));
    }

    @Test
    public void toZonedDateTime_dateWithTimeZone_bothNull() {
        assertThrows(NullPointerException.class, () -> DateUtil.toZonedDateTime((Date) null, (TimeZone) null));
    }

    @Test
    public void toZonedDateTime_dateWithZoneId() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        ZonedDateTime l = DateUtil.toZonedDateTime(d, zoneId);
        assertEquals(2012, l.getYear());
        assertEquals(2, l.getMonthValue());
        assertEquals(4, l.getDayOfMonth());
        assertEquals(15, l.getHour());
        assertEquals(36, l.getMinute());
        assertEquals(2, l.getSecond());
    }

    @Test
    public void toZonedDateTime_dateWithZoneId_dateNull() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        assertNull(DateUtil.toZonedDateTime((Date) null, zoneId));
    }

    @Test
    public void toZonedDateTime_dateWithZoneId_zoneNull() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        assertThrows(NullPointerException.class, () -> DateUtil.toZonedDateTime(d, (ZoneId) null));
    }

    @Test
    public void toZonedDateTime_dateWithZoneId_bothNull() {
        assertNull(DateUtil.toZonedDateTime((Date) null, (ZoneId) null));
    }
}
