package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.text.Collator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SuppressWarnings("static-method")
public class CompareToBuilderTest {

    @Test
    public void comparable() {
        CompareToBuilder c = new CompareToBuilder();
        c.append("abc", "def");
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void comparableArray() {
        CompareToBuilder c = new CompareToBuilder();
        String[] a1 = new String[]{"abc", "def"};
        String[] a2 = new String[]{"abc", "dd"};
        c.append(a1, a2);
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void comparableArray2() {
        CompareToBuilder c = new CompareToBuilder();
        String[] a1 = new String[]{"abc", "def"};
        c.append(a1, a1);
        assertTrue(c.toValue() == 0);
    }

    @Test
    public void comparableArray3() {
        CompareToBuilder c = new CompareToBuilder();
        String[] a1 = new String[]{"abc", "def"};
        String[] a2 = new String[]{"abc", "def", "xyz"};
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void intType() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(5, 4);
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void intType2() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(2, 3);
        c.append(5, 4);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void intArray() {
        CompareToBuilder c = new CompareToBuilder();
        int[] a1 = new int[]{5, 4, 3, 2};
        int[] a2 = new int[]{5, 4, 3};
        c.append(a1, a2);
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void intArray2() {
        CompareToBuilder c = new CompareToBuilder();
        int[] a1 = new int[]{5, 4, 3, 2};
        c.append(a1, a1);
        assertTrue(c.toValue() == 0);
    }

    @Test
    public void intArray3() {
        CompareToBuilder c = new CompareToBuilder();
        int[] a1 = new int[]{5, 4, 3, 2};
        int[] a2 = new int[]{5, 5, 3};
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void longType() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(7L, 9L);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void longType2() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(7L, 9L);
        c.append(10L, 8L);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void longArray() {
        CompareToBuilder c = new CompareToBuilder();
        long[] a1 = new long[]{2L, 5L};
        long[] a2 = new long[]{2L, 3L};
        c.append(a1, a2);
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void longArray2() {
        CompareToBuilder c = new CompareToBuilder();
        long[] a1 = new long[]{2L, 5L};
        c.append(a1, a1);
        assertTrue(c.toValue() == 0);
    }

    @Test
    public void longArray3() {
        CompareToBuilder c = new CompareToBuilder();
        long[] a1 = new long[]{2L, 5L};
        long[] a2 = new long[]{2L, 5L, 8L};
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void doubleType() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(3.45, 3.47);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void doubleType2() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(3.45, 3.47);
        c.append(3.47, 3.45);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void doubleArray() {
        CompareToBuilder c = new CompareToBuilder();
        double[] a1 = new double[]{2.4, 5.3};
        double[] a2 = new double[]{2.4, 4.9};
        c.append(a1, a2);
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void doubleArray2() {
        CompareToBuilder c = new CompareToBuilder();
        double[] a1 = new double[]{2.4, 5.3};
        c.append(a1, a1);
        assertTrue(c.toValue() == 0);
    }

    @Test
    public void doubleArray3() {
        CompareToBuilder c = new CompareToBuilder();
        double[] a1 = new double[]{2.4, 5.3};
        double[] a2 = new double[]{2.4, 5.3, 4.9};
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void booleanType() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(false, true);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void booleanType2() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(false, true);
        c.append(true, false);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void booleanArray() {
        CompareToBuilder c = new CompareToBuilder();
        boolean[] a1 = new boolean[]{false, true};
        boolean[] a2 = new boolean[]{false, false};
        c.append(a1, a2);
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void booleanArray2() {
        CompareToBuilder c = new CompareToBuilder();
        boolean[] a1 = new boolean[]{false, true};
        c.append(a1, a1);
        assertTrue(c.toValue() == 0);
    }

    @Test
    public void booleanArray3() {
        CompareToBuilder c = new CompareToBuilder();
        boolean[] a1 = new boolean[]{false, true};
        boolean[] a2 = new boolean[]{false, true, false};
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void charType() {
        CompareToBuilder c = new CompareToBuilder();
        c.append('a', 'b');
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void charType2() {
        CompareToBuilder c = new CompareToBuilder();
        c.append('a', 'b');
        c.append('a', 'a');
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void charType3() {
        CompareToBuilder c = new CompareToBuilder();
        c.append('b', 'a');
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void charType4() {
        CompareToBuilder c = new CompareToBuilder();
        c.append('a', 'a');
        assertTrue(c.toValue() == 0);
    }

    @Test
    public void collator() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(1, 2);
        Collator collator = mock(Collator.class);
        c.append("a", "b", collator);
        assertTrue(c.toValue() < 0);
        verify(collator, never()).compare(anyString(), anyString());
    }

    @Test
    public void collator2() {
        CompareToBuilder c = new CompareToBuilder();
        Collator collator = mock(Collator.class);
        when(collator.compare(anyString(), anyString())).thenReturn(-1);
        c.append("a", "b", collator);
        assertTrue(c.toValue() < 0);
        verify(collator).compare(anyString(), anyString());
    }

    @Test
    public void collator3() {
        CompareToBuilder c = new CompareToBuilder();
        Collator collator = mock(Collator.class);
        when(collator.compare(anyString(), anyString())).thenReturn(1);
        c.append("a", "b", collator);
        assertTrue(c.toValue() > 0);
        verify(collator).compare(anyString(), anyString());
    }

    // the test method to compare Strings with Collator is skipped as it is Locale
    // dependent.

    @Test
    public void classType() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(CompareToBuilder.class, CompareToBuilderTest.class);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void classType2() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(CompareToBuilder.class, CompareToBuilderTest.class);
        c.append(CompareToBuilderTest.class, CompareToBuilder.class);
        assertTrue(c.toValue() < 0);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void classArray() {
        CompareToBuilder c = new CompareToBuilder();
        Class[] a1 = new Class[]{CompareToBuilder.class};
        Class[] a2 = new Class[]{CompareToBuilderTest.class};
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void classArray2() {
        CompareToBuilder c = new CompareToBuilder();
        Class[] a1 = new Class[]{CompareToBuilder.class};
        c.append(a1, a1);
        assertTrue(c.toValue() == 0);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void classArray3() {
        CompareToBuilder c = new CompareToBuilder();
        Class[] a1 = new Class[]{CompareToBuilder.class};
        Class[] a2 = new Class[]{CompareToBuilder.class, CompareToBuilderTest.class};
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    public void classArray_elementNull() {
        CompareToBuilder c = new CompareToBuilder();
        Class[] a1 = new Class[]{CompareToBuilder.class, null, null};
        Class[] a2 = new Class[]{CompareToBuilder.class, null, CompareToBuilderTest.class};
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void methodType() throws Exception {
        Method m1 = CompareToBuilder.class.getMethod("append", Integer.TYPE, Integer.TYPE);
        Method m2 = CompareToBuilder.class.getMethod("append", Long.TYPE, Long.TYPE);
        CompareToBuilder c = new CompareToBuilder();
        c.append(m1, m2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void methodType2() throws Exception {
        Method m1 = CompareToBuilder.class.getMethod("append", Integer.TYPE, Integer.TYPE);
        Method m2 = CompareToBuilder.class.getMethod("append", Long.TYPE, Long.TYPE);
        CompareToBuilder c = new CompareToBuilder();
        c.append(m1, m2);
        c.append(m2, m1);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void methodType3() throws Exception {
        Method m1 = String.class.getMethod("toString");
        Method m2 = String.class.getMethod("length");
        CompareToBuilder c = new CompareToBuilder();
        c.append(m1, m2);
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void methodArray() throws Exception {
        Method m1 = CompareToBuilder.class.getMethod("append", Integer.TYPE, Integer.TYPE);
        Method m2 = CompareToBuilder.class.getMethod("append", Long.TYPE, Long.TYPE);
        Method[] a1 = new Method[]{m1};
        Method[] a2 = new Method[]{m2};
        CompareToBuilder c = new CompareToBuilder();
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void methodArray2() throws Exception {
        Method m1 = CompareToBuilder.class.getMethod("append", Integer.TYPE, Integer.TYPE);
        Method[] a1 = new Method[]{m1};
        CompareToBuilder c = new CompareToBuilder();
        c.append(a1, a1);
        assertTrue(c.toValue() == 0);
    }

    @Test
    public void methodArray3() throws Exception {
        Method m1 = CompareToBuilder.class.getMethod("append", Integer.TYPE, Integer.TYPE);
        Method m2 = CompareToBuilder.class.getMethod("append", Long.TYPE, Long.TYPE);
        Method[] a1 = new Method[]{m1};
        Method[] a2 = new Method[]{m1, m2};
        CompareToBuilder c = new CompareToBuilder();
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void methodArray_elementNull() throws Exception {
        Method m1 = CompareToBuilder.class.getMethod("append", Integer.TYPE, Integer.TYPE);
        Method m2 = CompareToBuilder.class.getMethod("append", Long.TYPE, Long.TYPE);
        Method[] a1 = new Method[]{m1, null, null};
        Method[] a2 = new Method[]{m1, null, m2};
        CompareToBuilder c = new CompareToBuilder();
        c.append(a1, a2);
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void byPass() {
        CompareToBuilder c = new CompareToBuilder();
        c.append(null, "a");
        assertTrue(c.toValue() < 0);
    }

    @Test
    public void byPass2() {
        CompareToBuilder c = new CompareToBuilder();
        c.append("a", null);
        assertTrue(c.toValue() > 0);
    }

    @Test
    public void bypass3() {
        CompareToBuilder c = new CompareToBuilder();
        Integer n = Integer.valueOf(5);
        c.append(n, n);
        assertEquals(0, c.toValue());
    }

}
