package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FreeStringBuilder_Format_Test {

    @Test
    public void format() {
        String result = FreeStringBuilder.format("{} and {}", "A", "B");
        assertEquals("A and B", result);
    }

    @Test
    public void format2() {
        String result = FreeStringBuilder.format("{} and {}", 1, 2);
        assertEquals("1 and 2", result);
    }

    @Test
    public void format_null() {
        String result = FreeStringBuilder.format(null, new Object[]{});
        assertEquals("null", result);
    }

    @Test
    public void format_valueNull() {
        Object[] values = new Object[]{"A", null};
        String result = FreeStringBuilder.format("{} and {}", values);
        assertEquals("A and null", result);
    }

    @Test
    public void format_allNull() {
        Object[] values = null;
        String result = FreeStringBuilder.format(null, values);
        assertEquals("null", result);
    }

    @Test
    public void format_index() {
        String result = FreeStringBuilder.format("{1} and {0}", "A", "B");
        assertEquals("B and A", result);
    }

    @Test
    public void format_invalidIndex() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> FreeStringBuilder.format("{1} and {2}", "A", "B"));
    }

    @Test
    public void format_invalidFormat() {
        assertThrows(IllegalArgumentException.class, () -> FreeStringBuilder.format("{1} and {", "A", "B"));
    }

    @Test
    public void appendFormat() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendFormat("{} is a String.", "abc");
        assertEquals("abc is a String.", sb.toS());
    }

    @Test
    public void appendFormat_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendFormat(null, "abc");
        assertEquals("null", sb.toS());
    }

    @Test
    public void appendFormat_index() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendFormat("{} equals to {0}.", "abc");
        assertEquals("abc equals to abc.", sb.toS());
    }

    @Test
    public void appendFormat_index2() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendFormat("{1} is greater than {0}.", 2, 5);
        assertEquals("5 is greater than 2.", sb.toS());
    }

    @Test
    public void appendFormat_noParameter() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendFormat("no parameter.");
        assertEquals("no parameter.", sb.toS());
    }

    @Test
    public void appendFormat_invalidFormat() {
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(IllegalArgumentException.class, () -> sb.appendFormat("this is bad format {  {", "abc"));
    }

    @Test
    public void appendFormat_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> sb.appendFormat("{2} is greater than {3}", "abc"));
    }

}
