package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

public class FreeStringBuilder_CommonTypes_Test {

    @Test
    public void enumType() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(Locale.Category.DISPLAY);
        assertEquals("DISPLAY", sb.toS());
    }

    @Test
    public void enumType_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Locale.Category category = null;
        sb.append(category);
        assertEquals("null", sb.toS());
    }

    @Test
    public void date() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date d = DateUtil.toDate("2012-03-04", "yyyy-MM-dd");
        sb.append(d, "yyyy-MM-dd");
        assertEquals("2012-03-04", sb.toS());
    }


    @Test
    public void date_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date d = null;
        sb.append(d, "yyyy-MM-dd");
        assertEquals("null", sb.toS());
    }

    @Test
    public void date_formatNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date d = DateUtil.toDate("2012-03-04", "yyyy-MM-dd");
        String format = null;
        assertThrows(NullPointerException.class, () -> sb.append(d, format));
    }

    @Test
    public void date_bothNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date d = null;
        String format = null;
        sb.append(d, format);
        assertEquals("null", sb.toS());
    }

    @Test
    public void dateWithFormat() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date d = DateUtil.toDate("2012-03-04", "yyyy-MM-dd");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        sb.append(d, format);
        assertEquals("2012-03-04", sb.toS());
    }

    @Test
    public void dateWithFormat_dateNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date d = null;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        sb.append(d, format);
        assertEquals("null", sb.toS());
    }

    @Test
    public void dateWithFormat_formatNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date d = DateUtil.toDate("2012-03-04", "yyyy-MM-dd");
        DateFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append(d, format));
    }

    @Test
    public void dateWithFormat_bothNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date d = null;
        DateFormat format = null;
        sb.append(d, format);
        assertEquals("null", sb.toS());
    }

    @Test
    public void temporal() {
        FreeStringBuilder sb = new FreeStringBuilder();
        LocalDate date = LocalDate.of(2012, 3, 4);
        DateTimeFormatter format = DateTimeFormatter.ISO_LOCAL_DATE;
        sb.append(date, format);
        assertEquals("2012-03-04", sb.toS());
    }

    @Test
    public void temporal_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        LocalDate date = null;
        DateTimeFormatter format = DateTimeFormatter.ISO_LOCAL_DATE;
        sb.append(date, format);
        assertEquals("null", sb.toS());
    }

    @Test
    public void temporal_formatNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        LocalDate date = LocalDate.of(2012, 3, 4);
        DateTimeFormatter format = null;
        assertThrows(NullPointerException.class, () -> sb.append(date, format));
    }

    @Test
    public void temporal_bothNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        LocalDate date = null;
        DateTimeFormatter format = null;
        sb.append(date, format);
        assertEquals("null", sb.toS());
    }

    @Test
    public void path() throws Exception {
        String pathName = "test_locale_zh.properties";
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL url = classLoader.getResource(pathName);
        Path p = Paths.get(url.toURI());
        assertNotNull(p);
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(p);
        assertEquals(p.toString(), sb.toS());
    }

    @Test
    public void path_null() {
        Path p = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(p);
        assertEquals("null", sb.toS());
    }

}
