package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DateUtil_toDate_Instant_Test {

    @Test
    public void toDate_instant() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        ZonedDateTime t = ZonedDateTime.of(2012, 2, 4, 15, 48, 3, 0, zoneId);
        Instant t2 = Instant.from(t);
        Date d = DateUtil.toDate(t2);
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(zone);
        String s = format.format(d);
        assertEquals("2012-02-04 15:48:03", s);
    }

    @Test
    public void toDate_instant_timeNull() {
        assertNull(DateUtil.toDate((Instant) null));
    }

    @Test
    public void toDate_zonedDateTime() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        ZonedDateTime t = ZonedDateTime.of(2012, 2, 4, 15, 48, 3, 0, zoneId);
        Date d = DateUtil.toDate(t);
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(zone);
        String s = format.format(d);
        assertEquals("2012-02-04 15:48:03", s);
    }

    @Test
    public void toDate_zonedDateTime_timeNull() {
        assertNull(DateUtil.toDate((ZonedDateTime) null));
    }
}
