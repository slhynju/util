package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BeanStringBuilder_Collection_Test {

    @Test
    public void collection() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Collection<String> list = new ArrayList<>();
        list.add("abc");
        list.add("def");
        sb.append("p", list);
        assertEquals("name.slhynju.util.Bean{p: [abc, def]}", sb.toS());
    }

    @Test
    public void collection_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Collection<String> list = null;
        sb.append("p", list);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void collectionMapper() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Collection<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(null);
        Function<Integer, String> mapper = (x) -> x == null ? null : x.toString();
        sb.append("p", list, mapper);
        assertEquals("name.slhynju.util.Bean{p: [1, 2, null]}", sb.toS());
    }

    @Test
    public void collectionMapper_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Collection<Integer> list = null;
        Function<Integer, String> mapper = (x) -> x == null ? null : x.toString();
        sb.append("p", list, mapper);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void collectionMapper_mapperNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Collection<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Function<Integer, String> mapper = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", list, mapper));
    }

    @Test
    public void collectionMapper_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Collection<Integer> list = null;
        Function<Integer, String> mapper = null;
        sb.append("p", list, mapper);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void intArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        int[] a = new int[]{1, 2, 3};
        sb.append("p", a);
        assertEquals("name.slhynju.util.Bean{p: [1, 2, 3]}", sb.toS());
    }

    @Test
    public void intArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        int[] a = null;
        sb.append("p", a);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void objectArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Object[] a = new Object[]{"s1", "s2", "s3"};
        sb.append("p", a);
        assertEquals("name.slhynju.util.Bean{p: [s1, s2, s3]}", sb.toS());
    }

    @Test
    public void objectArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Object[] a = null;
        sb.append("p", a);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void arrayMapper() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Integer[] a = new Integer[]{1, 2, 3};
        Function<Integer, String> mapper = (x) -> x == null ? null : String.valueOf(x.intValue() * x.intValue());
        sb.append("p", a, mapper);
        assertEquals("name.slhynju.util.Bean{p: [1, 4, 9]}", sb.toS());
    }

    @Test
    public void arrayMapper_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Integer[] a = null;
        Function<Integer, String> mapper = (x) -> x == null ? null : String.valueOf(x.intValue() * x.intValue());
        sb.append("p", a, mapper);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void arrayMapper_mapperNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Integer[] a = new Integer[]{1, 2, 3};
        Function<Integer, String> mapper = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", a, mapper));
    }

    @Test
    public void arrayMapper_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Integer[] a = null;
        Function<Integer, String> mapper = null;
        sb.append("p", a, mapper);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void map() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        sb.append("p", map);
        assertEquals("name.slhynju.util.Bean{p: {k1: v1, k2: v2}}", sb.toS());
    }

    @Test
    public void map_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Map<String, String> map = null;
        sb.append("p", map);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void mapMapper() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 3);
        map.put(2, 5);
        Function<Integer, String> keyMapper = (x) -> x == null ? null : x.toString();
        Function<Integer, String> valueMapper = (x) -> x == null ? null : String.valueOf(x.intValue() * x.intValue());
        sb.append("p", map, keyMapper, valueMapper);
        assertEquals("name.slhynju.util.Bean{p: {1: 9, 2: 25}}", sb.toS());
    }

    @Test
    public void mapMapper_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Map<Integer, Integer> map = null;
        Function<Integer, String> keyMapper = (x) -> x == null ? null : x.toString();
        Function<Integer, String> valueMapper = (x) -> x == null ? null : String.valueOf(x.intValue() * x.intValue());
        sb.append("p", map, keyMapper, valueMapper);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void mapMapper_keyMaperNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 3);
        map.put(2, 5);
        Function<Integer, String> keyMapper = null;
        Function<Integer, String> valueMapper = (x) -> x == null ? null : String.valueOf(x.intValue() * x.intValue());
        assertThrows(NullPointerException.class, () -> sb.append("p", map, keyMapper, valueMapper));
    }

    @Test
    public void mapMapper_valueMapperNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 3);
        map.put(2, 5);
        Function<Integer, String> keyMapper = (x) -> x == null ? null : x.toString();
        Function<Integer, String> valueMapper = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", map, keyMapper, valueMapper));
    }

    @Test
    public void mapMapper_allNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Map<Integer, Integer> map = null;
        Function<Integer, String> keyMapper = null;
        Function<Integer, String> valueMapper = null;
        sb.append("p", map, keyMapper, valueMapper);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void stack() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        sb.appendStack("p", stack);
        assertEquals("name.slhynju.util.Bean{p: [h1, h2, h3]}", sb.toS());
    }

    @Test
    public void stack_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Deque<String> stack = null;
        sb.appendStack("p", stack);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void stackMapper() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Deque<Integer> stack = new ArrayDeque<>();
        stack.addFirst(1);
        stack.addFirst(2);
        stack.addFirst(3);
        Function<Integer, String> mapper = (x) -> x == null ? null : String.valueOf(x.intValue() * x.intValue());
        sb.appendStack("p", stack, mapper);
        assertEquals("name.slhynju.util.Bean{p: [1, 4, 9]}", sb.toS());
    }

    @Test
    public void stackMapper_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Deque<Integer> stack = null;
        Function<Integer, String> mapper = (x) -> x == null ? null : String.valueOf(x.intValue() * x.intValue());
        sb.appendStack("p", stack, mapper);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void stackMapper_mapperNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Deque<Integer> stack = new ArrayDeque<>();
        stack.addFirst(1);
        stack.addFirst(2);
        stack.addFirst(3);
        Function<Integer, String> mapper = null;
        assertThrows(NullPointerException.class, () -> sb.appendStack("p", stack, mapper));
    }

    @Test
    public void stackMapper_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Deque<Integer> stack = null;
        Function<Integer, String> mapper = null;
        sb.appendStack("p", stack, mapper);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

}
