package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FreeStringBuilder_StringArray_Test {

    @Test
    public void toS() {
        String[] a0 = new String[0];
        assertEquals("", FreeStringBuilder.toS(a0, ","));
        String[] a1 = new String[]{"abc"};
        assertEquals("abc", FreeStringBuilder.toS(a1, ","));
        String[] a2 = new String[]{"abc", "def"};
        assertEquals("abc,def", FreeStringBuilder.toS(a2, ","));
    }

    @Test
    public void toS_null() {
        assertEquals("null", FreeStringBuilder.toS((String[]) null, ","));
    }

    @Test
    public void toS_elementNull() {
        String[] a = new String[]{"abc", null, "def"};
        assertEquals("abc,null,def", FreeStringBuilder.toS(a, ","));
    }

    @Test
    public void toS_separatorNull() {
        String[] a = new String[]{"abc", "def"};
        assertThrows(NullPointerException.class, () -> FreeStringBuilder.toS(a, null));
    }

    @Test
    public void toS_bothNull() {
        assertEquals("null", FreeStringBuilder.toS((String[]) null, null));
    }

    @Test
    public void stringArray() {
        String[] a = new String[]{"a", "b", "c"};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a);
        assertEquals("[a, b, c]", sb.toS());
    }

    @Test
    public void stringArray_null() {
        String[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a);
        assertEquals("null", sb.toS());
    }

    @Test
    public void stringArray_elementNull() {
        String[] a = new String[]{"a", "b", null};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a);
        assertEquals("[a, b, null]", sb.toS());
    }

    @Test
    public void stringArrayWithoutBoundary() {
        String[] a = new String[]{"a", "b", "c"};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendWithoutMarks(a);
        assertEquals("a, b, c", sb.toS());
    }

    @Test
    public void stringArrayWithoutBoundary_null() {
        String[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendWithoutMarks(a);
        assertEquals("null", sb.toS());
    }

    @Test
    public void stringArrayWithoutBoundary_elementNull() {
        String[] a = new String[]{"a", null, "c"};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendWithoutMarks(a);
        assertEquals("a, null, c", sb.toS());
    }

    @Test
    public void stringArrayWithBoundary() {
        String[] a = new String[]{"v1", "v2", "v3"};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "(", "+", ")");
        assertEquals("(v1+v2+v3)", sb.toS());
    }

    @Test
    public void stringArrayWithBoundary_null() {
        String[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "(", "+", ")");
        assertEquals("null", sb.toS());
    }

    @Test
    public void stringArrayWithBoundary_elementNull() {
        String[] a = new String[]{"v1", "v2", null};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "(", "+", ")");
        assertEquals("(v1+v2+null)", sb.toS());
    }

    @Test
    public void stringArrayWithBoundary_empty() {
        String[] a = new String[]{};
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(a, "[", ",", "]");
        assertEquals("[]", sb.toS());
    }

    @Test
    public void stringArrayWithBoundary_leftBoundaryNull() {
        String[] a = new String[]{"abc", "def", "ghi"};
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(a, null, ",", "]");
        assertEquals("nullabc,def,ghi]", sb.toS());
    }

    @Test
    public void stringArrayWithBoundary_rightBoundaryNull() {
        String[] a = new String[]{"abc", "def", "ghi"};
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(a, "[", ",", null);
        assertEquals("[abc,def,ghinull", sb.toS());
    }

    @Test
    public void stringArrayWithBoundary_separatorNull() {
        String[] a = new String[]{"abc", "def", "ghi"};
        FreeStringBuilder sb = new FreeStringBuilder(5);
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]"));
    }

    @Test
    public void stringArrayWithBoundary_allNull() {
        String[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(a, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void quotedStringArrayWithBoundary() {
        String[] a = new String[]{"abc", "def", "ghi"};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", "]");
        assertEquals("[\"abc\",\"def\",\"ghi\"]", sb.toS());
    }

    @Test
    public void quotedStringArrayWithBoundary_elementNull() {
        String[] a = new String[]{"abc", null, "ghi"};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", "]");
        assertEquals("[\"abc\",null,\"ghi\"]", sb.toS());
    }

    @Test
    public void quotedStringArrayWithBoundary_null() {
        String[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", "]");
        assertEquals("null", sb.toS());
    }

    @Test
    public void quotedStringArrayWithBoundary_empty() {
        String[] a = new String[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", "]");
        assertEquals("[]", sb.toS());
    }

    @Test
    public void quotedStringArrayWithBoundary_leftBoundryNull() {
        String[] a = new String[]{"abc", "def", "ghi"};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, null, ",", "]");
        assertEquals("null\"abc\",\"def\",\"ghi\"]", sb.toS());
    }

    @Test
    public void quotedStringArrayWithBoundary_separatorNull() {
        String[] a = new String[]{"abc", "def", "ghi"};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.appendQuoted(a, "[", null, "]"));
    }

    @Test
    public void quotedStringArrayWithBoundary_rightBoundryNull() {
        String[] a = new String[]{"abc", "def", "ghi"};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", null);
        assertEquals("[\"abc\",\"def\",\"ghi\"null", sb.toS());
    }

    @Test
    public void quotedStringArrayWithBoundary_allNull() {
        String[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, null, null, null);
        assertEquals("null", sb.toS());
    }

}

