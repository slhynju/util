package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringUtil_deleteSuffix_Test {

    @Test
    public void deleteSuffix() {
        assertEquals("abc", StringUtil.deleteSuffix("abc", 0));
        assertEquals("ab", StringUtil.deleteSuffix("abc", 1));
        assertEquals("a", StringUtil.deleteSuffix("abc", 2));
        assertEquals("", StringUtil.deleteSuffix("abc", 3));
        assertEquals("", StringUtil.deleteSuffix("abc", 4));
        assertEquals("", StringUtil.deleteSuffix("abc", 5));
        assertEquals("a", StringUtil.deleteSuffix("a", 0));
        assertEquals("", StringUtil.deleteSuffix("a", 1));
        assertEquals("", StringUtil.deleteSuffix("a", 2));
        assertEquals("", StringUtil.deleteSuffix("", 0));
        assertEquals("", StringUtil.deleteSuffix("", 1));
        assertEquals("", StringUtil.deleteSuffix("", 2));
    }

    @Test
    public void deleteSuffix_zeroLength() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deleteSuffix(sb, 0);
        assertEquals("abc", sb.toString());
    }

    @Test
    public void deleteSuffix_negativeLength() {
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.deleteSuffix("abc", -7));
    }

    @Test
    public void deleteSuffix_sNull() {
        String s = StringUtil.deleteSuffix((String) null, 3);
        assertEquals("", s);
    }

    @Test
    public void deleteSuffix_sNull_zeroLength() {
        String s = StringUtil.deleteSuffix((String) null, 0);
        assertEquals("", s);
    }

    @Test
    public void deleteSuffix_sNull_negativeLength() {
        String s = StringUtil.deleteSuffix((String) null, -5);
        assertEquals("", s);
    }

    @Test
    public void deleteSuffixIf() {
        String s = StringUtil.deleteSuffixIf("users-api", "-api");
        assertEquals("users", s);
        s = StringUtil.deleteSuffixIf("users", "-api");
        assertEquals("users", s);
        s = StringUtil.deleteSuffixIf((String) null, "-api");
        assertEquals("", s);
    }

    @Test
    public void deleteSuffixIf_suffixNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.deleteSuffixIf("users-api", null));
    }

    @Test
    public void deleteSuffixIf_bothNull() {
        String s = StringUtil.deleteSuffixIf((String) null, null);
        assertEquals("", s);
    }


    @Test
    public void deleteSuffix_sb() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deleteSuffix(sb, 1);
        assertEquals("ab", sb.toString());
    }

    @Test
    public void deleteSuffix_sb_2() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deleteSuffix(sb, 2);
        assertEquals("a", sb.toString());
    }

    @Test
    public void deleteSuffix_sb_3() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deleteSuffix(sb, 3);
        assertEquals("", sb.toString());
    }

    @Test
    public void deleteSuffix_sb_4() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deleteSuffix(sb, 4);
        assertEquals("", sb.toString());
    }

    @SuppressWarnings("StringOperationCanBeSimplified")
    @Test
    public void deleteSuffix_sb_zeroLength() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deleteSuffix(sb, 0);
        assertEquals("abc", sb.toString());
    }

    @Test
    public void deleteSuffix_sb_negativeLength() {
        StringBuilder sb = new StringBuilder("abc");
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.deleteSuffix(sb, -3));
    }

    @SuppressWarnings("StringOperationCanBeSimplified")
    @Test
    public void deleteSuffix_sb_empty() {
        StringBuilder sb = new StringBuilder("");
        StringUtil.deleteSuffix(sb, 2);
        assertEquals("", sb.toString());
    }

    @Test
    public void deleteSuffix_sb_empty_zeroLength() {
        StringBuilder sb = new StringBuilder("");
        StringUtil.deleteSuffix(sb, 0);
        assertEquals("", sb.toString());
    }

    @Test
    public void deleteSuffix_sb_empty_negativeLength() {
        StringBuilder sb = new StringBuilder("");
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.deleteSuffix(sb, -3));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void deleteSuffix_sb_null() {
        StringBuilder sb = null;
        StringUtil.deleteSuffix(sb, 3);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void deleteSuffix_sb_null_zeroLength() {
        StringBuilder sb = null;
        StringUtil.deleteSuffix(sb, 0);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void deleteSuffix_sb_null_negativeLength() {
        StringBuilder sb = null;
        StringUtil.deleteSuffix(sb, -7);
    }

    @Test
    public void deleteSuffixIf_sb() {
        StringBuilder sb = new StringBuilder("users-api");
        StringUtil.deleteSuffixIf(sb, "-api");
        assertEquals("users", sb.toString());
    }

    @Test
    public void deleteSuffixIf_sb_2() {
        StringBuilder sb = new StringBuilder("users_department");
        StringUtil.deleteSuffixIf(sb, "-api");
        assertEquals("users_department", sb.toString());
    }

    @Test
    public void deleteSuffixIf_sb_3() {
        StringBuilder sb = new StringBuilder("api");
        StringUtil.deleteSuffixIf(sb, "-api");
        assertEquals("api", sb.toString());
    }

    @Test
    public void deleteSuffixIf_sb_null() {
        StringBuilder sb = null;
        StringUtil.deleteSuffixIf(sb, "-api");
    }

    @Test
    public void deleteSuffixIf_sb_suffixNull() {
        StringBuilder sb = new StringBuilder("users-api");
        assertThrows(NullPointerException.class, () -> StringUtil.deleteSuffixIf(sb, null));
    }

    @Test
    public void deleteSuffixIf_sb_bothNull() {
        StringBuilder sb = null;
        StringUtil.deleteSuffixIf(sb, null);
    }

}
