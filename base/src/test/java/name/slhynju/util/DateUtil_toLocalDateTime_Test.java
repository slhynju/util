package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtil_toLocalDateTime_Test {

    @Test
    public void toLocalDateTime_date() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = format.parse("2012-02-04 15:36:02");
        LocalDateTime l = DateUtil.toLocalDateTime(d);
        assertEquals(2012, l.getYear());
        assertEquals(2, l.getMonthValue());
        assertEquals(4, l.getDayOfMonth());
        assertEquals(15, l.getHour());
        assertEquals(36, l.getMinute());
        assertEquals(2, l.getSecond());
    }

    @Test
    public void toLocalDateTime_date_dateNull() {
        assertNull(DateUtil.toLocalDateTime((Date) null));
    }

    @Test
    public void toLocalDateTime_dateWithTimeZone() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        LocalDateTime l = DateUtil.toLocalDateTime(d, zone);
        assertEquals(2012, l.getYear());
        assertEquals(2, l.getMonthValue());
        assertEquals(4, l.getDayOfMonth());
        assertEquals(15, l.getHour());
        assertEquals(36, l.getMinute());
        assertEquals(2, l.getSecond());
    }

    @Test
    public void toLocalDateTime_dateWithTimeZone_dateNull() {
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        assertNull(DateUtil.toLocalDateTime((Date) null, zone));
    }

    @Test
    public void toLocalDateTime_dateWithTimeZone_zoneNull() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        assertThrows(NullPointerException.class, () -> DateUtil.toLocalDateTime(d, (TimeZone) null));
    }

    @Test
    public void toLocalDateTime_dateWithTimeZone_bothNull() {
        assertThrows(NullPointerException.class, () -> DateUtil.toLocalDateTime((Date) null, (TimeZone) null));
    }

    @Test
    public void toLocalDateTime_dateWithZoneId() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        LocalDateTime l = DateUtil.toLocalDateTime(d, zoneId);
        assertEquals(2012, l.getYear());
        assertEquals(2, l.getMonthValue());
        assertEquals(4, l.getDayOfMonth());
        assertEquals(15, l.getHour());
        assertEquals(36, l.getMinute());
        assertEquals(2, l.getSecond());
    }

    @Test
    public void toLocalDateTime_dateWithZoneId_dateNull() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        assertNull(DateUtil.toLocalDateTime((Date) null, zoneId));
    }

    @Test
    public void toLocalDateTime_dateWithZoneId_zoneNull() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04 15:36:02");
        assertThrows(NullPointerException.class, () -> DateUtil.toLocalDateTime(d, (ZoneId) null));
    }

    @Test
    public void toLocalDateTime_dateWithZoneId_bothNull() {
        assertNull(DateUtil.toLocalDateTime((Date) null, (ZoneId) null));
    }
}
