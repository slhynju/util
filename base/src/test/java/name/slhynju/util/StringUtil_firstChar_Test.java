package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SuppressWarnings("ConstantConditions")
public class StringUtil_firstChar_Test {

    @Test
    public void firstCharToUpperCase() {
        assertEquals("Abc", StringUtil.firstCharToUpperCase("abc"));
        assertEquals("", StringUtil.firstCharToUpperCase((String) null));
        assertEquals("", StringUtil.firstCharToUpperCase(""));
        assertEquals("ABc", StringUtil.firstCharToUpperCase("ABc"));
    }

    @Test
    public void firstCharToUpperCase_sb() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.firstCharToUpperCase(sb);
        assertEquals("Abc", sb.toString());
    }

    @Test
    public void firstCharToUpperCase_sb_null() {
        StringBuilder sb = null;
        StringUtil.firstCharToUpperCase(sb);
        assertNull(sb);
    }

    @SuppressWarnings("StringOperationCanBeSimplified")
    @Test
    public void firstCharToUpperCase_sb_empty() {
        StringBuilder sb = new StringBuilder("");
        StringUtil.firstCharToUpperCase(sb);
        assertEquals("", sb.toString());
    }

    @Test
    public void firstCharToUpperCase_sb_2() {
        StringBuilder sb = new StringBuilder("ABc");
        StringUtil.firstCharToUpperCase(sb);
        assertEquals("ABc", sb.toString());
    }

    @Test
    public void firstCharToLowerCase() {
        assertEquals("aBC", StringUtil.firstCharToLowerCase("ABC"));
        assertEquals("", StringUtil.firstCharToLowerCase((String) null));
        assertEquals("", StringUtil.firstCharToLowerCase(""));
        assertEquals("aBc", StringUtil.firstCharToLowerCase("ABc"));
        assertEquals("abc", StringUtil.firstCharToLowerCase("abc"));
    }

    @Test
    public void firstCharToLowerCase_sb() {
        StringBuilder sb = new StringBuilder("ABC");
        StringUtil.firstCharToLowerCase(sb);
        assertEquals("aBC", sb.toString());
        sb = new StringBuilder("aBC");
        StringUtil.firstCharToLowerCase(sb);
        assertEquals("aBC", sb.toString());
    }

    @Test
    public void firstCharToLowerCase_sb_null() {
        StringBuilder sb = null;
        StringUtil.firstCharToLowerCase(sb);
        assertNull(sb);
    }

    @SuppressWarnings("StringOperationCanBeSimplified")
    @Test
    public void firstCharToLowerCase_sb_empty() {
        StringBuilder sb = new StringBuilder("");
        StringUtil.firstCharToLowerCase(sb);
        assertEquals("", sb.toString());
    }

    @Test
    public void firstCharToLowerCase_sb_2() {
        StringBuilder sb = new StringBuilder("ABc");
        StringUtil.firstCharToLowerCase(sb);
        assertEquals("aBc", sb.toString());
    }
}
