package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FreeStringBuilder_Map_Test {

    @Test
    public void toS() {
        Map<String, String> m = new HashMap<>();
        m.put("k1", "v1");
        m.put("k2", "v2");
        assertEquals("k1=v1,k2=v2", FreeStringBuilder.toS(m, "=", ","));
        Map<String, String> m2 = new HashMap<>();
        m2.put("k2", "v2");
        assertEquals("k2=v2", FreeStringBuilder.toS(m2, "=", ","));
        Map<String, String> m3 = new HashMap<>();
        assertEquals("", FreeStringBuilder.toS(m3, "=", ","));

    }

    @Test
    public void toS_null() {
        assertEquals("null", FreeStringBuilder.toS(null, "=", ","));
    }

    @Test
    public void toS_elementNull() {
        Map<String, String> m = new HashMap<>();
        m.put(null, "v1");
        m.put("k2", null);
        assertEquals("null=v1,k2=null", FreeStringBuilder.toS(m, "=", ","));
    }

    @Test
    public void toS_separatorNull() {
        Map<String, String> m = new HashMap<>();
        m.put("k1", "v1");
        m.put("k2", "v2");
        assertEquals("k1nullv1,k2nullv2", FreeStringBuilder.toS(m, null, ","));
        assertThrows(NullPointerException.class, () -> FreeStringBuilder.toS(m, "=", null));
        assertThrows(NullPointerException.class, () -> FreeStringBuilder.toS(m, null, null));
    }

    @Test
    public void toS_allNull() {
        Map<String, String> m = null;
        assertEquals("null", FreeStringBuilder.toS(m, null, null));
    }

    @Test
    public void map() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        sb.append(map);
        assertEquals("{k1: v1, k2: v2}", sb.toS());
    }

    @Test
    public void map_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = null;
        sb.append(map);
        assertEquals("null", sb.toS());
    }

    @Test
    public void map_entryValueNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", null);
        sb.append(map);
        assertEquals("{k1: v1, k2: null}", sb.toS());
    }

    @Test
    public void map_entryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put(null, null);
        sb.append(map);
        assertEquals("{null: null, k1: v1}", sb.toS());
    }

    @Test
    public void mapWithoutBoundary() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        sb.appendWithoutMarks(map);
        assertEquals("k1: v1, k2: v2", sb.toS());
    }

    @Test
    public void mapWithoutBoundary_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = null;
        sb.appendWithoutMarks(map);
        assertEquals("null", sb.toS());
    }

    @Test
    public void mapWithoutBoundary_entryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put(null, null);
        sb.appendWithoutMarks(map);
        assertEquals("null: null, k1: v1", sb.toS());
    }

    @Test
    public void mapWithBoundary() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        sb.append(map, "l", "e", "s", "r");
        assertEquals("lk1ev1sk2ev2r", sb.toS());
    }

    @Test
    public void mapWithBoundary_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = null;
        sb.append(map, "l", "e", "s", "r");
        assertEquals("null", sb.toS());
    }

    @Test
    public void mapWithBoundary_entryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put(null, null);
        sb.append(map, "l", "e", "s", "r");
        assertEquals("lnullenullsk1ev1r", sb.toS());
    }

    @Test
    public void mapWithBoundary_leftBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        sb.append(map, null, "e", "s", "r");
        assertEquals("nullk1ev1sk2ev2r", sb.toS());
    }

    @Test
    public void mapWithBoundary_rightBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        sb.append(map, "l", "e", "s", null);
        assertEquals("lk1ev1sk2ev2null", sb.toS());
    }

    @Test
    public void mapWithBoundary_keyValueSeparatorNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        sb.append(map, "l", null, "s", "r");
        assertEquals("lk1nullv1sk2nullv2r", sb.toS());
    }


    @Test
    public void mapWithBoundary_entrySeparatorNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        assertThrows(NullPointerException.class, () -> sb.append(map, "l", "e", null, "r"));
    }

    @Test
    public void mapWithBoundary_allNull() {
        Map<String, String> map = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(map, null, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void mapWithBoundaryAndMapper() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        Function<String, String> keyMapper = (x) -> x + x;
        Function<String, String> valueMapper = (x) -> x + x + x;
        sb.append(map, "l", "e", "s", "r", keyMapper, valueMapper);
        assertEquals("lk1k1ev1v1v1sk2k2ev2v2v2r", sb.toS());
    }

    @Test
    public void mapWithBoundaryAndMapper_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = null;
        Function<String, String> keyMapper = (x) -> x + x;
        Function<String, String> valueMapper = (x) -> x + x + x;
        sb.append(map, "l", "e", "s", "r", keyMapper, valueMapper);
        assertEquals("null", sb.toS());
    }

    @Test
    public void mapWithBoundaryAndMapper_leftBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        Function<String, String> keyMapper = (x) -> x + x;
        Function<String, String> valueMapper = (x) -> x + x + x;
        sb.append(map, null, "e", "s", "r", keyMapper, valueMapper);
        assertEquals("nullk1k1ev1v1v1sk2k2ev2v2v2r", sb.toS());
    }

    @Test
    public void mapWithBoundaryAndMapper_rightBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        Function<String, String> keyMapper = (x) -> x + x;
        Function<String, String> valueMapper = (x) -> x + x + x;
        sb.append(map, "l", "e", "s", null, keyMapper, valueMapper);
        assertEquals("lk1k1ev1v1v1sk2k2ev2v2v2null", sb.toS());
    }

    @Test
    public void mapWithBoundaryAndMapper_keyValueSeparatorNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        Function<String, String> keyMapper = (x) -> x + x;
        Function<String, String> valueMapper = (x) -> x + x + x;
        sb.append(map, "l", null, "s", "r", keyMapper, valueMapper);
        assertEquals("lk1k1nullv1v1v1sk2k2nullv2v2v2r", sb.toS());
    }

    @Test
    public void mapWithBoundaryAndMapper_entrySeparatorNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        Function<String, String> keyMapper = (x) -> x + x;
        Function<String, String> valueMapper = (x) -> x + x + x;
        assertThrows(NullPointerException.class, () -> sb.append(map, "l", "e", null, "r", keyMapper, valueMapper));
    }

    @Test
    public void mapWithBoundaryAndMapper_keyMapperNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        Function<String, String> keyMapper = null;
        Function<String, String> valueMapper = (x) -> x + x + x;
        assertThrows(NullPointerException.class, () -> sb.append(map, "l", "e", "s", "r", keyMapper, valueMapper));
    }

    @Test
    public void mapWithBoundaryAndMapper_valueMapperNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        Function<String, String> keyMapper = (x) -> x + x;
        Function<String, String> valueMapper = null;
        assertThrows(NullPointerException.class, () -> sb.append(map, "l", "e", "s", "r", keyMapper, valueMapper));
    }

    @Test
    public void mapWithBoundaryAndMapper_allNull() {
        Map<String, String> map = null;
        Function<String, String> keyMapper = null;
        Function<String, String> valueMapper = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(map, null, null, null, null, keyMapper, valueMapper);
        assertEquals("null", sb.toS());
    }

}
