package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("static-method")
public class PairTest {

    @Test
    public void pair1() {
        Pair<String, Integer> p = new Pair<>();
        p.a = "abc";
        assertEquals("abc", p.a);
        Integer n = Integer.valueOf(5);
        p.b = n;
        assertEquals(n, p.b);
        Object[] array = new Object[]{"abc", n};
        assertArrayEquals(array, p.toArray());
    }

    @Test
    public void pair2() {
        Integer n = Integer.valueOf(5);
        Pair<String, Integer> p = new Pair<>("abc", n);
        assertEquals("abc", p.a);
        assertEquals(n, p.b);
        Object[] array = new Object[]{"abc", n};
        assertArrayEquals(array, p.toArray());
    }

    @Test
    public void equals() {
        Integer n = Integer.valueOf(5);
        Pair<String, Integer> p = new Pair<>("abc", n);
        Pair<String, Integer> p2 = new Pair<>("abc", n);
        assertTrue(p.equals(p2));
        assertEquals(p.hashCode(), p2.hashCode());
        assertEquals("name.slhynju.util.Pair{a: abc, b: 5}", p.toString());
        Pair<String, Integer> p3 = new Pair<>("def", n);
        assertFalse(p.equals(p3));
        assertFalse(p.equals("abc"));
        assertFalse(p.equals(null));
    }

}
