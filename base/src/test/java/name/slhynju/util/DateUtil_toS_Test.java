package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SuppressWarnings({"ConstantConditions", "RedundantCast"})
public class DateUtil_toS_Test {

    @Test
    public void toS_date() throws ParseException {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d = f.parse("2012-02-04");
        assertEquals("2012-02-04", DateUtil.toS(d, "yyyy-MM-dd"));
    }

    @Test
    public void toS_date_dateNull() {
        assertEquals("null", DateUtil.toS(null, "yyyy-MM-dd"));
    }

    @Test
    public void toS_date_formatNull() throws ParseException {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d = f.parse("2012-02-04");
        assertThrows(NullPointerException.class, () -> DateUtil.toS(d, (String) null));
    }

    @Test
    public void toS_date_bothNull() {
        assertEquals("null", DateUtil.toS(null, (String) null));
    }

    @Test
    public void toS_dateWithFormat() throws ParseException {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d = f.parse("2012-02-04");
        assertEquals("2012-02-04", DateUtil.toS(d, f));
    }

    @Test
    public void toS_dateWithFormat_dateNull() {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        assertEquals("null", DateUtil.toS(null, f));
    }

    @Test
    public void toS_dateWithFormat_formatNull() throws ParseException {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d = f.parse("2012-02-04");
        assertThrows(NullPointerException.class, () -> DateUtil.toS(d, (DateFormat) null));
    }

    @Test
    public void toS_dateWithFormat_bothNull() {
        assertEquals("null", DateUtil.toS(null, (DateFormat) null));
    }

}
