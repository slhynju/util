package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtil_toDate_LocalDateTime_Test {

    @Test
    public void toDate_localDateTime() throws ParseException {
        LocalDateTime time = LocalDateTime.of(2012, 2, 4, 15, 48, 0);
        Date d = DateUtil.toDate(time);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d2 = format.parse("2012-02-04 15:48:00");
        assertEquals(d2, d);
    }

    @Test
    public void toDate_localDateTime_timeNull() {
        assertNull(DateUtil.toDate((LocalDateTime) null));
    }

    @Test
    public void toDate_localDateTimeWithZoneId() throws ParseException {
        LocalDateTime time = LocalDateTime.of(2012, 2, 4, 15, 48, 0);
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        Date d = DateUtil.toDate(time, zoneId);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        Date d2 = format.parse("2012-02-04 15:48:00");
        assertEquals(d2, d);
    }

    @Test
    public void toDate_localDateTimeWithZoneId_timeNull() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        assertNull(DateUtil.toDate((LocalDateTime) null, zoneId));
    }

    @Test
    public void toDate_localDateTimeWithZoneId_zoneNull() {
        LocalDateTime time = LocalDateTime.of(2012, 2, 4, 15, 48, 0);
        ZoneId zoneId = null;
        assertThrows(NullPointerException.class, () -> DateUtil.toDate(time, zoneId));
    }

    @Test
    public void toDate_localDateTimeWithZoneId_bothNull() {
        assertNull(DateUtil.toDate((LocalDateTime) null, (ZoneId) null));
    }

}
