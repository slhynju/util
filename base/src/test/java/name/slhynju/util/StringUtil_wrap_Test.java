package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SuppressWarnings("SpellCheckingInspection")
public class StringUtil_wrap_Test {

    @Test
    public void wrap() {
        assertEquals("abc", StringUtil.wrap("abc", ""));
        assertEquals("dabcd", StringUtil.wrap("abc", "d"));
        assertEquals("\"aabc\"a", StringUtil.wrap("abc", "\"a"));
        assertEquals("cc", StringUtil.wrap("", "c"));
    }

    @Test
    public void wrap_null() {
        assertEquals("abcabc", StringUtil.wrap(null, "abc"));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void wrap_wrapperNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.wrap("abc", null));
    }

    @Test
    public void wrap_both_null() {
        assertThrows(NullPointerException.class, () -> StringUtil.wrap(null, null));
    }

    @Test
    public void unwrap() {
        assertEquals("abc", StringUtil.unwrap("abc", 0));
        assertEquals("b", StringUtil.unwrap("abc", 1));
        assertEquals("", StringUtil.unwrap("abc", 2));
        assertEquals("", StringUtil.unwrap("", 0));
        assertEquals("", StringUtil.unwrap("", 1));
    }

    @Test
    public void unwrap_null() {
        assertEquals("", StringUtil.unwrap(null, 2));
    }

    @Test
    public void unwrap_negativeLength() {
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.unwrap("abc", -1));
    }

    @Test
    public void unwrap_null_negativeLength() {
        assertEquals("", StringUtil.unwrap(null, -1));
    }
}
