package name.slhynju.util;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"static-method", "ConstantConditions", "SpellCheckingInspection"})
public class FreeStringBuilderTest {

    @Test
    public void testToString() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append("abc");
        assertEquals("abc", sb.toString());
    }

    @Test
    public void indent() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.indent(3);
        assertEquals("\t\t\t", sb.toS());
    }

    @Test
    public void indent_zero() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.indent(0);
        assertEquals("", sb.toS());
    }

    @Test
    public void indent_negative() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.indent(-5);
        assertEquals("", sb.toS());
    }

    @Test
    public void startsWith() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertTrue(sb.startsWith("ab"));
        assertFalse(sb.startsWith("bc"));
    }

    @Test
    public void startsWith_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertThrows(NullPointerException.class, () -> sb.startsWith(null));
    }

    @Test
    public void startsWith_empty() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertTrue(sb.startsWith(""));
    }


    @Test
    public void deleteStart() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        sb.deletePrefix(2);
        assertEquals("c", sb.toS());
    }

    @Test
    public void deleteStart_zero() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        sb.deletePrefix(0);
        assertEquals("abc", sb.toS());
    }

    @Test
    public void deleteStart_negative() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertThrows(StringIndexOutOfBoundsException.class, () -> sb.deletePrefix(-3));
    }

    @Test
    public void deleteStartIf() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        sb.deletePrefixIf("ab");
        assertEquals("c", sb.toS());
        sb = new FreeStringBuilder("abc");
        sb.deletePrefixIf("bc");
        assertEquals("abc", sb.toS());
    }

    @Test
    public void deleteStartIf_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertThrows(NullPointerException.class, () -> sb.deletePrefixIf(null));
    }

    @Test
    public void endsWith() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertTrue(sb.endsWith("bc"));
        assertFalse(sb.endsWith("a"));
    }

    @Test
    public void endsWith_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertThrows(NullPointerException.class, () -> sb.endsWith(null));
    }

    @Test
    public void endsWith_empty() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertTrue(sb.endsWith(""));
    }

    @Test
    public void deleteEnd() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        sb.deleteEnd(2);
        assertEquals("a", sb.toS());
    }

    @Test
    public void deleteEnd_zero() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        sb.deleteEnd(0);
        assertEquals("abc", sb.toS());
    }

    @Test
    public void deleteEnd_negative() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertThrows(StringIndexOutOfBoundsException.class, () -> sb.deleteEnd(-5));
    }

    @Test
    public void deleteEndIf() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        sb.deleteEndIf("bc");
        assertEquals("a", sb.toS());
        sb = new FreeStringBuilder("abc");
        sb.deleteEndIf("a");
        assertEquals("abc", sb.toS());
    }

    @Test
    public void deleteEndIf_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        assertThrows(NullPointerException.class, () -> sb.deleteEndIf(null));
    }

    @Test
    public void delete() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        sb.delete(0, 2);
        assertEquals("cde", sb.toS());
    }

    @Test
    public void delete_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        assertThrows(StringIndexOutOfBoundsException.class, () -> sb.delete(-3, 2));
    }

    @Test
    public void insert() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        sb.insert("uvw", 1);
        assertEquals("auvwbcde", sb.toS());
    }

    @Test
    public void insert_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        sb.insert(null, 1);
        assertEquals("anullbcde", sb.toS());
    }

    @Test
    public void insert_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        assertThrows(StringIndexOutOfBoundsException.class, () -> sb.insert("uvw", -3));
    }

    @Test
    public void indexOf() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.indexOf("cd");
        assertEquals(2, index);
    }

    @Test
    public void indexOf_notFound() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.indexOf("dc");
        assertEquals(-1, index);
    }

    @Test
    public void indexOf_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        assertThrows(NullPointerException.class, () -> sb.indexOf(null));
    }

    @Test
    public void indexOf_empty() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.indexOf("");
        assertEquals(0, index);
    }

    @Test
    public void indexOfFrom() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.indexOf("de", 1);
        assertEquals(3, index);
    }

    @Test
    public void indexOfFrom_notFound() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.indexOf("xy", 1);
        assertEquals(-1, index);
        index = sb.indexOf("ab", 2);
        assertEquals(-1, index);
    }

    @Test
    public void indexOfFrom_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        assertThrows(NullPointerException.class, () -> sb.indexOf(null, 2));
    }

    @Test
    public void indexOfFrom_empty() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.indexOf("", 2);
        assertEquals(2, index);
    }

    @Test
    public void indexOfFrom_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.indexOf("bc", 7);
        assertEquals(-1, index);
    }

    @Test
    public void indexOfFrom_negativeIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.indexOf("de", -3);
        assertEquals(3, index);
    }

    @Test
    public void lastIndexOf() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.lastIndexOf("cd");
        assertEquals(2, index);
    }

    @Test
    public void lastIndexOf_notFound() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.lastIndexOf("dc");
        assertEquals(-1, index);
    }

    @Test
    public void lastIndexOf_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        assertThrows(NullPointerException.class, () -> sb.lastIndexOf(null));
    }

    @Test
    public void lastIndexOf_empty() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.lastIndexOf("");
        assertEquals(4, index);
    }

    @Test
    public void lastIndexOfFrom() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.lastIndexOf("de", 4);
        assertEquals(3, index);
    }

    @Test
    public void lastIndexOfFrom_notFound() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.lastIndexOf("xy", 4);
        assertEquals(-1, index);
        index = sb.lastIndexOf("cd", 1);
        assertEquals(-1, index);
    }

    @Test
    public void lastIndexOfFrom_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        assertThrows(NullPointerException.class, () -> sb.lastIndexOf(null, 2));
    }

    @Test
    public void lastIndexOfFrom_empty() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.lastIndexOf("", 2);
        assertEquals(2, index);
    }

    @Test
    public void lastIndexOfFrom_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.lastIndexOf("bc", 7);
        assertEquals(1, index);
    }

    @Test
    public void lastIndexOfFrom_negativeIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("abcde");
        int index = sb.lastIndexOf("de", -3);
        assertEquals(-1, index);
    }


    @Test
    public void firstCharToUpperCase() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        sb.firstCharToUpperCase();
        assertEquals("Abc", sb.toS());
    }

    @Test
    public void firstCharToLowerCase() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        sb.firstCharToLowerCase();
        assertEquals("aBC", sb.toS());
    }

    @Test
    public void clear() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        sb.clear();
        assertEquals("", sb.toS());
    }

    @Test
    public void isEmpty() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        assertFalse(sb.isEmpty());
        sb.clear();
        assertTrue(sb.isEmpty());
    }

    @Test
    public void length() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        assertEquals(3, sb.length());
        sb.clear();
        assertEquals(0, sb.length());
    }

    @Test
    public void charAt() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        assertEquals('A', sb.charAt(0));
        assertEquals('B', sb.charAt(1));
    }

    @Test
    public void charAt_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        assertThrows(StringIndexOutOfBoundsException.class, () -> sb.charAt(5));
    }

    @Test
    public void setCharAt() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        sb.setCharAt(1, 'k');
        assertEquals("AkC", sb.toS());
    }

    @Test
    public void setChatAt_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        assertThrows(StringIndexOutOfBoundsException.class, () -> sb.setCharAt(8, 'F'));
    }

    @Test
    public void subSequence() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        CharSequence seq = sb.subSequence(1, 2);
        assertEquals("B", seq.toString());
    }

    @Test
    public void subSequence_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder("ABC");
        assertThrows(StringIndexOutOfBoundsException.class, () -> sb.subSequence(2, 5));
    }

}
