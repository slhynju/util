package name.slhynju.util.reflect;

import name.slhynju.util.Pair;
import name.slhynju.util.collection.CollectionUtil;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.lang.constant.Constable;
import java.lang.constant.ConstantDesc;
import java.lang.reflect.*;
import java.nio.charset.Charset;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"static-method", "ConstantConditions"})
public class ClassUtilTest {

    @Test
    public void getSuperClasses() {
        Class<?> c = Integer.class;
        Set<Class<?>> set = ClassUtil.getSuperClasses(c);
        Set<Class<?>> target = new HashSet<>();
        target.add(Object.class);
        target.add(Number.class);
        target.add(Serializable.class);
        target.add(Comparable.class);
        target.add(Constable.class);
        target.add(ConstantDesc.class);
        assertEquals(target, set);
    }

    @Test
    public void getSuperClasses_null() {
        Class<?> c = null;
        Set<Class<?>> set = ClassUtil.getSuperClasses(c);
        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void getInterfaces() {
        Class<?> c = Integer.class;
        Set<Class<?>> set = ClassUtil.getInterfaces(c);
        Set<Class<?>> target = new HashSet<>();
        target.add(Serializable.class);
        target.add(Comparable.class);
        target.add(Constable.class);
        target.add(ConstantDesc.class);
        assertEquals(target, set);
    }

    @Test
    public void getInterfaces_null() {
        Class<?> c = null;
        Set<Class<?>> set = ClassUtil.getInterfaces(c);
        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void isAbstract() {
        assertTrue(ClassUtil.isAbstract(List.class));
        assertTrue(ClassUtil.isAbstract(AbstractList.class));
        assertFalse(ClassUtil.isAbstract(ArrayList.class));
    }

    @Test
    public void isAbstract_null() {
        assertFalse(ClassUtil.isAbstract(null));
    }

    @Test
    public void isAbstractClass() {
        assertFalse(ClassUtil.isAbstractClass(List.class));
        assertTrue(ClassUtil.isAbstractClass(AbstractList.class));
        assertFalse(ClassUtil.isAbstractClass(ArrayList.class));
    }

    @Test
    public void isAbstractClass_null() {
        assertFalse(ClassUtil.isAbstractClass(null));
    }

    @Test
    public void isImpl() {
        assertFalse(ClassUtil.isImpl(List.class));
        assertFalse(ClassUtil.isImpl(AbstractList.class));
        assertTrue(ClassUtil.isImpl(ArrayList.class));
    }

    @Test
    public void isImpl_null() {
        assertFalse(ClassUtil.isImpl(null));
    }

    @Test
    public void isImplOf() {
        assertFalse(ClassUtil.isImplOf(List.class, List.class));
        assertFalse(ClassUtil.isImplOf(AbstractList.class, List.class));
        assertTrue(ClassUtil.isImplOf(ArrayList.class, List.class));
    }

    @Test
    public void isImplOf_null() {
        assertFalse(ClassUtil.isImplOf(null, String.class));
        assertFalse(ClassUtil.isImplOf(String.class, null));
        assertFalse(ClassUtil.isImplOf(null, null));
    }

    @Test
    public void getPackageName() {
        assertEquals("", ClassUtil.getPackageName(Integer.TYPE));
        assertEquals("name.slhynju.util.reflect", ClassUtil.getPackageName(ClassUtil.class));
    }

    @Test
    public void getPackageName_null() {
        assertEquals("", ClassUtil.getPackageName(null));
    }

    @Test
    public void getModuleName() {
        assertEquals("java.base", ClassUtil.getModuleName(Collection.class));
        //assertEquals( "name.slhynju.util", ClassUtil.getModuleName(StringUtil.class) );
    }

    @Test
    public void getModuleName_null() {
        assertEquals("", ClassUtil.getModuleName(null));
    }

    @Test
    public void isStatic() {
        Method m = ClassUtil.getMethod(ClassUtil.class, "isImpl", Class.class);
        assertTrue(ClassUtil.isStatic(m));
        Method m2 = ClassUtil.getMethod(List.class, "size");
        assertFalse(ClassUtil.isStatic(m2));
    }

    @Test
    public void isStatic_null() {
        assertFalse(ClassUtil.isStatic(null));
    }

    @Test
    public void isVoidReturnType() {
        Method m = ClassUtil.getMethod(ClassUtilTest.class, "isVoidReturnType");
        assertTrue(ClassUtil.isVoidReturnType(m));
        Method m2 = ClassUtil.getMethod(List.class, "size");
        assertFalse(ClassUtil.isVoidReturnType(m2));
    }

    @Test
    public void isVoidReturnType_null() {
        assertFalse(ClassUtil.isVoidReturnType(null));
    }

    @Test
    public void isGetter() {
        Method m = ClassUtil.getMethod(String.class, "isEmpty");
        assertTrue(ClassUtil.isGetter(m));
        Method m2 = ClassUtil.getMethod(String.class, "getBytes");
        assertTrue(ClassUtil.isGetter(m2));
        Method m3 = ClassUtil.getMethod(String.class, "getBytes", Charset.class);
        assertFalse(ClassUtil.isGetter(m3));
        Method m4 = ClassUtil.getMethod(String.class, "length");
        assertFalse(ClassUtil.isGetter(m4));
    }

    @Test
    public void isGetter_null() {
        assertFalse(ClassUtil.isGetter(null));
    }

    @Test
    public void isSetter() {
        Method m = ClassUtil.getMethod(Map.Entry.class, "setValue", Object.class);
        assertFalse(ClassUtil.isSetter(m));
        Method m2 = ClassUtil.getMethod(List.class, "set", Integer.TYPE, Object.class);
        assertFalse(ClassUtil.isSetter(m2));
        Method m3 = ClassUtil.getMethod(Date.class, "setDate", Integer.TYPE);
        assertTrue(ClassUtil.isSetter(m3));
    }

    @Test
    public void isSetter_null() {
        assertFalse(ClassUtil.isSetter(null));
    }

    @Test
    public void getGetters() {
        List<Method> getters = ClassUtil.getGetters(String.class);
        assertEquals(4, getters.size());
    }

    @Test
    public void getGetters_null() {
        List<Method> getters = ClassUtil.getGetters(null);
        assertNotNull(getters);
        assertTrue(getters.isEmpty());
    }

    @Test
    public void getSetters() {
        List<Method> setters = ClassUtil.getSetters(Map.Entry.class);
        assertEquals(0, setters.size());
        setters = ClassUtil.getSetters(Date.class);
        assertEquals(7, setters.size());
    }

    @Test
    public void getSetters_null() {
        List<Method> setters = ClassUtil.getSetters(null);
        assertNotNull(setters);
        assertTrue(setters.isEmpty());
    }

    @Test
    public void getSetterParamClass() {
        Method m = ClassUtil.getMethod(Date.class, "setTime", Long.TYPE);
        Class<?> c = ClassUtil.getSetterParamClass(m);
        assertEquals(Long.TYPE, c);
    }

    @Test
    public void getSetterParamClass_null() {
        assertNull(ClassUtil.getSetterParamClass(null));
    }

    @Test
    public void getSetterParamType() {
        Method m = ClassUtil.getMethod(Date.class, "setTime", Long.TYPE);
        Type c = ClassUtil.getSetterParamType(m);
        assertEquals(Long.TYPE, c);
    }

    @Test
    public void getSetterParamType_null() {
        assertNull(ClassUtil.getSetterParamType(null));
    }

    @Test
    public void getMethods() {
        List<Method> methods = ClassUtil.getMethods(String.class, "indexOf");
        assertEquals(4, methods.size());
    }

    @Test
    public void getMethods_null() {
        List<Method> methods = ClassUtil.getMethods(null, "indexOf");
        assertNotNull(methods);
        assertTrue(methods.isEmpty());
        methods = ClassUtil.getMethods(String.class, null);
        assertNotNull(methods);
        assertTrue(methods.isEmpty());
        methods = ClassUtil.getMethods(null, null);
        assertNotNull(methods);
        assertTrue(methods.isEmpty());
    }

    @Test
    public void getMethod() {
        Method m = ClassUtil.getMethod(String.class, "indexOf", Integer.TYPE);
        assertEquals("indexOf", m.getName());
        assertEquals(Integer.TYPE, m.getReturnType());
        Method m2 = ClassUtil.getMethod(String.class, "indexOf2");
        assertNull(m2);
    }

    @Test
    public void getMethod_null() {
        Method m = ClassUtil.getMethod(null, "indexOf");
        assertNull(m);
        m = ClassUtil.getMethod(String.class, null);
        assertNull(m);
        m = ClassUtil.getMethod(null, null);
        assertNull(m);
        m = ClassUtil.getMethod(String.class, "length", null);
        assertNotNull(m);
    }

    @Test
    public void getStaticMethod() {
        Method m = ClassUtil.getStaticMethod(String.class, "join", CharSequence.class, CharSequence[].class);
        assertEquals(String.class, m.getReturnType());
    }

    @Test
    public void getStaticMethod_notFound() {
        Method m = ClassUtil.getStaticMethod(String.class, "indexOf", Integer.TYPE);
        assertNull(m);
    }

    @Test
    public void getStaticMethod_null() {
        Method m = ClassUtil.getStaticMethod(null, "indexOf");
        assertNull(m);
        m = ClassUtil.getStaticMethod(String.class, null);
        assertNull(m);
        m = ClassUtil.getStaticMethod(null, null);
        assertNull(m);
    }

    @Test
    public void getConstructor() {
        Constructor<?> c = ClassUtil.getConstructor(String.class);
        assertNotNull(c);
        Constructor<?> c2 = ClassUtil.getConstructor(String.class, Date.class);
        assertNull(c2);
    }

    @Test
    public void getConstructor_null() {
        Constructor<?> c = ClassUtil.getConstructor(null);
        assertNull(c);
        c = ClassUtil.getConstructor(String.class, null);
        assertNotNull(c);
    }

    @Test
    public void getField() {
        Field field = ClassUtil.getField(CollectionUtil.class, "EMPTY_BOOLEAN_ARRAY");
        assertNotNull(field);
        field = ClassUtil.getField(Pair.class, "c");
        assertNull(field);
    }

    @Test
    public void getField_null() {
        Field field = ClassUtil.getField(null, "c");
        assertNull(field);
        field = ClassUtil.getField(Pair.class, null);
        assertNull(field);
        field = ClassUtil.getField(null, null);
        assertNull(field);
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void classToS() {
        assertEquals("String", ClassUtil.toS(String.class));
    }

    @Test
    public void classToS_null() {
        assertEquals("", ClassUtil.toS((Class) null));
    }

    @Test
    public void typeToS() {
        assertEquals("String", ClassUtil.toS((Type) String.class));

        Method m = ClassUtil.getMethod(List.class, "containsAll", Collection.class);
        Type t = m.getGenericParameterTypes()[0];
        assertEquals("Collection<?>", ClassUtil.toS(t));

        Method m2 = ClassUtil.getMethod(List.class, "toArray", Object[].class);
        Type t2 = m2.getGenericParameterTypes()[0];
        assertEquals("T[]", ClassUtil.toS(t2));

        Method m3 = ClassUtil.getMethod(List.class, "add", Object.class);
        Type t3 = m3.getGenericParameterTypes()[0];
        assertEquals("E", ClassUtil.toS(t3));

        Method m4 = ClassUtil.getMethod(List.class, "containsAll", Collection.class);
        ParameterizedType t4 = (ParameterizedType) m4.getGenericParameterTypes()[0];
        Type t41 = t4.getActualTypeArguments()[0];
        assertEquals("?", ClassUtil.toS(t41));
    }

    @Test
    public void typeToS_null() {
        assertEquals("", ClassUtil.toS((Type) null));
    }

    @Test
    public void parameterizedTypeToS() {
        Method m = ClassUtil.getMethod(List.class, "containsAll", Collection.class);
        ParameterizedType t = (ParameterizedType) m.getGenericParameterTypes()[0];
        assertEquals("Collection<?>", ClassUtil.toS(t));
    }

    @Test
    public void parameterizedTypeToS_null() {
        ParameterizedType t = null;
        assertEquals("", ClassUtil.toS(t));
    }

    @Test
    public void genericArrayTypeToS() {
        Method m = ClassUtil.getMethod(List.class, "toArray", Object[].class);
        GenericArrayType t = (GenericArrayType) m.getGenericParameterTypes()[0];
        assertEquals("T[]", ClassUtil.toS(t));
    }

    @Test
    public void genericArrayTypeToS_null() {
        GenericArrayType t = null;
        assertEquals("", ClassUtil.toS(t));
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void typeVariableToS() {
        Method m = ClassUtil.getMethod(List.class, "add", Object.class);
        TypeVariable t = (TypeVariable) m.getGenericParameterTypes()[0];
        assertEquals("E", ClassUtil.toS(t));
    }

    @Test
    public void typeVariableToS_null() {
        TypeVariable<?> t = null;
        assertEquals("", ClassUtil.toS(t));
    }

    @Test
    public void wildcardTypeToS() {
        Method m = ClassUtil.getMethod(List.class, "containsAll", Collection.class);
        ParameterizedType t = (ParameterizedType) m.getGenericParameterTypes()[0];
        WildcardType t2 = (WildcardType) t.getActualTypeArguments()[0];
        assertEquals("?", ClassUtil.toS(t2));
    }

    @Test
    public void wildcardTypeToS_null() {
        WildcardType t = null;
        assertEquals("", ClassUtil.toS(t));
    }

}
