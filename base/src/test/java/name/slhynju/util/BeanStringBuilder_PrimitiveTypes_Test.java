package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BeanStringBuilder_PrimitiveTypes_Test {

    @Test
    public void byteObject() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Byte b = Byte.valueOf((byte) 3);
        sb.append("p", b);
        assertEquals("name.slhynju.util.Bean{p: 3}", sb.toS());
    }

    @Test
    public void byteObject_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Byte b = null;
        sb.append("p", b);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void byteArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        byte[] b = new byte[]{1, 2, 3};
        sb.append("p", b);
        assertEquals("name.slhynju.util.Bean{p: [1, 2, 3]}", sb.toS());
    }

    @Test
    public void byteArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        byte[] b = null;
        sb.append("p", b);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void intType() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", 3);
        assertEquals("name.slhynju.util.Bean{p: 3}", sb.toS());
    }

    @Test
    public void integer() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Integer n = Integer.valueOf(3);
        sb.append("p", n);
        assertEquals("name.slhynju.util.Bean{p: 3}", sb.toS());
    }

    @Test
    public void integer_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Integer n = null;
        sb.append("p", n);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void intArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        int[] a = new int[]{1, 2, 3};
        sb.append("p", a);
        assertEquals("name.slhynju.util.Bean{p: [1, 2, 3]}", sb.toS());
    }

    @Test
    public void intArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        int[] a = null;
        sb.append("p", a);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void longType() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", 3L);
        assertEquals("name.slhynju.util.Bean{p: 3}", sb.toS());
    }

    @Test
    public void longWithFormat() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        DecimalFormat format = new DecimalFormat("#,##0");
        format.setParseIntegerOnly(true);
        sb.append("p", 30000L, format);
        assertEquals("name.slhynju.util.Bean{p: 30,000}", sb.toS());
    }

    @Test
    public void longWithFormat_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        NumberFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", 30000L, format));
    }

    @Test
    public void longObject() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", Long.valueOf(3L));
        assertEquals("name.slhynju.util.Bean{p: 3}", sb.toS());
    }

    @Test
    public void longObject_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", (Long) null);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void longObjectWithFormat() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        DecimalFormat format = new DecimalFormat("#,##0");
        format.setParseIntegerOnly(true);
        Long l = Long.valueOf(30000L);
        sb.append("p", l, format);
        assertEquals("name.slhynju.util.Bean{p: 30,000}", sb.toS());
    }

    @Test
    public void longObjectWithFormat_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        DecimalFormat format = new DecimalFormat("#,##0");
        format.setParseIntegerOnly(true);
        sb.append("p", (Long) null, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void longObjectWithFormat_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Long l = Long.valueOf(30000L);
        NumberFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", l, format));
    }

    @Test
    public void longObjectWithFormat_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Long l = null;
        NumberFormat format = null;
        sb.append("p", l, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void longArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        long[] a = new long[]{1L, 2L, 3L};
        sb.append("p", a);
        assertEquals("name.slhynju.util.Bean{p: [1, 2, 3]}", sb.toS());
    }

    @Test
    public void longArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", (long[]) null);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void longArrayWithFormat() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        long[] a = new long[]{10000L, 20000L, 30000L};
        DecimalFormat format = new DecimalFormat("#,##0");
        format.setParseIntegerOnly(true);
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: [10,000, 20,000, 30,000]}", sb.toS());
    }

    @Test
    public void longArrayWithFormat_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        long[] a = null;
        DecimalFormat format = new DecimalFormat("#,##0");
        format.setParseIntegerOnly(true);
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void longArrayWithFormat_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        long[] a = new long[]{10000L, 20000L, 30000L};
        DecimalFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", a, format));
    }

    @Test
    public void longArrayWithFormat_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        long[] a = null;
        DecimalFormat format = null;
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void floatObjectWithFormat() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Float f = Float.valueOf(1.3245f);
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", f, format);
        assertEquals("name.slhynju.util.Bean{p: 1.32}", sb.toS());
    }

    @Test
    public void floatObjectWithFormat_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Float f = null;
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", f, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void floatObjectWithFormat_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Float f = Float.valueOf(1.3245f);
        DecimalFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", f, format));
    }

    @Test
    public void floatObjectWithFormat_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Float f = null;
        DecimalFormat format = null;
        sb.append("p", f, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void floatArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        float[] a = new float[]{1.1542f, 1.2347f, 1.3782f};
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: [1.15, 1.23, 1.38]}", sb.toS());
    }

    @Test
    public void floatArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        float[] a = null;
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void floatArray_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        float[] a = new float[]{1.1542f, 1.2347f, 1.3782f};
        DecimalFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", a, format));
    }

    @Test
    public void floatArray_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        float[] a = null;
        DecimalFormat format = null;
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void doubleWithFormat() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", 3.57962, format);
        assertEquals("name.slhynju.util.Bean{p: 3.58}", sb.toS());
    }

    @Test
    public void doubleWithFormat_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        DecimalFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", 3.578, format));
    }

    @Test
    public void doubleObjectWithFormat() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Double d = Double.valueOf(3.57958);
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", d, format);
        assertEquals("name.slhynju.util.Bean{p: 3.58}", sb.toS());
    }

    @Test
    public void doubleObjectWithFormat_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Double d = null;
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", d, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void doubleObjectWithFormat_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Double d = Double.valueOf(3.57958);
        DecimalFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", d, format));
    }

    @Test
    public void doubleObjectWithFormat_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Double d = null;
        DecimalFormat format = null;
        sb.append("p", d, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void doubleArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        double[] a = new double[]{1.1542, 1.2347, 1.3782};
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: [1.15, 1.23, 1.38]}", sb.toS());
    }

    @Test
    public void doubleArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        double[] a = null;
        DecimalFormat format = new DecimalFormat("#,##0.00");
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void doubleArray_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        double[] a = new double[]{1.1542, 1.2347, 1.3782};
        DecimalFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", a, format));
    }

    @Test
    public void doubleArray_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        double[] a = null;
        DecimalFormat format = null;
        sb.append("p", a, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void booleanType() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", false);
        assertEquals("name.slhynju.util.Bean{p: false}", sb.toS());
    }

    @Test
    public void booleanObject() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", Boolean.FALSE);
        assertEquals("name.slhynju.util.Bean{p: false}", sb.toS());
    }

    @Test
    public void booleanObject_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", (Boolean) null);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void booleanArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        boolean[] a = new boolean[]{true, false};
        sb.append("p", a);
        assertEquals("name.slhynju.util.Bean{p: [true, false]}", sb.toS());
    }

    @Test
    public void booleanArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", (boolean[]) null);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void charType() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendQuoted("p", 'a');
        assertEquals("name.slhynju.util.Bean{p: \'a\'}", sb.toS());
    }

    @Test
    public void charObject() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Character c = Character.valueOf('a');
        sb.appendQuoted("p", c);
        assertEquals("name.slhynju.util.Bean{p: \'a\'}", sb.toS());
    }

    @Test
    public void charObject_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Character c = Character.valueOf('a');
        sb.appendQuoted("p1", c);
        sb.appendQuoted("p2", (Character) null);
        assertEquals("name.slhynju.util.Bean{p1: \'a\', p2: null}", sb.toS());
    }

    @Test
    public void charArray() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        char[] a = new char[]{'a', 'b', 'c'};
        sb.appendQuoted("p", a);
        assertEquals("name.slhynju.util.Bean{p: [\'a\', \'b\', \'c\']}", sb.toS());
    }

    @Test
    public void charArray_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        char[] a = null;
        sb.appendQuoted("p", a);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

}
