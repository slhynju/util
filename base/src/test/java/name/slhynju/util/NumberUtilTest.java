package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"UnnecessaryBoxing", "ConstantConditions"})
public class NumberUtilTest {

    @Test
    public void newFormat_long() {
        NumberFormat f = NumberUtil.newFormat(3, 5, false);
        assertEquals("3.250", f.format(3.25));
        assertEquals("-28.28329", f.format(-28.283293));
        // groupingUsed is Locale dependent.
    }

    @Test
    public void newFormat_invalidFraction() {
        NumberFormat f = NumberUtil.newFormat(-8, -2, false);
        assertEquals("3", f.format(3.25));
    }

    @Test
    public void newFormat_short() {
        NumberFormat f = NumberUtil.newFormat(3);
        assertEquals("2.240", f.format(2.24));
        assertEquals("-23.592", f.format(-23.5921));
    }

    @Test
    public void newFormat_short_invalidFraction() {
        NumberFormat f = NumberUtil.newFormat(-8);
        assertEquals("3", f.format(3.25));
    }

    @Test
    public void toInt() {
        assertEquals(89, NumberUtil.toInt("89", 7));
        assertEquals(-123, NumberUtil.toInt("-123", -4));
        assertEquals(7, NumberUtil.toInt("", 7));
        assertEquals(7, NumberUtil.toInt(null, 7));
        assertEquals(7, NumberUtil.toInt("null", 7));
    }

    @Test
    public void toInt_invalid() {
        assertThrows(NumberFormatException.class, () -> NumberUtil.toInt("abc", 7));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toInt("56abc", 7));
    }

    @Test
    public void toInteger() {
        Integer n = Integer.valueOf(25);
        assertEquals(n, NumberUtil.toInteger("25", 10));
        Integer n2 = Integer.valueOf(10);
        assertEquals(n2, NumberUtil.toInteger(null, 10));
        assertEquals(n2, NumberUtil.toInteger("", 10));
        assertEquals(n2, NumberUtil.toInteger("null", 10));
    }

    @Test
    public void toInteger_invalid() {
        assertThrows(NumberFormatException.class, () -> NumberUtil.toInteger("abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toInteger("25abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toInteger("8.0", 10));
    }

    @Test
    public void toS_Integer() {
        Integer n = Integer.valueOf(25);
        assertEquals("25", NumberUtil.toS(n));
        n = null;
        assertEquals("null", NumberUtil.toS(n));
    }

    @Test
    public void toLong() {
        assertEquals(89L, NumberUtil.toLong("89", 7L));
        assertEquals(-123L, NumberUtil.toLong("-123", -4L));
        assertEquals(7L, NumberUtil.toLong("", 7L));
        assertEquals(7L, NumberUtil.toLong(null, 7L));
        assertEquals(7L, NumberUtil.toLong("null", 7L));
    }

    @Test
    public void toLong_invalid() {
        assertThrows(NumberFormatException.class, () -> NumberUtil.toLong("abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toLong("25abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toLong("8.0", 10));
    }

    @Test
    public void toLongObject() {
        Long l = Long.valueOf(25);
        assertEquals(l, NumberUtil.toLongObject("25", 10L));
        l = Long.valueOf(10);
        assertEquals(l, NumberUtil.toLongObject(null, 10L));
        assertEquals(l, NumberUtil.toLongObject("null", 10L));
    }

    @Test
    public void toLongObject_invalid() {
        assertThrows(NumberFormatException.class, () -> NumberUtil.toLongObject("abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toLongObject("25abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toLongObject("8.0", 10));
    }

    @Test
    public void toS_Long() {
        Long n = Long.valueOf(25);
        assertEquals("25", NumberUtil.toS(n));
        n = null;
        assertEquals("null", NumberUtil.toS(n));
    }

    @Test
    public void toS_LongWithFormat() {
        Long l = Long.valueOf(259283);
        NumberFormat f = new DecimalFormat("#,###");
        assertEquals("259,283", NumberUtil.toS(l, f));
    }

    @Test
    public void toS_LongWithFormat_longNull() {
        Long l = null;
        NumberFormat f = new DecimalFormat("#,###");
        assertEquals("null", NumberUtil.toS(l, f));
    }

    @Test
    public void toS_LongWithFormat_formatNull() {
        Long l = Long.valueOf(259283);
        NumberFormat f = null;
        assertThrows(NullPointerException.class, () -> NumberUtil.toS(l, f));
    }

    @Test
    public void toS_LongWithFormat_bothNull() {
        Long l = null;
        NumberFormat f = null;
        assertEquals("null", NumberUtil.toS(l, f));
    }

    @Test
    public void toDouble() {
        assertEquals(89.5, NumberUtil.toDouble("89.5", 1.2), 0.000001);
        assertEquals(-123.47, NumberUtil.toDouble("-123.47", -4.5), 0.000001);
        assertEquals(2.3, NumberUtil.toDouble("", 2.3), 0.000001);
        assertEquals(2.3, NumberUtil.toDouble(null, 2.3), 0.000001);
        assertEquals(2.3, NumberUtil.toDouble("null", 2.3), 0.000001);
    }

    @Test
    public void toDouble_invalid() {
        assertThrows(NumberFormatException.class, () -> NumberUtil.toDouble("abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toDouble("25abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toDouble("8.0.7", 10));
    }

    @Test
    public void toDoubleObject() throws Exception {
        Double d = Double.valueOf("25.34");
        assertEquals(d, NumberUtil.toDoubleObject("25.34", 5.7));
        d = Double.valueOf(5.7);
        assertEquals(d, NumberUtil.toDoubleObject(null, 5.7));
        assertEquals(d, NumberUtil.toDoubleObject("null", 5.7));
    }

    @Test
    public void toDoubleObject_invalid() {
        assertThrows(NumberFormatException.class, () -> NumberUtil.toDoubleObject("abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toDoubleObject("25abc", 10));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toDoubleObject("8.0.7", 10));
    }

    @Test
    public void toS_Double() {
        Double n = Double.valueOf(25.34);
        assertEquals("25.34", NumberUtil.toS(n));
        n = null;
        assertEquals("null", NumberUtil.toS(n));
    }

    @Test
    public void toS_DoubleWithFormat() {
        Double n = Double.valueOf(6425.34);
        NumberFormat f = new DecimalFormat("#,##.00##");
        assertEquals("64,25.34", NumberUtil.toS(n, f));
        n = Double.valueOf(29328.392839);
        assertEquals("2,93,28.3928", NumberUtil.toS(n, f));
    }

    @Test
    public void toS_DoubleWithFormat_numberNull() {
        Double n = null;
        NumberFormat f = new DecimalFormat("#,##.00##");
        assertEquals("null", NumberUtil.toS(n, f));
    }

    @Test
    public void toS_DoubleWithFormat_formatNull() {
        Double n = Double.valueOf(6425.34);
        NumberFormat f = null;
        assertThrows(NullPointerException.class, () -> NumberUtil.toS(n, f));
    }

    @Test
    public void toS_DoubleWithFormat_bothNull() {
        Double n = null;
        NumberFormat f = null;
        assertEquals("null", NumberUtil.toS(n, f));
    }

    @Test
    public void toS_doubleWithMinMaxFraction() {
        assertEquals("23.24", NumberUtil.toS(23.24, 2, 5, false));
        assertEquals("-23.2400", NumberUtil.toS(-23.24, 4, 5, false));
        assertEquals("23.24893", NumberUtil.toS(23.24892783, 2, 5, false));
    }

    @Test
    public void toS_doubleWithMinMaxFraction_invalid() {
        assertEquals("23", NumberUtil.toS(23.24, -8, -5, false));
    }

    @Test
    public void toS_doubleWithFraction() {
        assertEquals("23.240", NumberUtil.toS(23.24, 3));
        assertEquals("-23.240", NumberUtil.toS(-23.24, 3));
        assertEquals("23.249", NumberUtil.toS(23.24892783, 3));
    }

    @Test
    public void toS_doubleWithFraction_invalid() {
        assertEquals("23", NumberUtil.toS(23.24, -5));
    }

    @Test
    public void toBigInteger() throws Exception {
        BigInteger l = new BigInteger("25");
        BigInteger default1 = new BigInteger("10");
        assertEquals(l, NumberUtil.toBigInteger("25", default1));
        assertEquals(default1, NumberUtil.toBigInteger(null, default1));
        assertEquals(default1, NumberUtil.toBigInteger("", default1));
        assertEquals(default1, NumberUtil.toBigInteger("null", default1));
        assertEquals(l, NumberUtil.toBigInteger("25", null));
        assertNull(NumberUtil.toBigInteger(null, null));
    }

    @Test
    public void toBigInteger_invalid() {
        BigInteger default1 = new BigInteger("10");
        assertThrows(NumberFormatException.class, () -> NumberUtil.toBigInteger("abc", default1));
        assertThrows(NumberFormatException.class, () -> NumberUtil.toBigInteger("25abc", default1));
    }

    @Test
    public void toS_BigInteger() {
        BigInteger n = new BigInteger("25");
        assertEquals("25", NumberUtil.toS(n));
        n = null;
        assertEquals("null", NumberUtil.toS(n));
    }

    @Test
    public void isIntegerString() {
        assertFalse(NumberUtil.isIntegerString(null));
        assertFalse(NumberUtil.isIntegerString(""));
        assertFalse(NumberUtil.isIntegerString(" "));
        assertFalse(NumberUtil.isIntegerString("+"));
        assertFalse(NumberUtil.isIntegerString("-"));
        assertFalse(NumberUtil.isIntegerString("."));
        assertTrue(NumberUtil.isIntegerString("0"));
        assertTrue(NumberUtil.isIntegerString("9"));
        assertFalse(NumberUtil.isIntegerString("a"));
        assertFalse(NumberUtil.isIntegerString("C"));
        assertTrue(NumberUtil.isIntegerString("25"));
        assertTrue(NumberUtil.isIntegerString("+6"));
        assertTrue(NumberUtil.isIntegerString("-3"));
        assertFalse(NumberUtil.isIntegerString("7C"));
        assertFalse(NumberUtil.isIntegerString("C7"));
        assertFalse(NumberUtil.isIntegerString("+-"));
        assertFalse(NumberUtil.isIntegerString("+."));
        assertTrue(NumberUtil.isIntegerString("125"));
        assertFalse(NumberUtil.isIntegerString("20 58"));
        assertFalse(NumberUtil.isIntegerString(" 2058"));
        assertFalse(NumberUtil.isIntegerString("2058 "));
        assertFalse(NumberUtil.isIntegerString("20+58"));
        assertTrue(NumberUtil.isIntegerString("222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222"));
        assertFalse(NumberUtil.isIntegerString("20.58"));
        assertFalse(NumberUtil.isIntegerString("0."));
    }

    @Test
    public void isIntegerStringWithRadix() {
        assertTrue(NumberUtil.isIntegerString("11010100", 2));
        assertFalse(NumberUtil.isIntegerString("12010100", 2));
        assertTrue(NumberUtil.isIntegerString("ABCD030EF", 16));
        assertFalse(NumberUtil.isIntegerString("ABCDG", 16));
        assertTrue(NumberUtil.isIntegerString("zzzzz", 36));
    }

    @Test
    public void isDoubleString() {
        assertFalse(NumberUtil.isNumberString(null));
        assertFalse(NumberUtil.isNumberString(""));
        assertFalse(NumberUtil.isNumberString(" "));
        assertFalse(NumberUtil.isNumberString("+"));
        assertFalse(NumberUtil.isNumberString("-"));
        assertFalse(NumberUtil.isNumberString("."));
        assertTrue(NumberUtil.isNumberString("0"));
        assertTrue(NumberUtil.isNumberString("9"));
        assertFalse(NumberUtil.isNumberString("a"));
        assertFalse(NumberUtil.isNumberString("C"));
        assertTrue(NumberUtil.isNumberString("25"));
        assertTrue(NumberUtil.isNumberString("+6"));
        assertTrue(NumberUtil.isNumberString("-3"));
        assertFalse(NumberUtil.isNumberString("7C"));
        assertFalse(NumberUtil.isNumberString("C7"));
        assertFalse(NumberUtil.isNumberString("+-"));
        assertFalse(NumberUtil.isNumberString("+."));
        assertTrue(NumberUtil.isNumberString("125"));
        assertFalse(NumberUtil.isNumberString("20 58"));
        assertFalse(NumberUtil.isNumberString(" 2058"));
        assertFalse(NumberUtil.isNumberString("2058 "));
        assertFalse(NumberUtil.isNumberString("20+58"));
        assertTrue(NumberUtil.isNumberString("222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222"));
        assertTrue(NumberUtil.isNumberString("20.58"));
        assertFalse(NumberUtil.isNumberString("0."));
        assertFalse(NumberUtil.isNumberString(".3"));
        assertTrue(NumberUtil.isNumberString("0.3"));
        assertFalse(NumberUtil.isNumberString("+0."));
        assertFalse(NumberUtil.isNumberString("+.3"));
        assertTrue(NumberUtil.isNumberString("+0.3"));
        assertFalse(NumberUtil.isNumberString("-0."));
        assertFalse(NumberUtil.isNumberString("-.3"));
        assertTrue(NumberUtil.isNumberString("-0.3"));
        assertFalse(NumberUtil.isNumberString("-0.-3"));
        assertFalse(NumberUtil.isNumberString("0.0.6"));
        assertTrue(NumberUtil.isNumberString("22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222.2222222222222222222222222222222222222"));
        assertFalse(NumberUtil.isNumberString("22222222222222222222222222222222222222222222222.222222222222222222222222222222222222222222222222222.2222222222222222222222222222222222222"));
        assertFalse(NumberUtil.isNumberString("75.6c"));
    }

    @Test
    public void isDoubleStringWithRadix() {
        assertTrue(NumberUtil.isNumberString("-01001100.11000011", 2));
        assertFalse(NumberUtil.isNumberString("11020100.1001", 2));
        assertTrue(NumberUtil.isNumberString("-DDEF.AB05", 16));
        assertFalse(NumberUtil.isNumberString("-DDGF.AB05", 16));
    }

    @Test
    public void invalidRadix() {
        assertThrows(IllegalArgumentException.class, () -> NumberUtil.isIntegerString("0000", 1));
        assertThrows(IllegalArgumentException.class, () -> NumberUtil.isNumberString("FFFFF", 37));
    }

}
