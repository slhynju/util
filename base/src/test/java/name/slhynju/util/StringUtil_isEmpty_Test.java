package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.function.IntPredicate;

import static org.junit.jupiter.api.Assertions.*;


public class StringUtil_isEmpty_Test {

    @Test
    public void isEmpty() {
        assertTrue(StringUtil.isEmpty(null));
        assertTrue(StringUtil.isEmpty(""));
        assertFalse(StringUtil.isEmpty("  "));
        assertFalse(StringUtil.isEmpty("abc"));
        assertFalse(StringUtil.isEmpty("123"));
    }

    @SuppressWarnings({"StringOperationCanBeSimplified", "RedundantCast"})
    @Test
    public void isEmpty_sb() {
        assertTrue(StringUtil.isEmpty((StringBuilder) null));
        assertTrue(StringUtil.isEmpty(new StringBuilder("")));
        assertFalse(StringUtil.isEmpty(new StringBuilder("  ")));
        assertFalse(StringUtil.isEmpty(new StringBuilder("abc")));
        assertFalse(StringUtil.isEmpty(new StringBuilder("123")));
    }

    @Test
    public void notEmpty() {
        assertFalse(StringUtil.notEmpty(null));
        assertFalse(StringUtil.notEmpty(""));
        assertTrue(StringUtil.notEmpty("  "));
        assertTrue(StringUtil.notEmpty("abc"));
        assertTrue(StringUtil.notEmpty("123"));
    }

    @SuppressWarnings({"StringOperationCanBeSimplified", "RedundantCast"})
    @Test
    public void notEmpty_sb() {
        assertFalse(StringUtil.notEmpty((StringBuilder) null));
        assertFalse(StringUtil.notEmpty(new StringBuilder("")));
        assertTrue(StringUtil.notEmpty(new StringBuilder("  ")));
        assertTrue(StringUtil.notEmpty(new StringBuilder("abc")));
        assertTrue(StringUtil.notEmpty(new StringBuilder("123")));
    }

    @Test
    public void isNull() {
        assertTrue(StringUtil.isNull(null));
        assertTrue(StringUtil.isNull(""));
        assertTrue(StringUtil.isNull("null"));
        assertFalse(StringUtil.isNull("abc"));
    }

    @Test
    public void notNull() {
        assertFalse(StringUtil.notNull(null));
        assertFalse(StringUtil.notNull(""));
        assertFalse(StringUtil.notNull("null"));
        assertTrue(StringUtil.notNull("abc"));
    }

    @Test
    public void emptyToNull() {
        assertNull(StringUtil.emptyToNull(null));
        assertNull(StringUtil.emptyToNull(""));
        assertEquals("abc", StringUtil.emptyToNull("abc"));
        assertEquals("   ", StringUtil.emptyToNull("   "));
    }

    @Test
    public void nullToEmpty() {
        assertEquals("", StringUtil.nullToEmpty(null));
        assertEquals("", StringUtil.nullToEmpty(""));
        assertEquals("abc", StringUtil.nullToEmpty("abc"));
        assertEquals("   ", StringUtil.nullToEmpty("   "));
    }

    @Test
    public void trim() {
        assertEquals("", StringUtil.trim(null));
        assertEquals("", StringUtil.trim(""));
        assertEquals("", StringUtil.trim(" "));
        assertEquals("", StringUtil.trim("  "));
        assertEquals("abc", StringUtil.trim("abc"));
        assertEquals("abc", StringUtil.trim("  abc"));
        assertEquals("abc", StringUtil.trim("abc  "));
        assertEquals("abc", StringUtil.trim(" abc "));
        assertEquals("abc  def", StringUtil.trim("   abc  def    "));
    }

    @Test
    public void isYes() {
        assertTrue(StringUtil.isYes("y"));
        assertTrue(StringUtil.isYes("Y"));
        assertTrue(StringUtil.isYes("yes"));
        assertTrue(StringUtil.isYes("YES"));
        assertTrue(StringUtil.isYes("yeS"));
        assertTrue(StringUtil.isYes("yEs"));
        assertTrue(StringUtil.isYes("t"));
        assertTrue(StringUtil.isYes("T"));
        assertTrue(StringUtil.isYes("true"));
        assertTrue(StringUtil.isYes("True"));
        assertTrue(StringUtil.isYes("TRUE"));
        assertTrue(StringUtil.isYes("trUe"));
        assertTrue(StringUtil.isYes("1"));
        assertFalse(StringUtil.isYes(""));
        assertFalse(StringUtil.isYes(null));
        assertFalse(StringUtil.isYes("abc"));
        assertFalse(StringUtil.isYes("no"));
        assertFalse(StringUtil.isYes("2"));
        assertFalse(StringUtil.isYes("0"));
    }

    @Test
    public void isNo() {
        assertTrue(StringUtil.isNo("n"));
        assertTrue(StringUtil.isNo("N"));
        assertTrue(StringUtil.isNo("no"));
        assertTrue(StringUtil.isNo("No"));
        assertTrue(StringUtil.isNo("NO"));
        assertTrue(StringUtil.isNo("f"));
        assertTrue(StringUtil.isNo("F"));
        assertTrue(StringUtil.isNo("false"));
        assertTrue(StringUtil.isNo("False"));
        assertTrue(StringUtil.isNo("FALSE"));
        assertTrue(StringUtil.isNo("faLse"));
        assertTrue(StringUtil.isNo("0"));
        assertFalse(StringUtil.isNo(""));
        assertFalse(StringUtil.isNo(null));
        assertFalse(StringUtil.isNo("abc"));
        assertFalse(StringUtil.isNo("true"));
        assertFalse(StringUtil.isNo("yes"));
        assertFalse(StringUtil.isNo("1"));
        assertFalse(StringUtil.isNo("2"));
    }

    @Test
    public void isDigits() {
        assertTrue(StringUtil.isDigits("1234567890"));
        assertFalse(StringUtil.isDigits(null));
        assertFalse(StringUtil.isDigits(""));
        assertFalse(StringUtil.isDigits("123L"));
        assertFalse(StringUtil.isDigits("0x55ff"));
        assertFalse(StringUtil.isDigits("-123"));
        assertFalse(StringUtil.isDigits("12.34"));
    }

    @Test
    public void notDigits() {
        assertFalse(StringUtil.notDigits("1234567890"));
        assertTrue(StringUtil.notDigits(null));
        assertTrue(StringUtil.notDigits(""));
        assertTrue(StringUtil.notDigits("123L"));
        assertTrue(StringUtil.notDigits("0x55ff"));
        assertTrue(StringUtil.notDigits("-123"));
        assertTrue(StringUtil.notDigits("12.34"));
    }

    @Test
    public void isUpperCase() {
        assertFalse(StringUtil.isUpperCases(null));
        assertFalse(StringUtil.isUpperCases(""));
        assertFalse(StringUtil.isUpperCases("abc"));
        assertFalse(StringUtil.isUpperCases("aBc"));
        assertTrue(StringUtil.isUpperCases("ABC"));
    }

    @Test
    public void notUpperCase() {
        assertTrue(StringUtil.notUpperCases(null));
        assertTrue(StringUtil.notUpperCases(""));
        assertTrue(StringUtil.notUpperCases("abc"));
        assertTrue(StringUtil.notUpperCases("aBc"));
        assertFalse(StringUtil.notUpperCases("ABC"));
    }

    @Test
    public void isLowerCase() {
        assertFalse(StringUtil.isLowerCases(null));
        assertFalse(StringUtil.isLowerCases(""));
        assertTrue(StringUtil.isLowerCases("abc"));
        assertFalse(StringUtil.isLowerCases("aBc"));
        assertFalse(StringUtil.isLowerCases("ABC"));
    }

    @Test
    public void notLowerCase() {
        assertTrue(StringUtil.notLowerCases(null));
        assertTrue(StringUtil.notLowerCases(""));
        assertFalse(StringUtil.notLowerCases("abc"));
        assertTrue(StringUtil.notLowerCases("aBc"));
        assertTrue(StringUtil.notLowerCases("ABC"));
    }

    @Test
    public void allMatch() {
        assertFalse(StringUtil.allMatch(null, i -> i == 5));
        assertFalse(StringUtil.allMatch("abc", i -> i == 5));
        assertFalse(StringUtil.allMatch("", i -> i == 'a'));
        assertTrue(StringUtil.allMatch("aaa", i -> i == 'a'));
    }

    @Test
    public void allMatch_predicateNull() {
        IntPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> StringUtil.allMatch("abc", matcher));
    }
}
