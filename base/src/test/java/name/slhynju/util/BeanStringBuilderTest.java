package name.slhynju.util;

import name.slhynju.ApplicationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SuppressWarnings("ALL")
public class BeanStringBuilderTest {

    @Test
    public void constructor() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        assertEquals("name.slhynju.util.Bean{}", sb.toS());
    }

    @Test
    public void constructor_null() {
        Class c = null;
        assertThrows(NullPointerException.class, () -> new BeanStringBuilder(c));
    }

    @Test
    public void nullProperty() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendNullProperty("p");
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void nullProperty_nameNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendNullProperty(null);
        assertEquals("name.slhynju.util.Bean{null: null}", sb.toS());
    }

    @Test
    public void stringProperty_nameNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendQuoted(null, "value");
        assertEquals("name.slhynju.util.Bean{null: \"value\"}", sb.toS());
    }

    @Test
    public void toStringMethod() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p1", "a1");
        String s1 = sb.toS();
        String s2 = sb.toString();
        assertEquals(s1, s2);
    }

    @Test
    public void length() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendNullProperty("p");
        assertEquals(30, sb.length());
        sb.toS();
        assertEquals(31, sb.length());
        sb.toS();
        assertEquals(31, sb.length());
    }

    @Test
    public void charAt() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendNullProperty("p");
        assertEquals('m', sb.charAt(2));
    }

    @Test
    public void subSequence() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendNullProperty("p");
        assertEquals("me.", sb.subSequence(2, 5));
    }

    @Test
    public void subSequence_invalidIndex() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        assertThrows(StringIndexOutOfBoundsException.class, () -> sb.subSequence(-5, -1));
    }

    @Test
    public void closed() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p1", "a1");
        sb.toS();
        assertThrows(ApplicationException.class, () -> {
            sb.append("p2", "a2");
        });
    }

}
