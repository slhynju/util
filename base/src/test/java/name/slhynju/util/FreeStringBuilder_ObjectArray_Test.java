package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FreeStringBuilder_ObjectArray_Test {

    @Test
    public void toS() {
        Integer[] a0 = new Integer[0];
        assertEquals("", FreeStringBuilder.toS(a0, ","));
        Integer[] a1 = new Integer[]{3};
        assertEquals("3", FreeStringBuilder.toS(a1, ","));
        Integer[] a2 = new Integer[]{3, 5};
        assertEquals("3,5", FreeStringBuilder.toS(a2, ","));
    }

    @Test
    public void toS_null() {
        assertEquals("null", FreeStringBuilder.toS((Integer[]) null, ","));
    }

    @Test
    public void toS_elementNull() {
        Integer[] a = new Integer[]{3, null, 5};
        assertEquals("3,null,5", FreeStringBuilder.toS(a, ","));
    }

    @Test
    public void toS_separatorNull() {
        Integer[] a = new Integer[]{3, 5};
        assertThrows(NullPointerException.class, () -> FreeStringBuilder.toS(a, null));
    }

    @Test
    public void toS_bothNull() {
        Integer[] a = null;
        assertEquals("null", FreeStringBuilder.toS(a, null));
    }

    @Test
    public void objectArray() {
        Integer[] a = new Integer[]{3, 5, 7};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a);
        assertEquals("[3, 5, 7]", sb.toS());
    }

    @Test
    public void objectArray_null() {
        Integer[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a);
        assertEquals("null", sb.toS());
    }

    @Test
    public void objectArray_elementNull() {
        Integer[] a = new Integer[]{3, 5, null};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a);
        assertEquals("[3, 5, null]", sb.toS());
    }

    @Test
    public void objectArrayWithoutBoundary() {
        Integer[] a = new Integer[]{3, 5, 7};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendWithoutMarks(a);
        assertEquals("3, 5, 7", sb.toS());
    }

    @Test
    public void objectArrayWithoutBoundary_null() {
        Integer[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendWithoutMarks(a);
        assertEquals("null", sb.toS());
    }

    @Test
    public void objectArrayWithoutBoundary_elementNull() {
        Integer[] a = new Integer[]{3, null, 7};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendWithoutMarks(a);
        assertEquals("3, null, 7", sb.toS());
    }

    @Test
    public void objectArrayWithBoundary() {
        Integer[] a = new Integer[]{3, 5, 7};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "{", "+", "}");
        assertEquals("{3+5+7}", sb.toS());
    }

    @Test
    public void objectArrayWithBoundary_null() {
        Integer[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "{", "+", "}");
        assertEquals("null", sb.toS());
    }

    @Test
    public void objectArrayWithBoundary_elementNull() {
        Integer[] a = new Integer[]{3, null, 7};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "{", "+", "}");
        assertEquals("{3+null+7}", sb.toS());
    }

    @Test
    public void objectArrayWithBoundary_leftBoundaryNull() {
        Integer[] a = new Integer[]{3, 5, 7};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, "+", "}");
        assertEquals("null3+5+7}", sb.toS());
    }

    @Test
    public void objectArrayWithBoundary_separatorNull() {
        Integer[] a = new Integer[]{3, 5, 7};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "{", null, "}"));
    }

    @Test
    public void objectArrayWithBoundary_rightBoundaryNull() {
        Integer[] a = new Integer[]{3, 5, 7};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "{", "+", null);
        assertEquals("{3+5+7null", sb.toS());
    }

    @Test
    public void objectArrayWithBoundary_allNull() {
        Integer[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "{", "+", "}");
        assertEquals("null", sb.toS());
    }

    @Test
    public void objectArrayMapper() {
        Integer[] a = new Integer[]{3, 5, 7};
        Function<Integer, String> mapper = (x) -> String.valueOf(x + x);
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ", ", "]", mapper);
        assertEquals("[6, 10, 14]", sb.toS());
    }

    @Test
    public void objectArrayMapper_null() {
        Integer[] a = null;
        Function<Integer, String> mapper = (x) -> String.valueOf(x + x);
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ", ", "]", mapper);
        assertEquals("null", sb.toS());
    }

    @Test
    public void objectArrayMapper_elementNull() {
        Integer[] a = new Integer[]{3, 5, null};
        Function<Integer, String> mapper = (x) -> x == null ? "100" : String.valueOf(x + x);
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ", ", "]", mapper);
        assertEquals("[6, 10, 100]", sb.toS());
    }

    @Test
    public void objectArrayMapper_mapperNull() {
        Integer[] a = new Integer[]{3, 5, 7};
        Function<Integer, String> mapper = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", ", ", "]", mapper));
    }

    @Test
    public void objectArrayMapper_leftBoundaryNull() {
        Integer[] a = new Integer[]{3, 5, 7};
        Function<Integer, String> mapper = (x) -> String.valueOf(x + x);
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, ", ", "]", mapper);
        assertEquals("null6, 10, 14]", sb.toS());
    }

    @Test
    public void objectArrayMapper_rightBoundaryNull() {
        Integer[] a = new Integer[]{3, 5, 7};
        Function<Integer, String> mapper = (x) -> String.valueOf(x + x);
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ", ", null, mapper);
        assertEquals("[6, 10, 14null", sb.toS());
    }

    @Test
    public void objectArrayMapper_separatorNull() {
        Integer[] a = new Integer[]{3, 5, 7};
        Function<Integer, String> mapper = (x) -> String.valueOf(x + x);
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]", mapper));
    }

    @Test
    public void objectArrayMapper_allNull() {
        Integer[] a = null;
        Function<Integer, String> mapper = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, null, null, mapper);
        assertEquals("null", sb.toS());
    }

}

