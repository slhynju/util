package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtil_truncate_Test {

    @Test
    public void truncateMonth_withTimeZone() {
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(zone);
        Date d = DateUtil.toDate("2012-04-05 15:24:37", format);
        d = DateUtil.truncate(d, Calendar.MONTH, zone);
        assertEquals("2012-04-01 00:00:00", DateUtil.toS(d, format));
    }

    @Test
    public void truncate_withTimeZone_zoneNull() {
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(zone);
        Date d = DateUtil.toDate("2012-04-05 15:24:37", format);
        assertThrows(NullPointerException.class, () -> DateUtil.truncate(d, Calendar.MONTH, null));
    }

    @Test
    public void truncateYear() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        d = DateUtil.truncate(d, Calendar.YEAR);
        assertEquals("2012-01-01 00:00:00", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void truncateMonth() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        d = DateUtil.truncate(d, Calendar.MONTH);
        assertEquals("2012-04-01 00:00:00", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void truncateDay() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        d = DateUtil.truncate(d, Calendar.DATE);
        assertEquals("2012-04-05 00:00:00", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void truncateHour() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        d = DateUtil.truncate(d, Calendar.HOUR_OF_DAY);
        assertEquals("2012-04-05 15:00:00", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void truncateMin() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        d = DateUtil.truncate(d, Calendar.MINUTE);
        assertEquals("2012-04-05 15:24:00", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void truncateSec() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        d = DateUtil.truncate(d, Calendar.SECOND);
        assertEquals("2012-04-05 15:24:37", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    // day of week is Locale dependent.

    @Test
    public void truncateIllegal() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        assertThrows(IllegalArgumentException.class, () -> DateUtil.truncate(d, 100));
    }

    @Test
    public void truncateNull() {
        Date d = DateUtil.truncate(null, Calendar.YEAR);
        assertNull(d);
    }
}
