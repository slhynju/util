package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("static-method")
public class StringUtil_splitPair_Test {

    @Test
    public void splitPair() {
        String[] a = StringUtil.splitPair(" ab = c ", "=");
        assertEquals(2, a.length);
        assertEquals("ab", a[0]);
        assertEquals("c", a[1]);
    }

    @Test
    public void splitPair_2() {
        String[] a = StringUtil.splitPair(" ab += c ", "+=");
        assertEquals(2, a.length);
        assertEquals("ab", a[0]);
        assertEquals("c", a[1]);
    }

    @Test
    public void splitPair_3() {
        String[] a = StringUtil.splitPair(" ab += c ", "b");
        assertEquals(2, a.length);
        assertEquals("a", a[0]);
        assertEquals("+= c", a[1]);
    }

    @Test
    public void splitPair_4() {
        String[] a = StringUtil.splitPair(" ab += c ", "k");
        assertEquals(2, a.length);
        assertEquals("ab += c", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitPair_separatorNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.splitPair(" ab += c ", null));
    }

    @Test
    public void splitPair_separatorEmpty() {
        String[] a = StringUtil.splitPair(" ab += c ", "");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("ab += c", a[1]);
    }

    @Test
    public void splitPair_empty() {
        String[] a = StringUtil.splitPair("", "b");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitPair_empty_separatorEmpty() {
        String[] a = StringUtil.splitPair("", "");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitPair_null() {
        String[] a = StringUtil.splitPair(null, "b");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitPair_null_separatorEmpty() {
        String[] a = StringUtil.splitPair(null, "");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitPair_null_separatorNull() {
        String[] a = StringUtil.splitPair(null, null);
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitPair_5() {
        String[] a = StringUtil.splitPair("a =", "=");
        assertEquals(2, a.length);
        assertEquals("a", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitTokens() {
        String[] a = StringUtil.splitTokens(" hello world ", "e", "o");
        assertEquals(3, a.length);
        assertEquals("h", a[0]);
        assertEquals("ll", a[1]);
        assertEquals("world", a[2]);
    }

    @Test
    public void splitTokens_2() {
        String[] a = StringUtil.splitTokens(" fee = 3.45$", "=");
        assertEquals(2, a.length);
        assertEquals("fee", a[0]);
        assertEquals("3.45$", a[1]);
    }

    @Test
    public void splitTokens_separatorEmpty() {
        String[] a = StringUtil.splitTokens(" a = b ", "");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("a = b", a[1]);
    }

    @Test
    public void splitTokens_separatorNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.splitTokens(" a = b ", (String[]) null));
    }

    @Test
    public void splitTokens_blank() {
        String[] a = StringUtil.splitTokens(" ", "k");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitTokens_blank_separatorEmpty() {
        String[] a = StringUtil.splitTokens(" ", "");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitTokens_blank_separatorNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.splitTokens(" ", (String[]) null));
    }

    @Test
    public void splitTokens_blank_separatorNullElement() {
        String[] separators = {"=", null};
        assertThrows(NullPointerException.class, () -> StringUtil.splitTokens("a = 5", separators));
    }

    @Test
    public void splitTokens_null() {
        String[] a = StringUtil.splitTokens(null, "k");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitTokens_null_separatorEmpty() {
        String[] a = StringUtil.splitTokens(null, "");
        assertEquals(2, a.length);
        assertEquals("", a[0]);
        assertEquals("", a[1]);
    }

    @Test
    public void splitTokens_null_separatorNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.splitTokens(null, (String[]) null));
    }

    @Test
    public void splitTokens_6() {
        String[] a = StringUtil.splitTokens("3 = 1 + 2", "=", "+");
        assertEquals(3, a.length);
        assertEquals("3", a[0]);
        assertEquals("1", a[1]);
        assertEquals("2", a[2]);
    }

    @Test
    public void splitTokens_7() {
        String[] a = StringUtil.splitTokens("5 = 5", "=", "+");
        assertEquals(3, a.length);
        assertEquals("5", a[0]);
        assertEquals("5", a[1]);
        assertEquals("", a[2]);
    }

    @Test
    public void splitTokens_8() {
        String[] a = StringUtil.splitTokens("5 = 4 +", "=", "+");
        assertEquals(3, a.length);
        assertEquals("5", a[0]);
        assertEquals("4", a[1]);
        assertEquals("", a[2]);
    }

    @Test
    public void splitKeyValues() {
        Map<String, String> map = StringUtil.splitKeyValues("k1=v1&k2=v2&k3=v3", "=", "&");
        assertEquals("v1", map.get("k1"));
        assertEquals("v2", map.get("k2"));
        assertEquals("v3", map.get("k3"));
        assertFalse(map.containsKey(""));
    }

    @Test
    public void splitKeyValues_allEntrySeparators() {
        Map<String, String> map = StringUtil.splitKeyValues("&&&&&", "=", "&");
        assertTrue(map.isEmpty());
    }

    @Test
    public void splitKeyValues_allKeyValueSeparators() {
        Map<String, String> map = StringUtil.splitKeyValues("======", "=", "&");
        assertTrue(map.isEmpty());
    }

    @Test
    public void splitKeyValues_null() {
        Map<String, String> map = StringUtil.splitKeyValues(null, "=", "&");
        assertTrue(map.isEmpty());
    }

    @Test
    public void splitKeyValues_empty() {
        Map<String, String> map = StringUtil.splitKeyValues("", "=", "&");
        assertTrue(map.isEmpty());
    }

    @Test
    public void splitKeyValues_keyValueSeparatorNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.splitKeyValues("k1=v1&k2=v2&k3=v3", null, "&"));
    }

    @Test
    public void splitKeyValues_entrySeparatorNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.splitKeyValues("k1=v1&k2=v2&k3=v3", "=", null));
    }

    @Test
    public void splitKeyValues_allNull() {
        Map<String, String> map = StringUtil.splitKeyValues(null, null, null);
        assertTrue(map.isEmpty());
    }

    @Test
    public void splitKeyValues_2() {
        Map<String, String> map = StringUtil.splitKeyValues("k1=&k2=v2&=v3&", "=", "&");
        assertEquals(2, map.size());
        assertEquals("", map.get("k1"));
        assertEquals("v2", map.get("k2"));
    }

}
