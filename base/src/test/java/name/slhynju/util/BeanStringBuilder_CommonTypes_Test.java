package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeanStringBuilder_CommonTypes_Test {

    @Test
    public void bigInteger() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        BigInteger n = new BigInteger("10000000000000000000000000000");
        sb.append("p", n);
        assertEquals("name.slhynju.util.Bean{p: 10000000000000000000000000000}", sb.toS());
    }

    @Test
    public void bigInteger_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        BigInteger n = null;
        sb.append("p", n);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void stringType() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendQuoted("p", "a");
        assertEquals("name.slhynju.util.Bean{p: \"a\"}", sb.toS());
    }

    @Test
    public void stringType_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        String n = null;
        sb.appendQuoted("p", n);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void charSequence() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendQuoted("p", (CharSequence) "a");
        assertEquals("name.slhynju.util.Bean{p: \"a\"}", sb.toS());
    }

    @Test
    public void charSequence_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.appendQuoted("p", (CharSequence) null);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void enumType() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        sb.append("p", Thread.State.BLOCKED);
        assertEquals("name.slhynju.util.Bean{p: BLOCKED}", sb.toS());
    }

    @Test
    public void enumType_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Thread.State s = null;
        sb.append("p", s);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void classType() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Class<?> c = Bean.class;
        sb.append("p", c);
        assertEquals("name.slhynju.util.Bean{p: name.slhynju.util.Bean}", sb.toS());
    }

    @Test
    public void classType_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Class<?> c = null;
        sb.append("p", c);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void object() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Object o = Integer.valueOf(5);
        sb.append("p", o);
        assertEquals("name.slhynju.util.Bean{p: 5}", sb.toS());
    }

    @Test
    public void object2() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Object o = "abc";
        sb.append("p", o);
        assertEquals("name.slhynju.util.Bean{p: abc}", sb.toS());
    }

    @Test
    public void object_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Object o = null;
        sb.append("p", o);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }


}
