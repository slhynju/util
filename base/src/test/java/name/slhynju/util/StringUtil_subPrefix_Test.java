package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringUtil_subPrefix_Test {

    @Test
    public void subPrefix() {
        assertEquals("", StringUtil.subPrefix("abc", 0));
        assertEquals("a", StringUtil.subPrefix("abc", 1));
        assertEquals("ab", StringUtil.subPrefix("abc", 2));
        assertEquals("abc", StringUtil.subPrefix("abc", 3));
        assertEquals("abc", StringUtil.subPrefix("abc", 4));
        assertEquals("abc", StringUtil.subPrefix("abc", 5));
        assertEquals("", StringUtil.subPrefix("a", 0));
        assertEquals("a", StringUtil.subPrefix("a", 1));
        assertEquals("a", StringUtil.subPrefix("a", 2));
        assertEquals("", StringUtil.subPrefix("", 0));
        assertEquals("", StringUtil.subPrefix("", 1));
        assertEquals("", StringUtil.subPrefix("", 2));
    }

    @Test
    public void subPrefix_negativeLength() {
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.subPrefix("abc", -7));
    }

    @Test
    public void subPrefix_null() {
        String s = StringUtil.subPrefix((String) null, 3);
        assertEquals("", s);
    }

    @Test
    public void subPrefix_null_zeroLength() {
        String s = StringUtil.subPrefix((String) null, 0);
        assertEquals("", s);
    }

    @Test
    public void subPrefix_null_negativeLength() {
        String s = StringUtil.subPrefix((String) null, -5);
        assertEquals("", s);
    }


    @SuppressWarnings("StringOperationCanBeSimplified")
    @Test
    public void subPrefix_sb() {
        StringBuilder sb = new StringBuilder("abc");
        assertEquals("", StringUtil.subPrefix(sb, 0));
        assertEquals("a", StringUtil.subPrefix(sb, 1));
        assertEquals("ab", StringUtil.subPrefix(sb, 2));
        assertEquals("abc", StringUtil.subPrefix(sb, 3));
        assertEquals("abc", StringUtil.subPrefix(sb, 4));
        assertEquals("abc", StringUtil.subPrefix(sb, 5));
        sb = new StringBuilder("a");
        assertEquals("", StringUtil.subPrefix(sb, 0));
        assertEquals("a", StringUtil.subPrefix(sb, 1));
        assertEquals("a", StringUtil.subPrefix(sb, 2));
        sb = new StringBuilder("");
        assertEquals("", StringUtil.subPrefix(sb, 0));
        assertEquals("", StringUtil.subPrefix(sb, 1));
        assertEquals("", StringUtil.subPrefix(sb, 2));
    }

    @Test
    public void subPrefix_sb_negativeLength() {
        StringBuilder sb = new StringBuilder("abc");
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.subPrefix(sb, -7));
    }

    @Test
    public void subPrefix_sb_null() {
        String s = StringUtil.subPrefix((StringBuilder) null, 3);
        assertEquals("", s);
    }

    @Test
    public void subPrefix_sb_null_zeroLength() {
        String s = StringUtil.subPrefix((StringBuilder) null, 0);
        assertEquals("", s);
    }

    @Test
    public void subPrefix_sb_null_negativeLength() {
        String s = StringUtil.subPrefix((StringBuilder) null, -5);
        assertEquals("", s);
    }

}
