package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FreeStringBuilder_Collection_Test {

    @SuppressWarnings("rawtypes")
    @Test
    public void toS() {
        List<String> l = new ArrayList<>();
        l.add("v1");
        assertEquals("v1", FreeStringBuilder.toS(l, ","));
        l.add("v2");
        assertEquals("v1,v2", FreeStringBuilder.toS(l, ","));
        l.clear();
        assertEquals("", FreeStringBuilder.toS(l, ","));

    }

    @Test
    public void toS_null() {
        List<String> l = null;
        assertEquals("null", FreeStringBuilder.toS(l, ","));
    }

    @Test
    public void toS_elementNull() {
        List<String> l = Arrays.asList("abc", null, "def");
        assertEquals("abc,null,def", FreeStringBuilder.toS(l, ","));
    }

    @Test
    public void toS_separatorNull() {
        List<String> l = Arrays.asList("v1", "v2", "v3");
        assertThrows(NullPointerException.class, () -> FreeStringBuilder.toS(l, null));
    }

    @Test
    public void toS_bothNull() {
        List<String> l = null;
        assertEquals("null", FreeStringBuilder.toS(l, null));
    }


    @Test
    public void collection() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        sb.append(list);
        assertEquals("[a, b]", sb.toS());
    }

    @Test
    public void collection_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = null;
        sb.append(list);
        assertEquals("null", sb.toS());
    }

    @Test
    public void collection_elementNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add(null);
        list.add("c");
        sb.append(list);
        assertEquals("[a, null, c]", sb.toS());
    }

    @Test
    public void collectionWithoutBoundary() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        sb.appendWithoutMarks(list);
        assertEquals("a, b", sb.toS());
    }

    @Test
    public void collectionWithoutBoundary_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = null;
        sb.appendWithoutMarks(list);
        assertEquals("null", sb.toS());
    }

    @Test
    public void collectionWithoutBoundary_elementNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add(null);
        list.add("c");
        sb.appendWithoutMarks(list);
        assertEquals("a, null, c", sb.toS());
    }

    @Test
    public void collectionWithBoundary() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        sb.append(list, "k", "f", "t");
        assertEquals("kafbt", sb.toS());
    }

    @Test
    public void collectionWithBoundary_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = null;
        sb.append(list, "k", "f", "t");
        assertEquals("null", sb.toS());
    }

    @Test
    public void collectionWithBoundary_elementNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add(null);
        list.add("c");
        sb.append(list, "k", "f", "t");
        assertEquals("kafnullfct", sb.toS());
    }

    @Test
    public void collectionWithBoundary_leftBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        sb.append(list, null, "f", "t");
        assertEquals("nullafbt", sb.toS());
    }

    @Test
    public void collectionWithBoundary_rightBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        sb.append(list, "k", "f", null);
        assertEquals("kafbnull", sb.toS());
    }

    @Test
    public void collectionWithBoundary_separatorNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        assertThrows(NullPointerException.class, () -> sb.append(list, "k", null, "t"));
    }

    @Test
    public void collectionWithBoundary_allNull() {
        List<String> list = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(list, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void collectionWithBoundaryAndMapper() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        Function<String, String> mapper = (x) -> x + x;
        sb.append(list, "{", ",", "}", mapper);
        assertEquals("{aa,bb}", sb.toS());
    }

    @Test
    public void collectionWithBoundaryAndMapper_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = null;
        Function<String, String> mapper = (x) -> x + x;
        sb.append(list, "{", ",", "}", mapper);
        assertEquals("null", sb.toS());
    }

    @Test
    public void collectionWithBoundaryAndMapper_leftBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        Function<String, String> mapper = (x) -> x + x;
        sb.append(list, null, ",", "}", mapper);
        assertEquals("nullaa,bb}", sb.toS());
    }

    @Test
    public void collectionWithBoundaryAndMapper_rightBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        Function<String, String> mapper = (x) -> x + x;
        sb.append(list, "{", ",", null, mapper);
        assertEquals("{aa,bbnull", sb.toS());
    }

    @Test
    public void collectionWithBoundaryAndMapper_separatorNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        Function<String, String> mapper = (x) -> x + x;
        assertThrows(NullPointerException.class, () -> sb.append(list, "{", null, "}", mapper));
    }

    @Test
    public void collectionWithBoundaryAndMapper_mapperNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        Function<String, String> mapper = null;
        assertThrows(NullPointerException.class, () -> sb.append(list, "{", ",", "}", mapper));
    }

    @Test
    public void collectionWithBoundaryAndMapper_allNull() {
        List<String> list = null;
        Function<String, String> mapper = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(list, null, null, null, mapper);
        assertEquals("null", sb.toS());
    }

}
