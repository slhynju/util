package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;

import static org.junit.jupiter.api.Assertions.*;

public class TemporalUtilTest {

    @Test
    public void toS_temporal() {
        TemporalAccessor t = LocalDate.of(1993, 2, 8);
        DateTimeFormatter f = DateTimeFormatter.ISO_LOCAL_DATE;
        String s = TemporalUtil.toS(t, f);
        assertEquals("1993-02-08", s);
    }

    @Test
    public void toS_temporal_temporalNull() {
        TemporalAccessor t = null;
        DateTimeFormatter f = DateTimeFormatter.ISO_LOCAL_DATE;
        String s = TemporalUtil.toS(t, f);
        assertEquals("null", s);
    }

    @Test
    public void toS_temporal_formatNull() {
        TemporalAccessor t = LocalDate.of(1993, 2, 8);
        DateTimeFormatter f = null;
        assertThrows(NullPointerException.class, () -> TemporalUtil.toS(t, f));
    }

    @Test
    public void toS_temporal_bothNull() {
        TemporalAccessor t = null;
        DateTimeFormatter f = null;
        assertEquals("null", TemporalUtil.toS(t, f));
    }

    @Test
    public void toDate() {
        StringBuilder sb = new StringBuilder("1993-02-08");
        LocalDate d = TemporalUtil.toDate(sb);
        LocalDate expected = LocalDate.of(1993, 2, 8);
        assertEquals(expected, d);
    }

    @Test
    public void toDate_null() {
        assertNull(TemporalUtil.toDate(null));
        assertNull(TemporalUtil.toDate(""));
        assertNull(TemporalUtil.toDate("null"));
    }

    @Test
    public void toDate_invalid() {
        StringBuilder sb = new StringBuilder("1993-ab-08");
        assertThrows(DateTimeParseException.class, () -> TemporalUtil.toDate(sb));
    }

    @Test
    public void toDateWithFormatter() {
        StringBuilder sb = new StringBuilder("19930208");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate d = TemporalUtil.toDate(sb, formatter);
        LocalDate expected = LocalDate.of(1993, 2, 8);
        assertEquals(expected, d);
    }

    @Test
    public void toDateWithFormatter_null() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        assertNull(TemporalUtil.toDate(null, formatter));
        assertNull(TemporalUtil.toDate("", formatter));
        assertNull(TemporalUtil.toDate("null", formatter));
    }

    @Test
    public void toDateWithFormatter_formatterNull() {
        StringBuilder sb = new StringBuilder("19930208");
        DateTimeFormatter formatter = null;
        assertThrows(NullPointerException.class, () -> TemporalUtil.toDate(sb, formatter));
    }

    @Test
    public void toDateWithFormatter_bothNull() {
        StringBuilder sb = null;
        DateTimeFormatter formatter = null;
        assertNull(TemporalUtil.toDate(sb, formatter));
    }

    @Test
    public void toDateTime() {
        StringBuilder sb = new StringBuilder("2007-11-04T15:24:03");
        LocalDateTime expected = LocalDateTime.of(2007, 11, 4, 15, 24, 3);
        assertEquals(expected, TemporalUtil.toDateTime(sb));
    }

    @Test
    public void toDateTime_null() {
        assertNull(TemporalUtil.toDateTime(null));
        assertNull(TemporalUtil.toDateTime(""));
        assertNull(TemporalUtil.toDateTime("null"));
    }

    @Test
    public void toDateTime_invalid() {
        assertThrows(DateTimeParseException.class, () -> TemporalUtil.toDateTime("abc"));
    }

    @Test
    public void toDateTimeWithFormatter() {
        StringBuilder sb = new StringBuilder("20171104152403");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime expected = LocalDateTime.of(2017, 11, 4, 15, 24, 3);
        assertEquals(expected, TemporalUtil.toDateTime(sb, formatter));
    }

    @Test
    public void toDateTimeWithFormatter_null() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        assertNull(TemporalUtil.toDateTime(null, formatter));
        assertNull(TemporalUtil.toDateTime("", formatter));
        assertNull(TemporalUtil.toDateTime("null", formatter));
    }

    @Test
    public void toDateTimeWithFormatter_formatterNull() {
        StringBuilder sb = new StringBuilder("20171104152403");
        DateTimeFormatter formatter = null;
        assertThrows(NullPointerException.class, () -> TemporalUtil.toDateTime(sb, formatter));
    }

    @Test
    public void toDateTimeWithFormatter_invalid() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        assertThrows(DateTimeParseException.class, () -> TemporalUtil.toDateTime("abc", formatter));
    }

    @Test
    public void toDateTimeWithFormatter_bothNull() {
        DateTimeFormatter formatter = null;
        assertNull(TemporalUtil.toDateTime(null, formatter));
        assertNull(TemporalUtil.toDateTime("", formatter));
        assertNull(TemporalUtil.toDateTime("null", formatter));
    }

    @Test
    public void toTime() {
        StringBuilder sb = new StringBuilder("15:24:03");
        LocalTime expected = LocalTime.of(15, 24, 3);
        assertEquals(expected, TemporalUtil.toTime(sb));
    }

    @Test
    public void toTime_null() {
        assertNull(TemporalUtil.toTime(null));
        assertNull(TemporalUtil.toTime(""));
        assertNull(TemporalUtil.toTime("null"));
    }

    @Test
    public void toTime_invalid() {
        assertThrows(DateTimeParseException.class, () -> TemporalUtil.toTime("abc"));
    }

    @Test
    public void toTimeWithFormatter() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
        LocalTime expected = LocalTime.of(15, 24, 3);
        assertEquals(expected, TemporalUtil.toTime("152403", formatter));
    }

    @Test
    public void toTimeWithFormatter_null() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
        assertNull(TemporalUtil.toTime(null, formatter));
        assertNull(TemporalUtil.toTime("", formatter));
        assertNull(TemporalUtil.toTime("null", formatter));
    }

    @Test
    public void toTimeWithFormatter_formatterNull() {
        DateTimeFormatter formatter = null;
        assertThrows(NullPointerException.class, () -> TemporalUtil.toTime("152403", formatter));
    }

    @Test
    public void toTimeWithFormatter_bothNull() {
        DateTimeFormatter formatter = null;
        assertNull(TemporalUtil.toTime(null, formatter));
        assertNull(TemporalUtil.toTime("", formatter));
        assertNull(TemporalUtil.toTime("null", formatter));
    }

    @Test
    public void toTimeWithFormatter_invalid() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
        assertThrows(DateTimeParseException.class, () -> TemporalUtil.toTime("abc", formatter));
    }

    // toZonedDateTime with system default Locale is Local dependent.

    @Test
    public void toZonedDateTime_null() {
        assertNull(TemporalUtil.toZonedDateTime(null));
        assertNull(TemporalUtil.toZonedDateTime(""));
        assertNull(TemporalUtil.toZonedDateTime("null"));
    }

    @Test
    public void toZonedDateTimeWithFormatter() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(zoneId);
        ZonedDateTime expected = ZonedDateTime.of(2015, 3, 5, 15, 8, 3, 0, zoneId);
        ZonedDateTime actual = TemporalUtil.toZonedDateTime("20150305150803", formatter);
        assertEquals(expected, actual);
    }

    @Test
    public void toZonedDateTimeWithFormatter_null() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(zoneId);
        assertNull(TemporalUtil.toZonedDateTime(null, formatter));
        assertNull(TemporalUtil.toZonedDateTime("", formatter));
        assertNull(TemporalUtil.toZonedDateTime("null", formatter));
    }

    @Test
    public void toZonedDateTimeWithFormatter_formatterNull() {
        DateTimeFormatter formatter = null;
        assertThrows(NullPointerException.class, () -> TemporalUtil.toZonedDateTime("20150305150803", formatter));
    }

    @Test
    public void toZonedDateTimeWithFormatter_bothNull() {
        DateTimeFormatter formatter = null;
        assertNull(TemporalUtil.toZonedDateTime(null, formatter));
        assertNull(TemporalUtil.toZonedDateTime("", formatter));
        assertNull(TemporalUtil.toZonedDateTime("null", formatter));
    }

    @Test
    public void toZonedDateTimeWithFormatter_invalid() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(zoneId);
        assertThrows(DateTimeParseException.class, () -> TemporalUtil.toZonedDateTime("abc", formatter));
    }

    @Test
    public void toInstant() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(zoneId);
        ZonedDateTime expectedDateTime = ZonedDateTime.of(2015, 3, 5, 15, 8, 3, 0, zoneId);
        Instant expected = expectedDateTime.toInstant();
        Instant actual = TemporalUtil.toInstant("2015-03-05T23:08:03Z");
        assertEquals(expected, actual);
    }

    @Test
    public void toInstant_null() {
        assertNull(TemporalUtil.toInstant(null));
        assertNull(TemporalUtil.toInstant(""));
        assertNull(TemporalUtil.toInstant("null"));
    }

}
