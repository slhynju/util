package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author slhynju
 */
public class StringUtil_splitChars_Test {

    @Test
    public void chars() {
        Set<Character> expected = new HashSet<>();
        expected.add(Character.valueOf('a'));
        expected.add(Character.valueOf('b'));
        expected.add(Character.valueOf('c'));
        assertEquals(expected, StringUtil.splitChars("abc"));
    }

    @Test
    public void chars_null() {
        Set<Character> actual = StringUtil.splitChars(null);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void chars_empty() {
        Set<Character> actual = StringUtil.splitChars("");
        assertTrue(actual.isEmpty());
    }

    @Test
    public void codePoints() {
        Set<Integer> expected = new HashSet<>();
        expected.add(Integer.valueOf('中'));
        expected.add(Integer.valueOf('文'));
        expected.add(Integer.valueOf('测'));
        expected.add(Integer.valueOf('试'));
        assertEquals(expected, StringUtil.splitCodePoints("中文测试"));
    }

    @Test
    public void codePoints_null() {
        Set<Integer> actual = StringUtil.splitCodePoints(null);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void codePoints_empty() {
        Set<Integer> actual = StringUtil.splitCodePoints("");
        assertTrue(actual.isEmpty());
    }

}

