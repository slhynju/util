package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtil_toDate_LocalDate_Test {

    @Test
    public void toDate_localDate() throws ParseException {
        LocalDate l = LocalDate.of(2012, 2, 4);
        Date d = DateUtil.toDate(l);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d2 = format.parse("2012-02-04");
        assertEquals(d2, d);
    }

    @Test
    public void toDate_localDate_DateNull() {
        assertNull(DateUtil.toDate((LocalDate) null));
    }

    @Test
    public void toDate_localDateWithZoneId() throws ParseException {
        LocalDate l = LocalDate.of(2012, 2, 4);
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        Date d = DateUtil.toDate(l, zoneId);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        Date d2 = format.parse("2012-02-04");
        assertEquals(d2, d);
    }

    @Test
    public void toDate_localDateWithZoneId_dateNull() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        Date d = DateUtil.toDate((LocalDate) null, zoneId);
        assertNull(d);
    }

    @Test
    public void toDate_localDateWithZoneId_zoneNull() {
        LocalDate l = LocalDate.of(2012, 2, 4);
        assertThrows(NullPointerException.class, () -> DateUtil.toDate(l, (ZoneId) null));
    }

    @Test
    public void toDate_localDateWithZoneId_bothNull() {
        assertNull(DateUtil.toDate((LocalDate) null, (ZoneId) null));
    }

}
