package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FreeStringBuilder_String_Test {

    @Test
    public void string() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append("abc");
        assertEquals("abc", sb.toS());
        sb.append((String) null);
        assertEquals("abcnull", sb.toS());
    }

    @Test
    public void quotedString() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted("abc");
        assertEquals("\"abc\"", sb.toS());
        sb.appendQuoted((String) null);
        assertEquals("\"abc\"null", sb.toS());
    }

    @Test
    public void charSequence() {
        FreeStringBuilder sb = new FreeStringBuilder();
        CharSequence sb2 = new FreeStringBuilder("abc");
        sb.append(sb2);
        assertEquals("abc", sb.toS());
        sb.append((CharSequence) null);
        assertEquals("abcnull", sb.toS());
    }

    @Test
    public void charSequenceWithIndex() {
        FreeStringBuilder sb = new FreeStringBuilder();
        CharSequence sb2 = new FreeStringBuilder("abcdefghi");
        sb.append(sb2, 3, 5);
        assertEquals("de", sb.toS());
    }

    @Test
    public void charSequenceWithIndex_null() {
        FreeStringBuilder sb = new FreeStringBuilder("abc");
        CharSequence sb2 = null;
        sb.append(sb2, 3, 5);
        assertEquals("abcnull", sb.toS());
    }

    @Test
    public void charSequenceWithIndex_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder();
        CharSequence sb2 = new FreeStringBuilder("abcdefghi");
        assertThrows(IndexOutOfBoundsException.class, () -> sb.append(sb2, 10, 15));
    }

    @Test
    public void quotedCharSequence() {
        FreeStringBuilder sb = new FreeStringBuilder();
        FreeStringBuilder sb2 = new FreeStringBuilder("abc");
        sb.appendQuoted(sb2);
        assertEquals("\"abc\"", sb.toS());
    }

    @Test
    public void quotedCharSequence_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        FreeStringBuilder sb2 = null;
        sb.appendQuoted(sb2);
        assertEquals("null", sb.toS());
    }

    @Test
    public void quotedCharSequenceWithIndex() {
        FreeStringBuilder sb = new FreeStringBuilder();
        FreeStringBuilder sb2 = new FreeStringBuilder("abcdefghi");
        sb.appendQuoted(sb2, 3, 6);
        assertEquals("\"def\"", sb.toS());
    }

    @Test
    public void quotedCharSequenceWithIndex_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        FreeStringBuilder sb2 = null;
        sb.appendQuoted(sb2, 3, 6);
        assertEquals("null", sb.toS());
    }

    @Test
    public void quotedCharSequenceWithIndex_invalidIndex() {
        FreeStringBuilder sb = new FreeStringBuilder();
        FreeStringBuilder sb2 = new FreeStringBuilder("abcdefghi");
        assertThrows(IndexOutOfBoundsException.class, () -> sb.appendQuoted(sb2, 10, 15));
    }

}
