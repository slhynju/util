package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FreeStringBuilder_PrimitiveArrays_Test {

    @Test
    public void booleanArray() {
        boolean[] a = new boolean[]{true, false, true};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[true,false,true]", sb.toS());
    }

    @Test
    public void booleanArray_null() {
        boolean[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("null", sb.toS());
    }

    @Test
    public void booleanArray_empty() {
        boolean[] a = new boolean[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[]", sb.toS());
    }

    @Test
    public void booleanArray_leftBoundaryNull() {
        boolean[] a = new boolean[]{true, false, true};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, ",", "]");
        assertEquals("nulltrue,false,true]", sb.toS());
    }

    @Test
    public void booleanArray_separatorNull() {
        boolean[] a = new boolean[]{true, false, true};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]"));
    }

    @Test
    public void booleanArray_rightBoundaryNull() {
        boolean[] a = new boolean[]{true, false, true};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", null);
        assertEquals("[true,false,truenull", sb.toS());
    }

    @Test
    public void booleanArray_boundaryNull_sepearatorNull() {
        boolean[] a = new boolean[]{true, false, true};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, null, null, null));
    }

    @Test
    public void booleanArray_allNull() {
        boolean[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void byteArray() {
        byte[] a = new byte[]{32, 64, 25};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[32,64,25]", sb.toS());
    }

    @Test
    public void byteArray_null() {
        byte[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("null", sb.toS());
    }

    @Test
    public void byteArray_empty() {
        byte[] a = new byte[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[]", sb.toS());
    }

    @Test
    public void byteArray_leftBoundaryNull() {
        byte[] a = new byte[]{32, 64, 25};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, ",", "]");
        assertEquals("null32,64,25]", sb.toS());
    }

    @Test
    public void byteArray_separatorNull() {
        byte[] a = new byte[]{32, 64, 25};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]"));
    }

    @Test
    public void byteArray_rightBoundaryNull() {
        byte[] a = new byte[]{32, 64, 25};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", null);
        assertEquals("[32,64,25null", sb.toS());
    }

    @Test
    public void byteArray_boundaryNull_separatorNull() {
        byte[] a = new byte[]{32, 64, 25};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, null, null, null));
    }

    @Test
    public void byteArray_allNull() {
        byte[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, null, null);
        assertEquals("null", sb.toS());
    }


    @Test
    public void charArray() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[a,b,c]", sb.toS());
    }

    @Test
    public void charArray_null() {
        char[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("null", sb.toS());
    }

    @Test
    public void charArray_empty() {
        char[] a = new char[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[]", sb.toS());
    }

    @Test
    public void charArray_leftBoundaryNull() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, ",", "]");
        assertEquals("nulla,b,c]", sb.toS());
    }

    @Test
    public void charArray_separatorNull() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]"));
    }

    @Test
    public void charArray_rightBoundaryNull() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", null);
        assertEquals("[a,b,cnull", sb.toS());
    }

    @Test
    public void charArray_boundaryNull_separatorNull() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, null, null, null));
    }

    @Test
    public void charArray_allNull() {
        char[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void quotedCharArray() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", "]");
        assertEquals("[\'a\',\'b\',\'c\']", sb.toS());
    }

    @Test
    public void quotedCharArray_null() {
        char[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", "]");
        assertEquals("null", sb.toS());
    }

    @Test
    public void quotedCharArray_empty() {
        char[] a = new char[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", "]");
        assertEquals("[]", sb.toS());
    }

    @Test
    public void quotedCharArray_leftBoundaryNull() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, null, ",", "]");
        assertEquals("null\'a\',\'b\',\'c\']", sb.toS());
    }

    @Test
    public void quotedCharArray_separatorNull() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.appendQuoted(a, "[", null, "]"));
    }

    @Test
    public void quotedCharArray_rightBoundaryNull() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, "[", ",", null);
        assertEquals("[\'a\',\'b\',\'c\'null", sb.toS());
    }

    @Test
    public void quotedCharArray_boundaryNull_separatorNull() {
        char[] a = new char[]{'a', 'b', 'c'};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.appendQuoted(a, null, null, null));
    }

    @Test
    public void quotedCharArray_allNull() {
        char[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendQuoted(a, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void floatArray() {
        float[] a = new float[]{1.1567f, 2.3496f, 3.5722f};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, "[", ",", "]", f);
        assertEquals("[1.16,2.35,3.57]", sb.toS());
    }

    @Test
    public void floatArray_null() {
        float[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, "[", ",", "]", f);
        assertEquals("null", sb.toS());
    }

    @Test
    public void floatArray_empty() {
        float[] a = new float[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, "[", ",", "]", f);
        assertEquals("[]", sb.toS());
    }

    @Test
    public void floatArray_leftBoundaryNull() {
        float[] a = new float[]{1.1567f, 2.3496f, 3.5722f};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, null, ",", "]", f);
        assertEquals("null1.16,2.35,3.57]", sb.toS());
    }

    @Test
    public void floatArray_separatorNull() {
        float[] a = new float[]{1.1567f, 2.3496f, 3.5722f};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]", f));
    }

    @Test
    public void floatArray_rightBoundaryNull() {
        float[] a = new float[]{1.1567f, 2.3496f, 3.5722f};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, "[", ",", null, f);
        assertEquals("[1.16,2.35,3.57null", sb.toS());
    }

    @Test
    public void floatArray_formatNull() {
        float[] a = new float[]{1.1567f, 2.3496f, 3.5722f};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = null;
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", ",", "]", f));
    }

    @Test
    public void floatArray_allNull() {
        float[] a = null;
        NumberFormat f = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        assertEquals("null", sb.append(a, null, null, null, f).toS());
    }

    @Test
    public void doubleArray() {
        double[] a = new double[]{1.1567, 2.3496, 3.5722};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, "[", ",", "]", f);
        assertEquals("[1.16,2.35,3.57]", sb.toS());
    }

    @Test
    public void doubleArray_null() {
        double[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, "[", ",", "]", f);
        assertEquals("null", sb.toS());
    }

    @Test
    public void doubleArray_empty() {
        double[] a = new double[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, "[", ",", "]", f);
        assertEquals("[]", sb.toS());
    }

    @Test
    public void doubleArray_leftBoundaryNull() {
        double[] a = new double[]{1.1567, 2.3496, 3.5722};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, null, ",", "]", f);
        assertEquals("null1.16,2.35,3.57]", sb.toS());
    }

    @Test
    public void doubleArray_separatorNull() {
        double[] a = new double[]{1.1567, 2.3496, 3.5722};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]", f));
    }

    @Test
    public void doubleArray_rightBoundaryNull() {
        double[] a = new double[]{1.1567, 2.3496, 3.5722};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = new DecimalFormat("0.00");
        sb.append(a, "[", ",", null, f);
        assertEquals("[1.16,2.35,3.57null", sb.toS());
    }

    @Test
    public void doubleArray_formatNull() {
        double[] a = new double[]{1.1567, 2.3496, 3.5722};
        FreeStringBuilder sb = new FreeStringBuilder();
        NumberFormat f = null;
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", ",", "]", f));
    }

    @Test
    public void doubleArray_allNull() {
        double[] a = null;
        NumberFormat f = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        assertEquals("null", sb.append(a, null, null, null, f).toS());
    }

    @Test
    public void intArray() {
        int[] a = new int[]{1, 2, 3};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[1,2,3]", sb.toS());
    }

    @Test
    public void intArray_null() {
        int[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("null", sb.toS());
    }

    @Test
    public void intArray_empty() {
        int[] a = new int[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[]", sb.toS());
    }

    @Test
    public void intArray_leftBoundryNull() {
        int[] a = new int[]{1, 2, 3};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, ",", "]");
        assertEquals("null1,2,3]", sb.toS());
    }

    @Test
    public void intArray_separatorNull() {
        int[] a = new int[]{1, 2, 3};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]"));
    }

    @Test
    public void intArray_rightBoundryNull() {
        int[] a = new int[]{1, 2, 3};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", null);
        assertEquals("[1,2,3null", sb.toS());
    }

    @Test
    public void intArray_allNull() {
        int[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void longArray() {
        long[] a = new long[]{1382, 2498, 3583};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[1382,2498,3583]", sb.toS());
    }

    @Test
    public void longArray_null() {
        long[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("null", sb.toS());
    }

    @Test
    public void longArray_empty() {
        long[] a = new long[]{};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]");
        assertEquals("[]", sb.toS());
    }

    @Test
    public void longArray_leftBoundryNull() {
        long[] a = new long[]{1382, 2498, 3583};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, ",", "]");
        assertEquals("null1382,2498,3583]", sb.toS());
    }

    @Test
    public void longArray_separatorNull() {
        long[] a = new long[]{1382, 2498, 3583};
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]"));
    }

    @Test
    public void longArray_rightBoundryNull() {
        long[] a = new long[]{1382, 2498, 3583};
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", null);
        assertEquals("[1382,2498,3583null", sb.toS());
    }

    @Test
    public void longArray_allNull() {
        long[] a = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void longArrayWithFormat() {
        long[] a = new long[]{1382, 2498, 3583};
        NumberFormat f = new DecimalFormat("#,##0");
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]", f);
        assertEquals("[1,382,2,498,3,583]", sb.toS());
    }

    @Test
    public void longArrayWithFormat_null() {
        long[] a = null;
        NumberFormat f = new DecimalFormat("#,##0");
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]", f);
        assertEquals("null", sb.toS());
    }

    @Test
    public void longArrayWithFormat_empty() {
        long[] a = new long[]{};
        NumberFormat f = new DecimalFormat("#,##0");
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", "]", f);
        assertEquals("[]", sb.toS());
    }

    @Test
    public void longArrayWithFormat_leftBoundryNull() {
        long[] a = new long[]{1382, 2498, 3583};
        NumberFormat f = new DecimalFormat("#,##0");
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, ",", "]", f);
        assertEquals("null1,382,2,498,3,583]", sb.toS());
    }

    @Test
    public void longArrayWithFormat_separatorNull() {
        long[] a = new long[]{1382, 2498, 3583};
        NumberFormat f = new DecimalFormat("#,##0");
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", null, "]", f));
    }

    @Test
    public void longArrayWithFormat_rightBoundryNull() {
        long[] a = new long[]{1382, 2498, 3583};
        NumberFormat f = new DecimalFormat("#,##0");
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, "[", ",", null, f);
        assertEquals("[1,382,2,498,3,583null", sb.toS());
    }

    @Test
    public void longArrayWithFormat_formatNull() {
        long[] a = new long[]{1382, 2498, 3583};
        NumberFormat f = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        assertThrows(NullPointerException.class, () -> sb.append(a, "[", ",", "]", f));
    }

    @Test
    public void longArrayWithFormat_allNull() {
        long[] a = null;
        NumberFormat f = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(a, null, null, null, f);
        assertEquals("null", sb.toS());
    }

}
