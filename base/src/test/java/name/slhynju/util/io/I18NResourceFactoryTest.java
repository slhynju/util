package name.slhynju.util.io;

import name.slhynju.ApplicationException;
import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

public class I18NResourceFactoryTest {

    @SuppressWarnings("unused")
    @Test
    public void getInstance_notFound() {
        Locale locale = Locale.CHINA;
        assertThrows(ApplicationException.class, () -> I18NResourceFactory.getInstance("not_exist", locale));
    }

    @Test
    public void getInstance() {
        Locale locale = Locale.CHINA;
        I18NResource resource = I18NResourceFactory.getInstance("test", locale);
        assertNotNull(resource);
    }

    @Test
    public void priority() {
        Locale locale = Locale.CHINA;
        I18NResource resource = I18NResourceFactory.getInstance("test2", locale);
        String text = resource.get("test2.key");
        assertEquals("test2.value", text);
    }

    @Test
    public void priority2() {
        Locale locale = Locale.CHINA;
        I18NResource resource = I18NResourceFactory.getInstance("test3", locale);
        String text = resource.get("test3.key");
        assertEquals("test3.value", text);
    }

    @Test
    public void setDefaultLocale() {
        Locale locale = Locale.CHINA;
        I18NResourceFactory.setDefaultLocale(locale);
        I18NResource resource = I18NResourceFactory.getDefaultInstance("test");
        assertNotNull(resource);
    }

    @Test
    public void getDefaultLocale() {
        Locale locale = Locale.CHINA;
        I18NResourceFactory.setDefaultLocale(locale);
        assertEquals(locale, I18NResourceFactory.getDefaultLocale());
    }


}
