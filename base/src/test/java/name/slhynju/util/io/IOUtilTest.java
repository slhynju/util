package name.slhynju.util.io;

import name.slhynju.ApplicationException;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("static-method")
public class IOUtilTest {

    @Test
    public void openResourceAsStream() throws IOException {
        InputStream in = IOUtil.openResourceAsStream("test_locale_zh.properties");
        assertNotNull(in);
        in.close();
    }

    @Test
    public void openResourceAsStream_notFound() {
        assertThrows(ApplicationException.class, () -> IOUtil.openResourceAsStream("file_not_exists"));
    }

    @Test
    public void getResourceAsStream() throws IOException {
        InputStream in = IOUtil.getResourceAsStream("test_locale_zh.properties");
        assertNotNull(in);
        in.close();
    }

    public void getResourceAsStream_notFound() {
        InputStream in = IOUtil.getResourceAsStream("file_not_exists");
        assertNull(in);
    }

    @Test
    public void closeReader_null() {
        Reader reader = null;
        Logger log = mock(Logger.class);
        IOUtil.close(reader, log);
    }

    @Test
    public void closeReader() throws IOException {
        Reader reader = mock(Reader.class);
        Logger log = mock(Logger.class);
        IOUtil.close(reader, log);
        verify(reader).close();
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void closeReader_whenException() throws IOException {
        Reader reader = mock(Reader.class);
        Logger log = mock(Logger.class);
        doThrow(IOException.class).when(reader).close();
        when(log.isWarnEnabled()).thenReturn(Boolean.TRUE);
        IOUtil.close(reader, log);
        verify(reader).close();
        verify(log).isWarnEnabled();
        verify(log).warn(anyString(), any(IOException.class));
    }

    @Test
    public void closeReader_whenException_warnDisabled() throws IOException {
        Reader reader = mock(Reader.class);
        Logger log = mock(Logger.class);
        doThrow(IOException.class).when(reader).close();
        when(log.isWarnEnabled()).thenReturn(Boolean.FALSE);
        IOUtil.close(reader, log);
        verify(reader).close();
        verify(log).isWarnEnabled();
        verify(log, never()).warn(anyString(), any(IOException.class));
    }

    @Test
    public void closeWriter_null() {
        Writer writer = null;
        Logger log = mock(Logger.class);
        IOUtil.close(writer, log);
    }

    @Test
    public void closeWriter() throws IOException {
        Writer writer = mock(Writer.class);
        Logger log = mock(Logger.class);
        IOUtil.close(writer, log);
        verify(writer).close();
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void closeWriter_exception() throws IOException {
        Writer writer = mock(Writer.class);
        Logger log = mock(Logger.class);
        doThrow(IOException.class).when(writer).close();
        when(log.isWarnEnabled()).thenReturn(Boolean.TRUE);
        IOUtil.close(writer, log);
        verify(writer).close();
        verify(log).isWarnEnabled();
        verify(log).warn(anyString(), any(IOException.class));
    }

    @Test
    public void closeInputStream_null() {
        InputStream in = null;
        Logger log = mock(Logger.class);
        IOUtil.close(in, log);
    }

    @Test
    public void closeInputStream() throws IOException {
        InputStream in = mock(InputStream.class);
        Logger log = mock(Logger.class);
        IOUtil.close(in, log);
        verify(in).close();
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void closeInputStream_whenException() throws IOException {
        InputStream in = mock(InputStream.class);
        Logger log = mock(Logger.class);
        doThrow(IOException.class).when(in).close();
        when(log.isWarnEnabled()).thenReturn(Boolean.TRUE);
        IOUtil.close(in, log);
        verify(in).close();
        verify(log).isWarnEnabled();
        verify(log).warn(anyString(), any(IOException.class));
    }

    @Test
    public void readSmallPropertiesFile_null() {
        InputStream in = null;
        IOUtil.readSmallPropertiesFile(in);
    }

    @Test
    public void readSmallPropertiesFile() {
        InputStream in = IOUtil.openResourceAsStream("test_locale_zh.properties");
        Map<String, String> map = IOUtil.readSmallPropertiesFile(in);
        assertEquals("expected.value", map.get("expected.key"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void readSmallPropertiesFile_whenException() throws IOException {
        InputStream in = mock(InputStream.class);
        when(in.read(any(byte[].class), anyInt(), anyInt())).thenThrow(IOException.class);
        assertThrows(ApplicationException.class, () -> IOUtil.readSmallPropertiesFile(in));
    }

    @Test
    public void buildProperties() {
        List<String> list = new ArrayList<>();
        list.add("");
        list.add("         ");
        list.add("   # comment 1  ");
        list.add("   key1 =   value1");
        list.add("  key 2 =");
        list.add("     ");
        list.add(" # comment 2");
        Map<String, String> map = IOUtil.buildProperties(list);
        assertEquals(2, map.size());
        assertEquals("value1", map.get("key1"));
        assertEquals("", map.get("key 2"));
    }

    @Test
    public void readAllBytes_null() {
        InputStream in = null;
        Logger log = mock(Logger.class);
        byte[] data = IOUtil.readAllBytes(in, 3, log);
        assertNotNull(data);
        assertEquals(0, data.length);
    }

    @Test
    public void readAllBytes() {
        InputStream in = IOUtil.openResourceAsStream("test_locale_zh.properties");
        Logger log = mock(Logger.class);
        byte[] data = IOUtil.readAllBytes(in, 3, log);
        assertNotNull(data);
        assertTrue(data.length > 0);
    }

    @Test
    public void readAllBytes_multipleRead() throws IOException {
        InputStream in = mock(InputStream.class);
        Logger log = mock(Logger.class);
        when(in.read(any(byte[].class), anyInt(), anyInt())).thenReturn(Integer.valueOf(200))
                .thenReturn(Integer.valueOf(50)).thenReturn(Integer.valueOf(-1));
        byte[] data = IOUtil.readAllBytes(in, 3, log);
        assertNotNull(data);
        assertEquals(250, data.length);
        verify(in).close();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void readAllBytes_whenException() throws IOException {
        InputStream in = mock(InputStream.class);
        Logger log = mock(Logger.class);
        when(in.read(any(byte[].class), anyInt(), anyInt())).thenReturn(Integer.valueOf(200))
                .thenReturn(Integer.valueOf(50)).thenThrow(IOException.class);
        assertThrows(ApplicationException.class, () -> IOUtil.readAllBytes(in, 3, log));
    }

}
