package name.slhynju.util.io;

import name.slhynju.util.collection.CollectionUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.annotation.ElementType;
import java.util.Locale;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("static-method")
public class I18NResourceTest {

    private static I18NResource RESOURCE;

    @BeforeAll
    public static void init() {
        RESOURCE = I18NResourceFactory.getInstance("test", Locale.CHINA);
    }

    @Test
    public void get() {
        String value = RESOURCE.get("expected.key");
        assertEquals("expected.value", value);
    }

    @Test
    public void get_notFound() {
        String value = RESOURCE.get("not.expected.key");
        assertEquals("not.expected.key", value);
    }

    @Test
    public void format() {
        String value = RESOURCE.format("greater.than", 10, 5);
        assertEquals("10 is greater than 5.", value);
    }

    @Test
    public void getArray() {
        String value = RESOURCE.get("expected", "key");
        assertEquals("expected.value", value);
    }

    @Test
    public void getEnum() {
        String value = RESOURCE.get(ElementType.METHOD);
        assertEquals("method annotation", value);
    }

    @Test
    public void testEquals() {
        Map<String, String> text1 = CollectionUtil.toMap("k1", "v1", "k2", "v2");
        Map<String, String> text2 = CollectionUtil.toMap("k1", "v1", "k2", "v2");
        I18NResource r1 = new I18NResource(text1);
        I18NResource r2 = new I18NResource(text2);
        assertEquals(r1, r2);
        assertFalse(r1.equals(null));
        Map<String, String> text3 = CollectionUtil.toMap("k1", "v1", "k2", "v2", "k3", "v3");
        I18NResource r3 = new I18NResource((text3));
        assertNotEquals(r1, r3);
    }

    @Test
    public void testHashCode() {
        Map<String, String> text1 = CollectionUtil.toMap("k1", "v1", "k2", "v2");
        I18NResource r1 = new I18NResource(text1);
        Map<String, String> text2 = CollectionUtil.toMap("k1", "v1", "k2", "v2");
        I18NResource r2 = new I18NResource(text2);
        assertEquals(r1.hashCode(), r2.hashCode());
        Map<String, String> text3 = CollectionUtil.toMap("k1", "v1", "k2", "v2", "k3", "v3");
        I18NResource r3 = new I18NResource((text3));
        assertNotEquals(r1.hashCode(), r3.hashCode());
    }

}
