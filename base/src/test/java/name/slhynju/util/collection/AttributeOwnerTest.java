package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AttributeOwnerTest {

    @Test
    public void getWithClass() {
        AttributeOwner o = new SampleAttributeOwner();
        o.setAttribute("k1", Integer.valueOf(5));
        Integer n = o.getAttribute("k1", Integer.class);
        assertEquals(5, n.intValue());
        o.setAttribute("k1", null);
        n = o.getAttribute("k1", Integer.class);
        assertNull(n);
    }

    @Test
    public void getWithDefaultValue() {
        AttributeOwner o = new SampleAttributeOwner();
        o.setAttribute("k1", Integer.valueOf(5));
        Integer defaultValue = Integer.valueOf(10);
        Integer n = (Integer) o.getAttribute("k1", defaultValue);
        assertEquals(5, n.intValue());
        o.setAttribute("k1", null);
        n = (Integer) o.getAttribute("k1", defaultValue);
        assertEquals(10, n.intValue());
    }

    @Test
    public void getWithClassAndDefaultValue() {
        AttributeOwner o = new SampleAttributeOwner();
        o.setAttribute("k1", Integer.valueOf(5));
        Integer defaultValue = Integer.valueOf(10);
        Integer n = o.getAttribute("k1", Integer.class, defaultValue);
        assertEquals(5, n.intValue());
        o.setAttribute("k1", null);
        n = o.getAttribute("k1", Integer.class, defaultValue);
        assertEquals(10, n.intValue());
    }

    @Test
    public void booleanAttribute() {
        AttributeOwner o = new SampleAttributeOwner();
        o.setBooleanAttribute("k1", true);
        boolean v = o.getBooleanAttribute("k1", false);
        assertTrue(v);
        v = o.getBooleanAttribute("k2", true);
        assertTrue(v);
        v = o.getBooleanAttribute("k3", false);
        assertFalse(v);
    }

    @Test
    public void intAttribute() {
        AttributeOwner o = new SampleAttributeOwner();
        o.setIntAttribute("k1", 7);
        int v = o.getIntAttribute("k1", 10);
        assertEquals(7, v);
        v = o.getIntAttribute("k2", 10);
        assertEquals(10, v);
    }

    @Test
    public void longAttribute() {
        AttributeOwner o = new SampleAttributeOwner();
        o.setLongAttribute("k1", 7L);
        long v = o.getLongAttribute("k1", 10L);
        assertEquals(7L, v);
        v = o.getLongAttribute("k2", 10L);
        assertEquals(10L, v);
    }

    @Test
    public void doubleAttribute() {
        AttributeOwner o = new SampleAttributeOwner();
        o.setDoubleAttribute("k1", 3.5);
        double v = o.getDoubleAttribute("k1", 4.9);
        assertEquals(3.5, v);
        v = o.getDoubleAttribute("k2", 4.9);
        assertEquals(4.9, v);
    }

    @Test
    public void stringAttribute() {
        AttributeOwner o = new SampleAttributeOwner();
        o.setStringAttribute("k1", "abc");
        String v = o.getStringAttribute("k1");
        assertEquals("abc", v);
        v = o.getStringAttribute("k2");
        assertNull(v);
        v = o.getStringAttribute("k1", "def");
        assertEquals("abc", v);
        v = o.getStringAttribute("k2", "def");
        assertEquals("def", v);
    }

}
