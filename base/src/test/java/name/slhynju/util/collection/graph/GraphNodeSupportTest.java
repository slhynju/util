package name.slhynju.util.collection.graph;

import name.slhynju.util.HashCodeBuilder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"static-method"})
public class GraphNodeSupportTest {

    @Test
    public void constructor() {
        GraphNodeSupport n = new GraphNodeSupport("abc");
        assertEquals("abc", n.getName());
    }

    @Test
    public void name() {
        GraphNodeSupport n = new GraphNodeSupport("abc");
        n.setName("def");
        assertEquals("def", n.getName());
    }

    @Test
    public void attribute() {
        GraphNodeSupport n = new GraphNodeSupport("A");
        n.setAttribute("visited", Boolean.TRUE);
        assertEquals(Boolean.TRUE, n.getAttribute("visited"));
        assertTrue(n.containsAttribute("visited"));
        assertFalse(n.containsAttribute("no_such_key"));
        n.removeAttribute("visited");
        assertFalse(n.containsAttribute("visited"));
    }

    @Test
    public void attributeDefaultValue() {
        GraphNodeSupport n = new GraphNodeSupport("A");
        assertEquals(Boolean.TRUE, n.getAttribute("visited", Boolean.TRUE));
    }

    @Test
    public void attributeDefaultValue2() {
        GraphNodeSupport n = new GraphNodeSupport("A");
        n.setAttribute("visited", Boolean.TRUE);
        assertEquals(Boolean.TRUE, n.getAttribute("visited", Boolean.FALSE));
    }

    @Test
    public void testEquals() {
        GraphNodeSupport n = new GraphNodeSupport("a");
        GraphNodeSupport n2 = new GraphNodeSupport("a");
        assertTrue(n.equals(n2));
    }

    @Test
    public void notEquals() {
        GraphNodeSupport n = new GraphNodeSupport("a");
        assertFalse(n.equals(Boolean.TRUE));
    }

    @Test
    public void testToString() {
        GraphNodeSupport n = new GraphNodeSupport("a");
        assertEquals("a", n.toString());
    }

    @Test
    public void testHashCode() {
        GraphNodeSupport n = new GraphNodeSupport("a");
        int hash = new HashCodeBuilder().append("a").toValue();
        assertEquals(hash, n.hashCode());
    }

}
