package name.slhynju.util.collection.graph.undirected;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"rawtypes", "unchecked", "static-method"})
public class UndirectedGraphTest {

    @Test
    public void test() {
        UndirectedGraph g = new UndirectedGraphSupport();
        Function<String, UndirectedGraphNode> creator = UndirectedGraphNodeSupport::new;
        BiFunction<UndirectedGraphNode, UndirectedGraphNode, UndirectedGraphEdge> edgeCreator = (a, b) -> new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphNode nodeA = (UndirectedGraphNode) g.addNodeIfAbsent("a", creator);
        UndirectedGraphNode nodeB = (UndirectedGraphNode) g.addNodeIfAbsent("b", creator);
        UndirectedGraphNode nodeC = (UndirectedGraphNode) g.addNodeIfAbsent("c", creator);
        UndirectedGraphNode nodeD = (UndirectedGraphNode) g.addNodeIfAbsent("d", creator);
        g.linkIfAbsent(nodeA, nodeB, edgeCreator);
        UndirectedGraphEdge edgeBC = g.linkIfAbsent(nodeB, nodeC, edgeCreator);
        UndirectedGraphEdge edgeBC2 = g.linkIfAbsent(nodeC, nodeB, edgeCreator);
        assertTrue(edgeBC == edgeBC2);
        g.linkIfAbsent(nodeB, nodeD, edgeCreator);
        UndirectedGraphEdge edgeCC = g.linkIfAbsent(nodeC, nodeC, edgeCreator);
        g.linkIfAbsent(nodeC, nodeD, edgeCreator);

        UndirectedGraphNode actualNodeC = (UndirectedGraphNode) g.getNode("c");
        assertTrue(nodeC == actualNodeC);
        assertEquals(3, actualNodeC.getDegree());

        List<UndirectedGraphNode> cSuccessors = actualNodeC.getAdjacentNodes();
        assertTrue(cSuccessors.contains(actualNodeC));
        assertTrue(cSuccessors.contains(nodeD));
        assertTrue(cSuccessors.contains(nodeB));
        assertFalse(cSuccessors.contains(nodeA));

        UndirectedGraphNodeSupport nodeE = new UndirectedGraphNodeSupport("e");
        g.addNode(nodeE);

        UndirectedGraphEdge edgeBE = new UndirectedGraphEdgeSupport(nodeB, nodeE);
        nodeB.addEdge(edgeBE);
        nodeE.addEdge(edgeBE);

        assertEquals(4, nodeB.getDegree());

        List<UndirectedGraphNode> beNodes = edgeBE.getNodes();
        assertEquals(2, beNodes.size());
        assertTrue(beNodes.contains(nodeB));
        assertTrue(beNodes.contains(nodeE));
        assertTrue(edgeBE.containsEnds(nodeB, nodeE));
        assertTrue(edgeBE.containsEnds(nodeE, nodeB));

        nodeB.removeEdge(edgeBE);
        nodeE.removeEdge(edgeBE);

        assertEquals(3, nodeB.getDegree());

        UndirectedGraphEdge edgeBE2 = new UndirectedGraphEdgeSupport(nodeE, nodeB);
        assertTrue(edgeBE.equals(edgeBE2));
        assertFalse(edgeBE.equals(edgeBC));
        assertFalse(edgeBE.equals(Boolean.TRUE));
        assertEquals(edgeBE.hashCode(), edgeBE2.hashCode());

        UndirectedGraphEdge edgeCC2 = new UndirectedGraphEdgeSupport(nodeC, nodeC);
        assertEquals(edgeCC.hashCode(), edgeCC2.hashCode());

        assertEquals("c -- c", edgeCC2.toString());
    }

    @Test
    public void adjacentEnd() {
        UndirectedGraph g = new UndirectedGraphSupport();
        Function<String, UndirectedGraphNode> creator = UndirectedGraphNodeSupport::new;
        BiFunction<UndirectedGraphNode, UndirectedGraphNode, UndirectedGraphEdge> edgeCreator = (a, b) -> new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphNode nodeA = (UndirectedGraphNode) g.addNodeIfAbsent("a", creator);
        UndirectedGraphNode nodeB = (UndirectedGraphNode) g.addNodeIfAbsent("b", creator);
        UndirectedGraphEdge edgeAB = g.linkIfAbsent(nodeA, nodeB, edgeCreator);
        assertTrue(nodeB == edgeAB.getOtherEnd(nodeA));
        assertTrue(nodeA == edgeAB.getOtherEnd(nodeB));
    }

    @Test
    public void adjacentEnd2() {
        UndirectedGraph g = new UndirectedGraphSupport();
        Function<String, UndirectedGraphNode> creator = UndirectedGraphNodeSupport::new;
        BiFunction<UndirectedGraphNode, UndirectedGraphNode, UndirectedGraphEdge> edgeCreator = (a, b) -> new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphNode nodeA = (UndirectedGraphNode) g.addNodeIfAbsent("a", creator);
        UndirectedGraphNode nodeB = (UndirectedGraphNode) g.addNodeIfAbsent("b", creator);
        UndirectedGraphNode nodeC = (UndirectedGraphNode) g.addNodeIfAbsent("c", creator);
        UndirectedGraphEdge edgeAB = g.linkIfAbsent(nodeA, nodeB, edgeCreator);
        assertThrows(IllegalArgumentException.class, () -> edgeAB.getOtherEnd(nodeC));
    }

    @Test
    public void notEquals() {
        UndirectedGraphNode nodeA = new UndirectedGraphNodeSupport("a");
        assertFalse(nodeA.equals(Boolean.TRUE));
    }

    @Test
    public void removeNode() {
        UndirectedGraph g = new UndirectedGraphSupport();
        Function<String, UndirectedGraphNode> creator = UndirectedGraphNodeSupport::new;
        BiFunction<UndirectedGraphNode, UndirectedGraphNode, UndirectedGraphEdge> edgeCreator = (a, b) -> new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphNode nodeA = (UndirectedGraphNode) g.addNodeIfAbsent("a", creator);
        UndirectedGraphNode nodeB = (UndirectedGraphNode) g.addNodeIfAbsent("b", creator);
        UndirectedGraphNode nodeC = (UndirectedGraphNode) g.addNodeIfAbsent("c", creator);
        g.linkIfAbsent(nodeA, nodeB, edgeCreator);
        g.linkIfAbsent(nodeA, nodeC, edgeCreator);
        assertEquals(2, nodeA.getDegree());
        g.removeNode(nodeB);
        assertEquals(1, nodeA.getDegree());

        nodeA.removeEdge(null);
        g.removeNode(null);
        g.removeEdge(null);
    }

    @Test
    public void removeNode2() {
        UndirectedGraph g = new UndirectedGraphSupport();
        Function<String, UndirectedGraphNode> creator = UndirectedGraphNodeSupport::new;
        BiFunction<UndirectedGraphNode, UndirectedGraphNode, UndirectedGraphEdge> edgeCreator = (a, b) -> new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphNode nodeA = (UndirectedGraphNode) g.addNodeIfAbsent("a", creator);
        g.linkIfAbsent(nodeA, nodeA, edgeCreator);
        assertEquals(1, g.getEdges().size());
        g.removeNode(nodeA);
        assertTrue(g.getNodes().isEmpty());
    }
}
