package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_isEmpty_Test {

    @Test
    public void isEmptyBooleanArray() {
        boolean[] a = null;
        assertTrue(CollectionUtil.isEmpty(a));
        a = new boolean[]{};
        assertTrue(CollectionUtil.isEmpty(a));
        a = new boolean[]{true};
        assertFalse(CollectionUtil.isEmpty(a));
    }

    @Test
    public void notEmptyBooleanArray() {
        boolean[] a = null;
        assertFalse(CollectionUtil.notEmpty(a));
        a = new boolean[]{};
        assertFalse(CollectionUtil.notEmpty(a));
        a = new boolean[]{true};
        assertTrue(CollectionUtil.notEmpty(a));
    }

    @Test
    public void isEmptyByteArray() {
        byte[] a = null;
        assertTrue(CollectionUtil.isEmpty(a));
        a = new byte[]{};
        assertTrue(CollectionUtil.isEmpty(a));
        a = new byte[]{5};
        assertFalse(CollectionUtil.isEmpty(a));
    }

    @Test
    public void notEmptyByteArray() {
        byte[] a = null;
        assertFalse(CollectionUtil.notEmpty(a));
        a = new byte[]{};
        assertFalse(CollectionUtil.notEmpty(a));
        a = new byte[]{5};
        assertTrue(CollectionUtil.notEmpty(a));
    }

    @Test
    public void isEmptyCharArray() {
        char[] a = null;
        assertTrue(CollectionUtil.isEmpty(a));
        a = new char[]{};
        assertTrue(CollectionUtil.isEmpty(a));
        a = new char[]{'c'};
        assertFalse(CollectionUtil.isEmpty(a));
    }

    @Test
    public void notEmptyCharArray() {
        char[] a = null;
        assertFalse(CollectionUtil.notEmpty(a));
        a = new char[]{};
        assertFalse(CollectionUtil.notEmpty(a));
        a = new char[]{'c'};
        assertTrue(CollectionUtil.notEmpty(a));
    }

    @Test
    public void isEmptyIntArray() {
        int[] a = null;
        assertTrue(CollectionUtil.isEmpty(a));
        a = new int[]{};
        assertTrue(CollectionUtil.isEmpty(a));
        a = new int[]{5};
        assertFalse(CollectionUtil.isEmpty(a));
    }

    @Test
    public void notEmptyIntArray() {
        int[] a = null;
        assertFalse(CollectionUtil.notEmpty(a));
        a = new int[]{};
        assertFalse(CollectionUtil.notEmpty(a));
        a = new int[]{5};
        assertTrue(CollectionUtil.notEmpty(a));
    }

    @Test
    public void isEmptyLongArray() {
        long[] a = null;
        assertTrue(CollectionUtil.isEmpty(a));
        a = new long[]{};
        assertTrue(CollectionUtil.isEmpty(a));
        a = new long[]{5L};
        assertFalse(CollectionUtil.isEmpty(a));
    }

    @Test
    public void notEmptyLongArray() {
        long[] a = null;
        assertFalse(CollectionUtil.notEmpty(a));
        a = new long[]{};
        assertFalse(CollectionUtil.notEmpty(a));
        a = new long[]{5L};
        assertTrue(CollectionUtil.notEmpty(a));
    }

    @Test
    public void isEmptyFloatArray() {
        float[] a = null;
        assertTrue(CollectionUtil.isEmpty(a));
        a = new float[]{};
        assertTrue(CollectionUtil.isEmpty(a));
        a = new float[]{4.78f};
        assertFalse(CollectionUtil.isEmpty(a));
    }

    @Test
    public void notEmptyFloatArray() {
        float[] a = null;
        assertFalse(CollectionUtil.notEmpty(a));
        a = new float[]{};
        assertFalse(CollectionUtil.notEmpty(a));
        a = new float[]{4.78f};
        assertTrue(CollectionUtil.notEmpty(a));
    }

    @Test
    public void isEmptyDoubleArray() {
        double[] a = null;
        assertTrue(CollectionUtil.isEmpty(a));
        a = new double[]{};
        assertTrue(CollectionUtil.isEmpty(a));
        a = new double[]{3.4};
        assertFalse(CollectionUtil.isEmpty(a));
    }

    @Test
    public void notEmptyDoubleArray() {
        double[] a = null;
        assertFalse(CollectionUtil.notEmpty(a));
        a = new double[]{};
        assertFalse(CollectionUtil.notEmpty(a));
        a = new double[]{3.4};
        assertTrue(CollectionUtil.notEmpty(a));
    }


    @Test
    public void isEmptyCollection() {
        assertTrue(CollectionUtil.isEmpty((Collection) null));
        Collection c = new ArrayList();
        assertTrue(CollectionUtil.isEmpty(c));
        c = new ArrayList();
        c.add("abc ");
        assertFalse(CollectionUtil.isEmpty(c));
    }

    @Test
    public void notEmptyCollection() {
        assertFalse(CollectionUtil.notEmpty((Collection) null));
        Collection c = new ArrayList();
        assertFalse(CollectionUtil.notEmpty(c));
        c = new ArrayList();
        c.add("abc ");
        assertTrue(CollectionUtil.notEmpty(c));
    }

    @Test
    public void isEmptyMap() {
        assertTrue(CollectionUtil.isEmpty((Map) null));
        Map c = new HashMap();
        assertTrue(CollectionUtil.isEmpty(c));
        c = new HashMap();
        c.put("abc", "def");
        assertFalse(CollectionUtil.isEmpty(c));
    }

    @Test
    public void notEmptyMap() {
        assertFalse(CollectionUtil.notEmpty((Map) null));
        Map c = new HashMap();
        assertFalse(CollectionUtil.notEmpty(c));
        c = new HashMap();
        c.put("abc", "def");
        assertTrue(CollectionUtil.notEmpty(c));
    }

    @Test
    public void isEmptyObjectArray() {
        String[] a = null;
        assertTrue(CollectionUtil.isEmpty(a));
        a = new String[]{};
        assertTrue(CollectionUtil.isEmpty(a));
        a = new String[]{"abc"};
        assertFalse(CollectionUtil.isEmpty(a));
    }

    @Test
    public void notEmptyObjectArray() {
        String[] a = null;
        assertFalse(CollectionUtil.notEmpty(a));
        a = new String[]{};
        assertFalse(CollectionUtil.notEmpty(a));
        a = new String[]{"abc"};
        assertTrue(CollectionUtil.notEmpty(a));
    }

    @Test
    public void calculateHashCapacity() {
        assertEquals(16, CollectionUtil.calculateHashCapacity(5));
        assertEquals(16, CollectionUtil.calculateHashCapacity(11));
        assertEquals(17, CollectionUtil.calculateHashCapacity(12));
    }

    @Test
    public void countEnum() {
        assertEquals(6, CollectionUtil.countEnum(Thread.State.class));
    }

    @Test
    public void countEnum_null() {
        assertThrows(NullPointerException.class, () -> CollectionUtil.countEnum(null));
    }
}
