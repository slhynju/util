package name.slhynju.util.collection.graph;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"rawtypes", "unchecked", "static-method"})
public class GraphSupportTest {

    @Test
    public void getNode() {
        GraphSupport<GraphNode, GraphEdge> g = new GraphSupport();
        GraphNode nodeA = g.addNodeIfAbsent("a", x -> new GraphNodeSupport(x));
        GraphNode actualNodeA = g.getNode("a");
        assertSame(nodeA, actualNodeA);
        GraphNode nodeB = g.getNode("b");
        assertNull(nodeB);
    }

    @Test
    public void containsNode() {
        GraphSupport<GraphNode, GraphEdge> g = new GraphSupport();
        g.addNodeIfAbsent("a", x -> new GraphNodeSupport(x));
        assertTrue(g.containsNode("a"));
        assertFalse(g.containsNode("b"));
    }

    @Test
    public void getNodes() {
        GraphSupport<GraphNode, GraphEdge> g = new GraphSupport();
        GraphNode nodeA = g.addNodeIfAbsent("a", x -> new GraphNodeSupport(x));
        GraphNode nodeA2 = g.addNodeIfAbsent("a", x -> new GraphNodeSupport(x));
        assertSame(nodeA, nodeA2);
        GraphNode nodeB = g.addNodeIfAbsent("b", x -> new GraphNodeSupport(x));
        Collection<GraphNode> nodes = g.getNodes();
        assertEquals(2, nodes.size());
        assertTrue(nodes.contains(nodeA));
        assertTrue(nodes.contains(nodeB));
    }

    @Test
    public void addNode() {
        GraphSupport<GraphNode, GraphEdge> g = new GraphSupport();
        GraphNode nodeA = new GraphNodeSupport("a");
        g.addNode(nodeA);
        GraphNode actualNodeA = g.getNode("a");
        assertSame(nodeA, actualNodeA);
    }

    @Test
    public void removeNode() {
        GraphSupport<GraphNode, GraphEdge> g = new GraphSupport();
        GraphNode nodeA = new GraphNodeSupport("a");
        g.addNode(nodeA);
        g.removeNode(nodeA);
        GraphNode actualNodeA = g.getNode("a");
        assertNull(actualNodeA);

        g.removeNode(null);
    }

    @Test
    public void getEdges() {
        GraphSupport<GraphNode, GraphEdge> g = new GraphSupport();
        GraphEdge e1 = new GraphEdgeSupport();
        g.addEdge(e1);
        Set<GraphEdge> edges = g.getEdges();
        assertEquals(1, edges.size());
        assertTrue(edges.contains(e1));
    }

    @Test
    public void removeEdge() {
        GraphSupport<GraphNode, GraphEdge> g = new GraphSupport();
        GraphEdge e1 = new GraphEdgeSupport();
        g.addEdge(e1);
        g.removeEdge(e1);
        Set<GraphEdge> edges = g.getEdges();
        assertEquals(0, edges.size());

        g.removeEdge(null);
    }

}
