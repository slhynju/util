package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"rawtypes", "static-method", "unchecked"})
public class CollectionUtil_forEach_Test {

    @Test
    public void collection_null() {
        Collection<String> c = null;
        final List<String> list = new ArrayList<>();
        CollectionUtil.forEach(c, list::add);
        assertTrue(list.isEmpty());
    }

    @Test
    public void collection() {
        Collection<String> c = Arrays.asList("a", "b", "c", "def");
        final List<String> list = new ArrayList<>();
        CollectionUtil.forEach(c, list::add);
        assertEquals(c, list);
    }

    @Test
    public void collection_actionNull() {
        Collection<String> c = Arrays.asList("a", "b", "c", "def");
        Consumer action = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.forEach(c, action));
    }

    @Test
    public void collection_bothNull() {
        Collection<String> c = null;
        Consumer action = null;
        CollectionUtil.forEach(c, action);
    }

    @Test
    public void array_null() {
        String[] array = null;
        final List<String> list = new ArrayList<>();
        CollectionUtil.forEach(array, (Consumer<String>) list::add);
        assertTrue(list.isEmpty());
    }

    @Test
    public void array() {
        String[] array = new String[]{"a", "b", "c"};
        final List<String> list = new ArrayList<>();
        CollectionUtil.forEach(array, (Consumer<String>) list::add);
        assertEquals(3, list.size());
        assertEquals("b", list.get(1));
    }

    @Test
    public void array_actionNull() {
        String[] array = new String[]{"a", "b", "c"};
        Consumer action = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.forEach(array, action));
    }

    @Test
    public void array_bothNull() {
        String[] array = null;
        Consumer action = null;
        CollectionUtil.forEach(array, action);
    }

    @Test
    public void listConsumer_null() {
        List<String> c = null;
        final List<String> list = new ArrayList<>();
        CollectionUtil.forEach(c, (i, x) -> list.add(x));
        assertTrue(list.isEmpty());
    }

    @Test
    public void listConsumer() {
        List<String> c = Arrays.asList("a", "b", "c", "def");
        final List<String> list = new ArrayList<>();
        CollectionUtil.forEach(c, (i, x) -> list.add(x));
        assertEquals(c, list);
    }

    @Test
    public void listConsumer_actionNull() {
        List<String> c = Arrays.asList("a", "b", "c", "def");
        IndexConsumer action = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.forEach(c, action));
    }

    @Test
    public void listConsumer_bothNull() {
        List<String> c = null;
        IndexConsumer action = null;
        CollectionUtil.forEach(c, action);
    }

    @Test
    public void arrayListConsumer_null() {
        String[] array = null;
        final List<String> list = new ArrayList<>();
        CollectionUtil.forEach(array, (i, x) -> list.add(x));
        assertTrue(list.isEmpty());
    }

    @Test
    public void arrayListConsumer() {
        String[] array = new String[]{"a", "b", "c"};
        final List<String> list = new ArrayList<>();
        CollectionUtil.forEach(array, (i, x) -> list.add(x));
        assertEquals(3, list.size());
        assertEquals("b", list.get(1));
    }

    @Test
    public void arrayListConsumer_actionNull() {
        String[] array = new String[]{"a", "b", "c"};
        IndexConsumer action = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.forEach(array, action));
    }

    @Test
    public void arrayListConsumer_bothNull() {
        String[] array = null;
        IndexConsumer action = null;
        CollectionUtil.forEach(array, action);
    }

    @Test
    public void map_null() {
        Map map = null;
        Map map2 = new HashMap();
        CollectionUtil.forEach(map, (BiConsumer<Object, Object>) map2::put);
        assertTrue(map2.isEmpty());
    }

    @Test
    public void map() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2");
        Map map2 = new HashMap();
        CollectionUtil.forEach(map, (BiConsumer<Object, Object>) map2::put);
        assertEquals(map, map2);
    }

    @Test
    public void map_actionNull() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2");
        BiConsumer action = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.forEach(map, action));
    }

    @Test
    public void map_bothNull() {
        Map map = null;
        BiConsumer action = null;
        CollectionUtil.forEach(map, action);
    }

}
