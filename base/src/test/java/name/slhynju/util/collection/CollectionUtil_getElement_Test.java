package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_getElement_Test {

    @Test
    public void list_null() {
        List<String> list = null;
        String s = CollectionUtil.getElement(list, 2);
        assertNull(s);
    }

    @Test
    public void list() {
        List<String> list = Arrays.asList("a", "b", "c");
        String s = CollectionUtil.getElement(list, 2);
        assertEquals("c", s);
    }

    @Test
    public void list_invalidIndex() {
        List<String> list = Arrays.asList("a", "b", "c");
        String s = CollectionUtil.getElement(list, -2);
        assertNull(s);
    }

    @Test
    public void list_invalidIndex2() {
        List<String> list = Arrays.asList("a", "b", "c");
        String s = CollectionUtil.getElement(list, 5);
        assertNull(s);
    }

    @Test
    public void array_null() {
        String[] a = null;
        String s = CollectionUtil.getElement(a, 2);
        assertNull(s);
    }

    @Test
    public void array() {
        String[] a = new String[]{"a", "b", "c"};
        String s = CollectionUtil.getElement(a, 2);
        assertEquals("c", s);
    }

    @Test
    public void array_invalidIndex() {
        String[] a = new String[]{"a", "b", "c"};
        String s = CollectionUtil.getElement(a, -2);
        assertNull(s);
    }

    @Test
    public void array_invalidIndex2() {
        String[] a = new String[]{"a", "b", "c"};
        String s = CollectionUtil.getElement(a, 5);
        assertNull(s);
    }

    @Test
    public void booleanArray_null() {
        boolean[] a = null;
        boolean s = CollectionUtil.getElement(a, 2, true);
        assertTrue(s);
    }

    @Test
    public void booleanArray() {
        boolean[] a = new boolean[]{false, true, false};
        boolean s = CollectionUtil.getElement(a, 2, true);
        assertEquals(false, s);
    }

    @Test
    public void booleanArray_invalidIndex() {
        boolean[] a = new boolean[]{false, true, false};
        boolean s = CollectionUtil.getElement(a, -2, true);
        assertEquals(true, s);
    }

    @Test
    public void booleanArray_invalidIndex2() {
        boolean[] a = new boolean[]{false, true, false};
        boolean s = CollectionUtil.getElement(a, 5, true);
        assertEquals(true, s);
    }

    @Test
    public void intArray_null() {
        int[] a = null;
        int s = CollectionUtil.getElement(a, 2, 5);
        assertEquals(5, s);
    }

    @Test
    public void intArray() {
        int[] a = new int[]{1, 2, 3};
        int s = CollectionUtil.getElement(a, 2, 5);
        assertEquals(3, s);
    }

    @Test
    public void intArray_invalidIndex() {
        int[] a = new int[]{1, 2, 3};
        int s = CollectionUtil.getElement(a, -2, 5);
        assertEquals(5, s);
    }

    @Test
    public void intArray_invalidIndex2() {
        int[] a = new int[]{1, 2, 3};
        int s = CollectionUtil.getElement(a, 5, 5);
        assertEquals(5, s);
    }

    @Test
    public void longArray_null() {
        long[] a = null;
        long s = CollectionUtil.getElement(a, 2, 5L);
        assertEquals(5L, s);
    }

    @Test
    public void longArray() {
        long[] a = new long[]{1L, 2L, 3L};
        long s = CollectionUtil.getElement(a, 2, 5L);
        assertEquals(3L, s);
    }

    @Test
    public void longArray_invalidIndex() {
        long[] a = new long[]{1L, 2L, 3L};
        long s = CollectionUtil.getElement(a, -2, 5L);
        assertEquals(5L, s);
    }

    @Test
    public void longArray_invalidIndex2() {
        long[] a = new long[]{1L, 2L, 3L};
        long s = CollectionUtil.getElement(a, 5, 5L);
        assertEquals(5L, s);
    }

    @Test
    public void doubleArray_null() {
        double[] a = null;
        double s = CollectionUtil.getElement(a, 2, 5.0);
        assertEquals(5.0, s, 0.0001);
    }

    @Test
    public void doubleArray() {
        double[] a = new double[]{1.0, 2.0, 3.0};
        double s = CollectionUtil.getElement(a, 2, 5.0);
        assertEquals(3.0, s, 0.0001);
    }

    @Test
    public void doubleArray_invalidIndex() {
        double[] a = new double[]{1.0, 2.0, 3.0};
        double s = CollectionUtil.getElement(a, -2, 5.0);
        assertEquals(5.0, s, 0.0001);
    }

    @Test
    public void doubleArray_invalidIndex2() {
        double[] a = new double[]{1.0, 2.0, 3.0};
        double s = CollectionUtil.getElement(a, 5, 5.0);
        assertEquals(5.0, s, 0.0001);
    }

}
