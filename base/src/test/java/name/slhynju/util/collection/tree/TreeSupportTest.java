package name.slhynju.util.collection.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"rawtypes", "unchecked", "static-method"})
public class TreeSupportTest {

    @Test
    public void isEmpty() {
        TreeSupport tree = new TreeSupport();
        assertTrue(tree.isEmpty());
    }

    @Test
    public void getRoot() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("n");
        TreeSupport tree = new TreeSupport(node);
        assertFalse(tree.isEmpty());
        assertEquals(node, tree.getRoot());
    }

    @Test
    public void setRoot() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("n");
        TreeSupport tree = new TreeSupport();
        tree.setRoot(node);
        assertFalse(tree.isEmpty());
        assertEquals(node, tree.getRoot());
    }

    @Test
    public void testEquals() {
        BinaryTreeNodeSupport a = new BinaryTreeNodeSupport("a");
        TreeSupport tree = new TreeSupport(a);
        BinaryTreeNodeSupport a2 = new BinaryTreeNodeSupport("a");
        TreeSupport tree2 = new TreeSupport(a2);
        assertEquals(tree, tree2);
        BinaryTreeNodeSupport b = new BinaryTreeNodeSupport("b");
        TreeSupport tree3 = new TreeSupport(b);
        assertNotEquals(tree, tree3);
        assertFalse(tree.equals(null));
        assertFalse(tree.equals("a"));
    }

    @Test
    public void testHashCode() {
        BinaryTreeNodeSupport a = new BinaryTreeNodeSupport("a");
        TreeSupport tree = new TreeSupport(a);
        BinaryTreeNodeSupport a2 = new BinaryTreeNodeSupport("a");
        TreeSupport tree2 = new TreeSupport(a2);
        assertEquals(tree.hashCode(), tree2.hashCode());
        BinaryTreeNodeSupport b = new BinaryTreeNodeSupport("b");
        TreeSupport tree3 = new TreeSupport(b);
        assertNotEquals(tree.hashCode(), tree3.hashCode());
    }

    @Test
    public void testToString() {
        BinaryTreeNodeSupport a = new BinaryTreeNodeSupport("a");
        TreeSupport tree = new TreeSupport(a);
        assertEquals("Tree a", tree.toString());
        TreeSupport tree2 = new TreeSupport();
        assertEquals("Tree null", tree2.toString());
    }
}
