package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_toSet_Test {

    @Test
    public void array_null() {
        String[] a = null;
        Set<String> set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void array() {
        String[] a = new String[]{"a", "b", "c"};
        Set<String> set = CollectionUtil.toSet(a);
        assertNotNull(set);
        Set<String> expected = new HashSet<>(Arrays.asList("a", "b", "c"));
        assertEquals(expected, set);
    }

    @Test
    public void collection_null() {
        Collection c = null;
        Set set = CollectionUtil.toSet(c);
        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void collection() {
        Collection c = Arrays.asList("a", "b", "c");
        Set set = CollectionUtil.toSet(c);
        assertNotNull(set);
        assertEquals(3, set.size());
        assertTrue(set.contains("a"));
        assertTrue(set.contains("b"));
        assertTrue(set.contains("c"));
    }

    @Test
    public void booleanArray_null() {
        boolean[] a = null;
        Set set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void booleanArray() {
        boolean[] a = new boolean[]{true, false, true};
        Set set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertEquals(2, set.size());
        assertTrue(set.contains(Boolean.TRUE));
        assertTrue(set.contains(Boolean.FALSE));
    }


    @Test
    public void intArray_null() {
        int[] a = null;
        Set set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void intArray() {
        int[] a = new int[]{1, 2, 3};
        Set set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertEquals(3, set.size());
        assertTrue(set.contains(Integer.valueOf(1)));
        assertTrue(set.contains(Integer.valueOf(2)));
        assertTrue(set.contains(Integer.valueOf(3)));
    }

    @Test
    public void longArray_null() {
        long[] a = null;
        Set set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void longArray() {
        long[] a = new long[]{1L, 2L, 3L};
        Set set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertEquals(3, set.size());
        assertTrue(set.contains(Long.valueOf(1L)));
        assertTrue(set.contains(Long.valueOf(2L)));
        assertTrue(set.contains(Long.valueOf(3L)));
    }


    @Test
    public void doubleArray_null() {
        double[] a = null;
        Set set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertTrue(set.isEmpty());
    }

    @Test
    public void doubleArray() {
        double[] a = new double[]{1.0, 2.0, 3.0};
        Set set = CollectionUtil.toSet(a);
        assertNotNull(set);
        assertEquals(3, set.size());
        assertTrue(set.contains(Double.valueOf(1.0)));
        assertTrue(set.contains(Double.valueOf(2.0)));
        assertTrue(set.contains(Double.valueOf(3.0)));
    }


    @Test
    public void transform_null() {
        Set<String> from = null;
        Function<String, Integer> mapper = x -> x == null ? null : Integer.valueOf(x);
        Set<Integer> to = CollectionUtil.transformToSet(from, mapper);
        assertNotNull(to);
        assertTrue(to.isEmpty());
    }

    @Test
    public void transfrom_mapperNull() {
        Set<String> from = CollectionUtil.toSet("1", "2", "3");
        Function<String, Integer> mapper = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformToSet(from, mapper));
    }

    @Test
    public void transform_bothNull() {
        Set<String> from = null;
        Function<String, Integer> mapper = null;
        Set<Integer> to = CollectionUtil.transformToSet(from, mapper);
        assertNotNull(to);
        assertTrue(to.isEmpty());
    }

    @Test
    public void transform() {
        Set<String> from = CollectionUtil.toSet("1", "2", "3");
        Function<String, Integer> mapper = x -> x == null ? null : Integer.valueOf(x);
        Set<Integer> to = CollectionUtil.transformToSet(from, mapper);
        assertNotNull(to);
        assertEquals(3, to.size());
        assertTrue(to.contains(Integer.valueOf(1)));
        assertTrue(to.contains(Integer.valueOf(2)));
        assertTrue(to.contains(Integer.valueOf(3)));
    }

    @Test
    public void transformArray_null() {
        String[] from = null;
        Function<String, Integer> mapper = x -> x == null ? null : Integer.valueOf(x);
        Set<Integer> to = CollectionUtil.transformToSet(from, mapper);
        assertNotNull(to);
        assertTrue(to.isEmpty());
    }

    @Test
    public void transformArray_mapperNull() {
        String[] from = new String[]{"1", "2", "3"};
        Function<String, Integer> mapper = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformToSet(from, mapper));
    }

    @Test
    public void transformArray_bothNull() {
        String[] from = null;
        Function<String, Integer> mapper = null;
        Set<Integer> to = CollectionUtil.transformToSet(from, mapper);
        assertNotNull(to);
        assertTrue(to.isEmpty());
    }

    @Test
    public void transformArray() {
        String[] from = new String[]{"1", "2", "3"};
        Function<String, Integer> mapper = x -> x == null ? null : Integer.valueOf(x);
        Set<Integer> to = CollectionUtil.transformToSet(from, mapper);
        assertNotNull(to);
        assertEquals(3, to.size());
        assertTrue(to.contains(Integer.valueOf(1)));
        assertTrue(to.contains(Integer.valueOf(2)));
        assertTrue(to.contains(Integer.valueOf(3)));
    }

}
