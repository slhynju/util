package name.slhynju.util.collection.tree.walker;

import name.slhynju.util.collection.tree.Tree;
import name.slhynju.util.collection.tree.TreeSupport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

public class TreeWalkerTest {

    private SampleBinaryTreeNode n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;

    private Tree<SampleBinaryTreeNode> tree;

    @BeforeEach
    public void createBinaryTree() {
        n1 = new SampleBinaryTreeNode("a");
        n2 = new SampleBinaryTreeNode("b");
        n3 = new SampleBinaryTreeNode("c");
        n4 = new SampleBinaryTreeNode("d");
        n5 = new SampleBinaryTreeNode("e");
        n6 = new SampleBinaryTreeNode("f");
        n7 = new SampleBinaryTreeNode("g");
        n8 = new SampleBinaryTreeNode("h");
        n9 = new SampleBinaryTreeNode("i");
        n10 = new SampleBinaryTreeNode("j");
        n11 = new SampleBinaryTreeNode("k");
        n1.setLeftChild(n2);
        n2.setParent(n1);
        n1.setRightChild(n3);
        n3.setParent(n1);
        n2.setLeftChild(n4);
        n4.setParent(n2);
        n2.setRightChild(n5);
        n5.setParent(n2);
        n4.setLeftChild(n8);
        n8.setParent(n4);
        n5.setRightChild(n9);
        n9.setParent(n5);
        n3.setLeftChild(n6);
        n6.setParent(n3);
        n3.setRightChild(n7);
        n7.setParent(n3);
        n6.setRightChild(n10);
        n10.setParent(n6);
        n7.setLeftChild(n11);
        n11.setParent(n7);
        tree = new TreeSupport(n1);
    }

    @Test
    public void preorder() {
        assertTrue(n8.isLastChild());
        assertFalse(n4.isLastChild());
        PreOrderTreeWalker<SampleBinaryTreeNode> walker = new PreOrderTreeWalker<>();
        StringBuilder sb = new StringBuilder();
        walker.walk(tree, x -> {
            sb.append(x.getName());
        });
        assertEquals("abdheicfjgk", sb.toString());
    }

    @Test
    public void postorder() {
        PostOrderTreeWalker<SampleBinaryTreeNode> walker = new PostOrderTreeWalker<>();
        StringBuilder sb = new StringBuilder();
        walker.walk(tree, x -> {
            sb.append(x.getName());
        });
        assertEquals("hdiebjfkgca", sb.toString());
    }

    @Test
    public void inorder() {
        InOrderBinaryTreeWalker<SampleBinaryTreeNode> walker = new InOrderBinaryTreeWalker<>();
        StringBuilder sb = new StringBuilder();
        walker.walk(tree, x -> {
            sb.append(x.getName());
        });
        assertEquals("hdbeiafjckg", sb.toString());
    }

    @Test
    public void breadthFirst() {
        BreadthFirstTreeWalker<SampleBinaryTreeNode> walker = new BreadthFirstTreeWalker<>();
        StringBuilder sb = new StringBuilder();
        walker.walk(tree, x -> {
            sb.append(x.getName());
        });
        assertEquals("abcdefghijk", sb.toString());
    }

    @Test
    public void breadthFirst_visited() {
        n11.setLeftChild(n9);
        BreadthFirstTreeWalker<SampleBinaryTreeNode> walker = new BreadthFirstTreeWalker<>();
        StringBuilder sb = new StringBuilder();
        walker.walk(tree, x -> {
            sb.append(x.getName());
        });
        assertEquals("abcdefghijk", sb.toString());
    }


    @Test
    public void breadthFirst_static() {
        StringBuilder sb = new StringBuilder();
        Consumer<String> action = x -> sb.append(x);
        Function<String, List<String>> expander = x -> {
            if (x.length() >= 3) {
                return Collections.emptyList();
            }
            List<String> list = new ArrayList<>();
            list.add(x + "1");
            list.add(x + "2");
            list.add(x + "3");
            return list;
        };
        BreadthFirstTreeWalker.walk("a", expander, action);
        assertEquals("aa1a2a3a11a12a13a21a22a23a31a32a33", sb.toString());
    }

    @Test
    public void tree_null() {
        PostOrderTreeWalker<SampleBinaryTreeNode> walker = new PostOrderTreeWalker<>();
        StringBuilder sb = new StringBuilder();
        walker.walk(null, x -> {
            sb.append(x.getName());
        });
        assertEquals("", sb.toString());
    }

    @Test
    public void tree_empty() {
        PostOrderTreeWalker<SampleBinaryTreeNode> walker = new PostOrderTreeWalker<>();
        tree.setRoot(null);
        StringBuilder sb = new StringBuilder();
        walker.walk(tree, x -> {
            sb.append(x.getName());
        });
        assertEquals("", sb.toString());
    }

    @Test
    public void tree_empty2() {
        InOrderBinaryTreeWalker<SampleBinaryTreeNode> walker = new InOrderBinaryTreeWalker<>();
        tree.setRoot(null);
        StringBuilder sb = new StringBuilder();
        walker.walk(tree, x -> {
            sb.append(x.getName());
        });
        assertEquals("", sb.toString());
    }

    @Test
    public void singleNode() {
        PostOrderTreeWalker<SampleBinaryTreeNode> walker = new PostOrderTreeWalker<>();
        SampleBinaryTreeNode root = tree.getRoot();
        root.setLeftChild(null);
        root.setRightChild(null);
        StringBuilder sb = new StringBuilder();
        walker.walk(tree, x -> {
            sb.append(x.getName());
        });
        assertEquals("a", sb.toString());
    }

}
