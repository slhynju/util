package name.slhynju.util.collection.graph;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


@SuppressWarnings("static-method")
public class GraphEdgeSupportTest {

    @Test
    public void attribute() {
        GraphEdgeSupport e = new GraphEdgeSupport();
        e.setAttribute("visited", Boolean.TRUE);
        assertEquals(Boolean.TRUE, e.getAttribute("visited"));

        assertTrue(e.containsAttribute("visited"));
        assertFalse(e.containsAttribute("no_such_key"));
        e.removeAttribute("visited");
        assertFalse(e.containsAttribute("visited"));
    }

}
