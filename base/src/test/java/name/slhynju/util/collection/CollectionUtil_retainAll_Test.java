package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_retainAll_Test {

    @Test
    public void retainAllMatchesCollection_null() {
        Collection c = null;
        CollectionUtil.retainAllMatches(c, "b"::equals);
    }

    @Test
    public void retainAllMatchesCollection() {
        List c = new ArrayList(Arrays.asList("a", "b", "b", "c"));
        CollectionUtil.retainAllMatches(c, "b"::equals);
        assertEquals(2, c.size());
        assertTrue(c.contains("b"));
        assertFalse(c.contains("a"));
        assertFalse(c.contains("c"));
    }

    @Test
    public void retainAllMatchesCollection_matcherNull() {
        List c = new ArrayList(Arrays.asList("a", "b", "b", "c"));
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.retainAllMatches(c, matcher));
    }

    @Test
    public void retainAllMatchesCollection_bothNull() {
        Collection c = null;
        Predicate matcher = null;
        CollectionUtil.retainAllMatches(c, matcher);
    }

    @Test
    public void retainAllMatchesList_null() {
        List c = null;
        CollectionUtil.retainAllMatches(c, (i, x) -> i % 2 == 0);
    }

    @Test
    public void retainAllMatchesList() {
        List c = new ArrayList(Arrays.asList("a", "b", "c", "d"));
        CollectionUtil.retainAllMatches(c, (i, x) -> i % 2 == 0);
        assertEquals(2, c.size());
        assertEquals("a", c.get(0));
        assertEquals("c", c.get(1));
    }

    @Test
    public void retainAllMatchesList_matcherNull() {
        List c = new ArrayList(Arrays.asList("a", "b", "b", "c"));
        IndexPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.retainAllMatches(c, matcher));
    }

    @Test
    public void retainAllMatchesList_bothNull() {
        List c = null;
        IndexPredicate matcher = null;
        CollectionUtil.retainAllMatches(c, matcher);
    }

    @Test
    public void retainAllMatchesMap_null() {
        Map map = null;
        CollectionUtil.retainAllMatches(map, (k, v) -> "b".equals(k));
    }

    @Test
    public void retainAllMatchesMap() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2", "c", "3");
        CollectionUtil.retainAllMatches(map, (k, v) -> "b".equals(k));
        assertEquals(1, map.size());
        assertFalse(map.containsKey("a"));
        assertFalse(map.containsValue("3"));
        assertTrue(map.containsKey("b"));
        assertTrue(map.containsValue("2"));
    }

    @Test
    public void retainAllMatchesMap_matcherNull() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2", "c", "3");
        BiPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.retainAllMatches(map, matcher));
    }

    @Test
    public void retainAllMatchesMap_bothNull() {
        Map map = null;
        BiPredicate matcher = null;
        CollectionUtil.retainAllMatches(map, matcher);
    }
}
