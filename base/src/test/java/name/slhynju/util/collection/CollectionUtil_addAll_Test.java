package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_addAll_Test {


    @Test
    public void collection_toNull() {
        List c = null;
        List c2 = Arrays.asList("abc", "def");
        CollectionUtil.addAll(c, c2);
    }

    @Test
    public void collection_fromNull() {
        List c = Arrays.asList("abc", "def");
        List c2 = null;
        CollectionUtil.addAll(c, c2);
        assertEquals(2, c.size());
    }

    @Test
    public void collection() {
        List c = new ArrayList(Arrays.asList("abc", "def"));
        List c2 = Arrays.asList("ghi", "jkl");
        CollectionUtil.addAll(c, c2);
        assertEquals(4, c.size());
        assertTrue(c.contains("ghi"));
    }

    @Test
    public void array_toNull() {
        List<String> c = null;
        String[] c2 = new String[]{"abc", "def"};
        CollectionUtil.addAll(c, c2);
    }

    @Test
    public void array_fromNull() {
        List<String> c = Arrays.asList("abc", "def");
        String[] c2 = null;
        CollectionUtil.addAll(c, c2);
        assertEquals(2, c.size());
    }

    @Test
    public void array() {
        List c = new ArrayList(Arrays.asList("abc", "def"));
        String[] c2 = new String[]{"ghi", "jkl"};
        CollectionUtil.addAll(c, c2);
        assertEquals(4, c.size());
        assertTrue(c.contains("ghi"));
    }

    @Test
    public void enumeration_toNull() {
        List<String> c = null;
        Enumeration c2 = new StringTokenizer("abc", "b");
        CollectionUtil.addAll(c, c2);
    }

    @Test
    public void enumeration_fromNull() {
        List<String> c = Arrays.asList("abc", "def");
        Enumeration c2 = null;
        CollectionUtil.addAll(c, c2);
        assertEquals(2, c.size());
    }

    @Test
    public void enumeration() {
        List c = new ArrayList(Arrays.asList("abc", "def"));
        Enumeration c2 = new StringTokenizer("abc", "b");
        CollectionUtil.addAll(c, c2);
        assertEquals(4, c.size());
        assertTrue(c.contains("a"));
    }

    @Test
    public void tokenizer() {
        List<String> to = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer("abc", "b");
        CollectionUtil.addAll(to, tokenizer);
        assertEquals(2, to.size());
        assertEquals("a", to.get(0));
        assertEquals("c", to.get(1));
    }

    @Test
    public void tokenizer_toNull() {
        List<String> c = null;
        StringTokenizer tokenizer = new StringTokenizer("abc", "b");
        CollectionUtil.addAll(c, tokenizer);
    }

    @Test
    public void tokenizer_fromNull() {
        List<String> c = Arrays.asList("abc", "def");
        StringTokenizer tokenizer = null;
        CollectionUtil.addAll(c, tokenizer);
        assertEquals(2, c.size());
    }

    @Test
    public void addAllKeys_toNull() {
        Collection c = null;
        Map map = new HashMap();
        CollectionUtil.addAllKeys(c, map);
    }

    @Test
    public void addAllKeys_fromNull() {
        List c = Arrays.asList("abc", "def");
        Map map = null;
        CollectionUtil.addAllKeys(c, map);
    }

    @Test
    public void addAllKeys() {
        List c = new ArrayList(Arrays.asList("abc", "def"));
        Map map = new HashMap();
        map.put("a", "bc");
        map.put("d", "ef");
        CollectionUtil.addAllKeys(c, map);
        assertEquals(4, c.size());
        assertTrue(c.contains("a"));
    }

    @Test
    public void addAllValues_toNull() {
        Collection c = null;
        Map map = new HashMap();
        CollectionUtil.addAllValues(c, map);
    }

    @Test
    public void addAllvalues_fromNull() {
        List c = Arrays.asList("abc", "def");
        Map map = null;
        CollectionUtil.addAllValues(c, map);
    }

    @Test
    public void addAllValues() {
        List c = new ArrayList(Arrays.asList("abc", "def"));
        Map map = new HashMap();
        map.put("a", "bc");
        map.put("d", "ef");
        CollectionUtil.addAllValues(c, map);
        assertEquals(4, c.size());
        assertTrue(c.contains("bc"));
    }

    @Test
    public void addAllMatchesCollection_toNull() {
        List c = null;
        List c2 = Arrays.asList("a", "b", "c");
        CollectionUtil.addAllMatches(c, c2, "b"::equals);
    }

    @Test
    public void addAllMatchesCollection_fromNull() {
        List c = Arrays.asList("a", "b", "c");
        List c2 = null;
        CollectionUtil.addAllMatches(c, c2, "b"::equals);
    }

    @Test
    public void addAllMatchesCollection() {
        List c = new ArrayList(Arrays.asList("a", "b", "c"));
        List c2 = Arrays.asList("d", "e", "f");
        CollectionUtil.addAllMatches(c, c2, "e"::equals);
        assertEquals(4, c.size());
        assertTrue(c.contains("e"));
    }

    @Test
    public void addAllMatchesCollection_matcherNull() {
        List c = new ArrayList(Arrays.asList("a", "b", "c"));
        List c2 = Arrays.asList("d", "e", "f");
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.addAllMatches(c, c2, matcher));
    }

    @Test
    public void addAllMatchesCollection_allNull() {
        List c = null;
        List c2 = null;
        Predicate matcher = null;
        CollectionUtil.addAllMatches(c, c2, matcher);
    }

    @Test
    public void addAllMatchesCollectionIndexMatcher_toNull() {
        List c = null;
        List c2 = Arrays.asList("a", "b", "c");
        CollectionUtil.addAllMatches(c, c2, (i, x) -> i == 1);
    }

    @Test
    public void addAllMatchesCollectionIndexMatcher_fromNull() {
        List c = Arrays.asList("a", "b", "c");
        List c2 = null;
        CollectionUtil.addAllMatches(c, c2, (i, x) -> i == 1);
    }

    @Test
    public void addAllMatchesCollectionIndexMatcher() {
        List c = new ArrayList(Arrays.asList("a", "b", "c"));
        List c2 = Arrays.asList("d", "e", "f");
        CollectionUtil.addAllMatches(c, c2, (i, x) -> i == 1);
        assertEquals(4, c.size());
        assertTrue(c.contains("e"));
    }

    @Test
    public void addAllMatchesCollectionIndexMatcher_matcherNull() {
        List c = new ArrayList(Arrays.asList("a", "b", "c"));
        List c2 = Arrays.asList("d", "e", "f");
        IndexPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.addAllMatches(c, c2, matcher));
    }

    @Test
    public void addAllMatchesCollectionIndexMatcher_allNull() {
        List c = null;
        List c2 = null;
        IndexPredicate matcher = null;
        CollectionUtil.addAllMatches(c, c2, matcher);
    }

    @Test
    public void addAllMatchesArray_toNull() {
        List c = null;
        String[] c2 = new String[]{"a", "b", "c"};
        CollectionUtil.addAllMatches(c, c2, "b"::equals);
    }

    @Test
    public void addAllMatchesArray_fromNull() {
        List c = Arrays.asList("a", "b", "c");
        String[] c2 = null;
        CollectionUtil.addAllMatches(c, c2, "b"::equals);
    }

    @Test
    public void addAllMatchesArray() {
        List c = new ArrayList(Arrays.asList("a", "b", "c"));
        String[] c2 = new String[]{"d", "e", "f"};
        CollectionUtil.addAllMatches(c, c2, "e"::equals);
        assertEquals(4, c.size());
        assertTrue(c.contains("e"));
    }

    @Test
    public void addAllMatchesArray_matcherNull() {
        List c = new ArrayList(Arrays.asList("a", "b", "c"));
        String[] c2 = new String[]{"d", "e", "f"};
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.addAllMatches(c, c2, matcher));
    }

    @Test
    public void addAllMatchesArray_allNull() {
        List c = null;
        String[] c2 = null;
        Predicate matcher = null;
        CollectionUtil.addAllMatches(c, c2, matcher);
    }

    @Test
    public void addAllMatchesArrayIndexMatcher_toNull() {
        List c = null;
        String[] c2 = new String[]{"a", "b", "c"};
        CollectionUtil.addAllMatches(c, c2, (i, x) -> i == 1);
    }

    @Test
    public void addAllMatchesArrayIndexMatcher_fromNull() {
        List c = Arrays.asList("a", "b", "c");
        String[] c2 = null;
        CollectionUtil.addAllMatches(c, c2, (i, x) -> i == 1);
    }

    @Test
    public void addAllMatchesArrayIndexMatcher() {
        List c = new ArrayList(Arrays.asList("a", "b", "c"));
        String[] c2 = new String[]{"d", "e", "f"};
        CollectionUtil.addAllMatches(c, c2, (i, x) -> i == 1);
        assertEquals(4, c.size());
        assertTrue(c.contains("e"));
    }

    @Test
    public void addAllMatchesArrayIndexMatcher_matcherNull() {
        List c = new ArrayList(Arrays.asList("a", "b", "c"));
        String[] c2 = new String[]{"d", "e", "f"};
        IndexPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.addAllMatches(c, c2, matcher));
    }

    @Test
    public void addAllMatchesArrayIndexMatcher_allNull() {
        List c = null;
        String[] c2 = null;
        IndexPredicate matcher = null;
        CollectionUtil.addAllMatches(c, c2, matcher);
    }

    @Test
    public void addAllMatchesMap_toNull() {
        Map m1 = null;
        Map m2 = new HashMap();
        m2.put("a", "1");
        CollectionUtil.addAllMatches(m1, m2, (k, v) -> "b".equals(k));
    }

    @Test
    public void addAllMatchesMap_fromNull() {
        Map m1 = new HashMap();
        m1.put("a", "1");
        Map m2 = null;
        CollectionUtil.addAllMatches(m1, m2, (k, v) -> "b".equals(k));
    }

    @Test
    public void addAllMatchesMap() {
        Map m1 = new HashMap();
        m1.put("a", "1");
        Map m2 = new HashMap();
        m2.put("b", "2");
        m2.put("c", "3");
        CollectionUtil.addAllMatches(m1, m2, (k, v) -> "b".equals(k));
        assertEquals(2, m1.size());
        assertTrue(m1.containsValue("2"));
    }

    @Test
    public void addAllMatchesMap_matcherNull() {
        Map m1 = new HashMap();
        m1.put("a", "1");
        Map m2 = new HashMap();
        m2.put("b", "2");
        m2.put("c", "3");
        BiPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.addAllMatches(m1, m2, matcher));
    }

    @Test
    public void addAllMatchesMap_allNull() {
        Map m1 = null;
        Map m2 = null;
        BiPredicate matcher = null;
        CollectionUtil.addAllMatches(m1, m2, matcher);
    }
}
