package name.slhynju.util.collection.graph.directed;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DirectedGraphNodeSupportTest {

    @Test
    public void inEdges() {
        DirectedGraphNodeSupport a = new DirectedGraphNodeSupport("a");
        DirectedGraphEdge e1 = mock(DirectedGraphEdge.class);
        DirectedGraphEdge e2 = mock(DirectedGraphEdge.class);
        DirectedGraphEdge e3 = mock(DirectedGraphEdge.class);
        a.addInEdge(e1);
        a.addInEdge(e2);
        a.addInEdge(e3);
        List<DirectedGraphEdge> inEdges = a.getInEdges();
        assertEquals(3, inEdges.size());
        assertSame(e1, inEdges.get(0));
        assertSame(e2, inEdges.get(1));
        assertSame(e3, inEdges.get(2));
        assertEquals(3, a.getInDegree());
        a.removeInEdge(e2);
        inEdges = a.getInEdges();
        assertEquals(2, inEdges.size());
        assertSame(e1, inEdges.get(0));
        assertSame(e3, inEdges.get(1));
        assertEquals(2, a.getInDegree());
    }

    @Test
    public void directPredecessor() {
        DirectedGraphNodeSupport a = new DirectedGraphNodeSupport("a");
        DirectedGraphEdge e1 = mock(DirectedGraphEdge.class);
        DirectedGraphEdge e2 = mock(DirectedGraphEdge.class);
        DirectedGraphEdge e3 = mock(DirectedGraphEdge.class);
        DirectedGraphNodeSupport b = new DirectedGraphNodeSupport("b");
        when(e1.getTail()).thenReturn(b);
        DirectedGraphNodeSupport c = new DirectedGraphNodeSupport("c");
        when(e2.getTail()).thenReturn(c);
        DirectedGraphNodeSupport d = new DirectedGraphNodeSupport("d");
        when(e3.getTail()).thenReturn(d);
        a.addInEdge(e1);
        a.addInEdge(e2);
        a.addInEdge(e3);
        List<DirectedGraphNodeSupport> predecessors = a.getDirectPredecessors();
        assertEquals(3, predecessors.size());
        assertSame(b, predecessors.get(0));
        assertSame(c, predecessors.get(1));
        assertSame(d, predecessors.get(2));
        assertSame(c, a.getDirectPredecessor("c"));
        assertNull(a.getDirectPredecessor("f"));
    }

    @Test
    public void outEdges() {
        DirectedGraphNodeSupport a = new DirectedGraphNodeSupport("a");
        DirectedGraphEdge e1 = mock(DirectedGraphEdge.class);
        DirectedGraphEdge e2 = mock(DirectedGraphEdge.class);
        DirectedGraphEdge e3 = mock(DirectedGraphEdge.class);
        a.addOutEdge(e1);
        a.addOutEdge(e2);
        a.addOutEdge(e3);
        List<DirectedGraphEdge> outEdges = a.getOutEdges();
        assertEquals(3, outEdges.size());
        assertSame(e1, outEdges.get(0));
        assertSame(e2, outEdges.get(1));
        assertSame(e3, outEdges.get(2));
        assertEquals(3, a.getOutDegree());
        a.removeOutEdge(e2);
        outEdges = a.getOutEdges();
        assertEquals(2, outEdges.size());
        assertSame(e1, outEdges.get(0));
        assertSame(e3, outEdges.get(1));
        assertEquals(2, a.getOutDegree());
    }

    @Test
    public void directSuccessor() {
        DirectedGraphNodeSupport a = new DirectedGraphNodeSupport("a");
        DirectedGraphEdge e1 = mock(DirectedGraphEdge.class);
        DirectedGraphEdge e2 = mock(DirectedGraphEdge.class);
        DirectedGraphEdge e3 = mock(DirectedGraphEdge.class);
        DirectedGraphNodeSupport b = new DirectedGraphNodeSupport("b");
        when(e1.getHead()).thenReturn(b);
        DirectedGraphNodeSupport c = new DirectedGraphNodeSupport("c");
        when(e2.getHead()).thenReturn(c);
        DirectedGraphNodeSupport d = new DirectedGraphNodeSupport("d");
        when(e3.getHead()).thenReturn(d);
        a.addOutEdge(e1);
        a.addOutEdge(e2);
        a.addOutEdge(e3);
        List<DirectedGraphNodeSupport> successors = a.getDirectSuccessors();
        assertEquals(3, successors.size());
        assertSame(b, successors.get(0));
        assertSame(c, successors.get(1));
        assertSame(d, successors.get(2));
        assertSame(c, a.getDirectSuccessor("c"));
        assertNull(a.getDirectSuccessor("f"));
    }

    @Test
    public void testEquals() {
        DirectedGraphNodeSupport a1 = new DirectedGraphNodeSupport("a");
        DirectedGraphNodeSupport a2 = new DirectedGraphNodeSupport("a");
        assertEquals(a1, a2);
        DirectedGraphNodeSupport b = new DirectedGraphNodeSupport("b");
        assertFalse(a1.equals(null));
        assertFalse(a1.equals(b));
    }
}
