package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_removeAll_Test {

    @Test
    public void removeAllMatchesCollection_null() {
        Collection c = null;
        CollectionUtil.removeAllMatches(c, "b"::equals);
    }

    @Test
    public void removeAllMatchesCollection() {
        List c = new ArrayList(Arrays.asList("a", "b", "b", "c"));
        CollectionUtil.removeAllMatches(c, "b"::equals);
        assertEquals(2, c.size());
        assertFalse(c.contains("b"));
    }

    @Test
    public void removeAllMatchesCollection_matcherNull() {
        List c = new ArrayList(Arrays.asList("a", "b", "b", "c"));
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.removeAllMatches(c, matcher));
    }

    @Test
    public void removeAllMatchesCollection_bothNull() {
        Collection c = null;
        Predicate matcher = null;
        CollectionUtil.removeAllMatches(c, matcher);
    }

    @Test
    public void removeAllMatchesListIndexMatcher_null() {
        List c = null;
        CollectionUtil.removeAllMatches(c, (i, x) -> i % 2 == 0);
    }

    @Test
    public void removeAllMatchesListIndexMatcher() {
        List c = new ArrayList(Arrays.asList("a", "b", "c", "d"));
        CollectionUtil.removeAllMatches(c, (i, x) -> i % 2 == 0);
        assertEquals(2, c.size());
        assertEquals("b", c.get(0));
        assertEquals("d", c.get(1));
    }

    @Test
    public void removeAllMatchesListIndexMatcher_matcherNull() {
        List c = new ArrayList(Arrays.asList("a", "b", "c", "d"));
        IndexPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.removeAllMatches(c, matcher));
    }

    @Test
    public void removeAllMatchesListIndexMatcher_bothNull() {
        List c = null;
        IndexPredicate matcher = null;
        CollectionUtil.removeAllMatches(c, matcher);
    }

    @Test
    public void removeAllMatchesMap_null() {
        Map map = null;
        CollectionUtil.removeAllMatches(map, (k, v) -> "b".equals(k));
    }

    @Test
    public void removeAllMatchesMap() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2", "c", "3");
        CollectionUtil.removeAllMatches(map, (k, v) -> "b".equals(k));
        assertEquals(2, map.size());
        assertTrue(map.containsKey("a"));
        assertTrue(map.containsValue("3"));
        assertFalse(map.containsKey("b"));
        assertFalse(map.containsValue("2"));
    }

    @Test
    public void removeAllMatchesMap_matcherNull() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2", "c", "3");
        BiPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.removeAllMatches(map, matcher));
    }

    @Test
    public void removeAllMatchesMap_bothNull() {
        Map map = null;
        BiPredicate matcher = null;
        CollectionUtil.removeAllMatches(map, matcher);
    }
}
