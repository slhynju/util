package name.slhynju.util.collection.tree.walker;

import name.slhynju.util.collection.tree.BinaryTreeNodeSupport;
import org.jetbrains.annotations.NotNull;

public class SampleBinaryTreeNode extends BinaryTreeNodeSupport<SampleBinaryTreeNode> {

    public SampleBinaryTreeNode(@NotNull String name) {
        super(name);
    }
}
