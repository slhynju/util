package name.slhynju.util.collection.tree;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class SampleTreeNode extends TreeNodeSupport<SampleTreeNode> {

    private List<SampleTreeNode> children = new ArrayList<>(4);

    public SampleTreeNode(@NotNull String name) {
        super(name);
    }

    @Override
    public @NotNull List<SampleTreeNode> getChildren() {
        return children;
    }

    public void addChild(@NotNull SampleTreeNode child) {
        children.add(child);
    }

    @Override
    public int indexOf(@Nullable SampleTreeNode child) {
        if (child == null) {
            return -1;
        }
        return children.indexOf(child);
    }

    @Override
    public @Nullable SampleTreeNode getPrevSibling() {
        if (parent == null) {
            return null;
        }
        List<SampleTreeNode> siblings = parent.children;
        int index = siblings.indexOf(this);
        return index == 0 ? null : siblings.get(index - 1);
    }

    @Override
    public @Nullable SampleTreeNode getNextSibling() {
        if (parent == null) {
            return null;
        }
        List<SampleTreeNode> siblings = parent.children;
        int index = siblings.indexOf(this);
        return index == siblings.size() - 1 ? null : siblings.get(index + 1);
    }
}
