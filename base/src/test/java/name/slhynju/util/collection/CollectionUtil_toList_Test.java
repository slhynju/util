package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CollectionUtil_toList_Test {


    @Test
    public void array_null() {
        String[] a = null;
        List<String> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
        list.add("abc");
        assertFalse(list.isEmpty());
    }

    @Test
    public void array_empty() {
        String[] a = new String[]{};
        List<String> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void array() {
        String[] a = new String[]{"abc", "def"};
        List<String> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals("abc", list.get(0));
        assertEquals("def", list.get(1));
    }

    @Test
    public void collection_null() {
        List<String> a = null;
        List<String> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
        list.add("abc");
        assertFalse(list.isEmpty());
    }

    @Test
    public void collection_empty() {
        List<String> a = new ArrayList<>();
        List<String> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void collection() {
        List<String> a = Arrays.asList("abc", "def");
        List<String> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals("abc", list.get(0));
        assertEquals("def", list.get(1));
    }

    @Test
    public void tokenizer_null() {
        StringTokenizer tokens = null;
        List<String> list = CollectionUtil.toList(tokens);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void tokenizer() {
        StringTokenizer tokens = new StringTokenizer("abc", "b");
        List<String> list = CollectionUtil.toList(tokens);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals("a", list.get(0));
        assertEquals("c", list.get(1));
    }

    @Test
    public void booleanArray_null() {
        boolean[] a = null;
        List<Boolean> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void booleanArray_empty() {
        boolean[] a = new boolean[]{};
        List<Boolean> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void booleanArray() {
        boolean[] a = new boolean[]{true, false};
        List<Boolean> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals(Boolean.TRUE, list.get(0));
        assertEquals(Boolean.FALSE, list.get(1));
    }


    @Test
    public void intArray_null() {
        int[] a = null;
        List<Integer> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void intArray_empty() {
        int[] a = new int[]{};
        List<Integer> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void intArray() {
        int[] a = new int[]{3, 5};
        List<Integer> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals(Integer.valueOf(3), list.get(0));
        assertEquals(Integer.valueOf(5), list.get(1));
    }

    @Test
    public void longArray_null() {
        long[] a = null;
        List<Long> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void longArray_empty() {
        long[] a = new long[]{};
        List<Long> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void longArray() {
        long[] a = new long[]{3L, 5L};
        List<Long> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals(Long.valueOf(3L), list.get(0));
        assertEquals(Long.valueOf(5L), list.get(1));
    }


    @Test
    public void doubleArray_null() {
        double[] a = null;
        List<Double> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void doubleArray_empty() {
        double[] a = new double[]{};
        List<Double> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void doubleArray() {
        double[] a = new double[]{3.0, 5.0};
        List<Double> list = CollectionUtil.toList(a);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals(Double.valueOf(3.0), list.get(0));
        assertEquals(Double.valueOf(5.0), list.get(1));
    }


    @Test
    public void enumeration_null() {
        Enumeration e = null;
        List list = CollectionUtil.toList(e);
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void enumeration() {
        Enumeration e = mock(Enumeration.class);
        when(e.hasMoreElements()).thenReturn(Boolean.TRUE, Boolean.TRUE, Boolean.FALSE);
        when(e.nextElement()).thenReturn("a", "b");

        List list = CollectionUtil.toList(e);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals("a", list.get(0));
        assertEquals("b", list.get(1));
    }

    @Test
    public void transformList_null() {
        List<String> from = null;
        Function<String, Integer> mapper = x -> x == null ? null : Integer.valueOf(x);
        List<Integer> to = CollectionUtil.transformToList(from, mapper);
        assertNotNull(to);
        assertTrue(to.isEmpty());
    }

    @Test
    public void transformList_mapperNull() {
        List<String> from = Arrays.asList("1", "2", "3");
        Function<String, Integer> mapper = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformToList(from, mapper));
    }

    @Test
    public void transformList_bothNull() {
        List<String> from = null;
        Function<String, Integer> mapper = null;
        List<Integer> to = CollectionUtil.transformToList(from, mapper);
        assertNotNull(to);
        assertTrue(to.isEmpty());
    }

    @Test
    public void transformList() {
        List<String> from = Arrays.asList("1", "2", "3");
        Function<String, Integer> mapper = x -> x == null ? null : Integer.valueOf(x);
        List<Integer> to = CollectionUtil.transformToList(from, mapper);
        assertNotNull(to);
        assertEquals(3, to.size());
        assertEquals(Integer.valueOf(1), to.get(0));
        assertEquals(Integer.valueOf(2), to.get(1));
        assertEquals(Integer.valueOf(3), to.get(2));
    }


    @Test
    public void transformArray_null() {
        String[] from = null;
        Function<String, Integer> mapper = x -> x == null ? null : Integer.valueOf(x);
        List<Integer> to = CollectionUtil.transformToList(from, mapper);
        assertNotNull(to);
        assertTrue(to.isEmpty());
    }

    @Test
    public void transformArray_mapperNull() {
        String[] from = new String[]{"1", "2", "3"};
        Function<String, Integer> mapper = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformToList(from, mapper));
    }

    @Test
    public void transformArray_bothNull() {
        String[] from = null;
        Function<String, Integer> mapper = null;
        List<Integer> to = CollectionUtil.transformToList(from, mapper);
        assertNotNull(to);
        assertTrue(to.isEmpty());
    }

    @Test
    public void transformArray() {
        String[] from = new String[]{"1", "2", "3"};
        Function<String, Integer> mapper = x -> x == null ? null : Integer.valueOf(x);
        List<Integer> to = CollectionUtil.transformToList(from, mapper);
        assertNotNull(to);
        assertEquals(3, to.size());
        assertEquals(Integer.valueOf(1), to.get(0));
        assertEquals(Integer.valueOf(2), to.get(1));
        assertEquals(Integer.valueOf(3), to.get(2));
    }

}
