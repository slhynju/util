package name.slhynju.util.collection.tree;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TreeNodeSupportTest {

    @Test
    public void name() {
        SampleTreeNode node = new SampleTreeNode("root");
        assertEquals("root", node.getName());
        node.setName("root2");
        assertEquals("root2", node.getName());
    }

    @Test
    public void parentChild() {
        SampleTreeNode root = new SampleTreeNode("root");
        SampleTreeNode a = new SampleTreeNode("a");
        a.setParent(root);
        root.addChild(a);
        assertNull(root.getParent());
        assertTrue(a.getParent() == root);
        assertTrue(root.isRoot());
        assertFalse(a.isRoot());
        assertFalse(root.isLeaf());
        assertTrue(a.isLeaf());
        assertTrue(root.hasChildren());
        assertFalse(a.hasChildren());
        List<SampleTreeNode> children = root.getChildren();
        assertEquals(1, children.size());
        assertSame(a, children.get(0));
        assertTrue(a.isFirstChild());
        assertTrue(a.isLastChild());
        assertSame(a, root.getFirstChild());
        assertSame(a, root.getLastChild());
        assertNull(a.getPrevSibling());
        assertNull(a.getNextSibling());
    }

    @Test
    public void isFirstChild() {
        SampleTreeNode root = new SampleTreeNode("root");
        SampleTreeNode a = new SampleTreeNode("a");
        a.setParent(root);
        root.addChild(a);
        SampleTreeNode b = new SampleTreeNode("b");
        b.setParent(root);
        root.addChild(b);
        assertFalse(root.isFirstChild());
        assertTrue(a.isFirstChild());
        assertFalse(b.isFirstChild());
    }

    @Test
    public void isLastChild() {
        SampleTreeNode root = new SampleTreeNode("root");
        SampleTreeNode a = new SampleTreeNode("a");
        a.setParent(root);
        root.addChild(a);
        SampleTreeNode b = new SampleTreeNode("b");
        b.setParent(root);
        root.addChild(b);
        assertFalse(root.isLastChild());
        assertFalse(a.isLastChild());
        assertTrue(b.isLastChild());
    }

    @Test
    public void getFirstChild() {
        SampleTreeNode root = new SampleTreeNode("root");
        SampleTreeNode a = new SampleTreeNode("a");
        a.setParent(root);
        root.addChild(a);
        SampleTreeNode b = new SampleTreeNode("b");
        b.setParent(root);
        root.addChild(b);
        assertSame(a, root.getFirstChild());
        assertNull(a.getFirstChild());
        assertNull(b.getFirstChild());
    }

    @Test
    public void getLastChild() {
        SampleTreeNode root = new SampleTreeNode("root");
        SampleTreeNode a = new SampleTreeNode("a");
        a.setParent(root);
        root.addChild(a);
        SampleTreeNode b = new SampleTreeNode("b");
        b.setParent(root);
        root.addChild(b);
        assertSame(b, root.getLastChild());
        assertNull(a.getLastChild());
        assertNull(b.getLastChild());
    }

    /*@Test
    public void previousSibling() {
        SampleTreeNode root = new SampleTreeNode("root");
        SampleTreeNode a = new SampleTreeNode("a");
        a.setParent(root);
        root.addChild(a);
        SampleTreeNode b = new SampleTreeNode("b");
        b.setParent(root);
        root.addChild(b);
        assertNull(root.getPrevSibling());
        assertNull(a.getPrevSibling());
        assertSame(a, b.getPrevSibling());
    }

    @Test
    public void nextSibling() {
        SampleTreeNode root = new SampleTreeNode("root");
        SampleTreeNode a = new SampleTreeNode("a");
        a.setParent(root);
        root.addChild(a);
        SampleTreeNode b = new SampleTreeNode("b");
        b.setParent(root);
        root.addChild(b);
        assertNull(root.getNextSibling());
        assertSame(b, a.getNextSibling());
        assertNull(b.getNextSibling());
    }*/

    @Test
    public void attribute() {
        SampleTreeNode root = new SampleTreeNode("root");
        root.setAttribute("key1", "value1");
        assertEquals("value1", root.getStringAttribute("key1"));
        assertTrue(root.containsAttribute("key1"));
        root.removeAttribute("key1");
        assertFalse(root.containsAttribute("key1"));
    }

    @Test
    public void testEquals() {
        SampleTreeNode a = new SampleTreeNode("a");
        SampleTreeNode a2 = new SampleTreeNode("a");
        assertEquals(a, a2);
        SampleTreeNode b = new SampleTreeNode("b");
        assertNotEquals(a, b);
        assertFalse(a.equals(null));
        assertFalse(a.equals("abc"));
    }

    @Test
    public void toStringTest() {
        SampleTreeNode root = new SampleTreeNode("root");
        assertEquals("root", root.toString());
        SampleTreeNode root2 = new SampleTreeNode("root");
        assertEquals(root, root2);
        assertEquals(root.toString(), root2.toString());
        assertEquals(root.hashCode(), root2.hashCode());
    }
}
