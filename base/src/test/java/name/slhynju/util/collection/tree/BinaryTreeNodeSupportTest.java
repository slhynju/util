package name.slhynju.util.collection.tree;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"rawtypes", "unchecked", "static-method"})
public class BinaryTreeNodeSupportTest {

    @Test
    public void leftChild() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("a");
        BinaryTreeNodeSupport left = new BinaryTreeNodeSupport("b");
        node.setLeftChild(left);
        assertEquals(left, node.getLeftChild());
    }

    @Test
    public void rightChild() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("a");
        BinaryTreeNodeSupport right = new BinaryTreeNodeSupport("b");
        node.setRightChild(right);
        assertEquals(right, node.getRightChild());
    }

    @Test
    public void children() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("a");
        BinaryTreeNodeSupport left = new BinaryTreeNodeSupport("b");
        left.setParent(node);
        node.setLeftChild(left);
        BinaryTreeNodeSupport right = new BinaryTreeNodeSupport("c");
        right.setParent(node);
        node.setRightChild(right);
        List<BinaryTreeNodeSupport> list = Arrays.asList(left, right);
        assertEquals(list, node.getChildren());
        assertFalse(node.isLeftChild());
        assertTrue(left.isLeftChild());
        assertFalse(right.isLeftChild());
        assertFalse(node.isRightChild());
        assertFalse(left.isRightChild());
        assertTrue(right.isRightChild());
    }

    @Test
    public void indexOf() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("a");
        BinaryTreeNodeSupport left = new BinaryTreeNodeSupport("b");
        left.setParent(node);
        node.setLeftChild(left);
        BinaryTreeNodeSupport right = new BinaryTreeNodeSupport("c");
        right.setParent(node);
        node.setRightChild(right);
        assertEquals(0, node.indexOf(left));
        assertEquals(1, node.indexOf(right));
        assertEquals(-1, node.indexOf(null));
        BinaryTreeNodeSupport nodeD = new BinaryTreeNodeSupport("d");
        left.setRightChild(nodeD);
        assertEquals(0, left.indexOf(nodeD));
        assertEquals(-1, right.indexOf(nodeD));
    }

    @Test
    public void parent() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("a");
        BinaryTreeNodeSupport parent = new BinaryTreeNodeSupport("b");
        node.setParent(parent);
        assertEquals(parent, node.getParent());
    }

    @Test
    public void attribute() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("c");
        node.setAttribute("key", "value");
        String value = (String) node.getAttribute("key");
        assertEquals("value", value);
    }

    @Test
    public void root() {
        BinaryTreeNodeSupport node = new BinaryTreeNodeSupport("a");
        BinaryTreeNodeSupport parent = new BinaryTreeNodeSupport("b");
        node.setParent(parent);
        parent.setLeftChild(node);
        assertTrue(node.isLeaf());
        assertTrue(parent.isRoot());
        assertFalse(node.isRoot());
        assertFalse(parent.isLeaf());
        assertTrue(parent.hasChildren());
        assertFalse(node.hasChildren());
    }

    @Test
    public void firstChild() {
        BinaryTreeNodeSupport root = new BinaryTreeNodeSupport("root");
        BinaryTreeNodeSupport a = new BinaryTreeNodeSupport("a");
        a.setParent(root);
        root.setLeftChild(a);
        BinaryTreeNodeSupport b = new BinaryTreeNodeSupport("b");
        b.setParent(root);
        root.setRightChild(b);
        assertSame(a, root.getFirstChild());
        assertNull(a.getFirstChild());
        assertNull(b.getFirstChild());
    }

    @Test
    public void firstChild2() {
        BinaryTreeNodeSupport root = new BinaryTreeNodeSupport("root");
        BinaryTreeNodeSupport mid = new BinaryTreeNodeSupport("mid");
        mid.setParent(root);
        root.setLeftChild(mid);
        BinaryTreeNodeSupport leaf = new BinaryTreeNodeSupport("leaf");
        leaf.setParent(mid);
        mid.setRightChild(leaf);
        assertSame(mid, root.getFirstChild());
        assertSame(leaf, mid.getFirstChild());
        assertNull(leaf.getFirstChild());
    }

    @Test
    public void lastChild() {
        BinaryTreeNodeSupport root = new BinaryTreeNodeSupport("root");
        BinaryTreeNodeSupport a = new BinaryTreeNodeSupport("a");
        a.setParent(root);
        root.setLeftChild(a);
        BinaryTreeNodeSupport b = new BinaryTreeNodeSupport("b");
        b.setParent(root);
        root.setRightChild(b);
        assertSame(b, root.getLastChild());
        assertNull(a.getLastChild());
        assertNull(b.getLastChild());
    }

    @Test
    public void lastChild2() {
        BinaryTreeNodeSupport root = new BinaryTreeNodeSupport("root");
        BinaryTreeNodeSupport mid = new BinaryTreeNodeSupport("mid");
        mid.setParent(root);
        root.setLeftChild(mid);
        BinaryTreeNodeSupport leaf = new BinaryTreeNodeSupport("leaf");
        leaf.setParent(mid);
        mid.setRightChild(leaf);
        assertSame(mid, root.getLastChild());
        assertSame(leaf, mid.getLastChild());
        assertNull(leaf.getLastChild());
    }

    @Test
    public void prevSibling() {
        BinaryTreeNodeSupport root = new BinaryTreeNodeSupport("root");
        BinaryTreeNodeSupport a = new BinaryTreeNodeSupport("a");
        a.setParent(root);
        root.setLeftChild(a);
        BinaryTreeNodeSupport b = new BinaryTreeNodeSupport("b");
        b.setParent(root);
        root.setRightChild(b);
        assertNull(root.getPrevSibling());
        assertNull(a.getPrevSibling());
        assertSame(a, b.getPrevSibling());
    }

    @Test
    public void nextSibling() {
        BinaryTreeNodeSupport root = new BinaryTreeNodeSupport("root");
        BinaryTreeNodeSupport a = new BinaryTreeNodeSupport("a");
        a.setParent(root);
        root.setLeftChild(a);
        BinaryTreeNodeSupport b = new BinaryTreeNodeSupport("b");
        b.setParent(root);
        root.setRightChild(b);
        assertNull(root.getNextSibling());
        assertSame(b, a.getNextSibling());
        assertNull(b.getNextSibling());
    }

    @Test
    public void testEquals() {
        BinaryTreeNodeSupport a = new BinaryTreeNodeSupport("a");
        BinaryTreeNodeSupport a2 = new BinaryTreeNodeSupport("a");
        assertEquals(a, a2);
        BinaryTreeNodeSupport b = new BinaryTreeNodeSupport("b");
        assertNotEquals(a, b);
        assertFalse(a.equals(null));
        assertFalse(a.equals("abc"));
    }

    @Test
    public void toStringTest() {
        BinaryTreeNodeSupport root = new BinaryTreeNodeSupport("root");
        assertEquals("root", root.toString());
        BinaryTreeNodeSupport root2 = new BinaryTreeNodeSupport("root");
        assertEquals(root, root2);
        assertEquals(root.toString(), root2.toString());
        assertEquals(root.hashCode(), root2.hashCode());
    }


}
