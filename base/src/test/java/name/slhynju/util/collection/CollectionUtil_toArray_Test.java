package name.slhynju.util.collection;

import name.slhynju.util.DateUtil;
import name.slhynju.util.reflect.ClassUtil;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_toArray_Test {

    @Test
    public void booleanArray_null() {
        List<Boolean> list = null;
        boolean[] array = CollectionUtil.toBooleanArray(list, false);
        assertNotNull(array);
        assertEquals(0, array.length);
    }

    @Test
    public void booleanArray() {
        List<Boolean> list = Arrays.asList(Boolean.FALSE, null, Boolean.TRUE);
        boolean[] array = CollectionUtil.toBooleanArray(list, false);
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals(false, array[0]);
        assertEquals(false, array[1]);
        assertEquals(true, array[2]);
    }

    @Test
    public void intArray_null() {
        List<Integer> list = null;
        int[] array = CollectionUtil.toIntArray(list, 4);
        assertNotNull(array);
        assertEquals(0, array.length);
    }

    @Test
    public void intArray() {
        List<Integer> list = Arrays.asList(Integer.valueOf(1), null, Integer.valueOf(3));
        int[] array = CollectionUtil.toIntArray(list, 4);
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals(1, array[0]);
        assertEquals(4, array[1]);
        assertEquals(3, array[2]);
    }

    @Test
    public void longArray_null() {
        List<Long> list = null;
        long[] array = CollectionUtil.toLongArray(list, 3L);
        assertNotNull(array);
        assertEquals(0, array.length);
    }

    @Test
    public void longArray() {
        List<Long> list = Arrays.asList(Long.valueOf(1L), null, Long.valueOf(4L));
        long[] array = CollectionUtil.toLongArray(list, 3L);
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals(1L, array[0]);
        assertEquals(3L, array[1]);
        assertEquals(4L, array[2]);
    }

    @Test
    public void doubleArray_null() {
        List<Double> list = null;
        double[] array = CollectionUtil.toDoubleArray(list, 3.0);
        assertNotNull(array);
        assertEquals(0, array.length);
    }

    @Test
    public void doubleArray() {
        List<Double> list = Arrays.asList(Double.valueOf(1.0), null, Double.valueOf(4.0));
        double[] array = CollectionUtil.toDoubleArray(list, 3.0);
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals(1.0, array[0], 0.0001);
        assertEquals(3.0, array[1], 0.0001);
        assertEquals(4.0, array[2], 0.0001);
    }

    @Test
    public void stringArray_null() {
        List<String> list = null;
        String[] array = CollectionUtil.toStringArray(list);
        assertNotNull(array);
        assertEquals(0, array.length);
    }

    @Test
    public void stringArray() {
        List<String> list = Arrays.asList("a", null, "c");
        String[] array = CollectionUtil.toStringArray(list);
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals("a", array[0]);
        assertNull(array[1]);
        assertEquals("c", array[2]);
    }

    @Test
    public void dateArray_null() {
        List<Date> list = null;
        Date[] array = CollectionUtil.toDateArray(list);
        assertNotNull(array);
        assertEquals(0, array.length);
    }

    @Test
    public void dateArray() {
        List<Date> list = new ArrayList<>();
        Date d1 = DateUtil.toDate("1980-03-04", "yyyy-MM-dd");
        list.add(d1);
        list.add(null);
        Date d3 = DateUtil.toDate("1982-04-09", "yyyy-MM-dd");
        list.add(d3);
        Date[] array = CollectionUtil.toDateArray(list);
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals(d1, array[0]);
        assertNull(array[1]);
        assertEquals(d3, array[2]);
    }

    @Test
    public void classArray_null() {
        List<Class<?>> list = null;
        Class[] array = CollectionUtil.toClassArray(list);
        assertNotNull(array);
        assertEquals(0, array.length);
    }

    @Test
    public void classArray() {
        List<Class<?>> list = new ArrayList<>();
        list.add(String.class);
        list.add(null);
        list.add(Date.class);
        Class[] array = CollectionUtil.toClassArray(list);
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals(String.class, array[0]);
        assertNull(array[1]);
        assertEquals(Date.class, array[2]);
    }

    @Test
    public void methodArray_null() {
        List<Method> list = null;
        Method[] array = CollectionUtil.toMethodArray(list);
        assertNotNull(array);
        assertEquals(0, array.length);
    }

    @Test
    public void methodArray() {
        List<Method> list = new ArrayList<>();
        Method m1 = ClassUtil.getMethod(List.class, "isEmpty");
        list.add(m1);
        list.add(null);
        Method[] array = CollectionUtil.toMethodArray(list);
        assertNotNull(array);
        assertEquals(2, array.length);
        assertEquals(m1, array[0]);
        assertNull(array[1]);
    }
}
