package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;


public class CollectionUtil_getFirst_Test {

    @Test
    public void list_null() {
        List<String> list = null;
        String s = CollectionUtil.getFirst(list);
        assertNull(s);
    }

    @Test
    public void list_empty() {
        List<String> list = new ArrayList<>();
        String s = CollectionUtil.getFirst(list);
        assertNull(s);
    }

    @Test
    public void list() {
        List<String> list = Arrays.asList("a", "b", "c");
        String s = CollectionUtil.getFirst(list);
        assertEquals("a", s);
    }

    @Test
    public void array_null() {
        String[] a = null;
        String s = CollectionUtil.getFirst(a);
        assertNull(s);
    }

    @Test
    public void array_empty() {
        String[] a = new String[]{};
        String s = CollectionUtil.getFirst(a);
        assertNull(s);
    }

    @Test
    public void array() {
        String[] a = new String[]{"a", "b", "c"};
        String s = CollectionUtil.getFirst(a);
        assertEquals("a", s);
    }

    @Test
    public void listMatcher_null() {
        List<String> list = null;
        String s = CollectionUtil.getFirst(list, "b"::equals);
        assertNull(s);
    }

    @Test
    public void listMatcher_empty() {
        List<String> list = new ArrayList<>();
        String s = CollectionUtil.getFirst(list, "b"::equals);
        assertNull(s);
    }

    @Test
    public void listMatcher() {
        List<String> list = Arrays.asList("a", "b", "c");
        String s = CollectionUtil.getFirst(list, "b"::equals);
        assertEquals("b", s);
    }

    @Test
    public void listMatcher_notFound() {
        List<String> list = Arrays.asList("a", "b", "c");
        String s = CollectionUtil.getFirst(list, "d"::equals);
        assertNull(s);
    }

    @Test
    public void listMatcher_matcherNull() {
        List<String> list = Arrays.asList("a", "b", "c");
        Predicate<String> matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.getFirst(list, matcher));
    }

    @Test
    public void listMatcher_bothNull() {
        List<String> list = null;
        Predicate<String> matcher = null;
        assertNull(CollectionUtil.getFirst(list, matcher));
    }

    @Test
    public void listIndexMatcher_null() {
        List<String> list = null;
        String s = CollectionUtil.getFirst(list, (i, x) -> i == 2);
        assertNull(s);
    }

    @Test
    public void listIndexMatcher_empty() {
        List<String> list = new ArrayList<>();
        String s = CollectionUtil.getFirst(list, (i, x) -> i == 2);
        assertNull(s);
    }

    @Test
    public void listIndexMatcher() {
        List<String> list = Arrays.asList("a", "b", "c");
        String s = CollectionUtil.getFirst(list, (i, x) -> i == 2);
        assertEquals("c", s);
    }

    @Test
    public void listIndexMatcher_notFound() {
        List<String> list = Arrays.asList("a", "b", "c");
        String s = CollectionUtil.getFirst(list, (i, x) -> i == 5);
        assertNull(s);
    }

    @Test
    public void listIndexMatcher_matcherNull() {
        List<String> list = Arrays.asList("a", "b", "c");
        IndexPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.getFirst(list, matcher));
    }

    @Test
    public void listIndexMatcher_bothNull() {
        List<String> list = null;
        IndexPredicate matcher = null;
        assertNull(CollectionUtil.getFirst(list, matcher));
    }

    @Test
    public void arrayMatcher_null() {
        String[] a = null;
        String s = CollectionUtil.getFirst(a, "b"::equals);
        assertNull(s);
    }

    @Test
    public void arrayMatcher_empty() {
        String[] a = new String[]{};
        String s = CollectionUtil.getFirst(a, "b"::equals);
        assertNull(s);
    }

    @Test
    public void arrayMatcher() {
        String[] a = new String[]{"a", "b", "c"};
        String s = CollectionUtil.getFirst(a, "b"::equals);
        assertEquals("b", s);
    }

    @Test
    public void arrayMatcher_notFound() {
        String[] a = new String[]{"a", "b", "c"};
        String s = CollectionUtil.getFirst(a, "d"::equals);
        assertNull(s);
    }

    @Test
    public void arrayMatcher_matcherNull() {
        String[] a = new String[]{"a", "b", "c"};
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.getFirst(a, matcher));
    }

    @Test
    public void arrayMatcher_bothNull() {
        String[] a = null;
        Predicate matcher = null;
        assertNull(CollectionUtil.getFirst(a, matcher));
    }

    @Test
    public void arrayIndexMatcher_null() {
        String[] a = null;
        String s = CollectionUtil.getFirst(a, (i, x) -> i == 2);
        assertNull(s);
    }

    @Test
    public void arrayIndexMatcher_empty() {
        String[] a = new String[]{};
        String s = CollectionUtil.getFirst(a, (i, x) -> i == 2);
        assertNull(s);
    }

    @Test
    public void arrayIndexMatcher() {
        String[] a = new String[]{"a", "b", "c"};
        String s = CollectionUtil.getFirst(a, (i, x) -> i == 2);
        assertEquals("c", s);
    }

    @Test
    public void arrayIndexMatcher_notFound() {
        String[] a = new String[]{"a", "b", "c"};
        String s = CollectionUtil.getFirst(a, (i, x) -> i == 5);
        assertNull(s);
    }

    @Test
    public void arrayIndexMatcher_matcherNull() {
        String[] a = new String[]{"a", "b", "c"};
        IndexPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.getFirst(a, matcher));
    }

    @Test
    public void arrayIndexMatcher_bothNull() {
        String[] a = null;
        IndexPredicate matcher = null;
        assertNull(CollectionUtil.getFirst(a, matcher));
    }

}
