package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_sub_Test {

    @Test
    public void subList_null() {
        List list = null;
        List sub = CollectionUtil.sub(list, "b"::equals);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subList() {
        List list = Arrays.asList("a", "b", "c");
        List sub = CollectionUtil.sub(list, "b"::equals);
        assertNotNull(sub);
        assertEquals(1, sub.size());
        assertEquals("b", sub.get(0));
    }

    @Test
    public void subList_matcherNull() {
        List list = Arrays.asList("a", "b", "c");
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.sub(list, matcher));
    }

    @Test
    public void subList_bothNull() {
        List list = null;
        Predicate matcher = null;
        List sub = CollectionUtil.sub(list, matcher);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subListIndexMatcher_null() {
        List list = null;
        List sub = CollectionUtil.sub(list, (i, x) -> i % 2 == 0);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subListIndexMatcher() {
        List list = Arrays.asList("a", "b", "c", "d");
        List sub = CollectionUtil.sub(list, (i, x) -> i % 2 == 0);
        assertNotNull(sub);
        assertEquals(2, sub.size());
        assertEquals("a", sub.get(0));
        assertEquals("c", sub.get(1));
    }

    @Test
    public void subListIndexMatcher_matcherNull() {
        List list = Arrays.asList("a", "b", "c");
        IndexPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.sub(list, matcher));
    }

    @Test
    public void subListIndexMatcher_bothNull() {
        List list = null;
        IndexPredicate matcher = null;
        List sub = CollectionUtil.sub(list, matcher);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subArray_null() {
        String[] a = null;
        List sub = CollectionUtil.sub(a, "b"::equals);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subArray() {
        String[] a = new String[]{"a", "b", "c"};
        List sub = CollectionUtil.sub(a, "b"::equals);
        assertNotNull(sub);
        assertEquals(1, sub.size());
        assertEquals("b", sub.get(0));
    }

    @Test
    public void subArray_matcherNull() {
        String[] a = new String[]{"a", "b", "c"};
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.sub(a, matcher));
    }

    @Test
    public void subArray_bothNull() {
        String[] a = null;
        Predicate matcher = null;
        List sub = CollectionUtil.sub(a, matcher);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }


    @Test
    public void subArrayIndexMatcher_null() {
        String[] a = null;
        List sub = CollectionUtil.sub(a, (i, x) -> i % 2 == 0);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subArrayIndexMatcher() {
        String[] a = new String[]{"a", "b", "c", "d"};
        List sub = CollectionUtil.sub(a, (i, x) -> i % 2 == 0);
        assertNotNull(sub);
        assertEquals(2, sub.size());
        assertEquals("a", sub.get(0));
        assertEquals("c", sub.get(1));
    }

    @Test
    public void subArrayIndexMatcher_matcherNull() {
        String[] a = new String[]{"a", "b", "c", "d"};
        IndexPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.sub(a, matcher));
    }

    @Test
    public void subArrayIndexMatcher_bothNull() {
        String[] a = null;
        IndexPredicate matcher = null;
        List sub = CollectionUtil.sub(a, matcher);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subSet_null() {
        Set set = null;
        Set sub = CollectionUtil.sub(set, "b"::equals);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subSet() {
        Set set = new HashSet(Arrays.asList("a", "b", "c"));
        Set sub = CollectionUtil.sub(set, "b"::equals);
        assertNotNull(sub);
        assertEquals(1, sub.size());
        assertTrue(sub.contains("b"));
    }

    @Test
    public void subSet_matcherNull() {
        Set set = new HashSet(Arrays.asList("a", "b", "c"));
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.sub(set, matcher));
    }

    @Test
    public void subSet_bothNull() {
        Set set = null;
        Predicate matcher = null;
        Set sub = CollectionUtil.sub(set, matcher);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }


    @Test
    public void subMap_null() {
        Map map = null;
        Map sub = CollectionUtil.sub(map, (k, v) -> "b".equals(k));
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subMap() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2");
        Map sub = CollectionUtil.sub(map, (k, v) -> "b".equals(k));
        assertNotNull(sub);
        assertEquals(1, sub.size());
        assertTrue(sub.containsKey("b"));
    }

    @Test
    public void subMap_matcherNull() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2");
        BiPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.sub(map, matcher));
    }

    @Test
    public void subMap_bothNull() {
        Map map = null;
        BiPredicate matcher = null;
        Map sub = CollectionUtil.sub(map, matcher);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }


    @Test
    public void subKeys_null() {
        Map map = null;
        List<String> sub = CollectionUtil.subKeys(map, "b"::equals);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subKeys() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2");
        List<String> sub = CollectionUtil.subKeys(map, "b"::equals);
        assertNotNull(sub);
        assertEquals(1, sub.size());
        assertTrue(sub.contains("b"));
    }

    @Test
    public void subKeys_matcherNull() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2");
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.subKeys(map, matcher));
    }

    @Test
    public void subKeys_bothNull() {
        Map map = null;
        Predicate matcher = null;
        List sub = CollectionUtil.subKeys(map, matcher);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }


    @Test
    public void subValues_null() {
        Map map = null;
        List<String> sub = CollectionUtil.subValues(map, "2"::equals);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

    @Test
    public void subValues() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2");
        List<String> sub = CollectionUtil.subValues(map, "2"::equals);
        assertNotNull(sub);
        assertEquals(1, sub.size());
        assertTrue(sub.contains("2"));
    }

    @Test
    public void subValues_matcherNull() {
        Map map = CollectionUtil.toMap("a", "1", "b", "2");
        Predicate matcher = null;
        assertThrows(NullPointerException.class, () -> CollectionUtil.subValues(map, matcher));
    }

    @Test
    public void subValues_bothNull() {
        Map map = null;
        Predicate matcher = null;
        List sub = CollectionUtil.subValues(map, matcher);
        assertNotNull(sub);
        assertTrue(sub.isEmpty());
    }

}
