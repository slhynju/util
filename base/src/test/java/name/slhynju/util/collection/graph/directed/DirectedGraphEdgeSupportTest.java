package name.slhynju.util.collection.graph.directed;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DirectedGraphEdgeSupportTest {

    @Test
    public void tail() {
        DirectedGraphNodeSupport a = new DirectedGraphNodeSupport("a");
        DirectedGraphNodeSupport b = new DirectedGraphNodeSupport("b");
        DirectedGraphEdgeSupport e = new DirectedGraphEdgeSupport(a, b);
        assertSame(a, e.getTail());
        assertSame(b, e.getHead());
    }

    @Test
    public void testEquals() {
        DirectedGraphNodeSupport a = new DirectedGraphNodeSupport("a");
        DirectedGraphNodeSupport b = new DirectedGraphNodeSupport("b");
        DirectedGraphEdgeSupport e = new DirectedGraphEdgeSupport(a, b);
        DirectedGraphEdgeSupport e2 = new DirectedGraphEdgeSupport(a, b);
        assertEquals(e, e2);
        DirectedGraphEdgeSupport e3 = new DirectedGraphEdgeSupport(b, a);
        assertNotEquals(e, e3);
        DirectedGraphNodeSupport b2 = new DirectedGraphNodeSupport("b");
        DirectedGraphEdgeSupport e4 = new DirectedGraphEdgeSupport(a, b2);
        assertNotEquals(e, e4);
        DirectedGraphNodeSupport c = new DirectedGraphNodeSupport("c");
        DirectedGraphEdgeSupport e5 = new DirectedGraphEdgeSupport(a, c);
        assertNotEquals(e, e5);
    }

    @Test
    public void testHashCode() {
        DirectedGraphNodeSupport a = new DirectedGraphNodeSupport("a");
        DirectedGraphNodeSupport b = new DirectedGraphNodeSupport("b");
        DirectedGraphEdgeSupport e = new DirectedGraphEdgeSupport(a, b);
        DirectedGraphEdgeSupport e2 = new DirectedGraphEdgeSupport(a, b);
        assertEquals(e.hashCode(), e2.hashCode());
        DirectedGraphEdgeSupport e3 = new DirectedGraphEdgeSupport(b, a);
        assertNotEquals(e.hashCode(), e3.hashCode());
        DirectedGraphNodeSupport b2 = new DirectedGraphNodeSupport("b");
        DirectedGraphEdgeSupport e4 = new DirectedGraphEdgeSupport(a, b2);
        assertEquals(e.hashCode(), e4.hashCode());
        DirectedGraphNodeSupport c = new DirectedGraphNodeSupport("c");
        DirectedGraphEdgeSupport e5 = new DirectedGraphEdgeSupport(a, c);
        assertNotEquals(e.hashCode(), e5.hashCode());
    }

    @Test
    public void testToString() {
        DirectedGraphNodeSupport a = new DirectedGraphNodeSupport("a");
        DirectedGraphNodeSupport b = new DirectedGraphNodeSupport("b");
        DirectedGraphEdgeSupport e = new DirectedGraphEdgeSupport(a, b);
        assertEquals("a -> b", e.toString());
    }

}
