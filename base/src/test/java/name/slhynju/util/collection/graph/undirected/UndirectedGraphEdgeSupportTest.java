package name.slhynju.util.collection.graph.undirected;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UndirectedGraphEdgeSupportTest {

    @Test
    public void getNodes() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNode b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, b);
        List<UndirectedGraphNode> nodes = e.getNodes();
        assertEquals(2, nodes.size());
        assertSame(a, nodes.get(0));
        assertSame(b, nodes.get(1));
    }

    @Test
    public void getNodes2() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, a);
        List<UndirectedGraphNode> nodes = e.getNodes();
        assertEquals(1, nodes.size());
        assertSame(a, nodes.get(0));
    }

    @Test
    public void getNodeArray() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNode b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphNode[] nodes = e.getNodeArray();
        assertEquals(2, nodes.length);
        assertSame(a, nodes[0]);
        assertSame(b, nodes[1]);
    }

    @Test
    public void getNodeArray2() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, a);
        UndirectedGraphNode[] nodes = e.getNodeArray();
        assertEquals(1, nodes.length);
        assertSame(a, nodes[0]);
    }

    @Test
    public void getOtherEnd() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNode b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, b);
        assertSame(b, e.getOtherEnd(a));
        assertSame(a, e.getOtherEnd(b));
        assertThrows(IllegalArgumentException.class, () -> e.getOtherEnd(null));
        UndirectedGraphNode b2 = new UndirectedGraphNodeSupport("b");
        assertThrows(IllegalArgumentException.class, () -> e.getOtherEnd(b2));
    }

    @Test
    public void getOtherEnd2() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, a);
        assertSame(a, e.getOtherEnd(a));
    }

    @Test
    public void containsEnd() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNode b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, b);
        assertTrue(e.containsEnd(a));
        assertTrue(e.containsEnd(b));
        UndirectedGraphNode c = new UndirectedGraphNodeSupport("c");
        assertFalse(e.containsEnd(c));
    }

    @Test
    public void containsEnds() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNode b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, b);
        assertTrue(e.containsEnds(a, b));
        assertTrue(e.containsEnds(b, a));
        UndirectedGraphNode b2 = new UndirectedGraphNodeSupport("b");
        assertFalse(e.containsEnds(a, b2));
        assertFalse(e.containsEnds(b2, a));
    }

    @Test
    public void testEquals() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNode b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e1 = new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphEdgeSupport e2 = new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphEdgeSupport e3 = new UndirectedGraphEdgeSupport(b, a);
        assertEquals(e1, e2);
        assertEquals(e2, e3);
        assertEquals(e1, e3);
        UndirectedGraphNode b2 = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e4 = new UndirectedGraphEdgeSupport(a, b2);
        assertNotEquals(e1, e4);
    }

    @Test
    public void testHashCode() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNode b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e1 = new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphEdgeSupport e2 = new UndirectedGraphEdgeSupport(a, b);
        UndirectedGraphEdgeSupport e3 = new UndirectedGraphEdgeSupport(b, a);
        assertEquals(e1.hashCode(), e2.hashCode());
        assertEquals(e2.hashCode(), e3.hashCode());
        assertEquals(e1.hashCode(), e3.hashCode());
        UndirectedGraphNode c = new UndirectedGraphNodeSupport("c");
        UndirectedGraphEdgeSupport e4 = new UndirectedGraphEdgeSupport(a, c);
        assertNotEquals(e1, e4);
    }

    @Test
    public void testToString() {
        UndirectedGraphNode a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNode b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphEdgeSupport e = new UndirectedGraphEdgeSupport(a, b);
        assertEquals("a -- b", e.toString());
    }

}
