package name.slhynju.util.collection.graph.undirected;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UndirectedGraphNodeSupportTest {

    @Test
    public void testConstructor() {
        UndirectedGraphNodeSupport n = new UndirectedGraphNodeSupport("a");
        assertEquals("a", n.getName());
    }

    @Test
    public void edges() {
        UndirectedGraphNodeSupport n = new UndirectedGraphNodeSupport("a");
        UndirectedGraphEdge e1 = mock(UndirectedGraphEdge.class);
        UndirectedGraphEdge e2 = mock(UndirectedGraphEdge.class);
        n.addEdge(e1);
        n.addEdge(e2);
        List<UndirectedGraphEdge> edges = n.getEdges();
        assertEquals(2, edges.size());
        assertSame(e1, edges.get(0));
        assertSame(e2, edges.get(1));
        assertEquals(2, n.getDegree());
        n.removeEdge(null);
        n.removeEdge(e1);
        edges = n.getEdges();
        assertEquals(1, edges.size());
        assertSame(e2, edges.get(0));
        assertEquals(1, n.getDegree());
    }

    @Test
    public void connectedNodes() {
        UndirectedGraphNodeSupport a = new UndirectedGraphNodeSupport("a");
        UndirectedGraphEdge e1 = mock(UndirectedGraphEdge.class);
        UndirectedGraphEdge e2 = mock(UndirectedGraphEdge.class);
        a.addEdge(e1);
        a.addEdge(e2);
        UndirectedGraphNodeSupport b = new UndirectedGraphNodeSupport("b");
        UndirectedGraphNodeSupport c = new UndirectedGraphNodeSupport("c");
        when(e1.getOtherEnd(a)).thenReturn(b);
        when(e2.getOtherEnd(a)).thenReturn(c);
        List<UndirectedGraphNodeSupport> connectedNodes = a.getAdjacentNodes();
        assertEquals(2, connectedNodes.size());
        assertSame(b, connectedNodes.get(0));
        assertSame(c, connectedNodes.get(1));
    }

    @Test
    public void testEquals() {
        UndirectedGraphNodeSupport a1 = new UndirectedGraphNodeSupport("a");
        UndirectedGraphNodeSupport a2 = new UndirectedGraphNodeSupport("a");
        assertEquals(a1, a2);
        UndirectedGraphNodeSupport b = new UndirectedGraphNodeSupport("b");
        assertFalse(a1.equals(null));
        assertFalse(a1.equals(b));
    }
}
