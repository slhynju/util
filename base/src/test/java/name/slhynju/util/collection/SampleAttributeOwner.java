package name.slhynju.util.collection;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class SampleAttributeOwner implements AttributeOwner {

    private Map<String, Object> map = new HashMap<>();

    @Override
    public boolean containsAttribute(@NotNull String key) {
        return map.containsKey(key);
    }

    @Override
    public @Nullable Object getAttribute(@NotNull String key) {
        return map.get(key);
    }

    @Override
    public void setAttribute(@NotNull String key, @Nullable Object value) {
        map.put(key, value);
    }

    @Override
    public void removeAttribute(@NotNull String key) {
        map.remove(key);
    }
}
