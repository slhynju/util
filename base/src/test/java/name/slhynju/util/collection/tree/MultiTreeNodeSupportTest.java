package name.slhynju.util.collection.tree;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"rawtypes", "unchecked", "static-method"})
public class MultiTreeNodeSupportTest {

    @Test
    public void getChild() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        MultiTreeNodeSupport child2 = new MultiTreeNodeSupport("c");
        node.addChild(child1);
        node.addChild(child2);
        assertEquals(child1, node.getChild(0));
        assertEquals(child2, node.getChild(1));
    }

    @Test
    public void setChild() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        MultiTreeNodeSupport child2 = new MultiTreeNodeSupport("c");
        node.addChild(child1);
        node.setChild(0, child2);
        assertEquals(child2, node.getChild(0));
    }

    @Test
    public void setChild2() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        MultiTreeNodeSupport child2 = new MultiTreeNodeSupport("c");
        node.setChild(0, child1);
        node.setChild(1, child2);
        assertEquals(child1, node.getChild(0));
        assertEquals(child2, node.getChild(1));
    }

    @Test
    public void setChild3() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        assertThrows(IllegalArgumentException.class, () -> node.setChild(-1, child1));
    }

    @Test
    public void setChild4() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        assertThrows(IllegalArgumentException.class, () -> node.setChild(1, child1));
    }

    @Test
    public void getChildren() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        MultiTreeNodeSupport child2 = new MultiTreeNodeSupport("c");
        node.addChild(child1);
        node.addChild(child2);
        List<MultiTreeNodeSupport> list = Arrays.asList(child1, child2);
        assertEquals(list, node.getChildren());
    }

    @Test
    public void indexOf() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        MultiTreeNodeSupport child2 = new MultiTreeNodeSupport("c");
        node.addChild(child1);
        node.addChild(child2);
        assertEquals(0, node.indexOf(child1));
        assertEquals(1, node.indexOf(child2));
        assertEquals(-1, node.indexOf(null));
        MultiTreeNodeSupport nodeD = new MultiTreeNodeSupport("d");
        assertEquals(-1, node.indexOf(nodeD));
    }

    @Test
    public void removeChild() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        MultiTreeNodeSupport child2 = new MultiTreeNodeSupport("c");
        node.addChild(child1);
        node.addChild(child2);
        MultiTreeNodeSupport actualChild2 = node.removeChild(1);
        assertSame(child2, actualChild2);
        MultiTreeNodeSupport actualChild1 = node.removeChild(0);
        assertSame(child1, actualChild1);
    }

    @Test
    public void removeChild2() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        MultiTreeNodeSupport child2 = new MultiTreeNodeSupport("c");
        node.addChild(child1);
        node.addChild(child2);
        assertThrows(IndexOutOfBoundsException.class, () -> node.removeChild(-2));
    }

    @Test
    public void removeChild3() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport child1 = new MultiTreeNodeSupport("b");
        MultiTreeNodeSupport child2 = new MultiTreeNodeSupport("c");
        node.addChild(child1);
        node.addChild(child2);
        assertThrows(IndexOutOfBoundsException.class, () -> node.removeChild(2));
    }

    @Test
    public void parent() {
        MultiTreeNodeSupport parent = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("b");
        node.setParent(parent);
        assertEquals(parent, node.getParent());
    }

    @Test
    public void attribute() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        node.setAttribute("key", "value");
        String value = (String) node.getAttribute("key");
        assertEquals("value", value);
    }

    @Test
    public void root() {
        MultiTreeNodeSupport node = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport parent = new MultiTreeNodeSupport("b");
        node.setParent(parent);
        parent.addChild(node);
        assertTrue(node.isLeaf());
        assertTrue(parent.isRoot());
        assertFalse(node.isRoot());
        assertFalse(parent.isLeaf());
        assertFalse(node.hasChildren());
        assertTrue(parent.hasChildren());
    }

    @Test
    public void firstChild() {
        MultiTreeNodeSupport root = new MultiTreeNodeSupport("root");
        MultiTreeNodeSupport a = new MultiTreeNodeSupport("a");
        a.setParent(root);
        root.addChild(a);
        MultiTreeNodeSupport b = new MultiTreeNodeSupport("b");
        b.setParent(root);
        root.addChild(b);
        MultiTreeNodeSupport c = new MultiTreeNodeSupport("c");
        c.setParent(root);
        root.addChild(c);
        assertFalse(root.isFirstChild());
        assertTrue(a.isFirstChild());
        assertFalse(b.isFirstChild());
        assertFalse(c.isFirstChild());
        assertSame(a, root.getFirstChild());
        assertNull(a.getFirstChild());
        assertNull(b.getFirstChild());
        assertNull(c.getFirstChild());
    }

    @Test
    public void lastChild() {
        MultiTreeNodeSupport root = new MultiTreeNodeSupport("root");
        MultiTreeNodeSupport a = new MultiTreeNodeSupport("a");
        a.setParent(root);
        root.addChild(a);
        MultiTreeNodeSupport b = new MultiTreeNodeSupport("b");
        b.setParent(root);
        root.addChild(b);
        MultiTreeNodeSupport c = new MultiTreeNodeSupport("c");
        c.setParent(root);
        root.addChild(c);
        assertFalse(root.isLastChild());
        assertFalse(a.isLastChild());
        assertFalse(b.isLastChild());
        assertTrue(c.isLastChild());
        assertSame(c, root.getLastChild());
        assertNull(a.getLastChild());
        assertNull(b.getLastChild());
        assertNull(c.getLastChild());
    }

    @Test
    public void prevSibling() {
        MultiTreeNodeSupport root = new MultiTreeNodeSupport("root");
        MultiTreeNodeSupport a = new MultiTreeNodeSupport("a");
        a.setParent(root);
        root.addChild(a);
        MultiTreeNodeSupport b = new MultiTreeNodeSupport("b");
        b.setParent(root);
        root.addChild(b);
        MultiTreeNodeSupport c = new MultiTreeNodeSupport("c");
        c.setParent(root);
        root.addChild(c);
        assertNull(root.getPrevSibling());
        assertNull(a.getPrevSibling());
        assertSame(a, b.getPrevSibling());
        assertSame(b, c.getPrevSibling());
    }

    @Test
    public void nextSibling() {
        MultiTreeNodeSupport root = new MultiTreeNodeSupport("root");
        MultiTreeNodeSupport a = new MultiTreeNodeSupport("a");
        a.setParent(root);
        root.addChild(a);
        MultiTreeNodeSupport b = new MultiTreeNodeSupport("b");
        b.setParent(root);
        root.addChild(b);
        MultiTreeNodeSupport c = new MultiTreeNodeSupport("c");
        c.setParent(root);
        root.addChild(c);
        assertNull(root.getNextSibling());
        assertSame(b, a.getNextSibling());
        assertSame(c, b.getNextSibling());
        assertNull(c.getNextSibling());
    }

    @Test
    public void testEquals() {
        MultiTreeNodeSupport a = new MultiTreeNodeSupport("a");
        MultiTreeNodeSupport a2 = new MultiTreeNodeSupport("a");
        assertEquals(a, a2);
        MultiTreeNodeSupport b = new MultiTreeNodeSupport("b");
        assertNotEquals(a, b);
        assertFalse(a.equals(null));
        assertFalse(a.equals("abc"));
    }
}
