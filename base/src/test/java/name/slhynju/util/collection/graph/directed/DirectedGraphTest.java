package name.slhynju.util.collection.graph.directed;

import name.slhynju.util.HashCodeBuilder;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"rawtypes", "unchecked", "static-method"})
public class DirectedGraphTest {

    @Test
    public void test() {
        DirectedGraph g = new DirectedGraphSupport();
        Function<String, DirectedGraphNode> creator = (x) -> new DirectedGraphNodeSupport(x);
        BiFunction<DirectedGraphNode, DirectedGraphNode, DirectedGraphEdge> edgeCreator = (a, b) -> new DirectedGraphEdgeSupport(a, b);
        DirectedGraphNode nodeA = (DirectedGraphNode) g.addNodeIfAbsent("a", creator);
        DirectedGraphNode nodeB = (DirectedGraphNode) g.addNodeIfAbsent("b", creator);
        DirectedGraphNode nodeC = (DirectedGraphNode) g.addNodeIfAbsent("c", creator);
        DirectedGraphNode nodeD = (DirectedGraphNode) g.addNodeIfAbsent("d", creator);
        g.linkIfAbsent(nodeA, nodeB, edgeCreator);
        DirectedGraphEdge edgeBC = g.linkIfAbsent(nodeB, nodeC, edgeCreator);
        DirectedGraphEdge edgeBC2 = g.linkIfAbsent(nodeB, nodeC, edgeCreator);
        assertSame(edgeBC, edgeBC2);
        g.linkIfAbsent(nodeB, nodeD, edgeCreator);
        g.linkIfAbsent(nodeC, nodeC, edgeCreator);
        g.linkIfAbsent(nodeC, nodeD, edgeCreator);

        DirectedGraphNode actualNodeC = (DirectedGraphNode) g.getNode("c");
        assertSame(nodeC, actualNodeC);
        assertEquals(2, actualNodeC.getOutDegree());
        assertEquals(2, actualNodeC.getInDegree());

        List<DirectedGraphNode> cSuccessors = actualNodeC.getDirectSuccessors();
        assertTrue(cSuccessors.contains(actualNodeC));
        assertTrue(cSuccessors.contains(nodeD));
        assertFalse(cSuccessors.contains(nodeB));

        DirectedGraphNode successorD = nodeC.getDirectSuccessor("d");
        assertSame(nodeD, successorD);
        DirectedGraphNode successorA = nodeC.getDirectSuccessor("a");
        assertNull(successorA);

        List<DirectedGraphNode> dPredecessors = nodeD.getDirectPredecessors();
        assertTrue(dPredecessors.contains(nodeB));
        assertTrue(dPredecessors.contains(nodeC));
        assertFalse(dPredecessors.contains(nodeA));

        DirectedGraphNode predecessorB = nodeD.getDirectPredecessor("b");
        assertSame(nodeB, predecessorB);
        DirectedGraphNode predecessorC = nodeD.getDirectPredecessor("c");
        assertSame(nodeC, predecessorC);
        DirectedGraphNode predecessorD = nodeD.getDirectPredecessor("d");
        assertNull(predecessorD);
        DirectedGraphNode predecessorA = nodeD.getDirectPredecessor("a");
        assertNull(predecessorA);

        DirectedGraphNodeSupport nodeE = new DirectedGraphNodeSupport("e");
        g.addNode(nodeE);

        DirectedGraphEdgeSupport edgeBE = new DirectedGraphEdgeSupport(nodeB, nodeE);
        nodeB.addOutEdge(edgeBE);
        nodeE.addInEdge(edgeBE);

        assertEquals(3, nodeB.getOutDegree());

        nodeB.removeOutEdge(edgeBE);
        nodeE.removeInEdge(edgeBE);

        assertEquals(2, nodeB.getOutDegree());

        DirectedGraphEdgeSupport edgeEA = new DirectedGraphEdgeSupport(nodeE, nodeA);
        assertSame(nodeE, edgeEA.getTail());
        assertSame(nodeA, edgeEA.getHead());
        assertEquals("e -> a", edgeEA.toString());
        int hashCode = new HashCodeBuilder().append(nodeE).append(nodeA).toValue();
        assertEquals(hashCode, edgeEA.hashCode());

        DirectedGraphEdgeSupport edgeEA2 = new DirectedGraphEdgeSupport(nodeE, nodeA);
        assertTrue(edgeEA.equals(edgeEA2));
        assertFalse(edgeEA.equals(edgeBE));
        assertFalse(edgeEA.equals(Boolean.TRUE));
    }

    @Test
    public void notEquals() {
        DirectedGraphNodeSupport nodeA = new DirectedGraphNodeSupport("a");
        assertFalse(nodeA.equals(Boolean.TRUE));
    }

    @Test
    public void removeNode() {
        DirectedGraph g = new DirectedGraphSupport();
        Function<String, DirectedGraphNode> creator = (x) -> new DirectedGraphNodeSupport(x);
        BiFunction<DirectedGraphNode, DirectedGraphNode, DirectedGraphEdge> edgeCreator = (a, b) -> new DirectedGraphEdgeSupport(a, b);
        DirectedGraphNode nodeA = (DirectedGraphNode) g.addNodeIfAbsent("a", creator);
        DirectedGraphNode nodeB = (DirectedGraphNode) g.addNodeIfAbsent("b", creator);
        DirectedGraphNode nodeC = (DirectedGraphNode) g.addNodeIfAbsent("c", creator);
        g.linkIfAbsent(nodeA, nodeB, edgeCreator);
        g.linkIfAbsent(nodeA, nodeC, edgeCreator);
        g.linkIfAbsent(nodeB, nodeC, edgeCreator);
        assertEquals(2, nodeA.getOutDegree());
        assertEquals(2, nodeC.getInDegree());
        g.removeNode(nodeB);
        assertEquals(1, nodeA.getOutDegree());
        assertEquals(1, nodeC.getInDegree());

        g.removeNode(null);
    }

    @Test
    public void removeNode2() {
        DirectedGraph g = new DirectedGraphSupport();
        Function<String, DirectedGraphNode> creator = (x) -> new DirectedGraphNodeSupport(x);
        BiFunction<DirectedGraphNode, DirectedGraphNode, DirectedGraphEdge> edgeCreator = (a, b) -> new DirectedGraphEdgeSupport(a, b);
        DirectedGraphNode nodeA = (DirectedGraphNode) g.addNodeIfAbsent("a", creator);
        g.linkIfAbsent(nodeA, nodeA, edgeCreator);
        assertEquals(1, g.getEdges().size());
        g.removeNode(nodeA);
        assertNull(g.getNode("a"));
    }

}
