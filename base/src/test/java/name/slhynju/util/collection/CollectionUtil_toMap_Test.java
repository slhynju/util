package name.slhynju.util.collection;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtil_toMap_Test {

    @Test
    public void toMap_null() {
        Map m = null;
        Map map = CollectionUtil.toMap(m);
        assertNotNull(map);
        assertTrue(map.isEmpty());
    }

    @Test
    public void toMap() {
        Map m = new HashMap();
        m.put("1", "a");
        m.put("2", "b");
        Map map = CollectionUtil.toMap(m);
        assertEquals(2, map.size());
        assertTrue(map.containsKey("1"));
        assertTrue(map.containsValue("b"));
    }

    @Test
    public void toMapArray_null() {
        Object[] a = null;
        Map map = CollectionUtil.toMap(a);
        assertNotNull(map);
        assertTrue(map.isEmpty());
    }

    @Test
    public void toMapArray_oddLength() {
        Object[] a = new String[]{"a", "1", "b"};
        assertThrows(IllegalArgumentException.class, () -> CollectionUtil.toMap(a));
    }

    @Test
    public void toMapArray() {
        Object[] a = new String[]{"a", "1", "b", "2"};
        Map map = CollectionUtil.toMap(a);
        assertEquals(2, map.size());
        assertTrue(map.containsKey("a"));
        assertTrue(map.containsValue("2"));
    }

    @Test
    public void transformList() {
        List<String> list = Arrays.asList("1", "2", "3");
        Map map = CollectionUtil.transformToMap(list, Integer::valueOf);
        assertEquals(3, map.size());
        assertTrue(map.containsKey(Integer.valueOf(2)));
        assertTrue(map.containsValue("3"));
    }

    @Test
    public void tranformList_null() {
        List<String> list = null;
        Map map = CollectionUtil.transformToMap(list, Integer::valueOf);
        assertNotNull(map);
        assertTrue(map.isEmpty());
    }

    @Test
    public void transformList_mapperNull() {
        List<String> list = Arrays.asList("1", "2", "3");
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformToMap(list, null));
    }

    @Test
    public void transformList_bothNull() {
        List<String> list = null;
        Map actual = CollectionUtil.transformToMap(list, null);
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void transformArray() {
        String[] from = new String[]{"1", "2", "3"};
        Map map = CollectionUtil.transformToMap(from, Integer::valueOf);
        assertEquals(3, map.size());
        assertTrue(map.containsKey(Integer.valueOf(2)));
        assertTrue(map.containsValue("3"));
    }

    @Test
    public void transformArray_null() {
        String[] from = null;
        Map map = CollectionUtil.transformToMap(from, Integer::valueOf);
        assertNotNull(map);
        assertTrue(map.isEmpty());
    }

    @Test
    public void transformArray_mapperNull() {
        String[] from = new String[]{"1", "2", "3"};
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformToMap(from, null));
    }

    @Test
    public void transformArray_bothNull() {
        String[] from = null;
        Map actual = CollectionUtil.transformToMap(from, null);
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void transformMap_null() {
        Map<String, String> m = null;
        Map map = CollectionUtil.transformToMap(m, Integer::valueOf, Integer::valueOf);
        assertNotNull(map);
        assertTrue(map.isEmpty());
    }

    @Test
    public void transformMap() {
        Map<String, String> m = CollectionUtil.toMap("1", "2", "3", "4");
        Map<Integer, Integer> map = CollectionUtil.transformToMap(m, Integer::valueOf, Integer::valueOf);
        assertEquals(2, map.size());
        assertEquals(Integer.valueOf(2), map.get(Integer.valueOf(1)));
        assertEquals(Integer.valueOf(4), map.get(Integer.valueOf(3)));
    }

    @Test
    public void transformMap_keyMapperNull() {
        Map<String, String> m = CollectionUtil.toMap("1", "2", "3", "4");
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformToMap(m, null, Integer::valueOf));
    }

    @Test
    public void transformMap_valueMapperNull() {
        Map<String, String> m = CollectionUtil.toMap("1", "2", "3", "4");
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformToMap(m, Integer::valueOf, null));
    }

    @Test
    public void transformMap_allNull() {
        Map<String, String> from = null;
        Map actual = CollectionUtil.transformToMap(from, null, null);
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void transformKeys_null() {
        Map<String, String> m = null;
        Map map = CollectionUtil.transformKeys(m, Integer::valueOf);
        assertNotNull(map);
        assertTrue(map.isEmpty());
    }

    @Test
    public void transformKeys() {
        Map<String, String> m = new HashMap();
        m.put("1", "a");
        m.put("2", "b");
        Map map = CollectionUtil.transformKeys(m, Integer::valueOf);
        assertNotNull(map);
        assertTrue(map.containsValue("a"));
        assertTrue(map.containsKey(Integer.valueOf(2)));
    }

    @Test
    public void transformKeys_mapperNull() {
        Map<String, String> m = new HashMap();
        m.put("1", "a");
        m.put("2", "b");
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformKeys(m, null));
    }

    @Test
    public void transformKeys_bothNull() {
        Map<String, String> from = null;
        Map actual = CollectionUtil.transformKeys(from, null);
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void transformValues_null() {
        Map<String, String> m = null;
        Map map = CollectionUtil.transformValues(m, Integer::valueOf);
        assertNotNull(map);
        assertTrue(map.isEmpty());
    }

    @Test
    public void transformValues() {
        Map<String, String> m = new HashMap();
        m.put("a", "1");
        m.put("b", "2");
        Map map = CollectionUtil.transformValues(m, Integer::valueOf);
        assertNotNull(map);
        assertTrue(map.containsKey("a"));
        assertTrue(map.containsValue(Integer.valueOf(2)));
    }

    @Test
    public void transformValues_mapperNull() {
        Map<String, String> m = new HashMap();
        m.put("a", "1");
        m.put("b", "2");
        assertThrows(NullPointerException.class, () -> CollectionUtil.transformValues(m, null));
    }

    @Test
    public void transformValues_bothNull() {
        Map<String, String> from = null;
        Map actual = CollectionUtil.transformValues(from, null);
        assertNotNull(actual);
        assertTrue(actual.isEmpty());
    }

}
