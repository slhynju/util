package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BeanStringBuilder_Date_Test {

    @Test
    public void date() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = DateUtil.toDate("2013-03-04", "yyyy-MM-dd");
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: 2013-03-04}", sb.toS());
    }

    @Test
    public void date_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = null;
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void dateWithFormat() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = DateUtil.toDate("2013-03-04 05:07:12", "yyyy-MM-dd HH:mm:ss");
        sb.append("p", d, "yyyy-MM-dd HH:mm:ss");
        assertEquals("name.slhynju.util.Bean{p: 2013-03-04 05:07:12}", sb.toS());
    }

    @Test
    public void dateWithFormat_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = null;
        sb.append("p", d, "yyyy-MM-dd HH:mm:ss");
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void dateWithFormat_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = DateUtil.toDate("2013-03-04 05:07:12", "yyyy-MM-dd HH:mm:ss");
        String format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", d, format));
    }

    @Test
    public void dateWithFormat_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = null;
        sb.append("p", d, (String) null);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void dateWithFormat2() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = DateUtil.toDate("2013-03-04 05:07:12", "yyyy-MM-dd HH:mm:ss");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sb.append("p", d, format);
        assertEquals("name.slhynju.util.Bean{p: 2013-03-04 05:07:12}", sb.toS());
    }

    @Test
    public void dateWithFormat2_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = null;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sb.append("p", d, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void dateWithFormat2_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = DateUtil.toDate("2013-03-04 05:07:12", "yyyy-MM-dd HH:mm:ss");
        DateFormat format = null;
        assertThrows(NullPointerException.class, () -> sb.append("p", d, format));
    }

    @Test
    public void dateWithFormat2_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        Date d = null;
        DateFormat format = null;
        sb.append("p", d, format);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void temporal() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDate d = LocalDate.of(2012, 3, 4);
        sb.append("p", d, DateTimeFormatter.ISO_LOCAL_DATE);
        assertEquals("name.slhynju.util.Bean{p: 2012-03-04}", sb.toS());
    }

    @Test
    public void temporal_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDate d = null;
        sb.append("p", d, DateTimeFormatter.ISO_LOCAL_DATE);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void temporal_formatNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDate d = LocalDate.of(2012, 3, 4);
        assertThrows(NullPointerException.class, () -> sb.append("p", d, (DateTimeFormatter) null));
    }

    @Test
    public void temporal_bothNull() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDate d = null;
        sb.append("p", d, (DateTimeFormatter) null);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void localDate() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDate d = LocalDate.of(2012, 3, 4);
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: 2012-03-04}", sb.toS());
    }

    @Test
    public void localDate_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDate d = null;
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void localDateTime() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDateTime d = LocalDateTime.of(2012, 3, 4, 13, 5, 8);
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: 2012-03-04T13:05:08}", sb.toS());
    }

    @Test
    public void localDateTime_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDateTime d = null;
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void localTime() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalTime d = LocalTime.of(13, 5, 8);
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: 13:05:08}", sb.toS());
    }

    @Test
    public void localTime_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalTime d = null;
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

    @Test
    public void zonedDateTime() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        LocalDateTime t = LocalDateTime.of(2012, 3, 8, 15, 7, 9);
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        ZonedDateTime d = ZonedDateTime.of(t, zoneId);
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: 2012-03-08T15:07:09-08:00[America/Los_Angeles]}", sb.toS());
    }

    @Test
    public void zonedDateTime_null() {
        BeanStringBuilder sb = new BeanStringBuilder(Bean.class);
        ZonedDateTime d = null;
        sb.append("p", d);
        assertEquals("name.slhynju.util.Bean{p: null}", sb.toS());
    }

}
