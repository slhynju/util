package name.slhynju.util;

import name.slhynju.ApplicationException;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtil_toDate_String_Test {

    @Test
    public void toDate_s() throws ParseException {
        Date d = DateUtil.toDate("2012-02-04", "yyyy-MM-dd");
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d2 = f.parse("2012-02-04");
        assertEquals(d, d2);
    }

    @Test
    public void toDate_s_dateNull() {
        Date d = DateUtil.toDate(null, "yyyy-MM-dd");
        assertNull(d);
    }

    @Test
    public void toDate_s_dateEmpty() {
        Date d = DateUtil.toDate("", "yyyy-MM-dd");
        assertNull(d);
    }

    @Test
    public void toDate_s_dateNullString() {
        Date d = DateUtil.toDate("null", "yyyy-MM-dd");
        assertNull(d);
    }

    @Test
    public void toDate_s_dateNullString2() {
        Date d = DateUtil.toDate("NULL", "yyyy-MM-dd");
        assertNull(d);
    }


    @Test
    public void toDate_s_parseException() {
        assertThrows(ApplicationException.class, () -> DateUtil.toDate("2012-hh", "yyyy-MM-dd"));
    }

    @Test
    public void toDate_s_formatNull() {
        assertThrows(NullPointerException.class, () -> DateUtil.toDate("2012-02-04", (String) null));
    }

    @Test
    public void toDate_s_bothNull() {
        Date d = DateUtil.toDate(null, (String) null);
        assertNull(d);
    }

    @Test
    public void toDate_sWithFormat() throws ParseException {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d = DateUtil.toDate("2012-02-04", f);
        Date d2 = f.parse("2012-02-04");
        assertEquals(d2, d);
    }

    @Test
    public void toDate_sWithFormat_dateNull() {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d = DateUtil.toDate(null, f);
        assertNull(d);
    }

    @Test
    public void toDate_sWithFormat_dateEmpty() {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d = DateUtil.toDate("", f);
        assertNull(d);
    }

    @Test
    public void toDate_sWithFormat_dateNullString() {
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date d = DateUtil.toDate("null", f);
        assertNull(d);
    }

    @Test
    public void toDate_sWithFormat_parseException() {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        assertThrows(ApplicationException.class, () -> DateUtil.toDate("2012-hh", f));
    }

    @Test
    public void toDate_sWithFormat_formatNull() {
        assertThrows(NullPointerException.class, () -> DateUtil.toDate("2012-02-04", (DateFormat) null));
    }

    @Test
    public void toDate_sWithFormat_bothNull() {
        assertNull(DateUtil.toDate(null, (DateFormat) null));
    }

}
