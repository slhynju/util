package name.slhynju.util.nio;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@SuppressWarnings("static-method")
public class PathUtilTest {

    @Test
    public void listAllFiles_pathNull() {
        Logger log = mock(Logger.class);
        List<Path> files = PathUtil.listAllFiles(null, "glob:**/*.java", log);
        assertTrue(files.isEmpty());
    }

    @Test
    public void listAllFiles() {
        Path classFile = PathUtil.findResource("name/slhynju/util/io/IOUtil.class");
        assertNotNull(classFile);
        // directory of "name"
        Path nameDir = classFile.getParent().getParent().getParent().getParent();
        Logger log = mock(Logger.class);
        List<Path> files = PathUtil.listAllFiles(nameDir, "glob:**/nio/*Collector.class", log);
        assertEquals(1, files.size());
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/FileCollector.class");
        assertTrue(files.contains(p1));
    }

    @Test
    public void listAllFiles_noMatch() {
        Path classFile = PathUtil.findResource("name/slhynju/util/io/IOUtil.class");
        assertNotNull(classFile);
        // directory of "name"
        Path nameDir = classFile.getParent().getParent().getParent().getParent();
        Logger log = mock(Logger.class);
        List<Path> files = PathUtil.listAllFiles(nameDir, "glob:**/nio/*.java", log);
        assertTrue(files.isEmpty());
    }

    @Test
    public void listAllFiles_globNull() {
        Path classFile = PathUtil.findResource("name/slhynju/util/io/IOUtil.class");
        assertNotNull(classFile);
        // directory of "name"
        Path nameDir = classFile.getParent().getParent().getParent().getParent();
        Logger log = mock(Logger.class);
        List<Path> files = PathUtil.listAllFiles(nameDir, null, log);
        assertTrue(files.isEmpty());
    }

    @Test
    public void listAllFiles_globEmpty() {
        Path classFile = PathUtil.findResource("name/slhynju/util/io/IOUtil.class");
        assertNotNull(classFile);
        Path nameDir = classFile.getParent().getParent().getParent().getParent();
        Logger log = mock(Logger.class);
        List<Path> files = PathUtil.listAllFiles(nameDir, "", log);
        assertTrue(files.isEmpty());
    }

    @Test
    public void isDirEmpty_pathNull() {
        assertFalse(PathUtil.isDirectoryEmpty(null));
    }

    @Test
    public void isDirEmpty() {
        Path classFile = PathUtil.findResource("name/slhynju/util/io/IOUtil.class");
        Path dir = classFile.getParent();

        assertFalse(PathUtil.isDirectoryEmpty(dir));
    }

    @Test
    public void isDirEmpty_emptyDir() throws Exception {
        Path classFile = PathUtil.findResource("name/slhynju/util/io/IOUtil.class");
        Path ioDir = classFile.getParent();
        Path dir = Files.createTempDirectory(ioDir, "temp");
        assertTrue(PathUtil.isDirectoryEmpty(dir));
        Files.delete(dir);
    }

    @Test
    public void toName() {
        Path classFile = PathUtil.findResource("name/slhynju/util/nio/PathUtilTest.class");
        String name = PathUtil.toName(classFile);
        assertTrue(name.endsWith("PathUtilTest.class"));
    }

    @Test
    public void toName_null() {
        String name = PathUtil.toName(null);
        assertEquals("", name);
    }

    @Test
    public void toNames_null() {
        List<Path> paths = null;
        List<String> names = PathUtil.toNames(paths);
        assertTrue(names.isEmpty());
    }

    @Test
    public void toNames() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        Path p2 = PathUtil.findResource("name/slhynju/util/nio/PathUtilTest.class");
        List<Path> paths = Arrays.asList(p1, p2);
        List<String> names = PathUtil.toNames(paths);
        String s1 = names.get(0);
        String s2 = names.get(1);
        assertTrue(s1.endsWith("PathUtil.class"));
        assertTrue(s2.endsWith("PathUtilTest.class"));
    }

    @Test
    public void toPath_null() {
        Path path = PathUtil.toPath(null);
        assertNull(path);
    }

    @Test
    public void toPath_empty() {
        Path path = PathUtil.toPath("");
        assertNull(path);
    }

    @Test
    public void toPath() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        String name = PathUtil.toName(p1);
        Path p2 = PathUtil.toPath(name);
        assertEquals(p1, p2);
    }

    @Test
    public void toPaths() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        Path p2 = PathUtil.findResource("name/slhynju/util/nio/PathUtilTest.class");
        List<Path> paths = Arrays.asList(p1, p2);
        List<String> names = PathUtil.toNames(paths);
        List<Path> paths2 = PathUtil.toPaths(names);
        assertEquals(paths, paths2);
    }

    @Test
    public void toPaths_null() {
        List<Path> paths = PathUtil.toPaths(null);
        assertTrue(paths.isEmpty());
    }

    @Test
    public void findResource_null() {
        Path p1 = PathUtil.findResource(null);
        assertNull(p1);
    }

    @Test
    public void findResource_notFound() {
        Path p = PathUtil.findResource("some_file_not_exist");
        assertNull(p);
    }

    @Test
    public void issDirectoryReadable_null() {
        assertFalse(PathUtil.isDirectoryReadable(null));
    }

    @Test
    public void isDirectoryReadable_isFile() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        assertFalse(PathUtil.isDirectoryReadable(p1));
    }

    @Test
    public void isDirectoryReadable() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        Path dir = p1.getParent();
        assertTrue(PathUtil.isDirectoryReadable(dir));
    }

    @Test
    public void isFileReadable_null() {
        assertFalse(PathUtil.isFileReadable(null));
    }

    @Test
    public void isFileReadable() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        assertTrue(PathUtil.isFileReadable(p1));
    }

    @Test
    public void isFileReadable_isDir() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        Path dir = p1.getParent();
        assertFalse(PathUtil.isFileReadable(dir));
    }

    @Test
    public void isDirectoryWritable_null() {
        assertFalse(PathUtil.isDirectoryWritable(null));
    }

    @Test
    public void isDirectoryWritable_isFile() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        assertFalse(PathUtil.isDirectoryWritable(p1));
    }

    @Test
    public void isDirectoryWritable() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        Path dir = p1.getParent();
        assertTrue(PathUtil.isDirectoryWritable(dir));
    }

    @Test
    public void isFileWritable_null() {
        assertFalse(PathUtil.isFileWritable(null));
    }

    @Test
    public void isFileWritable_notFound() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        Path p2 = p1.resolveSibling("non_exist.class");
        assertFalse(PathUtil.isFileWritable(p2));
    }

    @Test
    public void isFileWritable_isDir() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        Path p2 = p1.getParent();
        assertFalse(PathUtil.isFileWritable(p2));
    }

    @Test
    public void isFileWritable() {
        Path p1 = PathUtil.findResource("name/slhynju/util/nio/PathUtil.class");
        assertTrue(PathUtil.isFileWritable(p1));
    }

    @Test
    public void readTextFile() throws IOException {
        Path p1 = PathUtil.findResource("test_locale_zh.properties");
        BufferedReader reader = PathUtil.readTextFile(p1);
        assertNotNull(reader);
        reader.readLine();
        reader.close();
    }

    @Test
    public void appendTextFile() throws IOException {
        Path p1 = PathUtil.findResource("test_locale_zh.properties");
        Path temp = p1.resolveSibling("write_temp");
        PrintWriter writer = PathUtil.writeTextFile(temp);
        writer.println("line1");
        writer.close();
        PrintWriter appendWriter = PathUtil.appendTextFile(temp);
        appendWriter.println("line2");
        appendWriter.close();
        Files.delete(temp);
    }

    @Test
    public void readSmallTextFile() {
        Path p1 = PathUtil.findResource("test_locale_zh.properties");
        List<String> lines = PathUtil.readSmallTextFile(p1);
        assertTrue(lines.size() > 0);
        assertTrue(lines.contains("expected.key=expected.value"));
    }

    @Test
    public void readSmallPropertiesFile() {
        Path p1 = PathUtil.findResource("test_locale_zh.properties");
        Map<String, String> properties = PathUtil.readSmallPropertiesFile(p1);
        String value = properties.get("expected.key");
        assertEquals("expected.value", value);
    }

    @Test
    public void writeSmallPropertiesFile() throws IOException {
        Path p1 = PathUtil.findResource("test_locale_zh.properties");
        Map<String, String> properties = PathUtil.readSmallPropertiesFile(p1);
        Path temp = p1.resolveSibling("write_temp");
        Logger log = mock(Logger.class);
        PathUtil.writeSmallPropertiesFile(properties, temp, log);
        Files.delete(temp);
    }

}
