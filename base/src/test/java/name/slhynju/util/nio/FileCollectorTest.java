package name.slhynju.util.nio;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("static-method")
public class FileCollectorTest {

    @SuppressWarnings("unused")
    @Test
    public void testConstructor() {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        new FileCollector(fileSystem, "glob:*.java", log);
    }

    @Test
    public void accept() {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:**/*.class", log);
        Path classFile = PathUtil.findResource("name/slhynju/util/nio/FileCollectorTest.class");
        assertNotNull(classFile);
        visitor.accept(classFile);
        List<Path> list = visitor.getFiles();
        assertTrue(list.contains(classFile));
    }

    @Test
    public void accept2() {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:**/*.java", log);
        Path classFile = PathUtil.findResource("name/slhynju/util/nio/FileCollectorTest.class");
        assertNotNull(classFile);
        visitor.accept(classFile);
        List<Path> list = visitor.getFiles();
        assertTrue(list.isEmpty());
    }

    @Test
    public void accept3() {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:**/slhynju/**/*.*", log);
        Path classFile = PathUtil.findResource("name/slhynju/util/nio/FileCollectorTest.class");
        assertNotNull(classFile);
        visitor.accept(classFile);
        List<Path> list = visitor.getFiles();
        assertTrue(list.contains(classFile));
    }

    @Test
    public void accept4() {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:**/slhynju/**/*.*", log);
        Path classFile = null;
        visitor.accept(classFile);
        List<Path> list = visitor.getFiles();
        assertTrue(list.isEmpty());
    }

    @Test
    public void visitFile() throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:**/*.class", log);
        Path classFile = PathUtil.findResource("name/slhynju/util/nio/FileCollectorTest.class");
        assertNotNull(classFile);
        BasicFileAttributes attrs = mock(BasicFileAttributes.class);
        FileVisitResult result = visitor.visitFile(classFile, attrs);
        assertEquals(FileVisitResult.CONTINUE, result);
        List<Path> list = visitor.getFiles();
        assertTrue(list.contains(classFile));
    }

    @Test
    public void visitFile_failed() throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:*.java", log);
        Path file = mock(Path.class);
        FileVisitResult result = visitor.visitFileFailed(file, null);
        assertEquals(FileVisitResult.CONTINUE, result);
        verify(log, never()).warn(anyString(), any(IOException.class));
    }

    @Test
    public void visitFile_failed2() throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:*.java", log);
        Path file = mock(Path.class);
        IOException e = mock(IOException.class);
        when(log.isWarnEnabled()).thenReturn(Boolean.FALSE);
        FileVisitResult result = visitor.visitFileFailed(file, e);
        assertEquals(FileVisitResult.CONTINUE, result);
        verify(log).isWarnEnabled();
        verify(log, never()).warn(anyString(), eq(e));
    }

    @Test
    public void visitFile_failed3() throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:*.java", log);
        Path file = mock(Path.class);
        IOException e = mock(IOException.class);
        when(log.isWarnEnabled()).thenReturn(Boolean.TRUE);
        FileVisitResult result = visitor.visitFileFailed(file, e);
        assertEquals(FileVisitResult.CONTINUE, result);
        verify(log).isWarnEnabled();
        verify(log).warn(anyString(), eq(e));
    }

    @Test
    public void preVisitDirectory() throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:*.java", log);
        Path dir = mock(Path.class);
        BasicFileAttributes attrs = mock(BasicFileAttributes.class);
        FileVisitResult result = visitor.preVisitDirectory(dir, attrs);
        assertEquals(FileVisitResult.CONTINUE, result);
    }

    @Test
    public void postVisitDirectory() throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:*.java", log);
        Path dir = mock(Path.class);
        FileVisitResult result = visitor.postVisitDirectory(dir, null);
        assertEquals(FileVisitResult.CONTINUE, result);
        verify(log, never()).warn(anyString(), any(IOException.class));
    }

    @Test
    public void postVisitDirectory2() throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:*.java", log);
        Path dir = mock(Path.class);
        IOException e = mock(IOException.class);
        FileVisitResult result = visitor.postVisitDirectory(dir, e);
        assertEquals(FileVisitResult.CONTINUE, result);
        verify(log, never()).warn(anyString(), eq(e));
    }

    @Test
    public void postVisitDirectory3() throws IOException {
        FileSystem fileSystem = FileSystems.getDefault();
        Logger log = mock(Logger.class);
        FileCollector visitor = new FileCollector(fileSystem, "glob:*.java", log);
        Path dir = mock(Path.class);
        IOException e = mock(IOException.class);
        when(log.isWarnEnabled()).thenReturn(Boolean.TRUE);
        FileVisitResult result = visitor.postVisitDirectory(dir, e);
        assertEquals(FileVisitResult.CONTINUE, result);
        verify(log).isWarnEnabled();
        verify(log).warn(anyString(), eq(e));
    }

}
