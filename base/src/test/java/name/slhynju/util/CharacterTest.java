package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class CharacterTest {

    @Test
    public void types() {
        assertEquals(Character.LOWERCASE_LETTER, Character.getType('a'));
        assertEquals(Character.UPPERCASE_LETTER, Character.getType('A'));
        assertEquals(Character.MATH_SYMBOL, Character.getType('+'));
        assertEquals(Character.DASH_PUNCTUATION, Character.getType('-'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('*'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('/'));
        assertEquals(Character.MATH_SYMBOL, Character.getType('='));
        assertEquals(Character.MATH_SYMBOL, Character.getType('<'));
        assertEquals(Character.MATH_SYMBOL, Character.getType('>'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('?'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('!'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('@'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('#'));
        assertEquals(Character.CURRENCY_SYMBOL, Character.getType('$'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('%'));
        assertEquals(Character.MODIFIER_SYMBOL, Character.getType('^'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('&'));
        assertEquals(Character.START_PUNCTUATION, Character.getType('('));
        assertEquals(Character.END_PUNCTUATION, Character.getType(')'));
        assertEquals(Character.START_PUNCTUATION, Character.getType('['));
        assertEquals(Character.END_PUNCTUATION, Character.getType(']'));
        assertEquals(Character.START_PUNCTUATION, Character.getType('{'));
        assertEquals(Character.END_PUNCTUATION, Character.getType('}'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('\''));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('"'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType(':'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType(','));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('.'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType(';'));
        assertEquals(Character.MATH_SYMBOL, Character.getType('|'));
        assertEquals(Character.OTHER_PUNCTUATION, Character.getType('\\'));
        assertEquals(Character.DIRECTIONALITY_WHITESPACE, Character.getType(' '));
        assertEquals(Character.CONNECTOR_PUNCTUATION, Character.getType('_'));
        assertEquals(Character.DIRECTIONALITY_BOUNDARY_NEUTRAL, Character.getType('1'));
        String a = "中";
        assertEquals(1, a.length());
        assertEquals(Character.DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR, Character.getType(a.codePointAt(0)));
        assertTrue(Character.isLetter(a.codePointAt(0)));
    }

    @Test
    public void alphabetic() {
        assertTrue(Character.isAlphabetic('a'));
        assertTrue(Character.isAlphabetic('A'));
        assertFalse(Character.isAlphabetic('+'));
        assertFalse(Character.isAlphabetic('-'));
        assertFalse(Character.isAlphabetic('*'));
        assertFalse(Character.isAlphabetic('$'));
        assertFalse(Character.isAlphabetic('^'));
        assertFalse(Character.isAlphabetic('('));
        assertFalse(Character.isAlphabetic(')'));
        assertFalse(Character.isAlphabetic(' '));
        assertFalse(Character.isAlphabetic('1'));
        assertFalse(Character.isAlphabetic('_'));
        String a = "中";
        assertTrue(Character.isAlphabetic(a.codePointAt(0)));
    }

    @Test
    public void digit() {
        assertFalse(Character.isDigit('a'));
        assertFalse(Character.isDigit('A'));
        assertFalse(Character.isDigit('+'));
        assertFalse(Character.isDigit('-'));
        assertFalse(Character.isDigit('*'));
        assertFalse(Character.isDigit('$'));
        assertFalse(Character.isDigit('^'));
        assertFalse(Character.isDigit('('));
        assertFalse(Character.isDigit(')'));
        assertFalse(Character.isDigit(' '));
        assertTrue(Character.isDigit('1'));
        assertFalse(Character.isDigit('_'));
        String a = "中";
        assertFalse(Character.isDigit(a.codePointAt(0)));
    }

    @Test
    public void letter() {
        assertTrue(Character.isLetter('a'));
        assertTrue(Character.isLetter('A'));
        assertFalse(Character.isLetter('+'));
        assertFalse(Character.isLetter('-'));
        assertFalse(Character.isLetter('*'));
        assertFalse(Character.isLetter('$'));
        assertFalse(Character.isLetter('^'));
        assertFalse(Character.isLetter('('));
        assertFalse(Character.isLetter(')'));
        assertFalse(Character.isLetter(' '));
        assertFalse(Character.isLetter('1'));
        assertFalse(Character.isLetter('_'));
        String a = "中";
        assertTrue(Character.isLetter(a.codePointAt(0)));
    }

    @Test
    public void letterOrDigit() {
        assertTrue(Character.isLetterOrDigit('a'));
        assertTrue(Character.isLetterOrDigit('A'));
        assertFalse(Character.isLetterOrDigit('+'));
        assertFalse(Character.isLetterOrDigit('-'));
        assertFalse(Character.isLetterOrDigit('*'));
        assertFalse(Character.isLetterOrDigit('$'));
        assertFalse(Character.isLetterOrDigit('^'));
        assertFalse(Character.isLetterOrDigit('('));
        assertFalse(Character.isLetterOrDigit(')'));
        assertFalse(Character.isLetterOrDigit(' '));
        assertTrue(Character.isLetterOrDigit('1'));
        assertFalse(Character.isLetterOrDigit('_'));
        String a = "中";
        assertTrue(Character.isLetterOrDigit(a.codePointAt(0)));
    }

    @Test
    public void space() {
        assertFalse(Character.isSpaceChar('a'));
        assertFalse(Character.isSpaceChar('A'));
        assertFalse(Character.isSpaceChar('+'));
        assertFalse(Character.isSpaceChar('-'));
        assertFalse(Character.isSpaceChar('*'));
        assertFalse(Character.isSpaceChar('$'));
        assertFalse(Character.isSpaceChar('^'));
        assertFalse(Character.isSpaceChar('('));
        assertFalse(Character.isSpaceChar(')'));
        assertTrue(Character.isSpaceChar(' '));
        assertFalse(Character.isSpaceChar('1'));
        assertFalse(Character.isSpaceChar('_'));
        String a = "中";
        assertFalse(Character.isSpaceChar(a.codePointAt(0)));
    }

    @Test
    public void whiteSpace() {
        assertFalse(Character.isWhitespace('a'));
        assertFalse(Character.isWhitespace('A'));
        assertFalse(Character.isWhitespace('+'));
        assertFalse(Character.isWhitespace('-'));
        assertFalse(Character.isWhitespace('*'));
        assertFalse(Character.isWhitespace('$'));
        assertFalse(Character.isWhitespace('^'));
        assertFalse(Character.isWhitespace('('));
        assertFalse(Character.isWhitespace(')'));
        assertTrue(Character.isWhitespace(' '));
        assertFalse(Character.isWhitespace('1'));
        assertFalse(Character.isWhitespace('_'));
        String a = "中";
        assertFalse(Character.isWhitespace(a.codePointAt(0)));
    }


}

