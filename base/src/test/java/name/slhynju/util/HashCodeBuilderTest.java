package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SuppressWarnings("static-method")
public class HashCodeBuilderTest {

    @Test
    public void object() {
        HashCodeBuilder b1 = new HashCodeBuilder().append("abc");
        HashCodeBuilder b2 = new HashCodeBuilder().append("abc");
        assertEquals(b1.toValue(), b2.toValue());
        b1.append("def");
        b2.append("ghi");
        assertNotEquals(b1.toValue(), b2.toValue());
    }

    @Test
    public void intType() {
        HashCodeBuilder b1 = new HashCodeBuilder().append(5);
        HashCodeBuilder b2 = new HashCodeBuilder().append(5);
        assertEquals(b1.toValue(), b2.toValue());
        b1.append(8);
        b2.append(12);
        assertNotEquals(b1.toValue(), b2.toValue());
    }

    @Test
    public void booleanType() {
        HashCodeBuilder b1 = new HashCodeBuilder().append(true);
        HashCodeBuilder b2 = new HashCodeBuilder().append(true);
        assertEquals(b1.toValue(), b2.toValue());
        b1.append(false);
        b2.append(true);
        assertNotEquals(b1.toValue(), b2.toValue());
    }

    @Test
    public void longType() {
        HashCodeBuilder b1 = new HashCodeBuilder().append(5L);
        HashCodeBuilder b2 = new HashCodeBuilder().append(5L);
        assertEquals(b1.toValue(), b2.toValue());
        b1.append(8L);
        b2.append(12L);
        assertNotEquals(b1.toValue(), b2.toValue());
    }

    @Test
    public void doubleType() {
        HashCodeBuilder b1 = new HashCodeBuilder().append(9.2);
        HashCodeBuilder b2 = new HashCodeBuilder().append(9.2);
        assertEquals(b1.toValue(), b2.toValue());
        b1.append(10.8);
        b2.append(10.79);
        assertNotEquals(b1.toValue(), b2.toValue());

    }

    @Test
    public void charType() {
        HashCodeBuilder b1 = new HashCodeBuilder().append('b');
        HashCodeBuilder b2 = new HashCodeBuilder().append('b');
        assertEquals(b1.toValue(), b2.toValue());
        b1.append('c');
        b2.append('d');
        assertNotEquals(b1.toValue(), b2.toValue());
    }

    @Test
    public void capacity() {
        HashCodeBuilder b = new HashCodeBuilder();
        for (int I = 0; I < 100; I++) {
            b.append(I);
        }
        for (int J = 0; J < 100; J++) {
            b.append("abcde");
        }
        for (int K = 0; K < 100; K++) {
            b.append(K / 3.0);
        }
    }

}
