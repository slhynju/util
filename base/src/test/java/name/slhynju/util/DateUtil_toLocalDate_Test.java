package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtil_toLocalDate_Test {

    @Test
    public void toLocalDate_date() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d = format.parse("2012-02-04");
        LocalDate l = DateUtil.toLocalDate(d);
        assertEquals(2012, l.getYear());
        assertEquals(2, l.getMonthValue());
        assertEquals(4, l.getDayOfMonth());
    }

    @Test
    public void toLocalDate_date_dateNull() {
        assertNull(DateUtil.toLocalDate((Date) null));
    }

    @Test
    public void toLocalDate_dateWithTimeZone() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04");
        LocalDate l = DateUtil.toLocalDate(d, zone);
        assertEquals(2012, l.getYear());
        assertEquals(2, l.getMonthValue());
        assertEquals(4, l.getDayOfMonth());
    }

    @Test
    public void toLocalDate_dateWithTimeZone_dateNull() {
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        assertNull(DateUtil.toLocalDate((Date) null, zone));
    }

    @Test
    public void toLocalDate_dateWithTimeZone_zoneNull() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04");
        assertThrows(NullPointerException.class, () -> DateUtil.toLocalDate(d, (TimeZone) null));
    }

    @Test
    public void toLocalDate_dateWithTimeZone_bothNull() {
        TimeZone zone = null;
        assertThrows(NullPointerException.class, () -> DateUtil.toLocalDate((Date) null, zone));
    }

    @Test
    public void toLocalDate_dateWithZoneId() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04");
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        LocalDate l = DateUtil.toLocalDate(d, zoneId);
        assertEquals(2012, l.getYear());
        assertEquals(2, l.getMonthValue());
        assertEquals(4, l.getDayOfMonth());
    }

    @Test
    public void toLocalDate_dateWithZoneId_dateNull() {
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        assertNull(DateUtil.toLocalDate((Date) null, zoneId));
    }

    @Test
    public void toLocalDate_dateWithZoneId_zoneNull() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = format.parse("2012-02-04");
        assertThrows(NullPointerException.class, () -> DateUtil.toLocalDate(d, (ZoneId) null));
    }

    @Test
    public void toLocalDate_dateWithZoneId_bothNull() {
        ZoneId zoneId = null;
        assertNull(DateUtil.toLocalDate((Date) null, zoneId));
    }
}
