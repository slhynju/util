package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.time.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FreeStringBuilder_Object_Test {

    @Test
    public void nullObject() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Object o = null;
        sb.append(o);
        assertEquals("null", sb.toS());
    }

    @Test
    public void string() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Object o = new String("abc");
        sb.append(o);
        assertEquals("abc", sb.toS());
    }

    @Test
    public void charSequence() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Object o = new StringBuilder("abc");
        sb.append(o);
        assertEquals("abc", sb.toS());
    }

    @Test
    public void collection() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Collection<String> o = new ArrayList();
        o.add("a");
        o.add("b");
        o.add("c");
        sb.append((Object) o);
        assertEquals("[a, b, c]", sb.toS());
    }

    @Test
    public void map() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Map<String, String> o = new HashMap<>();
        o.put("k1", "v1");
        o.put("k2", "v2");
        sb.append((Object) o);
        assertEquals("{k1: v1, k2: v2}", sb.toS());
    }

    @Test
    public void date() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Date o = DateUtil.toDate("2012-03-08", "yyyy-MM-dd");
        sb.append((Object) o);
        assertEquals("2012-03-08", sb.toS());
    }

    @Test
    public void localDate() {
        FreeStringBuilder sb = new FreeStringBuilder();
        LocalDate o = LocalDate.of(2012, 3, 8);
        sb.append((Object) o);
        assertEquals("2012-03-08", sb.toS());
    }

    @Test
    public void localTime() {
        FreeStringBuilder sb = new FreeStringBuilder();
        LocalTime o = LocalTime.of(15, 7, 9);
        sb.append((Object) o);
        assertEquals("15:07:09", sb.toS());
    }

    @Test
    public void localDateTime() {
        FreeStringBuilder sb = new FreeStringBuilder();
        LocalDateTime o = LocalDateTime.of(2012, 3, 8, 15, 7, 9);
        sb.append((Object) o);
        assertEquals("2012-03-08T15:07:09", sb.toS());
    }

    @Test
    public void zonedDateTime() {
        FreeStringBuilder sb = new FreeStringBuilder();
        LocalDateTime t = LocalDateTime.of(2012, 3, 8, 15, 7, 9);
        ZoneId zoneId = ZoneId.of("America/Los_Angeles");
        ZonedDateTime o = ZonedDateTime.of(t, zoneId);
        sb.append((Object) o);
        assertEquals("2012-03-08T15:07:09-08:00[America/Los_Angeles]", sb.toS());
    }

    @Test
    public void stringArray() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Object[] o = new String[]{"a", "b", "c"};
        sb.append((Object) o);
        assertEquals("[a, b, c]", sb.toS());
    }

    @Test
    public void objectArray() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Object[] o = new Integer[]{1, 2, 3};
        sb.append((Object) o);
        assertEquals("[1, 2, 3]", sb.toS());
    }

    @Test
    public void bigInteger() {
        FreeStringBuilder sb = new FreeStringBuilder();
        BigInteger o = new BigInteger("789293");
        sb.append((Object) o);
        assertEquals("789293", sb.toS());
    }

}
