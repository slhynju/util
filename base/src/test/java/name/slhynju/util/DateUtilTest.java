package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"ConstantConditions", "RedundantCast"})
public class DateUtilTest {

    @Test
    public void add() {
        Date d = DateUtil.toDate("2012-02-01", "yyyy-MM-dd");
        d = DateUtil.add(d, Calendar.DATE, 3);
        d = DateUtil.add(d, Calendar.HOUR, 5);
        assertEquals("2012-02-04 05", DateUtil.toS(d, "yyyy-MM-dd HH"));
    }

    @Test
    public void add_illegalUnit() {
        Date d = DateUtil.toDate("2012-02-01", "yyyy-MM-dd");
        assertThrows(IllegalArgumentException.class, () -> DateUtil.add(d, 100, 3));
    }

    @Test
    public void add_null() {
        Date d = DateUtil.add(null, Calendar.DATE, 3);
        assertNull(d);
    }

    @Test
    public void max() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        d = DateUtil.max(d, Calendar.DATE);
        assertEquals("2012-04-30 15:24:37", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void max_illegalUnit() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> DateUtil.max(d, 100));
    }

    @Test
    public void max_null() {
        Date d = DateUtil.max((Date) null, Calendar.DATE);
        assertNull(d);
    }

    @Test
    public void maxCalendar() {
        Date d = DateUtil.toDate("2012-02-01", "yyyy-MM-dd");
        Calendar c = DateUtil.toCalendar(d);
        DateUtil.max(c, Calendar.DATE);
        d = c.getTime();
        assertEquals("2012-02-29 00:00:00", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void maxCalendar_illegalUnit() {
        Date d = DateUtil.toDate("2012-02-01", "yyyy-MM-dd");
        Calendar c = DateUtil.toCalendar(d);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> DateUtil.max(c, 100));
    }

    @Test
    public void maxCalendar_null() {
        DateUtil.max((Calendar) null, Calendar.DATE);
    }

    @Test
    public void min() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        d = DateUtil.min(d, Calendar.DATE);
        assertEquals("2012-04-01 15:24:37", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void min_illegalUnit() {
        Date d = DateUtil.toDate("2012-04-05 15:24:37", "yyyy-MM-dd HH:mm:ss");
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> DateUtil.min(d, 100));
    }


    @Test
    public void min_null() {
        Date d = DateUtil.min((Date) null, Calendar.DATE);
        assertNull(d);
    }

    @Test
    public void minCalendar() {
        Date d = DateUtil.toDate("2012-02-05", "yyyy-MM-dd");
        Calendar c = DateUtil.toCalendar(d);
        DateUtil.min(c, Calendar.DATE);
        d = c.getTime();
        assertEquals("2012-02-01 00:00:00", DateUtil.toS(d, "yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    public void minCalendar_illegalUnit() {
        Date d = DateUtil.toDate("2012-02-05", "yyyy-MM-dd");
        Calendar c = DateUtil.toCalendar(d);
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> DateUtil.min(c, 100));
    }

    @Test
    public void minCalendar_null() {
        DateUtil.min((Calendar) null, Calendar.DATE);
    }

    @Test
    public void toCalendar_date() {
        Date d = DateUtil.toDate("2012-02-01", "yyyy-MM-dd");
        Calendar c = DateUtil.toCalendar(d);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(d);
        assertEquals(c2, c);
    }

    @Test
    public void toCalendar_date_dateNull() {
        Calendar c = DateUtil.toCalendar(null);
        assertNull(c);
    }

    @Test
    public void toCalendar_dateWithTimeZone() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = DateUtil.toDate("2012-02-01", format);
        Calendar c = DateUtil.toCalendar(d, zone);
        Calendar c2 = Calendar.getInstance();
        c2.setTimeZone(zone);
        c2.setTime(d);
        assertEquals(c2, c);
    }

    @Test
    public void toCalendar_dateWithTimeZone_dateNull() {
        Date d = null;
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        assertNull(DateUtil.toCalendar(d, zone));
    }

    @Test
    public void toCalendar_dateWithTimeZone_zoneNull() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone zone = TimeZone.getTimeZone("America/Los_Angeles");
        format.setTimeZone(zone);
        Date d = DateUtil.toDate("2012-02-01", format);
        assertThrows(NullPointerException.class, () -> DateUtil.toCalendar(d, null));
    }

    @Test
    public void toCalendar_dateWithTimeZone_bothNull() {
        Date d = null;
        TimeZone zone = null;
        assertNull(DateUtil.toCalendar(d, zone));
    }

    @Test
    public void today() throws ParseException {
        String s = DateUtil.toS(new Date(), "yyyy-MM-dd");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d = format.parse(s);
        Date today = DateUtil.today();
        assertEquals(d, today);
    }

}
