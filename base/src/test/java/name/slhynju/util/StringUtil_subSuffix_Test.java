package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringUtil_subSuffix_Test {

    @Test
    public void subSuffix() {
        assertEquals("", StringUtil.subSuffix("abc", 0));
        assertEquals("c", StringUtil.subSuffix("abc", 1));
        assertEquals("bc", StringUtil.subSuffix("abc", 2));
        assertEquals("abc", StringUtil.subSuffix("abc", 3));
        assertEquals("abc", StringUtil.subSuffix("abc", 4));
        assertEquals("abc", StringUtil.subSuffix("abc", 5));
        assertEquals("", StringUtil.subSuffix("a", 0));
        assertEquals("a", StringUtil.subSuffix("a", 1));
        assertEquals("a", StringUtil.subSuffix("a", 2));
        assertEquals("", StringUtil.subSuffix("", 0));
        assertEquals("", StringUtil.subSuffix("", 1));
        assertEquals("", StringUtil.subSuffix("", 2));
    }

    @Test
    public void subSuffix_negativeLength() {
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.subSuffix("abc", -7));
    }

    @Test
    public void subSuffix_null() {
        String s = StringUtil.subSuffix((String) null, 3);
        assertEquals("", s);
    }

    @Test
    public void subSuffix_null_zeroLength() {
        String s = StringUtil.subSuffix((String) null, 0);
        assertEquals("", s);
    }

    @Test
    public void subSuffix_null_negativeLength() {
        String s = StringUtil.subSuffix((String) null, -5);
        assertEquals("", s);
    }

    @SuppressWarnings("StringOperationCanBeSimplified")
    @Test
    public void subSuffix_sb() {
        StringBuilder sb = new StringBuilder("abc");
        assertEquals("", StringUtil.subSuffix(sb, 0));
        assertEquals("c", StringUtil.subSuffix(sb, 1));
        assertEquals("bc", StringUtil.subSuffix(sb, 2));
        assertEquals("abc", StringUtil.subSuffix(sb, 3));
        assertEquals("abc", StringUtil.subSuffix(sb, 4));
        assertEquals("abc", StringUtil.subSuffix(sb, 5));
        sb = new StringBuilder("a");
        assertEquals("", StringUtil.subSuffix(sb, 0));
        assertEquals("a", StringUtil.subSuffix(sb, 1));
        assertEquals("a", StringUtil.subSuffix(sb, 2));
        sb = new StringBuilder("");
        assertEquals("", StringUtil.subSuffix(sb, 0));
        assertEquals("", StringUtil.subSuffix(sb, 1));
        assertEquals("", StringUtil.subSuffix(sb, 2));
    }

    @Test
    public void subSuffix_sb_negativeLength() {
        StringBuilder sb = new StringBuilder("abc");
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.subSuffix(sb, -7));
    }

    @Test
    public void subSuffix_sb_null() {
        String s = StringUtil.subSuffix((StringBuilder) null, 3);
        assertEquals("", s);
    }

    @Test
    public void subSuffix_sb_null_zeroLength() {
        String s = StringUtil.subSuffix((StringBuilder) null, 0);
        assertEquals("", s);
    }

    @Test
    public void subSuffix_sb_null_negativeLength() {
        String s = StringUtil.subSuffix((StringBuilder) null, -5);
        assertEquals("", s);
    }

}
