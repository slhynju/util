package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SuppressWarnings({"ConstantConditions", "SpellCheckingInspection"})
public class FreeStringBuilder_PrimitiveTypes_Test {

    @Test
    public void constructor_default() {
        assertEquals("", new FreeStringBuilder().toS());
    }

    @Test
    public void constructor_capacity() {
        assertEquals("", new FreeStringBuilder(10).toS());
    }

    @Test
    public void constructor_negative_capacity() {
        assertThrows(NegativeArraySizeException.class, () -> new FreeStringBuilder(-5));
    }

    @Test
    public void constructor_S() {
        assertEquals("abc", new FreeStringBuilder("abc").toS());
    }

    @Test
    public void constructor_S_null() {
        assertThrows(NullPointerException.class, () -> new FreeStringBuilder(null));
    }

    @Test
    public void booleanType() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(false);
        assertEquals("false", sb.toS());
        sb.append(true);
        assertEquals("falsetrue", sb.toS());
    }

    @Test
    public void charType() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append('a');
        assertEquals("a", sb.toS());
    }

    @Test
    public void quotedChar() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.appendQuoted('a');
        assertEquals("'a'", sb.toS());
    }

    @Test
    public void intType() {
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.append(42);
        assertEquals("42", sb.toS());
    }

    @Test
    public void longType() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(42L);
        assertEquals("42", sb.toS());
    }

    @Test
    public void longWithFormat() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        NumberFormat f = new DecimalFormat("#,###");
        sb.append(2004L, f);
        assertEquals("2,004", sb.toS());
    }

    @Test
    public void longWithFormat_formatNull() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        NumberFormat f = null;
        assertThrows(NullPointerException.class, () -> sb.append(2004L, f));
    }

    @Test
    public void floatType() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(3.45f);
        assertEquals("3.45", sb.toS());
    }

    @Test
    public void doubleType() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(3.45);
        assertEquals("3.45", sb.toS());
    }

    @Test
    public void doubleType_fraction() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        sb.append(3.4587, 2, 2, false);
        assertEquals("3.46", sb.toS());
    }

    @Test
    public void doubleWithFormat() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        NumberFormat f = NumberFormat.getInstance();
        f.setMaximumFractionDigits(2);
        f.setMinimumFractionDigits(2);
        f.setGroupingUsed(false);
        sb.append(3.4587, f);
        assertEquals("3.46", sb.toS());
    }

    @Test
    public void doubleWithFormat_formatNull() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        NumberFormat f = null;
        assertThrows(NullPointerException.class, () -> sb.append(3.4587, f));
    }

    @Test
    public void bigInteger() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        BigInteger n = new BigInteger("203838392");
        sb.append(n);
        assertEquals("203838392", sb.toS());
    }

    @Test
    public void bigInteger_null() {
        FreeStringBuilder sb = new FreeStringBuilder(5);
        BigInteger n = null;
        sb.append(n);
        assertEquals("null", sb.toS());
    }

}
