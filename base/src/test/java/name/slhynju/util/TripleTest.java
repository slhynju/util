package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("static-method")
public class TripleTest {

    @Test
    public void triple() {
        Triple<String, Integer, Long> p = new Triple<>();
        p.a = "abc";
        assertEquals("abc", p.a);
        Integer n = Integer.valueOf(5);
        p.b = n;
        assertEquals(n, p.b);
        Long l = Long.valueOf(7L);
        p.c = l;
        assertEquals(l, p.c);
        Object[] array = new Object[]{"abc", n, l};
        assertArrayEquals(array, p.toArray());
    }

    @Test
    public void triple2() {
        Integer n = Integer.valueOf(5);
        Long l = Long.valueOf(7L);
        Triple<String, Integer, Long> p = new Triple<>("abc", n, l);
        assertEquals("abc", p.a);
        assertEquals(n, p.b);
        assertEquals(l, p.c);
        Object[] array = new Object[]{"abc", n, l};
        assertArrayEquals(array, p.toArray());
    }

    @Test
    public void equals() {
        Integer n = Integer.valueOf(5);
        Boolean b = Boolean.TRUE;
        Triple<String, Integer, Boolean> t = new Triple<>("abc", n, b);
        Triple<String, Integer, Boolean> t2 = new Triple<>("abc", n, b);
        assertTrue(t.equals(t2));
        assertEquals(t.hashCode(), t2.hashCode());
        assertEquals("name.slhynju.util.Triple{a: abc, b: 5, c: true}", t.toString());
        Triple<String, Integer, Boolean> t3 = new Triple<>("def", n, b);
        assertFalse(t.equals(t3));
        assertFalse(t.equals("abc"));
        assertFalse(t.equals(null));
    }

}
