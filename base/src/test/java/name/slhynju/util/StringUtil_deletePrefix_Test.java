package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringUtil_deletePrefix_Test {

    @Test
    public void deletePrefix() {
        assertEquals("abc", StringUtil.deletePrefix("abc", 0));
        assertEquals("bc", StringUtil.deletePrefix("abc", 1));
        assertEquals("c", StringUtil.deletePrefix("abc", 2));
        assertEquals("", StringUtil.deletePrefix("abc", 3));
        assertEquals("", StringUtil.deletePrefix("abc", 4));
        assertEquals("", StringUtil.deletePrefix("abc", 5));
        assertEquals("a", StringUtil.deletePrefix("a", 0));
        assertEquals("", StringUtil.deletePrefix("a", 1));
        assertEquals("", StringUtil.deletePrefix("a", 2));
        assertEquals("", StringUtil.deletePrefix("", 0));
        assertEquals("", StringUtil.deletePrefix("", 1));
        assertEquals("", StringUtil.deletePrefix("", 2));
    }

    @Test
    public void deletePrefix_negativeLength() {
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.deletePrefix("abc", -7));
    }

    @Test
    public void deletePrefix_null() {
        String s = StringUtil.deletePrefix((String) null, 3);
        assertEquals("", s);
    }

    @Test
    public void deletePrefix_null_zeroLength() {
        String s = StringUtil.deletePrefix((String) null, 0);
        assertEquals("", s);
    }

    @Test
    public void deletePrefix_null_negativeLength() {
        String s = StringUtil.deletePrefix((String) null, -5);
        assertEquals("", s);
    }

    @Test
    public void deletePrefixIf() {
        String s = StringUtil.deletePrefixIf("prefix-abc", "prefix-");
        assertEquals("abc", s);
        s = StringUtil.deletePrefixIf("def", "prefix-");
        assertEquals("def", s);
    }

    @Test
    public void deletePrefixIf_null() {
        String s = StringUtil.deletePrefixIf((String) null, "prefix-");
        assertEquals("", s);
    }

    @Test
    public void deletePrefixIf_prefixNull() {
        assertThrows(NullPointerException.class, () -> StringUtil.deleteSuffixIf("prefix-abc", null));
    }

    @Test
    public void deletePrefixIf_bothNull() {
        String s = StringUtil.deletePrefixIf((String) null, null);
        assertEquals("", s);
    }

    @Test
    public void deletePrefix_sb() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deletePrefix(sb, 1);
        assertEquals("bc", sb.toString());
    }

    @Test
    public void deletePrefix_sb_2() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deletePrefix(sb, 2);
        assertEquals("c", sb.toString());
    }

    @Test
    public void deletePrefix_sb_3() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deletePrefix(sb, 3);
        assertEquals("", sb.toString());
    }

    @Test
    public void deletePrefix_sb_4() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deletePrefix(sb, 4);
        assertEquals("", sb.toString());
    }

    @Test
    public void deletePrefix_sb_zeroLength() {
        StringBuilder sb = new StringBuilder("abc");
        StringUtil.deletePrefix(sb, 0);
        assertEquals("abc", sb.toString());
    }

    @Test
    public void deletePrefix_sb_negativeLength() {
        StringBuilder sb = new StringBuilder("abc");
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.deletePrefix(sb, -7));
    }

    @SuppressWarnings("StringOperationCanBeSimplified")
    @Test
    public void deletePrefix_sb_empty() {
        StringBuilder sb = new StringBuilder("");
        StringUtil.deletePrefix(sb, 2);
        assertEquals("", sb.toString());
    }

    @SuppressWarnings("StringOperationCanBeSimplified")
    @Test
    public void deletePrefix_sb_empty_zeroLength() {
        StringBuilder sb = new StringBuilder("");
        StringUtil.deletePrefix(sb, 0);
        assertEquals("", sb.toString());
    }

    @Test
    public void deletePrefix_sb_empty_negativeLength() {
        StringBuilder sb = new StringBuilder("");
        assertThrows(StringIndexOutOfBoundsException.class, () -> StringUtil.deletePrefix(sb, -3));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void deletePrefix_sb_null() {
        StringBuilder sb = null;
        StringUtil.deletePrefix(sb, 3);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void deletePrefix_sb_null_zeroLength() {
        StringBuilder sb = null;
        StringUtil.deletePrefix(sb, 0);
    }


    @SuppressWarnings("ConstantConditions")
    @Test
    public void deletePrefix_sb_null_negativeLength() {
        StringBuilder sb = null;
        StringUtil.deletePrefix(sb, -7);
    }

    @Test
    public void deletePrefixIf_sb() {
        StringBuilder sb = new StringBuilder("prefix-abc");
        StringUtil.deletePrefixIf(sb, "prefix-");
        assertEquals("abc", sb.toString());
    }

    @Test
    public void deletePrefixIf_sb_2() {
        StringBuilder sb = new StringBuilder("pre");
        StringUtil.deletePrefixIf(sb, "prefix-");
        assertEquals("pre", sb.toString());
    }

    @Test
    public void deletePrefixIf_sb_3() {
        StringBuilder sb = new StringBuilder("this is a long string.");
        StringUtil.deletePrefixIf(sb, "prefix-");
        assertEquals("this is a long string.", sb.toString());
    }

    @Test
    public void deletePrefixIf_sb_null() {
        StringBuilder sb = null;
        StringUtil.deletePrefixIf(sb, "prefix-");
    }

    @Test
    public void deletePrefixIf_sb_prefixNull() {
        StringBuilder sb = new StringBuilder("prefix-abc");
        assertThrows(NullPointerException.class, () -> StringUtil.deletePrefixIf(sb, null));
    }

    @Test
    public void deletePrefixIf_sb_bothNull() {
        StringBuilder sb = null;
        StringUtil.deletePrefixIf(sb, null);
    }

}
