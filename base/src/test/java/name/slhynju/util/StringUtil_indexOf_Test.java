package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.function.IntPredicate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class StringUtil_indexOf_Test {

    @SuppressWarnings("ConstantConditions")
    @Test
    public void indexOf_null() {
        String s = null;
        int index = StringUtil.indexOf(s, Character::isAlphabetic);
        assertEquals(-1, index);
    }

    @Test
    public void indexOf_empty() {
        String s = "";
        int index = StringUtil.indexOf(s, Character::isAlphabetic);
        assertEquals(-1, index);
    }

    @Test
    public void indexOf() {
        String s = "8E92D";
        int index = StringUtil.indexOf(s, Character::isAlphabetic);
        assertEquals(1, index);
    }

    @Test
    public void indexOf_notFound() {
        String s = "8356";
        int index = StringUtil.indexOf(s, Character::isAlphabetic);
        assertEquals(-1, index);
    }

    @Test
    public void indexOf_matcherNull() {
        String s = "8356";
        IntPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> StringUtil.indexOf(s, matcher));
    }

    @Test
    public void indexOf_nonLatin() {
        String s = "中文测试";
        IntPredicate matcher = x -> x == '文';
        int index = StringUtil.indexOf(s, matcher);
        assertEquals(1, index);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void lastIndexOf_null() {
        String s = null;
        int index = StringUtil.lastIndexOf(s, Character::isAlphabetic);
        assertEquals(-1, index);
    }

    @Test
    public void lastIndexOf_empty() {
        String s = "";
        int index = StringUtil.lastIndexOf(s, Character::isAlphabetic);
        assertEquals(-1, index);
    }

    @Test
    public void lastIndexOf() {
        String s = "8E92D";
        int index = StringUtil.lastIndexOf(s, Character::isAlphabetic);
        assertEquals(4, index);
    }

    @Test
    public void lastIndexOf_notFound() {
        String s = "8356";
        int index = StringUtil.lastIndexOf(s, Character::isAlphabetic);
        assertEquals(-1, index);
    }

    @Test
    public void lastIndexOf_matcherNull() {
        String s = "8356";
        IntPredicate matcher = null;
        assertThrows(NullPointerException.class, () -> StringUtil.lastIndexOf(s, matcher));
    }
}
