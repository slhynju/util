package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FreeStringBuilder_Stack_Test {

    @Test
    public void stack() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        sb.appendStack(stack, "<", "-", ">");
        assertEquals("<h1-h2-h3>", sb.toS());
    }

    @Test
    public void stack_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = null;
        sb.appendStack(stack, "<", "-", ">");
        assertEquals("null", sb.toS());
    }

    @Test
    public void stack_elementNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        //LinkedList accepts null element while ArrayDeque does not.
        Deque<String> stack = new LinkedList<>();
        stack.addFirst("h1");
        stack.addFirst(null);
        stack.addFirst("h3");
        sb.appendStack(stack, "<", "-", ">");
        assertEquals("<h1-null-h3>", sb.toS());
    }

    @Test
    public void stack_leftBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        sb.appendStack(stack, null, "-", ">");
        assertEquals("nullh1-h2-h3>", sb.toS());
    }

    @Test
    public void stack_rightBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        sb.appendStack(stack, "<", "-", null);
        assertEquals("<h1-h2-h3null", sb.toS());
    }

    @Test
    public void stack_separatorNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        assertThrows(NullPointerException.class, () -> sb.appendStack(stack, "<", null, ">"));
    }

    @Test
    public void stack_allNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = null;
        sb.appendStack(stack, null, null, null);
        assertEquals("null", sb.toS());
    }

    @Test
    public void stackWithMapper() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        Function<String, String> mapper = (x) -> x.substring(1);
        sb.appendStack(stack, "<", "-", ">", mapper);
        assertEquals("<1-2-3>", sb.toS());
    }

    @Test
    public void stackWithMapper_null() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = null;
        Function<String, String> mapper = (x) -> x.substring(1);
        sb.appendStack(stack, "<", "-", ">", mapper);
        assertEquals("null", sb.toS());
    }

    @Test
    public void stackWithMapper_elementNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        //LinkedList accepts null element while ArrayDeque does not.
        Deque<String> stack = new LinkedList<>();
        stack.addFirst("h1");
        stack.addFirst(null);
        stack.addFirst("h3");
        Function<String, String> mapper = (x) -> x == null ? null : x.substring(1);
        sb.appendStack(stack, "<", "-", ">", mapper);
        assertEquals("<1-null-3>", sb.toS());
    }

    @Test
    public void stackWithMapper_leftBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        Function<String, String> mapper = (x) -> x.substring(1);
        sb.appendStack(stack, null, "-", ">", mapper);
        assertEquals("null1-2-3>", sb.toS());
    }

    @Test
    public void stackWithMapper_rightBoundaryNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        Function<String, String> mapper = (x) -> x.substring(1);
        sb.appendStack(stack, "<", "-", null, mapper);
        assertEquals("<1-2-3null", sb.toS());
    }

    @Test
    public void stackWithMapper_separatorNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        Function<String, String> mapper = (x) -> x.substring(1);
        assertThrows(NullPointerException.class, () -> sb.appendStack(stack, "<", null, ">", mapper));
    }

    @Test
    public void stackWithMapper_mapperNull() {
        FreeStringBuilder sb = new FreeStringBuilder();
        Deque<String> stack = new ArrayDeque<>();
        stack.addFirst("h1");
        stack.addFirst("h2");
        stack.addFirst("h3");
        Function<String, String> mapper = null;
        assertThrows(NullPointerException.class, () -> sb.appendStack(stack, "<", "-", ">", mapper));
    }

    @Test
    public void stackWithMapper_allNull() {
        Deque<String> stack = null;
        Function<String, String> mapper = null;
        FreeStringBuilder sb = new FreeStringBuilder();
        sb.appendStack(stack, null, null, null, mapper);
        assertEquals("null", sb.toS());
    }

}
