package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("StringOperationCanBeSimplified")
public class EqualsUtilTest {

    @Test
    public void isEquals_obj() {
        assertTrue(EqualsUtil.isEquals((Object) null, (Object) null));
        assertFalse(EqualsUtil.isEquals(null, "abc"));
        assertFalse(EqualsUtil.isEquals("abc", null));
        assertTrue(EqualsUtil.isEquals(new String("abc"), new String("abc")));
    }

    @Test
    public void notEquals_obj() {
        assertFalse(EqualsUtil.notEquals((Object) null, (Object) null));
        assertTrue(EqualsUtil.notEquals(null, "abc"));
        assertTrue(EqualsUtil.notEquals("abc", null));
        assertFalse(EqualsUtil.notEquals(new String("abc"), new String("abc")));
    }

    @Test
    public void isEquals_objArray() {
        Integer[] a1 = new Integer[]{1, 3, 5, 7, 9};
        Integer[] a2 = new Integer[]{1, 3, 5, 7, 9};
        assertTrue(EqualsUtil.isEquals(a1, a2));
        Integer[] a3 = new Integer[]{1, 3, 5, 7};
        assertFalse(EqualsUtil.isEquals(a1, a3));
        Integer[] a4 = null;
        assertFalse(EqualsUtil.isEquals(a1, a4));
        assertFalse(EqualsUtil.isEquals(a4, a2));
        assertTrue(EqualsUtil.isEquals(a4, a4));
    }

    @Test
    public void notEquals_objArray() {
        Integer[] a1 = new Integer[]{1, 3, 5, 7, 9};
        Integer[] a2 = new Integer[]{1, 3, 5, 7, 9};
        assertFalse(EqualsUtil.notEquals(a1, a2));
        Integer[] a3 = new Integer[]{1, 3, 5, 7};
        assertTrue(EqualsUtil.notEquals(a1, a3));
        Integer[] a4 = null;
        assertTrue(EqualsUtil.notEquals(a1, a4));
        assertTrue(EqualsUtil.notEquals(a4, a2));
        assertFalse(EqualsUtil.notEquals(a4, a4));
    }

    @Test
    public void isEquals_int() {
        assertTrue(EqualsUtil.isEquals(2, 2));
        assertFalse(EqualsUtil.isEquals(2, -2));
    }

    @Test
    public void notEquals_int() {
        assertFalse(EqualsUtil.notEquals(2, 2));
        assertTrue(EqualsUtil.notEquals(2, -2));
    }

    @Test
    public void isEquals_intArray() {
        int[] a1 = new int[]{1, 3, 5, 7, 9};
        int[] a2 = new int[]{1, 3, 5, 7, 9};
        assertTrue(EqualsUtil.isEquals(a1, a2));
        int[] a3 = new int[]{1, 3, 5, 7};
        assertFalse(EqualsUtil.isEquals(a1, a3));
        int[] a4 = null;
        assertFalse(EqualsUtil.isEquals(a1, a4));
        assertFalse(EqualsUtil.isEquals(a4, a2));
        assertTrue(EqualsUtil.isEquals(a4, a4));
    }

    @Test
    public void notEquals_intArray() {
        int[] a1 = new int[]{1, 3, 5, 7, 9};
        int[] a2 = new int[]{1, 3, 5, 7, 9};
        assertFalse(EqualsUtil.notEquals(a1, a2));
        int[] a3 = new int[]{1, 3, 5, 7};
        assertTrue(EqualsUtil.notEquals(a1, a3));
        int[] a4 = null;
        assertTrue(EqualsUtil.notEquals(a1, a4));
        assertTrue(EqualsUtil.notEquals(a4, a2));
        assertFalse(EqualsUtil.notEquals(a4, a4));
    }

    @Test
    public void isEquals_long() {
        assertTrue(EqualsUtil.isEquals(2L, 2L));
        assertFalse(EqualsUtil.isEquals(2L, -2L));
    }

    @Test
    public void notEquals_long() {
        assertFalse(EqualsUtil.notEquals(2L, 2L));
        assertTrue(EqualsUtil.notEquals(2L, -2L));
    }

    @Test
    public void isEquals_longArray() {
        long[] a1 = new long[]{1L, 3L, 5L, 7L, 9L};
        long[] a2 = new long[]{1L, 3L, 5L, 7L, 9L};
        assertTrue(EqualsUtil.isEquals(a1, a2));
        long[] a3 = new long[]{1L, 3L, 5L, 7L};
        assertFalse(EqualsUtil.isEquals(a1, a3));
        long[] a4 = null;
        assertFalse(EqualsUtil.isEquals(a1, a4));
        assertFalse(EqualsUtil.isEquals(a4, a2));
        assertTrue(EqualsUtil.isEquals(a4, a4));
    }

    @Test
    public void notEquals_longArray() {
        long[] a1 = new long[]{1L, 3L, 5L, 7L, 9L};
        long[] a2 = new long[]{1L, 3L, 5L, 7L, 9L};
        assertFalse(EqualsUtil.notEquals(a1, a2));
        long[] a3 = new long[]{1L, 3L, 5L, 7L};
        assertTrue(EqualsUtil.notEquals(a1, a3));
        long[] a4 = null;
        assertTrue(EqualsUtil.notEquals(a1, a4));
        assertTrue(EqualsUtil.notEquals(a4, a2));
        assertFalse(EqualsUtil.notEquals(a4, a4));
    }

    @Test
    public void isEquals_boolean() {
        assertTrue(EqualsUtil.isEquals(true, true));
        assertFalse(EqualsUtil.isEquals(true, false));
    }

    @Test
    public void notEquals_boolean() {
        assertFalse(EqualsUtil.notEquals(true, true));
        assertTrue(EqualsUtil.notEquals(true, false));
    }

    @Test
    public void isEquals_booleanArray() {
        boolean[] a1 = new boolean[]{true, true, false, true};
        boolean[] a2 = new boolean[]{true, true, false, true};
        assertTrue(EqualsUtil.isEquals(a1, a2));
        boolean[] a3 = new boolean[]{true, true, false};
        assertFalse(EqualsUtil.isEquals(a1, a3));
        boolean[] a4 = null;
        assertFalse(EqualsUtil.isEquals(a1, a4));
        assertFalse(EqualsUtil.isEquals(a4, a2));
        assertTrue(EqualsUtil.isEquals(a4, a4));
    }

    @Test
    public void notEquals_booleanArray() {
        boolean[] a1 = new boolean[]{true, true, false, true};
        boolean[] a2 = new boolean[]{true, true, false, true};
        assertFalse(EqualsUtil.notEquals(a1, a2));
        boolean[] a3 = new boolean[]{true, true, false};
        assertTrue(EqualsUtil.notEquals(a1, a3));
        long[] a4 = null;
        assertTrue(EqualsUtil.notEquals(a1, a4));
        assertTrue(EqualsUtil.notEquals(a4, a2));
        assertFalse(EqualsUtil.notEquals(a4, a4));
    }

    @Test
    public void isEquals_double() {
        assertTrue(EqualsUtil.isEquals(3.2452, 3.247, 2));
        assertFalse(EqualsUtil.isEquals(3.2452, 3.247, 3));
    }

    @Test
    public void notEquals_double() {
        assertFalse(EqualsUtil.notEquals(3.2452, 3.247, 2));
        assertTrue(EqualsUtil.notEquals(3.2452, 3.247, 3));
    }

    @Test
    public void isEquals_doubleArray() {
        double[] a1 = new double[]{3.2452, 3.8927, 3.7736};
        double[] a2 = new double[]{3.2478, 3.8876, 3.7723};
        assertTrue(EqualsUtil.isEquals(a1, a2, 2));
        double[] a3 = new double[]{3.2452, 3.8927};
        assertFalse(EqualsUtil.isEquals(a1, a3, 2));
        double[] a4 = null;
        assertFalse(EqualsUtil.isEquals(a1, a4, 2));
        assertFalse(EqualsUtil.isEquals(a4, a2, 2));
        assertTrue(EqualsUtil.isEquals(a4, a4, 2));

        double[] a5 = new double[]{3.2453, 3.8627, 3.7722};
        assertFalse(EqualsUtil.isEquals(a1, a5, 2));
    }

    @Test
    public void notEquals_doubleArray() {
        double[] a1 = new double[]{3.2452, 3.8927, 3.7736};
        double[] a2 = new double[]{3.2478, 3.8876, 3.7723};
        assertFalse(EqualsUtil.notEquals(a1, a2, 2));
        double[] a3 = new double[]{3.2452, 3.8927};
        assertTrue(EqualsUtil.notEquals(a1, a3, 2));
        double[] a4 = null;
        assertTrue(EqualsUtil.notEquals(a1, a4, 2));
        assertTrue(EqualsUtil.notEquals(a4, a2, 2));
        assertFalse(EqualsUtil.notEquals(a4, a4, 2));

        double[] a5 = new double[]{3.2453, 3.8627, 3.7722};
        assertTrue(EqualsUtil.notEquals(a1, a5, 2));
    }

    @Test
    public void isEquals_char() {
        assertTrue(EqualsUtil.isEquals('a', 'a'));
        assertFalse(EqualsUtil.isEquals('a', 'A'));
    }

    @Test
    public void notEquals_char() {
        assertFalse(EqualsUtil.notEquals('a', 'a'));
        assertTrue(EqualsUtil.notEquals('a', 'A'));
    }

    @Test
    public void isEqualsIgnoreCase() {
        assertTrue(EqualsUtil.isEqualsIgnoreCase("abc", "aBc"));
        assertFalse(EqualsUtil.isEqualsIgnoreCase("abc", "cba"));
        assertTrue(EqualsUtil.isEqualsIgnoreCase((String) null, (String) null));
        assertFalse(EqualsUtil.isEqualsIgnoreCase(null, ""));
        assertFalse(EqualsUtil.isEqualsIgnoreCase("", null));
    }

    @Test
    public void notEqualsIgnoreCase() {
        assertFalse(EqualsUtil.notEqualsIgnoreCase("abc", "aBc"));
        assertTrue(EqualsUtil.notEqualsIgnoreCase("abc", "cba"));
        assertFalse(EqualsUtil.notEqualsIgnoreCase((String) null, (String) null));
        assertTrue(EqualsUtil.notEqualsIgnoreCase(null, ""));
        assertTrue(EqualsUtil.notEqualsIgnoreCase("", null));
    }

    @Test
    public void isEqualsIgnoreCase_stringArray() {
        String[] a1 = new String[]{"abc", "def"};
        String[] a2 = new String[]{"ABC", "DEF"};
        assertTrue(EqualsUtil.isEqualsIgnoreCase(a1, a2));
        String[] a3 = new String[]{"abc"};
        assertFalse(EqualsUtil.isEqualsIgnoreCase(a1, a3));
        String[] a4 = null;
        assertFalse(EqualsUtil.isEqualsIgnoreCase(a1, a4));
        assertFalse(EqualsUtil.isEqualsIgnoreCase(a4, a2));
        assertTrue(EqualsUtil.isEqualsIgnoreCase(a4, a4));

        String[] a5 = new String[]{"aBc", "de"};
        assertFalse(EqualsUtil.isEqualsIgnoreCase(a1, a5));
    }

    @Test
    public void notEqualsIgnoreCase_stringArray() {
        String[] a1 = new String[]{"abc", "def"};
        String[] a2 = new String[]{"ABC", "DEF"};
        assertFalse(EqualsUtil.notEqualsIgnoreCase(a1, a2));
        String[] a3 = new String[]{"abc"};
        assertTrue(EqualsUtil.notEqualsIgnoreCase(a1, a3));
        String[] a4 = null;
        assertTrue(EqualsUtil.notEqualsIgnoreCase(a1, a4));
        assertTrue(EqualsUtil.notEqualsIgnoreCase(a4, a2));
        assertFalse(EqualsUtil.notEqualsIgnoreCase(a4, a4));

        String[] a5 = new String[]{"aBc", "de"};
        assertTrue(EqualsUtil.notEqualsIgnoreCase(a1, a5));
    }

    @Test
    public void isEquals_date() {
        Date d1 = DateUtil.toDate("2012-04-03 15:24:34", DateUtil.LONG_FORMAT);
        Date d2 = DateUtil.toDate("2012-04-03 15:24:42", DateUtil.LONG_FORMAT);
        assertTrue(EqualsUtil.isEquals(d1, d2, Calendar.MINUTE));
        assertFalse(EqualsUtil.isEquals(d1, d2, Calendar.SECOND));
        assertFalse(EqualsUtil.isEquals(d1, null, Calendar.MINUTE));
        assertFalse(EqualsUtil.isEquals(null, d2, Calendar.MINUTE));
        assertTrue(EqualsUtil.isEquals((Date) null, (Date) null, Calendar.MINUTE));

        assertThrows(IllegalArgumentException.class, () -> EqualsUtil.isEquals(d1, d2, 100));
    }

    @Test
    public void notEquals_date() {
        Date d1 = DateUtil.toDate("2012-04-03 15:24:34", DateUtil.LONG_FORMAT);
        Date d2 = DateUtil.toDate("2012-04-03 15:24:42", DateUtil.LONG_FORMAT);
        assertFalse(EqualsUtil.notEquals(d1, d2, Calendar.MINUTE));
        assertTrue(EqualsUtil.notEquals(d1, d2, Calendar.SECOND));
        assertTrue(EqualsUtil.notEquals(d1, null, Calendar.MINUTE));
        assertTrue(EqualsUtil.notEquals(null, d2, Calendar.MINUTE));
        assertFalse(EqualsUtil.notEquals((Date) null, (Date) null, Calendar.MINUTE));

        assertThrows(IllegalArgumentException.class, () -> EqualsUtil.notEquals(d1, d2, 100));
    }

}
