package name.slhynju;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class ApplicationExceptionTest {

    @Test
    public void message() {
        var e = new ApplicationException("abc");
        assertEquals("abc", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void message_null() {
        String message = null;
        var e = new ApplicationException(message);
        assertNull(e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void charSequence() {
        CharSequence sb = new StringBuilder("abc");
        var e = new ApplicationException(sb);
        assertEquals("abc", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void charSequence_null() {
        CharSequence sb = null;
        assertThrows(NullPointerException.class, () -> new ApplicationException(sb));
    }

    @Test
    public void messageAndCause() {
        var cause = new RuntimeException("def");
        var e = new ApplicationException("abc", cause);
        assertEquals("abc", e.getMessage());
        assertSame(cause, e.getCause());
    }

    @Test
    public void messageAndCause_messageNull() {
        String message = null;
        var cause = new RuntimeException("def");
        var e = new ApplicationException(message, cause);
        assertNull(e.getMessage());
        assertSame(cause, e.getCause());
    }

    @Test
    public void messageAndCause_causeNull() {
        var e = new ApplicationException("abc", null);
        assertEquals("abc", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void messageAndCause_bothNull() {
        String message = null;
        var e = new ApplicationException(message, null);
        assertNull(e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void charSequenceAndCause() {
        CharSequence sb = new StringBuilder("abc");
        var cause = new RuntimeException("def");
        var e = new ApplicationException(sb, cause);
        assertEquals("abc", e.getMessage());
        assertSame(cause, e.getCause());
    }

    @Test
    public void charSequenceAndCause_messageNull() {
        CharSequence sb = null;
        Throwable cause = new RuntimeException("def");
        assertThrows(NullPointerException.class, () -> new ApplicationException(sb, cause));
    }

    @Test
    public void charSequenceAndCause_causeNull() {
        CharSequence sb = new StringBuilder("abc");
        var e = new ApplicationException(sb, null);
        assertEquals("abc", e.getMessage());
        assertNull(e.getCause());
    }

    @Test
    public void charSequenceAndCause_bothNull() {
        CharSequence sb = null;
        Throwable cause = null;
        assertThrows(NullPointerException.class, () -> new ApplicationException(sb, cause));
    }

}
