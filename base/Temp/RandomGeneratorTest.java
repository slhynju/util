package name.slhynju.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings("static-method")
public class RandomGeneratorTest {

    @SuppressWarnings("unused")
    @Test
    public void testRandomGenerator() {
        new RandomGenerator();
    }

    @SuppressWarnings("unused")
    @Test
    public void testRandomGeneratorLong() {
        new RandomGenerator(1025L);
    }

    @Test
    public void testNextInt() {
        RandomGenerator r = new RandomGenerator();
        for (int I = 0; I < 10000; I++) {
            int n = r.nextInt(287);
            assertTrue(n >= 0 && n < 287);
        }
    }

    @Test
    public void testNextString() {
        RandomGenerator r = new RandomGenerator();
        for (int I = 0; I < 10000; I++) {
            String s = r.nextLetterString(5, 8);
            int n = s.length();
            assertTrue(n >= 5 && n < 8);
        }
    }

    @Test
    public void testNextChar() {
        RandomGenerator r = new RandomGenerator();
        for (int I = 0; I < 10000; I++) {
            char n = r.nextLetter();
            assertTrue((n >= 'a' && n <= 'z') || (n >= 'A' && n <= 'Z'));
        }
    }

}
