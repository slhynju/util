package name.slhynju.util;

import name.slhynju.ApplicationException;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

/**
 * Utility class to generate random data.
 *
 * @author slhynju
 */
public class RandomGenerator {

    private final Random rand;

    public RandomGenerator() {
        rand = new Random();
    }

    public RandomGenerator(long seed) {
        rand = new Random(seed);
    }

    public boolean nextBoolean() {
        return rand.nextBoolean();
    }

    public byte nextByte() {
        return (byte) rand.nextInt(256);
    }

    public byte nextByte(int bound) {
        if (bound <= 0 || bound > 256) {
            StringBuilder sb = new StringBuilder();
            sb.append("Illegal argument of bound ").append(bound).append(". It shall be between 1 to 256.");
            throw new ApplicationException(sb);
        }
        return (byte) rand.nextInt(bound);
    }

    public int nextInt() {
        return rand.nextInt();
    }

    public int nextInt(int bound) {
        return rand.nextInt(bound);
    }

    public long nextLong() {
        return rand.nextLong();
    }

    public float nextFloat() {
        return rand.nextFloat();
    }

    public double nextDouble() {
        return rand.nextDouble();
    }

    @NotNull
    public String nextLetterString(int minLength, int maxLength) {
        int length = minLength + nextInt(maxLength - minLength);
        StringBuilder sb = new StringBuilder(length);
        for (int I = 0; I < length; I++) {
            char c = nextLetter();
            sb.append(c);
        }
        return sb.toString();
    }

    public char nextLetter() {
        int i = nextInt(52);
        if (i < 26) {
            return (char) ('a' + i);
        }
        return (char) ('A' + i - 26);
    }

    @NotNull
    public String nextDigitString(int minLength, int maxLength) {
        int length = minLength + nextInt(maxLength - minLength);
        StringBuilder sb = new StringBuilder(length);
        for (int I = 0; I < length; I++) {
            char c = nextDigit();
            sb.append(c);
        }
        return sb.toString();
    }

    public char nextDigit() {
        int i = nextInt(10);
        return (char) ('0' + i);
    }

    @NotNull
    public String nextLetterOrDigitString(int minLength, int maxLength) {
        int length = minLength + nextInt(maxLength - minLength);
        StringBuilder sb = new StringBuilder(length);
        for (int I = 0; I < length; I++) {
            char c = nextLetterOrDigit();
            sb.append(c);
        }
        return sb.toString();
    }

    public char nextLetterOrDigit() {
        int i = nextInt(62);
        if (i < 26) {
            return (char) ('a' + i);
        } else if (i < 52) {
            return (char) ('A' + i - 26);
        } else {
            return (char) ('0' + i - 52);
        }
    }

}
