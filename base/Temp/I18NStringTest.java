package name.slhynju.util;

import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

public class I18NStringTest {

    @Test
    public void testLocale() {
        I18NString s = new I18NString();
        s.setText(Locale.CANADA, "abc");
        assertEquals("abc", s.getText(Locale.CANADA));
    }

    @Test
    public void testDefault() {
        I18NResource.setDefaultLocale(Locale.ENGLISH);
        I18NString s = new I18NString();
        s.setDefaultText("ff");
        assertEquals("ff", s.getDefaultText());
    }

    @Test
    public void testEquals() {
        I18NResource.setDefaultLocale(Locale.ENGLISH);
        I18NString s1 = new I18NString();
        I18NString s2 = new I18NString();
        s1.setDefaultText("ff");
        s2.setDefaultText("ff");
        s1.setText(Locale.CANADA, "abc");
        s2.setText(Locale.CANADA, "abc");
        assertTrue(s1.equals(s2));
        s2.setText(Locale.ITALIAN, "123");
        assertFalse(s1.equals(s2));
        assertFalse(s1.equals(null));

        assertEquals("ff", s1.toString());
    }

    @Test
    public void testIsEmpty() {
        I18NResource.setDefaultLocale(Locale.ITALIAN);
        I18NString s = new I18NString();
        assertTrue( s.isEmpty());
        s.setDefaultText("abc");
        assertFalse(s.isEmpty());
        s.setDefaultText("");
        assertTrue(s.isEmpty());
    }

    @Test
    public void testLength() {
        I18NResource.setDefaultLocale(Locale.ITALIAN);
        I18NString s = new I18NString();
        assertEquals(0, s.length());
        s.setDefaultText("abc");
        assertEquals(3, s.length());
        s.setDefaultText("");
        assertEquals(0, s.length());
    }
}
