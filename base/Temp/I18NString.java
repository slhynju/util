package name.slhynju.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;

public class I18NString implements CharSequence, Serializable {

    private static final long serialVersionUID = -523992932983786211L;

    private final HashMap<Locale, String> texts;

    public I18NString() {
        texts = new HashMap<>();
    }

    public void setText(@NotNull Locale locale, @NotNull String text) {
        texts.put(locale, text);
    }

    @Nullable
    public String getText(@NotNull Locale locale) {
        return texts.get(locale);
    }

    public void setDefaultText(@NotNull String text) {
        setText(I18NResource.getDefaultLocale(), text);
    }

    @Nullable
    public String getDefaultText() {
        return getText(I18NResource.getDefaultLocale());
    }

    public boolean isEmpty() {
        return StringUtil.isEmpty(getDefaultText());
    }

    public int length() {
        String s = getDefaultText();
        if (s == null) {
            return 0;
        }
        return s.length();
    }

    @Override
    public char charAt(int index) {
        String s = getDefaultText();
        if (s == null) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return s.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        String s = getDefaultText();
        if (s == null) {
            return null;
        }
        return s.subSequence(start, end);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (o instanceof I18NString) {
            I18NString other = (I18NString) o;
            return EqualsUtil.isEquals(texts, other.texts);
        }
        return false;
    }

    @Override
    @Nullable
    public String toString() {
        return getDefaultText();
    }

    @Override
    public int hashCode() {
        return texts.hashCode();
    }
}
