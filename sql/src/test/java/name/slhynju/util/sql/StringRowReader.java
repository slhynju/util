package name.slhynju.util.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author slhynju
 */
public class StringRowReader implements RowReader<String> {

    @Override
    public String readRow(ResultSet rs) throws SQLException {
        return rs.getString(1);
    }
}
