package name.slhynju.util.sql;

import name.slhynju.ApplicationException;
import name.slhynju.util.DateUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("static-method")
public class SQLUtilTest {

    private Connection conn;

    private PreparedStatement ps;

    private ResultSet rs;

    private Logger log;

    @BeforeEach
    public void setUp() {
        conn = mock(Connection.class);
        ps = mock(PreparedStatement.class);
        rs = mock(ResultSet.class);
        log = mock(Logger.class);
    }

    @SuppressWarnings("unused")
    @Test
    public void dummyConstructor() {
        new SQLUtil();
    }

    @Test
    public void testToDateDate() {
        Date d = DateUtil.toDate("2012-03-04", "yyyy-MM-dd");
        java.sql.Date sqlD = new java.sql.Date(d.getTime());
        Date d2 = SQLUtil.toDate(sqlD);
        assertEquals(d, d2);
        Date d3 = SQLUtil.toDate((java.sql.Date) null);
        assertNull(d3);
    }

    @Test
    public void testToSQLDate() {
        Date d = DateUtil.toDate("2012-03-04", "yyyy-MM-dd");
        java.sql.Date sqlD = SQLUtil.toSQLDate(d);
        assertEquals("2012-03-04", DateUtil.toS(sqlD, "yyyy-MM-dd"));
        assertNull(SQLUtil.toSQLDate(null));
    }

    @Test
    public void testToDateTimestamp() {
        Date d = DateUtil.toDate("2012-03-04 05:12:25", "yyyy-MM-dd HH:mm:ss");
        java.sql.Timestamp sqlD = new java.sql.Timestamp(d.getTime());
        Date d2 = SQLUtil.toDate(sqlD);
        assertEquals(d, d2);
        Date d3 = SQLUtil.toDate((java.sql.Timestamp) null);
        assertNull(d3);
    }

    @Test
    public void testToSQLTimestamp() {
        Date d = DateUtil.toDate("2012-03-04 05:12:25", "yyyy-MM-dd HH:mm:ss");
        java.sql.Timestamp sqlD = SQLUtil.toSQLTimestamp(d);
        assertEquals(d.getTime(), sqlD.getTime());
        assertNull(SQLUtil.toSQLTimestamp((Date)null));
    }

    @Test
    public void toInstant() {
        Date d = DateUtil.toDate("2012-03-04 05:12:25", "yyyy-MM-dd HH:mm:ss");
        java.sql.Timestamp sqlD = SQLUtil.toSQLTimestamp(d);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(2012,3,4,5,12,25, 0, ZoneId.systemDefault());
        Instant t = zonedDateTime.toInstant();
        assertEquals(t, SQLUtil.toInstant(sqlD));
        sqlD = null;
        assertNull(SQLUtil.toInstant(sqlD));
    }

    @Test
    public void instantToSQLTimestamp() {
        Date d = DateUtil.toDate("2012-03-04 05:12:25", "yyyy-MM-dd HH:mm:ss");
        java.sql.Timestamp sqlD = SQLUtil.toSQLTimestamp(d);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(2012,3,4,5,12,25, 0, ZoneId.systemDefault());
        Instant t = zonedDateTime.toInstant();
        assertEquals(sqlD, SQLUtil.toSQLTimestamp(t));
        t = null;
        assertNull(SQLUtil.toSQLTimestamp(t));
    }

    @Test
    public void toZonedDateTime() {
        Date d = DateUtil.toDate("2012-03-04 05:12:25", "yyyy-MM-dd HH:mm:ss");
        java.sql.Timestamp sqlD = SQLUtil.toSQLTimestamp(d);
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zonedDateTime = ZonedDateTime.of(2012,3,4,5,12,25, 0, zoneId);
        assertEquals(zonedDateTime, SQLUtil.toZonedDateTime( sqlD, zoneId ));
        sqlD = null;
        assertNull(SQLUtil.toZonedDateTime(sqlD, zoneId));
    }

    @Test
    public void zonedDateTimeToSQLTimestamp() {
        Date d = DateUtil.toDate("2012-03-04 05:12:25", "yyyy-MM-dd HH:mm:ss");
        java.sql.Timestamp sqlD = SQLUtil.toSQLTimestamp(d);
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zonedDateTime = ZonedDateTime.of(2012,3,4,5,12,25, 0, zoneId);
        assertEquals(sqlD, SQLUtil.toSQLTimestamp(zonedDateTime));
        zonedDateTime = null;
        assertNull(SQLUtil.toSQLTimestamp(zonedDateTime));
    }

    @Test
    public void testCommit() throws Exception {
        SQLUtil.commit(conn, log);
        verify(log, never()).isErrorEnabled();
    }

    @Test
    public void testCommitException() throws Exception {
        doThrow(SQLException.class).when(conn).commit();
        when(log.isErrorEnabled()).thenReturn(Boolean.TRUE);
        SQLUtil.commit(conn, log);
        verify(conn).commit();
        verify(log).isErrorEnabled();
        verify(log).error(anyString(), any(SQLException.class));
    }

    @Test
    public void testCommitException2() throws Exception {
        doThrow(SQLException.class).when(conn).commit();
        when(log.isErrorEnabled()).thenReturn(Boolean.FALSE);
        SQLUtil.commit(conn, log);
        verify(conn).commit();
        verify(log).isErrorEnabled();
        verify(log, never()).error(anyString(), any(SQLException.class));
    }

    @Test
    public void testCommitNull() {
        SQLUtil.commit(null, log);
        verify(log, never()).isErrorEnabled();
    }

    @Test
    public void testRollback() throws Exception {
        SQLUtil.rollback(conn, log);
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void tesRollbackException() throws Exception {
        doThrow(SQLException.class).when(conn).rollback();
        when(log.isWarnEnabled()).thenReturn(Boolean.TRUE);
        SQLUtil.rollback(conn, log);
        verify(conn).rollback();
        verify(log).isWarnEnabled();
        verify(log).warn(anyString(), any(SQLException.class));
    }

    @Test
    public void tesRollbackException2() throws Exception {
        doThrow(SQLException.class).when(conn).rollback();
        when(log.isWarnEnabled()).thenReturn(Boolean.FALSE);
        SQLUtil.rollback(conn, log);
        verify(conn).rollback();
        verify(log).isWarnEnabled();
        verify(log, never()).warn(anyString(), any(SQLException.class));
    }

    @Test
    public void testRollbackNull() {
        SQLUtil.rollback(null, log);
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void testCloseResultSetLogger() throws Exception {
        SQLUtil.close(rs, log);
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void testCloseResultSetException() throws Exception {
        doThrow(SQLException.class).when(rs).close();
        when(log.isWarnEnabled()).thenReturn(Boolean.TRUE);
        SQLUtil.close(rs, log);
        verify(rs).close();
        verify(log).isWarnEnabled();
        verify(log).warn(anyString(), any(SQLException.class));
    }

    @Test
    public void testCloseResultSetException2() throws Exception {
        doThrow(SQLException.class).when(rs).close();
        when(log.isWarnEnabled()).thenReturn(Boolean.FALSE);
        SQLUtil.close(rs, log);
        verify(rs).close();
        verify(log).isWarnEnabled();
        verify(log, never()).warn(anyString(), any(SQLException.class));
    }

    @Test
    public void testCloseResultSetNull() {
        SQLUtil.close((ResultSet) null, log);
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void testClosePreparedStatementLogger() throws Exception {
        SQLUtil.close(ps, log);
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void testClosePreparedStatementLoggerException() throws Exception {
        doThrow(SQLException.class).when(ps).close();
        when(log.isWarnEnabled()).thenReturn(Boolean.TRUE);
        SQLUtil.close(ps, log);
        verify(ps).close();
        verify(log).isWarnEnabled();
        verify(log).warn(anyString(), any(SQLException.class));
    }

    @Test
    public void testClosePreparedStatementLoggerException2() throws Exception {
        doThrow(SQLException.class).when(ps).close();
        when(log.isWarnEnabled()).thenReturn(Boolean.FALSE);
        SQLUtil.close(ps, log);
        verify(ps).close();
        verify(log).isWarnEnabled();
        verify(log, never()).warn(anyString(), any(SQLException.class));
    }

    @Test
    public void testClosePreparedStatementNull() {
        SQLUtil.close((PreparedStatement) null, log);
        verify(log, never()).isWarnEnabled();
    }

    @Test
    public void testCloseConnectionLogger() throws Exception {
        SQLUtil.close(conn, log);
        verify(log, never()).isErrorEnabled();
    }

    @Test
    public void testCloseConnectionException() throws Exception {
        doThrow(SQLException.class).when(conn).close();
        when(log.isErrorEnabled()).thenReturn(Boolean.TRUE);
        SQLUtil.close(conn, log);
        verify(conn).close();
        verify(log).isErrorEnabled();
        verify(log).error(anyString(), any(SQLException.class));
    }

    @Test
    public void testCloseConnectionException2() throws Exception {
        doThrow(SQLException.class).when(conn).close();
        when(log.isErrorEnabled()).thenReturn(Boolean.FALSE);
        SQLUtil.close(conn, log);
        verify(conn).close();
        verify(log).isErrorEnabled();
        verify(log, never()).error(anyString(), any(SQLException.class));
    }

    @Test
    public void testCloseConnectionNull() {
        SQLUtil.close((Connection) null, log);
        verify(log, never()).isErrorEnabled();
    }

    @Test
    public void testExecuteQuery() throws Exception {
        when(ps.executeQuery()).thenReturn(rs);
        when(rs.next()).thenReturn(Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.FALSE);
        when(rs.getString(1)).thenReturn("abc", null, "def");
        List<String> l = SQLUtil.executeQuery(ps, new StringRowReader());
        assertEquals(2, l.size());
        assertEquals("abc", l.get(0));
        assertEquals("def", l.get(1));
        verify(rs).close();
    }

    @Test
    public void testExecuteQueryFirst() throws Exception {
        when(ps.executeQuery()).thenReturn(rs);
        when(rs.next()).thenReturn(Boolean.TRUE);
        when(rs.getString(1)).thenReturn("abc");
        String s = SQLUtil.executeQueryFirst(ps, new StringRowReader());
        assertEquals("abc", s);
        verify(rs).close();
    }

    @Test
    public void testExecuteCount() throws Exception {
        when(ps.executeQuery()).thenReturn(rs);
        when(rs.next()).thenReturn(Boolean.TRUE);
        when(rs.getInt(1)).thenReturn(Integer.valueOf(120));
        int count = SQLUtil.executeCount(ps);
        assertEquals(120, count);
    }

    @Test
    public void testExecuteCount2() throws Exception {
        when(ps.executeQuery()).thenReturn(rs);
        when(rs.next()).thenReturn(Boolean.FALSE);
        assertThrows(ApplicationException.class, () -> {
            SQLUtil.executeCount(ps);
        });
    }

    @Test
    public void testExecuteCountLong() throws Exception {
        when(ps.executeQuery()).thenReturn(rs);
        when(rs.next()).thenReturn(Boolean.TRUE);
        when(rs.getLong(1)).thenReturn(Long.valueOf(120L));
        long count = SQLUtil.executeCountLong(ps);
        assertEquals(120L, count);
    }

    @Test
    public void testExecuteCountLong2() throws Exception {
        when(ps.executeQuery()).thenReturn(rs);
        when(rs.next()).thenReturn(Boolean.FALSE);
        assertThrows(ApplicationException.class, () -> {
            SQLUtil.executeCountLong(ps);
        });
    }

}
