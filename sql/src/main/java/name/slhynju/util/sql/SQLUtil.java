package name.slhynju.util.sql;

import name.slhynju.UnexpectedApplicationFlowException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * JDBC utility methods.
 *
 * @author slhynju
 */
public final class SQLUtil {

    /**
     * Transforms a SQL {@code Date} to a Java {@code Date}.
     * @param sqlDate a SQL {@code Date}. It can be {@code null}.
     * @return the transformed Java {@code Date}.
     */
    public static @Nullable Date toDate(@Nullable java.sql.Date sqlDate) {
        return sqlDate == null ? null : new Date(sqlDate.getTime());
    }

    /**
     * Transforms a Java {@code Date} to a SQL {@code Date}.
     * @param date a Java {@code Date}. It can be {@code null}.
     * @return the transformed SQL {@code Date}.
     */
    public static @Nullable java.sql.Date toSQLDate(@Nullable Date date) {
        return date == null ? null : new java.sql.Date(date.getTime());
    }

    /**
     * Transforms a SQL {@code Timestamp} to a Java {@code Date}.
     * @param sqlTimestamp a SQL {@code Timestamp}. It can be {@code null}.
     * @return the transformed Java {@code Date}.
     */
    @SuppressWarnings("UnnecessaryFullyQualifiedName")
    public static @Nullable Date toDate(@Nullable java.sql.Timestamp sqlTimestamp) {
        return sqlTimestamp == null ? null : new Date(sqlTimestamp.getTime());
    }

    /**
     * Transforms a Java {@code Date} to a SQL {@code Timestamp}.
     * @param date a Java {@code Date}. It can be {@code null}.
     * @return the transformed SQL {@code Timestamp}.
     */
    @SuppressWarnings("UnnecessaryFullyQualifiedName")
    public static @Nullable java.sql.Timestamp toSQLTimestamp(@Nullable Date date) {
        return date == null ? null : new java.sql.Timestamp(date.getTime());
    }

    /**
     * Transforms a SQL {@code Timestamp} to an {@code Instant}.
     * @param sqlTimestamp a SQL {@code Timestamp}. It can be {@code null}.
     * @return the transformed {@code Instant}.
     */
    @SuppressWarnings("UnnecessaryFullyQualifiedName")
    public static @Nullable Instant toInstant(@Nullable java.sql.Timestamp sqlTimestamp) {
        return  sqlTimestamp == null? null : Instant.ofEpochMilli( sqlTimestamp.getTime() );
    }

    /**
     * Transforms an {@code Instant} to a SQL {@code Timestamp}.
     * @param instant an {@code Instant}. It can be {@code null}.
     * @return the transformed SQL {@code Timestamp}.
     */
    @SuppressWarnings("UnnecessaryFullyQualifiedName")
    public static @Nullable java.sql.Timestamp toSQLTimestamp(@Nullable Instant instant) {
        return instant == null ? null : new java.sql.Timestamp(instant.toEpochMilli());
    }

    /**
     * Transforms a SQL {@code Timestamp} to a {@code ZonedDateTime}.
     * @param sqlTimestamp a SQL {@code Timestamp}. It can be {@code null}.
     * @param zoneId a target {@code ZoneId}. It shall not be {@code null}.
     * @return the transformed {@code ZonedDateTime}.
     */
    @SuppressWarnings("UnnecessaryFullyQualifiedName")
    public static @Nullable ZonedDateTime toZonedDateTime(@Nullable java.sql.Timestamp sqlTimestamp, @NotNull ZoneId zoneId) {
        return sqlTimestamp == null? null : ZonedDateTime.ofInstant( Instant.ofEpochMilli(sqlTimestamp.getTime()) , zoneId );
    }

    /**
     * Transforms a {@code ZonedDateTime} to a SQL {@code Timestamp}.
     * @param zonedDateTime a {@code ZonedDateTime}. It can be {@code null}.
     * @return the transformed SQL {@code Timestamp}.
     */
    @SuppressWarnings("UnnecessaryFullyQualifiedName")
    public static @Nullable java.sql.Timestamp toSQLTimestamp(@Nullable ZonedDateTime zonedDateTime) {
        return zonedDateTime == null? null : new java.sql.Timestamp( zonedDateTime.toInstant().toEpochMilli());
    }

    /**
     * Commits the current JDBC transaction.
     * @param con a JDBC {@code Connection}. It can be {@code null}.
     * @param log a {@code Logger}. It shall not be {@code null}.
     */
    public static void commit(@Nullable Connection con, @NotNull Logger log) {
        if (con == null) {
            return;
        }
        try {
            con.commit();
        } catch (SQLException e) {
            if (log.isErrorEnabled()) {
                log.error("Failed to commit transaction.", e);
            }
        }
    }

    /**
     * Rolls back the current JDBC transaction.
     * @param con a JDBC {@code Connection}. It can be {@code null}.
     * @param log a {@code Logger}. It shall not be {@code null}.
     */
    public static void rollback(@Nullable Connection con, @NotNull Logger log) {
        if (con == null) {
            return;
        }
        try {
            con.rollback();
        } catch (SQLException e) {
            if (log.isWarnEnabled()) {
                log.warn("Failed to rollback transaction.", e);
            }
        }
    }

    /**
     * Closes a {@code ResultSet}.
     * @param rs a {@code ResultSet}. It can be {@code null}.
     * @param log a {@code Logger}. It shall not be {@code null}.
     */
    public static void close(@Nullable ResultSet rs, @NotNull Logger log) {
        if (rs == null) {
            return;
        }
        try {
            rs.close();
        } catch (SQLException e) {
            if (log.isWarnEnabled()) {
                log.warn("Failed to close ResultSet.", e);
            }
        }
    }

    /**
     * Closes a {@code PreparedStatement}.
     * @param ps a {@code PreparedStatement}. It can be {@code null}.
     * @param log a {@code Logger}. It shall not be {@code null}.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static void close(@Nullable PreparedStatement ps, @NotNull Logger log) {
        if (ps == null) {
            return;
        }
        try {
            ps.close();
        } catch (SQLException e) {
            if (log.isWarnEnabled()) {
                log.warn("Failed to close PreparedStatement.", e);
            }
        }
    }

    /**
     * Closes a {@code Connection}.
     * @param con a {@code Connection}. It can be {@code null}.
     * @param log a {@code Logger}. It shall not be {@code null}.
     */
    public static void close(@Nullable Connection con, @NotNull Logger log) {
        if (con == null) {
            return;
        }
        try {
            con.close();
        } catch (SQLException e) {
            if (log.isErrorEnabled()) {
                log.error("Failed to close connection.", e);
            }
        }
    }

    /**
     * Executes a {@code SELECT} query and parses the {@code ResultSet} to a {@code List} with a custom {@code RowReader}.
     * @param ps a {@code PreparedStatement}. It shall not be {@code null}.
     * @param reader a custom {@code RowReader}. It shall not be {@code null}.
     * @param <T> the target type.
     * @return a {@code List} of parsed {@code Objects}.
     * @throws SQLException if {@code SQLException} occurs
     */
    @SuppressWarnings("BoundedWildcard")
    public static @NotNull <T> List<T> executeQuery(@NotNull PreparedStatement ps, @NotNull RowReader<T> reader) throws SQLException {
        //noinspection CollectionWithoutInitialCapacity
        List<T> list = new ArrayList<>();
        try (ResultSet rs = ps.executeQuery()) {
            //noinspection ConstantConditions
            while (rs.next()) {
                T o = reader.readRow(rs);
                if (o != null) {
                    list.add(o);
                }
            }
        }
        //noinspection ReturnSeparatedFromComputation
        return list;
    }

    /**
     * Executes a {@code SELECT} query and parses the first row to an {@code Object}.
     * @param ps a {@code PreparedStatement}. It shall not be {@code null}.
     * @param reader a custom {@code RowReader}. It shall not be {@code null}.
     * @param <T> the target type.
     * @return the parsed {@code Object}. Returns null if all rows cannot be parsed.
     * @throws SQLException if {@code SQLException} occurs
     */
    public static @Nullable <T> T executeQueryFirst(@NotNull PreparedStatement ps, @NotNull RowReader<T> reader) throws SQLException {
        try (ResultSet rs = ps.executeQuery()) {
            //noinspection ConstantConditions
            while (rs.next()) {
                T obj = reader.readRow(rs);
                if (obj != null) {
                    return obj;
                }
            }
        }
        return null;
    }

    /**
     * Executes a {@code SELECT COUNT} query and returns the result as an {@code int} number.
     * @param ps a {@code PreparedStatement}. It shall not be {@code null}.
     * @return the result.
     * @throws SQLException if {@code SQLException} occurs
     */
    public static int executeCount(@NotNull PreparedStatement ps) throws SQLException {
        try (ResultSet rs = ps.executeQuery()) {
            //noinspection ConstantConditions
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        // shall never happen
        throw new UnexpectedApplicationFlowException("No ResultSet returned from query.");
    }

    /**
     * Executes a {@code SELECT COUNT} query and returns the result as a {@code long} number.
     * @param ps a {@code PreparedStatement}. It shall not be {@code null}.
     * @return the result.
     * @throws SQLException if {@code SQLException} occurs
     */
    public static long executeCountLong(@NotNull PreparedStatement ps) throws SQLException {
        try (ResultSet rs = ps.executeQuery()) {
            //noinspection ConstantConditions
            if (rs.next()) {
                return rs.getLong(1);
            }
        }
        // shall never happen.
        throw new UnexpectedApplicationFlowException("No ResultSet returned from query.");
    }

}
