/**
 * Utility classes for JDBC operations.
 *
 * @author slhynju
 */
package name.slhynju.util.sql;