package name.slhynju.util.sql;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Reads a single row from {@code ResultSet} and parses it to an {@code Object}.
 * @param <T> the target type.
 * @author slhynju
 */
@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface RowReader<T> {

    /**
     * Reads a row from {@code ResultSet} and parses it to an {@code Object}.
     * @param rs a {@code ResultSet}. It shall not be {@code null}.
     * @return the parsed object. Returns {@code null} to skip current row.
     * @throws SQLException if {@code SQLException} occurs.
     */
    @Nullable
    T readRow(@NotNull ResultSet rs) throws SQLException;

}
