module name.slhynju.util.sql {
    requires java.sql;
    requires org.slf4j;
    requires org.jetbrains.annotations;
    requires name.slhynju.util;

    exports name.slhynju.util.sql;
}