package name.slhynju.util.validation.impl;

import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.SampleBean;
import name.slhynju.util.validation.rules.Rule;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.expression.EvaluationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author slhynju
 */
public class RuleValidatorTest {

    @Test
    public void validate() {
        Object target = new SampleBean();
        List<Failure> failures = new ArrayList<>();
        Consumer<Failure> action = f -> failures.add(f);
        Rule r1 = mock(Rule.class);
        Rule r2 = mock(Rule.class);
        Rule r3 = mock(Rule.class);
        Failure f1 = new Failure("path1.a.b", "value1", RuleValidator.class);
        doAnswer(invocationOnMock -> {
            Consumer action1 = (Consumer)invocationOnMock.getArgument(2);
            action1.accept(f1);
            return null;
        }).when(r1).apply(same(target), any(EvaluationContext.class), same(action));
        doNothing().when(r2).apply(same(target), any(EvaluationContext.class), same(action));
        Failure f31 = new Failure("path3.a.b", "value3", RuleValidator.class);
        Failure f32 = new Failure("path3.a.b", "value3", RuleValidator.class);
        doAnswer( invocationOnMock ->{
            Consumer action1 = (Consumer)invocationOnMock.getArgument(2);
            action1.accept(f31);
            action1.accept(f32);
            return null;
        }).when(r3).apply(same(target), any(EvaluationContext.class), same(action));

        List<Rule> rules = new ArrayList<>();
        rules.add(r1);
        rules.add(r2);
        rules.add(r3);

        RuleValidator v = new RuleValidator(rules);
        v.validate(target, action);
        assertEquals(3, failures.size());
        assertEquals(f1, failures.get(0));
        assertEquals(f31, failures.get(1));
        assertEquals(f32, failures.get(2));

        verify(r1).apply( same(target), any(EvaluationContext.class), same(action));
        verify(r2).apply( same(target), any(EvaluationContext.class), same(action));
        verify(r3).apply( same(target), any(EvaluationContext.class), same(action));
    }
}

