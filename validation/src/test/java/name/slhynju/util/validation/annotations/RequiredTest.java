package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.SampleBean;
import name.slhynju.util.validation.Validator;
import name.slhynju.util.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class RequiredTest {

    @Test
    public void required() {
        SampleBean root = new SampleBean();
        root.setS1("abc");
        SampleBean child1 = new SampleBean();
        root.setChild1(child1);
        child1.setS1("a");

        Validator v = ValidatorFactory.getInstance(RequiredValidator.class);
        List<Failure> errors = new ArrayList<>();
        v.validate(root, error->errors.add(error));
        assertEquals(2, errors.size());
        Failure f1 = errors.get(0);
        assertEquals("s2", f1.getPath());
        assertEquals("s2.required", f1.getClientMessageKey());
        assertNull(f1.getValue());
        assertEquals("RequiredValidator",f1.getSource());
        Failure f2 = errors.get(1);
        assertEquals("child1.s2",f2.getPath());
        assertEquals("child1.s2.required", f2.getClientMessageKey());
        assertNull(f2.getValue());
    }

    @Test
    public void required2() {
        SampleBean root = new SampleBean();
        root.setS1("abc");
        SampleBean child1 = new SampleBean();
        root.setChild1(child1);
        child1.setS1("b");

        Validator v = ValidatorFactory.getInstance(RequiredValidator.class);
        List<Failure> errors = new ArrayList<>();
        v.validate(root, error->errors.add(error));
        assertEquals(1, errors.size());
        Failure f1 = errors.get(0);
        assertEquals("s2", f1.getPath());
        assertEquals("s2.required", f1.getClientMessageKey());
        assertNull(f1.getValue());
        assertEquals("RequiredValidator",f1.getSource());
    }

    @Test
    public void pass() {
        SampleBean root = new SampleBean();
        root.setS2("abc");
        SampleBean child1 = new SampleBean();
        root.setChild1(child1);
        child1.setS1("a");
        child1.setS2("def");

        Validator v = ValidatorFactory.getInstance(RequiredValidator.class);
        List<Failure> errors = new ArrayList<>();
        v.validate(root, error->errors.add(error));
        assertTrue(errors.isEmpty());
    }
}
