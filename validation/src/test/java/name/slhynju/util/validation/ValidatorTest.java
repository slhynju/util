package name.slhynju.util.validation;

import name.slhynju.util.validation.annotations.RequiredValidator;
import org.junit.jupiter.api.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author slhynju
 */
public class ValidatorTest {

    @Test
    public void demo() {
        SampleBean root = new SampleBean();
        root.setS1("abc");
        SampleBean child1 = new SampleBean();
        root.setChild1(child1);
        child1.setS1("a1a2a3");

        ExpressionParser parser = new SpelExpressionParser();
        Expression e1 = parser.parseExpression("s1");
        Expression e2 = parser.parseExpression("child1.s1");
        EvaluationContext context = SimpleEvaluationContext.forReadOnlyDataBinding().withRootObject(root).build();
        String s1 = e1.getValue(context, root, String.class);
        String s2 = e2.getValue(context, root, String.class);
        assertEquals("abc",s1);
        assertEquals("a1a2a3",s2);

        Expression invalidExpression = parser.parseExpression("no_such_key");
        assertThrows(SpelEvaluationException.class, () -> invalidExpression.getValue(context,root) );
    }


}
