package name.slhynju.util.validation.rules;

import name.slhynju.ApplicationException;
import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.SampleBean;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class RequiredRuleTest {

    private static ExpressionParser parser;

    private List<Failure> failures;

    private Consumer<Failure> action;

    private SampleBean target;

    private EvaluationContext context;

    @BeforeAll
    public static void init() {
        parser = new SpelExpressionParser();
    }

    @BeforeEach
    public void setUp() {
        failures = new ArrayList<>();
        action = f -> failures.add(f);
        target = new SampleBean();
        context = SimpleEvaluationContext.forReadOnlyDataBinding().withRootObject(target).build();
    }

    @Test
    public void target_null() {
        RequiredRule rule = new RequiredRule("s1", "", "", "", "", RequiredRuleTest.class, parser);
        rule.apply( null, context, action );
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("s1", f.getPath());
        assertEquals("s1.required", f.getClientMessageKey());
        assertNull(f.getInternalErrorCode());
        assertNull(f.getClientErrorCode());
        assertNull(f.getValue());
        assertEquals("RequiredRuleTest",f.getSource());
    }

    @Test
    public void pass() {
        target.setS1("");
        RequiredRule rule = new RequiredRule("s1", "", "", "", "", RequiredRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed() {
        RequiredRule rule = new RequiredRule("s1", "", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        rule.apply(target, context, action);
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("s1", f.getPath());
        assertEquals("key1", f.getClientMessageKey());
        assertEquals("A1",f.getInternalErrorCode());
        assertEquals("B1",f.getClientErrorCode());
        assertNull(f.getValue());
        assertEquals("RequiredRuleTest",f.getSource());
    }

    @Test
    public void pass_enabled() {
        target.setS1("");
        target.setI1(4);
        RequiredRule rule = new RequiredRule("s1", "i1 == 4", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed_enabled() {
        target.setI1(4);
        RequiredRule rule = new RequiredRule("s1", "i1 == 4", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        rule.apply(target, context, action);
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("s1", f.getPath());
        assertEquals("key1", f.getClientMessageKey());
        assertEquals("A1",f.getInternalErrorCode());
        assertEquals("B1",f.getClientErrorCode());
        assertNull(f.getValue());
        assertEquals("RequiredRuleTest",f.getSource());
    }

    @Test
    public void pass_disabled() {
        target.setS1("");
        target.setI1(3);
        RequiredRule rule = new RequiredRule("s1", "i1 == 4", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed_disabled() {
        target.setI1(3);
        RequiredRule rule = new RequiredRule("s1", "i1 == 4", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass_whenInvalid() {
        target.setS1("");
        RequiredRule rule = new RequiredRule("s1", "i1", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        assertThrows(ClassCastException.class, () -> rule.apply(target, context, action));
    }

    @Test
    public void failed_whenInvalid() {
        RequiredRule rule = new RequiredRule("s1", "i1", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        assertThrows(ClassCastException.class, () -> rule.apply(target, context, action));
    }

    @Test
    public void when_invalid2() {
        RequiredRule rule = new RequiredRule("s1", "i5 == 4", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        rule.apply(target,context,action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void when_disabled2() {
        RequiredRule rule = new RequiredRule("s1", "child1.i1 == 4", "key1", "A1", "B1", RequiredRuleTest.class, parser);
        rule.apply(target,context,action);
        assertTrue(failures.isEmpty());
    }

}
