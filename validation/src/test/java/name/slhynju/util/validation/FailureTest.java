package name.slhynju.util.validation;

import name.slhynju.util.EqualsUtil;
import name.slhynju.util.HashCodeBuilder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class FailureTest {

    @Test
    public void testConstructor() {
        Failure f = new Failure("a.b.c", "value", FailureTest.class);
        assertEquals("a.b.c",f.getPath());
        assertEquals("value",f.getValue());
        assertEquals("FailureTest", f.getSource());
    }

    @Test
    public void moreProperties() {
        Failure f = new Failure("a.b.c", "value", FailureTest.class);
        assertNull(f.getInternalErrorCode());
        assertNull(f.getClientErrorCode());
        assertNull(f.getClientMessageKey());
        assertNull(f.getClientMessageParams());
        f.setInternalErrorCode("A-B5");
        assertEquals("A-B5",f.getInternalErrorCode());
        f.setClientErrorCode("4028");
        assertEquals("4028",f.getClientErrorCode());
        f.setClientMessageKey("abc.required");
        assertEquals("abc.required",f.getClientMessageKey());
        f.setClientMessageParams( "param1", "param2", "param3" );
        Object[] expected = new Object[]{"param1", "param2", "param3"};
        assertArrayEquals(expected, f.getClientMessageParams());
    }

    @Test
    public void addPathPrefix() {
        Failure f = new Failure("c", "value", FailureTest.class);
        f.addPathPrefix("b");
        assertEquals("b.c",f.getPath());
    }

    @Test
    public void testEquals() {
        Failure f1 = new Failure("path1", "value1", FailureTest.class);
        f1.setClientMessageKey("path1.error1");
        Failure f2 = new Failure("path1", "value2", FailureTest.class);
        f2.setClientMessageKey("path1.error1");
        assertEquals(f1,f2);
        f2.setClientMessageKey("path1.error2");
        assertNotEquals(f1,f2);
        assertFalse(f1.equals(null));
        assertFalse(f1.equals("path1"));
    }

    @Test
    public void testHashCode() {
        Failure f1 = new Failure("path1", "value1", FailureTest.class);
        f1.setClientMessageKey("message1");
        HashCodeBuilder hash = new HashCodeBuilder();
        hash.append("path1").append(FailureTest.class).append("message1");
        assertEquals(hash.toValue(), f1.hashCode());
        Failure f2 = new Failure("path1", "value2", FailureTest.class);
        f2.setClientMessageKey("message1");
        assertEquals(f1.hashCode(),f2.hashCode());
    }

    @Test
    public void testToString() {
        Failure f1 = new Failure("path1", "value1", FailureTest.class);
        assertFalse(f1.toString().contains("value1"));
    }
}
