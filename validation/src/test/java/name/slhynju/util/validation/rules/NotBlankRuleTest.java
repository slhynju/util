package name.slhynju.util.validation.rules;

import name.slhynju.ApplicationException;
import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.SampleBean;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class NotBlankRuleTest {

    private static ExpressionParser parser;

    private List<Failure> failures;

    private Consumer<Failure> action;

    private SampleBean target;

    private EvaluationContext context;

    @BeforeAll
    public static void init() {
        parser = new SpelExpressionParser();
    }

    @BeforeEach
    public void setUp() {
        failures = new ArrayList<>();
        action = f -> failures.add(f);
        target = new SampleBean();
        context = SimpleEvaluationContext.forReadOnlyDataBinding().withRootObject(target).build();
    }

    @Test
    public void target_null() {
        NotBlankRule rule = new NotBlankRule("s1", "","","","",NotBlankRuleTest.class, parser);
        rule.apply(null,context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass() {
        target.setS1("   a    ");
        NotBlankRule rule = new NotBlankRule("s1", "","","","",NotBlankRuleTest.class, parser);
        rule.apply(target,context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed() {
        target.setS1("     ");
        NotBlankRule rule = new NotBlankRule("s1", "","","","",NotBlankRuleTest.class, parser);
        rule.apply(target,context, action);
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("s1",f.getPath());
        assertEquals("     ",f.getValue());
        assertEquals("s1.not_blank",f.getClientMessageKey());
        assertEquals("NotBlankRuleTest",f.getSource());
    }

    @Test
    public void path_illegal() {
        target.setI1(4);
        NotBlankRule rule = new NotBlankRule("i1", "","","","",NotBlankRuleTest.class, parser);
        assertThrows(ClassCastException.class, ()->rule.apply(target,context,action));
    }
}
