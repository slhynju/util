package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.SampleBean;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class AssertRuleTest {

    private static ExpressionParser parser;

    private List<Failure> failures;

    private Consumer<Failure> action;

    private SampleBean target;

    private EvaluationContext context;

    @BeforeAll
    public static void init() {
        parser = new SpelExpressionParser();
    }

    @BeforeEach
    public void setUp() {
        failures = new ArrayList<>();
        action = f -> failures.add(f);
        target = new SampleBean();
        context = SimpleEvaluationContext.forReadOnlyDataBinding().withRootObject(target).build();
    }

    @Test
    public void target_null() {
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "", "", "", "", AssertRuleTest.class, parser);
        rule.apply(null,context,action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass() {
        target.setI1(5);
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "", "", "", "", AssertRuleTest.class, parser);
        rule.apply(target,context,action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed() {
        target.setI1(-3);
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "", "", "", "", AssertRuleTest.class, parser);
        rule.apply(target,context,action);
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("i1",f.getPath());
        assertEquals(Integer.valueOf(-3),f.getValue());
        assertEquals("i1.value_invalid",f.getClientMessageKey());
        assertEquals("AssertRuleTest",f.getSource());
    }

    @Test
    public void pass_enabled() {
        target.setI1(5);
        target.setS1("A");
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "s1 == 'A'", "", "", "", AssertRuleTest.class, parser);
        rule.apply(target,context,action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed_enabled() {
        target.setI1(-3);
        target.setS1("A");
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "s1 == 'A'", "", "", "", AssertRuleTest.class, parser);
        rule.apply(target,context,action);
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("i1",f.getPath());
        assertEquals(Integer.valueOf(-3),f.getValue());
        assertEquals("i1.value_invalid",f.getClientMessageKey());
        assertEquals("AssertRuleTest",f.getSource());
    }

    @Test
    public void pass_disabled() {
        target.setI1(5);
        target.setS1("B");
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "s1 == 'A'", "", "", "", AssertRuleTest.class, parser);
        rule.apply(target,context,action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed_disabled() {
        target.setI1(-3);
        target.setS1("B");
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "s1 == 'A'", "", "", "", AssertRuleTest.class, parser);
        rule.apply(target,context,action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass_when_illegal() {
        target.setS1("abc");
        target.setI1(4);
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "s1", "", "", "", AssertRuleTest.class, parser);
        assertThrows(ClassCastException.class, () -> rule.apply(target,context,action));
    }

    @Test
    public void failed_when_illegal() {
        target.setS1("abc");
        target.setI1(-3);
        AssertRule rule = new AssertRule("i1", "i1 > 0 && i1 < 10", "s1", "", "", "", AssertRuleTest.class, parser);
        assertThrows(ClassCastException.class, () -> rule.apply(target,context,action));
    }

    @Test
    public void value_null() {
        target.setI1(-3);
        AssertRule rule = new AssertRule("s1", "i1 > 0 && i1 < 10", "", "", "", "", AssertRuleTest.class, parser);
        rule.apply(target,context,action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void test_null() {
        target.setS1("abc");
        AssertRule rule = new AssertRule("s1", "s2", "", "", "", "", AssertRuleTest.class, parser);
        rule.apply(target,context,action);
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("s1",f.getPath());
        assertEquals("abc",f.getValue());
        assertEquals("s1.value_invalid",f.getClientMessageKey());
        assertEquals("AssertRuleTest",f.getSource());
    }

    @Test
    public void test_illegal() {
        target.setS1("abc");
        target.setS2("def");
        AssertRule rule = new AssertRule("s1", "s2", "", "", "", "", AssertRuleTest.class, parser);
        assertThrows(ClassCastException.class, () -> rule.apply(target,context,action));
    }

}
