package name.slhynju.util.validation.annotations;

/**
 * @author slhynju
 */
@Required(path="s2")
@Required(path="child1.s2", when="child1.s1 == 'a'")
public interface RequiredValidator {
}
