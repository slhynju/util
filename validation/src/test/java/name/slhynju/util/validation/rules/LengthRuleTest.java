package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.SampleBean;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class LengthRuleTest {

    private static ExpressionParser parser;

    private List<Failure> failures;

    private Consumer<Failure> action;

    private SampleBean target;

    private EvaluationContext context;

    @BeforeAll
    public static void init() {
        parser = new SpelExpressionParser();
    }

    @BeforeEach
    public void setUp() {
        failures = new ArrayList<>();
        action = f -> failures.add(f);
        target = new SampleBean();
        context = SimpleEvaluationContext.forReadOnlyDataBinding().withRootObject(target).build();
    }

    @Test
    public void target_null() {
        LengthRule r = new LengthRule("child1.s1", 3, 8, "", "", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(null, context, action );
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass() {
        target.setS1("anystr");
        LengthRule r = new LengthRule("s1", 3, 8, "", "name.length_invalid", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass_nested() {
        SampleBean child = new SampleBean();
        target.setChild1(child);
        child.setS1("anystr");
        LengthRule r = new LengthRule("child1.s1", 3, 8, "", "name.length_invalid", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass_valueNull() {
        LengthRule r = new LengthRule("child1.s1", 3, 8, "", "name.length_invalid", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );

        assertTrue(failures.isEmpty());
    }

    @Test
    public void minlength() {
        SampleBean child = new SampleBean();
        target.setChild1(child);
        child.setS1("a");

        LengthRule r = new LengthRule("child1.s1", 3, 8, "", "name.length_invalid", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );

        assertFalse(failures.isEmpty());
        Failure f = failures.get(0);
        assertEquals("child1.s1",f.getPath());
        assertEquals("a",f.getValue());
        assertEquals("name.length_invalid",f.getClientMessageKey());
        assertEquals("AB31",f.getInternalErrorCode());
        assertEquals("2700", f.getClientErrorCode());
        assertEquals("LengthRuleTest", f.getSource());
        Object[] expectedParams = new Object[] { Integer.valueOf(3), Integer.valueOf(8)};
        assertArrayEquals( expectedParams, f.getClientMessageParams() );
    }

    @Test
    public void maxlength() {
        SampleBean child = new SampleBean();
        target.setChild1(child);
        child.setS1("this is a long string");

        LengthRule r = new LengthRule("child1.s1", 3, 8, "", "name.length_invalid", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );

        assertFalse(failures.isEmpty());
        Failure f = failures.get(0);
        assertEquals("child1.s1",f.getPath());
        assertEquals("this is a long string",f.getValue());
    }

    @Test
    public void negativeMinLength() {
        SampleBean child = new SampleBean();
        target.setChild1(child);
        child.setS1("this is a long string");

        LengthRule r = new LengthRule("child1.s1", -3, 8, "", "name.length_invalid", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );

        assertFalse(failures.isEmpty());
        Failure f = failures.get(0);
        assertEquals("child1.s1",f.getPath());
        assertEquals("this is a long string",f.getValue());
        Object[] expectedParams = new Object[] { Integer.valueOf(0), Integer.valueOf(8)};
        assertArrayEquals( expectedParams, f.getClientMessageParams() );
    }

    @Test
    public void negativeMaxLength() {
        SampleBean child = new SampleBean();
        target.setChild1(child);
        child.setS1("this is a long string");

        LengthRule r = new LengthRule("child1.s1", -3, -5, "", "name.length_invalid", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );

        assertFalse(failures.isEmpty());
        Failure f = failures.get(0);
        assertEquals("child1.s1",f.getPath());
        assertEquals("this is a long string",f.getValue());
        Object[] expectedParams = new Object[] { Integer.valueOf(0), Integer.valueOf(0)};
        assertArrayEquals( expectedParams, f.getClientMessageParams() );
    }

    @Test
    public void illegalRule() {
        assertThrows( IllegalArgumentException.class, () -> new LengthRule("child1.s1", 5, 3, "", "name.length_invalid", "AB31", "2700", LengthRuleTest.class, parser));
    }

    @Test
    public void defaultMessageKey() {
        SampleBean child = new SampleBean();
        target.setChild1(child);
        child.setS1("a");

        LengthRule r = new LengthRule("child1.s1", 3, 8, "", "", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );

        assertFalse(failures.isEmpty());
        Failure f = failures.get(0);
        assertEquals("child1.s1",f.getPath());
        assertEquals("a",f.getValue());
        assertEquals("child1.s1.length_invalid",f.getClientMessageKey());
        assertEquals("AB31",f.getInternalErrorCode());
        assertEquals("2700", f.getClientErrorCode());
        assertEquals("LengthRuleTest", f.getSource());
        Object[] expectedParams = new Object[] { Integer.valueOf(3), Integer.valueOf(8)};
        assertArrayEquals( expectedParams, f.getClientMessageParams() );
    }

    @Test
    public void when_enabled() {
        SampleBean child = new SampleBean();
        target.setChild1(child);
        child.setS1("a");
        child.setS2("on");

        LengthRule r = new LengthRule("child1.s1", 3, 8, "child1.s2 == 'on'", "", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );

        assertFalse(failures.isEmpty());
        Failure f = failures.get(0);
        assertEquals("child1.s1",f.getPath());
        assertEquals("a",f.getValue());
    }

    @Test
    public void when_disabled() {
        SampleBean child = new SampleBean();
        target.setChild1(child);
        child.setS1("a");
        child.setS2("off");

        LengthRule r = new LengthRule("child1.s1", 3, 8, "child1.s2 == 'on'", "", "AB31", "2700", LengthRuleTest.class, parser);
        r.apply(target, context, action );

        assertTrue(failures.isEmpty());
    }
}
