package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.SampleBean;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author slhynju
 */
public class NotAllowedRuleTest {

    private static ExpressionParser parser;

    private List<Failure> failures;

    private Consumer<Failure> action;

    private SampleBean target;

    private EvaluationContext context;

    @BeforeAll
    public static void init() {
        parser = new SpelExpressionParser();
    }

    @BeforeEach
    public void setUp() {
        failures = new ArrayList<>();
        action = f -> failures.add(f);
        target = new SampleBean();
        context = SimpleEvaluationContext.forReadOnlyDataBinding().withRootObject(target).build();
    }

    @Test
    public void target_null() {
        NotAllowedRule rule = new NotAllowedRule("s1","","","","",NotAllowedRuleTest.class, parser);
        rule.apply(null,context,action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass() {
        NotAllowedRule rule = new NotAllowedRule("s1", "", "", "", "", NotAllowedRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed() {
        target.setS1("abc");
        NotAllowedRule rule = new NotAllowedRule("s1", "", "", "", "", NotAllowedRuleTest.class, parser);
        rule.apply(target, context, action);
        assertFalse(failures.isEmpty());
        Failure f = failures.get(0);
        assertEquals("s1",f.getPath());
        assertEquals("abc",f.getValue());
        assertEquals("s1.not_allowed", f.getClientMessageKey());
        assertEquals("NotAllowedRuleTest", f.getSource());
    }

    @Test
    public void pass_nested() {
        target.setS1("abc");
        NotAllowedRule rule = new NotAllowedRule("child1.s1", "", "", "", "", NotAllowedRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed_nested() {
        SampleBean child = new SampleBean();
        child.setS1("abc");
        target.setChild1(child);
        NotAllowedRule rule = new NotAllowedRule("child1.s1", "", "", "", "", NotAllowedRuleTest.class, parser);
        rule.apply(target, context, action);
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("child1.s1",f.getPath());
        assertEquals("abc",f.getValue());
        assertEquals("NotAllowedRuleTest",f.getSource());
    }

    @Test
    public void pass_enabled() {
        target.setI1(4);
        NotAllowedRule rule = new NotAllowedRule("s1", "i1 == 4", "key1", "A1", "B1", NotAllowedRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed_enabled() {
        target.setS1("abc");
        target.setI1(4);
        NotAllowedRule rule = new NotAllowedRule("s1", "i1 == 4", "key1", "A1", "B1", NotAllowedRuleTest.class, parser);
        rule.apply(target, context, action);
        assertEquals(1, failures.size());
        Failure f = failures.get(0);
        assertEquals("s1", f.getPath());
        assertEquals("key1", f.getClientMessageKey());
        assertEquals("A1",f.getInternalErrorCode());
        assertEquals("B1",f.getClientErrorCode());
        assertEquals("abc",f.getValue());
        assertEquals("NotAllowedRuleTest",f.getSource());
    }

    @Test
    public void pass_disabled() {
        target.setI1(3);
        NotAllowedRule rule = new NotAllowedRule("s1", "i1 == 4", "key1", "A1", "B1", NotAllowedRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void failed_disabled() {
        target.setS1("abc");
        target.setI1(3);
        NotAllowedRule rule = new NotAllowedRule("s1", "i1 == 4", "key1", "A1", "B1", NotAllowedRuleTest.class, parser);
        rule.apply(target, context, action);
        assertTrue(failures.isEmpty());
    }

    @Test
    public void pass_whenInvalid() {
        NotAllowedRule rule = new NotAllowedRule("s1", "i1", "key1", "A1", "B1", NotAllowedRuleTest.class, parser);
        assertThrows(ClassCastException.class, () -> rule.apply(target, context, action));
    }

    @Test
    public void failed_whenInvalid() {
        target.setS1("abc");
        NotAllowedRule rule = new NotAllowedRule("s1", "i1", "key1", "A1", "B1", NotAllowedRuleTest.class, parser);
        assertThrows(ClassCastException.class, () -> rule.apply(target, context, action));
    }

}
