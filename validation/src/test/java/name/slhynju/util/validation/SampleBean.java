package name.slhynju.util.validation;

/**
 * @author slhynju
 */
public class SampleBean {

    private String s1;

    private String s2;

    private String s3;

    private int i1;

    private int i2;

    private int i3;

    private SampleBean child1;

    private SampleBean child2;

    private SampleBean child3;

    public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

    public String getS2() {
        return s2;
    }

    public void setS2(String s2) {
        this.s2 = s2;
    }

    public String getS3() {
        return s3;
    }

    public void setS3(String s3) {
        this.s3 = s3;
    }

    public int getI1() {
        return i1;
    }

    public void setI1(int i1) {
        this.i1 = i1;
    }

    public int getI2() {
        return i2;
    }

    public void setI2(int i2) {
        this.i2 = i2;
    }

    public int getI3() {
        return i3;
    }

    public void setI3(int i3) {
        this.i3 = i3;
    }

    public SampleBean getChild1() {
        return child1;
    }

    public void setChild1(SampleBean child1) {
        this.child1 = child1;
    }

    public SampleBean getChild2() {
        return child2;
    }

    public void setChild2(SampleBean child2) {
        this.child2 = child2;
    }

    public SampleBean getChild3() {
        return child3;
    }

    public void setChild3(SampleBean child3) {
        this.child3 = child3;
    }
}
