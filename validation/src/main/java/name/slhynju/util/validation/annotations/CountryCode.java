package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.CountryCodeContainer;

import java.lang.annotation.*;

/**
 * Reports a {@code Failure} if the {@code path} expression is not evaluated as a valid country or subdivision code.
 * It follows ISO 3166 alpha-2 and alpha-3 standards.
 * <p>This rule does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}. See {@code @Required} rule instead.</p>
 * <p>This rule uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
 *
 * @author slhynju
 */
@Repeatable(CountryCodeContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CountryCode {

    /**
     * the path of the country code. It is relative to the root {@code Object}. Its evaluated value will be reported in the {@code Failure} if any.
     * <p>This rule does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}.</p>
     */
    String path();

    /**
     * indicates whether two-letter codes or three-letter codes shall be used.
     * <p>It shall be either 2 or 3.</p>
     */
    int length() default 2;

    /**
     * a {@code boolean} condition to enable this rule. A {@code null} or empty value is treated as enabled.
     * <p>This rule does not report {@code Failures} if the {@code when} condition is not evaluated as {@code true}.</p>
     */
    String when() default "";

    /**
     * the i18n client error message key.
     * <p>This rule uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
     */
    String messageKey() default "";

    /**
     * the error code to use for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code to use for external client reference.
     */
    String clientErrorCode() default "";
}
