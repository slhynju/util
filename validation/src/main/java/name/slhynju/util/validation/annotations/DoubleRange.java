package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.DoubleRangeContainer;

import java.lang.annotation.*;

/**
 * Reports failure if target double value is out of range.
 *
 * @author slhynju
 */
@Repeatable(DoubleRangeContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DoubleRange {

    /**
     * the relative path to retrieve the target string.
     */
    String path();

    /**
     * the minimum value allowed.
     */
    double min() default 0.0;

    /**
     * the maximum value allowed.
     */
    double max() default Double.MAX_VALUE;

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";
}
