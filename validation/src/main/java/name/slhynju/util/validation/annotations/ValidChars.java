package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.ValidCharsContainer;

import java.lang.annotation.*;

/**
 * Reports failure if the target string contains invalid characters.
 * This rule only supports English string and is not applicable to i18n strings. See @I18NValidChars rule instead.
 * Note this rule does not report failure for null values. See @Required annotation instead.
 *
 * @author slhynju
 * @see I18NValidChars
 */
@Repeatable(ValidCharsContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidChars {

    /**
     * the relative path to retrieve the target string.
     */
    String path();

    /**
     * whether a..z and A..Z letters are valid.
     */
    boolean letters() default true;

    /**
     * whether 0..9 digits are valid.
     */
    boolean digits() default true;

    /**
     * whether space character is valid.
     */
    boolean space() default true;

    /**
     * Additional valid characters.
     */
    String moreValidChars() default "";

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";

}
