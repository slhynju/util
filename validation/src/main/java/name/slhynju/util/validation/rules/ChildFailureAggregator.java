package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

/**
 * This class is used by ForEachRule.
 * @author slhynju
 */
@SuppressWarnings("MethodParameterOfConcreteClass")
public final class ChildFailureAggregator implements Consumer<Failure> {

    private final @NotNull String pathPrefix;

    private final @NotNull Consumer<Failure> aggregator;

    @SuppressWarnings("BoundedWildcard")
    public ChildFailureAggregator(@NotNull String pathPrefix, @NotNull Consumer<Failure> aggregator) {
        this.pathPrefix = pathPrefix;
        this.aggregator = aggregator;
    }

    @SuppressWarnings("ParameterNameDiffersFromOverriddenParameter")
    @Override
    public void accept(Failure failure) {
        failure.addPathPrefix(pathPrefix);
        aggregator.accept(failure);
    }
}
