package name.slhynju.util.validation.annotations.container;

import name.slhynju.util.validation.annotations.ForEach;

import java.lang.annotation.*;

/**
 * @author slhynju
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ForEachContainer {

    ForEach[] value();
}
