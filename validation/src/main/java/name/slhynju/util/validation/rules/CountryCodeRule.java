package name.slhynju.util.validation.rules;

import name.slhynju.util.FreeStringBuilder;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Reports a {@code Failure} if the target {@code String} is not a valid country or subdivision code.
 * It follows ISO 3166 alpha-2 and alpha-3 standards.
 * <p>This {@code Rule} does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}.</p>
 * <p>This {@code Rule} uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
 *
 * @author slhynju
 */
public final class CountryCodeRule extends StringRuleSupport {

    /**
     * indicates whether two-letter codes or three-letter codes shall be used.
     * <p>It shall be either 2 or 3.</p>
     */
    private final int length;

    /**
     * The ISO 3166 alpha-2 codes.
     */
    private static final Set<String> TWO_LETTER_COUNTRY_CODES = new HashSet<>();

    /**
     * The ISO 3166 alpha-3 codes.
     */
    private static final Set<String> THREE_LETTER_COUNTRY_CODES = new HashSet<>();

    /**
     * Creates a new instance.
     * @param path a relative path the the root object. It shall not be {@code null}.
     * @param length indicates whether two-letter codes or three-letter codes shall be used. It shall be either 2 or 3.
     * @param when a condition to enable this {@code Rule}. It can be {@code null}. A {@code null} or empty value is treated as enabled.
     * @param messageKey an i18n client error message key. It can be {@code null}.
     * @param internalErrorCode an error code to use for internal monitoring. It can be {@code null}.
     * @param clientErrorCode an error code to use for external client reference. It can be {@code null}.
     * @param source a defining source of this {@code Rule}. It shall not be {@code null}.
     * @param parser a SpEL {@code ExpressionParser}. It shall not be {@code null}.
     * @throws IllegalArgumentException if the {@code length} is invalid.
     */
    public CountryCodeRule(@NotNull String path, int length, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        if (length != 2 && length != 3) {
            String message = FreeStringBuilder.format("Invalid length {} defined in source {}. It shall be either 2 or 3.",
                    String.valueOf(length), source.getName());
            throw new IllegalArgumentException(message);
        }
        guardMessageKey("value_invalid");
        this.length = length;
    }

    @SuppressWarnings("OverlyComplexBooleanExpression")
    @Override
    protected void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator) {
        if ((length == 2 && value.length() != 2) || (length == 3 && value.length() != 3)) {
            reportFailure(value, failureAggregator);
            return;
        }
        if (length == 2 && TWO_LETTER_COUNTRY_CODES.contains(value)) {
            return;
        }
        if (length == 3 && THREE_LETTER_COUNTRY_CODES.contains(value)) {
            return;
        }
        reportFailure(value, failureAggregator);
    }

    static {
        // country codes start. see https://www.iso.org/obp/ui/#search
        // page 1
        add("AF", "AFG");
        add("AL", "ALB");
        add("DZ", "DZA");
        add("AS", "ASM");
        add("AD", "AND");
        add("AO", "AGO");
        add("AI", "AIA");
        add("AQ", "ATA");
        add("AG", "ATG");
        add("AR", "ARG");
        add("AM", "ARM");
        add("AW", "ABW");
        add("AU", "AUS");
        add("AT", "AUT");
        add("AZ", "AZE");
        add("BS", "BHS");
        add("BH", "BHR");
        add("BD", "BGD");
        add("BB", "BRB");
        add("BY", "BLR");
        add("BE", "BEL");
        add("BZ", "BLZ");
        add("BJ", "BEN");
        add("BM", "BMU");
        add("AX", "ALA");
        // page 2
        add("BT", "BTN");
        add("BO", "BOL");
        add("BQ", "BES");
        add("BA", "BIH");
        add("BW", "BWA");
        add("BV", "BVT");
        add("BR", "BRA");
        add("IO", "IOT");
        add("BN", "BRN");
        add("BG", "BGR");
        add("BF", "BFA");
        add("BI", "BDI");
        add("CV", "CPV");
        add("KH", "KHM");
        add("CM", "CMR");
        add("CA", "CAN");
        add("KY", "CYM");
        add("CF", "CAF");
        add("TD", "TCD");
        add("CL", "CHL");
        add("CN", "CHN");
        add("CX", "CXR");
        add("CC", "CCK");
        add("CO", "COL");
        add("KM", "COM");
        // page 3
        add("CD", "COD");
        add("CG", "COG");
        add("CK", "COK");
        add("CR", "CRI");
        add("HR", "HRV");
        add("CU", "CUB");
        add("CW", "CUW");
        add("CY", "CYP");
        add("CZ", "CZE");
        add("CI", "CIV");
        add("DK", "DNK");
        add("DJ", "DJI");
        add("DM", "DMA");
        add("DO", "DOM");
        add("EC", "ECU");
        add("EG", "EGY");
        add("SV", "SLV");
        add("GQ", "GNQ");
        add("ER", "ERI");
        add("EE", "EST");
        add("SZ", "SWZ");
        add("ET", "ETH");
        add("FK", "FLK");
        add("FO", "FRO");
        add("FJ", "FJI");
        // page 4
        add("FI", "FIN");
        add("FR", "FRA");
        add("GF", "GUF");
        add("PF", "PYF");
        add("TF", "ATF");
        add("GA", "GAB");
        add("GM", "GMB");
        add("GE", "GEO");
        add("DE", "DEU");
        add("GH", "GHA");
        add("GI", "GIB");
        add("GR", "GRC");
        add("GL", "GRL");
        add("GD", "GRD");
        add("GP", "GLP");
        add("GU", "GUM");
        add("GT", "GTM");
        add("GG", "GGY");
        add("GN", "GIN");
        add("GW", "GNB");
        add("GY", "GUY");
        add("HT", "HTI");
        add("HM", "HMD");
        add("VA", "VAT");
        add("HN", "HND");
        // page 5
        add("HK", "HKG");
        add("HU", "HUN");
        add("IS", "ISL");
        add("IN", "IND");
        add("ID", "IDN");
        add("IR", "IRN");
        add("IQ", "IRQ");
        add("IE", "IRL");
        add("IM", "IMN");
        add("IL", "ISR");
        add("IT", "ITA");
        add("JM", "JAM");
        add("JP", "JPN");
        add("JE", "JEY");
        add("JO", "JOR");
        add("KZ", "KAZ");
        add("KE", "KEN");
        add("KI", "KIR");
        add("KP", "PRK");
        add("KR", "KOR");
        add("KW", "KWT");
        add("KG", "KGZ");
        add("LA", "LAO");
        add("LV", "LVA");
        add("LB", "LBN");
        // page 6
        add("LS", "LSO");
        add("LR", "LBR");
        add("LY", "LBY");
        add("LI", "LIE");
        add("LT", "LTU");
        add("LU", "LUX");
        add("MO", "MAC");
        add("MG", "MDG");
        add("MW", "MWI");
        add("MY", "MYS");
        add("MV", "MDV");
        add("ML", "MLI");
        add("MT", "MLT");
        add("MH", "MHL");
        add("MQ", "MTQ");
        add("MR", "MRT");
        add("MU", "MUS");
        add("YT", "MYT");
        add("MX", "MEX");
        add("FM", "FSM");
        add("MD", "MDA");
        add("MC", "MCO");
        add("MN", "MNG");
        add("ME", "MNE");
        add("MS", "MSR");
        // page 7
        add("MA", "MAR");
        add("MZ", "MOZ");
        add("MM", "MMR");
        add("NA", "NAM");
        add("NR", "NRU");
        add("NP", "NPL");
        add("NL", "NLD");
        add("NC", "NCL");
        add("NZ", "NZL");
        add("NI", "NIC");
        add("NE", "NER");
        add("NG", "NGA");
        add("NU", "NIU");
        add("NF", "NFK");
        add("MK", "MKD");
        add("MP", "MNP");
        add("NO", "NOR");
        add("OM", "OMN");
        add("PK", "PAK");
        add("PW", "PLW");
        add("PS", "PSE");
        add("PA", "PAN");
        add("PG", "PNG");
        add("PY", "PRY");
        add("PE", "PER");
        // page 8
        add("PH", "PHL");
        add("PN", "PCN");
        add("PL", "POL");
        add("PT", "PRT");
        add("PR", "PRI");
        add("QA", "QAT");
        add("RO", "ROU");
        add("RU", "RUS");
        add("RW", "RWA");
        add("RE", "REU");
        add("BL", "BLM");
        add("SH", "SHN");
        add("KN", "KNA");
        add("LC", "LCA");
        add("MF", "MAF");
        add("PM", "SPM");
        add("VC", "VCT");
        add("WS", "WSM");
        add("SM", "SMR");
        add("ST", "STP");
        add("SA", "SAU");
        add("SN", "SEN");
        add("RS", "SRB");
        add("SC", "SYC");
        add("SL", "SLE");
        // page 9
        add("SG", "SGP");
        add("SX", "SXM");
        add("SK", "SVK");
        add("SI", "SVN");
        add("SB", "SLB");
        add("SO", "SOM");
        add("ZA", "ZAF");
        add("GS", "SGS");
        add("SS", "SSD");
        add("ES", "ESP");
        add("LK", "LKA");
        add("SD", "SDN");
        add("SR", "SUR");
        add("SJ", "SJM");
        add("SE", "SWE");
        add("CH", "CHE");
        add("SY", "SYR");
        add("TW", "TWN");
        add("TJ", "TJK");
        add("TZ", "TZA");
        add("TH", "THA");
        add("TL", "TLS");
        add("TG", "TGO");
        add("TK", "TKL");
        add("TO", "TON");
        // page 10
        add("TT", "TTO");
        add("TN", "TUN");
        add("TR", "TUR");
        add("TM", "TKM");
        add("TC", "TCA");
        add("TV", "TUV");
        add("UG", "UGA");
        add("UA", "UKR");
        add("AE", "ARE");
        add("GB", "GBR");
        add("UM", "UMI");
        add("US", "USA");
        add("UY", "URY");
        add("UZ", "UZB");
        add("VU", "VUT");
        add("VE", "VEN");
        add("VN", "VNM");
        add("VG", "VGB");
        add("VI", "VIR");
        add("WF", "WLF");
        add("EH", "ESH");
        add("YE", "YEM");
        add("ZM", "ZMB");
        add("ZW", "ZWE");
        // country codes end
    }

    private static void add(String twoLetterCode, String threeLetterCode) {
        TWO_LETTER_COUNTRY_CODES.add(twoLetterCode);
        THREE_LETTER_COUNTRY_CODES.add(threeLetterCode);
    }
}
