package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.I18NValidCharsContainer;

import java.lang.annotation.*;

/**
 * Reports failure if the target string contains invalid characters.
 * This rule supports i18n strings.
 * Note this rule does not report failure for null values. See @Required annotation instead.
 *
 * @author slhynju
 * @see ValidChars
 */
@Repeatable(I18NValidCharsContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface I18NValidChars {

    /**
     * the relative path to retrieve the target string.
     */
    String path();

    /**
     * whether unicode letters are valid.
     */
    boolean letters() default true;

    /**
     * whether unicode digits are valid.
     */
    boolean digits() default true;

    /**
     * whether unicode space characters are valid.
     */
    boolean space() default true;

    /**
     * Additional valid characters.
     */
    String moreValidChars() default "";

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";

}
