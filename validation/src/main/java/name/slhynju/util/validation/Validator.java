package name.slhynju.util.validation;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

/**
 * A validator to validate an input {@code Object}.
 * <p>Any implementation shall be thread-safe.</p>
 *
 * @author slhynju
 */
@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface Validator {

    /**
     * Validates an {@code Object} with pre-defined rules.
     *
     * @param target            a target {@code Object} to validate. It can be {@code null}.
     * @param failureAggregator an action to aggregate all reported {@code Failures}. It shall not be {@code null}.
     */
    void validate(@Nullable Object target, @NotNull Consumer<Failure> failureAggregator);
}
