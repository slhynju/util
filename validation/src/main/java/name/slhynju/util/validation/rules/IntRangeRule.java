package name.slhynju.util.validation.rules;

import name.slhynju.util.FreeStringBuilder;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class IntRangeRule extends ObjectRuleSupport {

    private final int min;

    private final int max;

    public IntRangeRule(@NotNull String path, int min, int max, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("value_invalid");
        this.min = min;
        this.max = max;
        if (min > max) {
            String message = FreeStringBuilder.format("Invalid min value {} and max value {} found in source {}. Min value shall be no more than max value.",
                    String.valueOf(min), String.valueOf(max), source.getName());
            throw new IllegalArgumentException(message);
        }
    }

    @Override
    protected void reviewValue(@NotNull Object value, @NotNull Consumer<Failure> failureAggregator) {
        if (value instanceof Integer) {
            Integer n = (Integer) value;
            if (n.intValue() < min || n.intValue() > max) {
                reportFailure(n, failureAggregator);
            }
            return;
        }
        String message = buildClassCastMessage("Integer", value);
        throw new ClassCastException(message);
    }

    @Override
    protected void addMessageParams(@NotNull Failure failure) {
        failure.setClientMessageParams(Integer.valueOf(min), Integer.valueOf(max));
    }
}
