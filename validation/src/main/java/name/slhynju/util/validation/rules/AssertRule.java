package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * Reports a {@code Failure} if the {@code test} condition is not evaluated as {@code true}.
 * <p>This {@code Rule} does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}.</p>
 * <p>This {@code Rule} uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
 * @author slhynju
 */
public final class AssertRule extends RuleSupport {

    /**
     * a {@code boolean} condition to check.
     * <p>This {@code Rule} reports a {@code Failure} if the {@code test} condition is not evaluated as {@code true}.</p>
     */
    private final @NotNull String test;

    /**
     * The parsed SpEL {@code Expression} of {@code test} attribute.
     */
    private final @NotNull Expression testExpr;

    /**
     * Creates a new instance.
     * @param path a relative path the the root object. It shall not be {@code null}.
     * @param test a condition to check. It shall not be {@code null}.
     * @param when a condition to enable this {@code Rule}. It can be {@code null}. A {@code null} or empty value is treated as enabled.
     * @param messageKey an i18n client error message key. It can be {@code null}.
     * @param internalErrorCode an error code to use for internal monitoring. It can be {@code null}.
     * @param clientErrorCode an error code to use for external client reference. It can be {@code null}.
     * @param source a defining source of this {@code Rule}. It shall not be {@code null}.
     * @param parser a SpEL {@code ExpressionParser}. It shall not be {@code null}.
     */
    public AssertRule(@NotNull String path, @NotNull String test, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("value_invalid");
        this.test = test;
        testExpr = parser.parseExpression(test);
    }

    @Override
    public void apply(@Nullable Object target, @NotNull EvaluationContext context,
                      @NotNull Consumer<Failure> failureAggregator) {
        if (target == null) {
            return;
        }
        if (isDisabled(target, context)) {
            return;
        }
        Object value = evaluatePath(target, context);
        if (value == null) {
            return;
        }
        Object testResultObj = evaluate(testExpr, target, context);
        if (testResultObj instanceof Boolean) {
            Boolean testResult = (Boolean) testResultObj;
            if (testResult.booleanValue()) {
                return;
            }
        }
        reportFailure(value, failureAggregator);
    }

}
