package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.LocalDateStringContainer;

import java.lang.annotation.*;

/**
 * Reports failure if the target string is not a valid date.
 *
 * @author slhynju
 */
@Repeatable(LocalDateStringContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LocalDateString {
    /**
     * the relative path to retrieve the target string.
     */
    String path();

    /**
     * the date pattern.
     */
    String pattern() default "yyyy-MM-dd";

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";
}
