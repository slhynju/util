package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class NotAllowedRule extends ObjectRuleSupport {

    public NotAllowedRule(@NotNull String path, @Nullable String when, @Nullable String messageKey,
                          @Nullable String internalErrorCode, @Nullable String clientErrorCode,
                          @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("not_allowed");
    }

    @Override
    protected void reviewValue(@NotNull Object value, @NotNull Consumer<Failure> failureAggregator) {
        reportFailure(value, failureAggregator);
    }

}
