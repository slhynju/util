package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.IntRangeContainer;

import java.lang.annotation.*;

/**
 * Reports failure if target int value is out of range.
 *
 * @author slhynju
 */
@Repeatable(IntRangeContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IntRange {

    /**
     * the relative path to retrieve the target string.
     */
    String path();

    /**
     * the minimum value allowed.
     */
    int min() default 0;

    /**
     * the maximum value allowed.
     */
    int max() default Integer.MAX_VALUE;

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";
}
