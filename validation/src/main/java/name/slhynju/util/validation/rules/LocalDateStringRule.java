package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class LocalDateStringRule extends StringRuleSupport {

    private final DateTimeFormatter formatter;

    public LocalDateStringRule(@NotNull String path, @NotNull String pattern, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("value_invalid");
        formatter = DateTimeFormatter.ofPattern(pattern);
    }

    @Override
    protected void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator) {
        try {
            formatter.parse(value);
        } catch (DateTimeParseException e) {
            reportFailure(value, failureAggregator);
        }
    }
}
