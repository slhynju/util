package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.Validator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author slhynju
 */
@SuppressWarnings({"TypeMayBeWeakened", "rawtypes", "ObjectAllocationInLoop"})
public final class ForEachRule extends ObjectRuleSupport {

    private final boolean nullElementAllowed;

    private final Validator childValidator;

    public ForEachRule(@NotNull String path, boolean nullElementAllowed, @NotNull Validator childValidator, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("required");
        this.nullElementAllowed = nullElementAllowed;
        this.childValidator = childValidator;
    }

    @SuppressWarnings("ChainOfInstanceofChecks")
    @Override
    protected void reviewValue(@NotNull Object value, @NotNull Consumer<Failure> failureAggregator) {
        if (value instanceof Object[]) {
            reviewArray((Object[]) value, failureAggregator);
            return;
        }
        if (value instanceof List) {
            reviewList((List) value, failureAggregator);
            return;
        }
        if (value instanceof Map) {
            reviewMap((Map) value, failureAggregator);
            return;
        }
        String message = buildClassCastMessage("Object array or List or Map", value);
        throw new ClassCastException(message);
    }

    private void reviewArray(@NotNull Object[] array, @NotNull Consumer<Failure> failureAggregator) {
        for (int I = 0; I < array.length; I++) {
            String elementPath = path + '[' + I + ']';
            Object element = array[I];
            if (element == null) {
                if (!nullElementAllowed) {
                    Failure failure = buildFailure(elementPath, null);
                    failureAggregator.accept(failure);
                }
                continue;
            }
            ChildFailureAggregator childFailureAggregator = new ChildFailureAggregator(elementPath, failureAggregator);
            childValidator.validate(element, childFailureAggregator);
        }
    }

    private void reviewList(@NotNull List list, @NotNull Consumer<Failure> failureAggregator) {
        for (int I = 0; I < list.size(); I++) {
            String elementPath = path + '[' + I + ']';
            Object element = list.get(I);
            if (element == null) {
                if (!nullElementAllowed) {
                    Failure failure = buildFailure(elementPath, null);
                    failureAggregator.accept(failure);
                }
                continue;
            }
            ChildFailureAggregator childFailureAggregator = new ChildFailureAggregator(elementPath, failureAggregator);
            childValidator.validate(element, childFailureAggregator);
        }
    }

    private void reviewMap(@NotNull Map<?, ?> map, @NotNull Consumer<Failure> failureAggregator) {
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            String elementPath = path + "['" + entry.getKey() + "']";
            Object element = entry.getValue();
            if (element == null) {
                if (!nullElementAllowed) {
                    Failure failure = buildFailure(elementPath, null);
                    failureAggregator.accept(failure);
                }
                continue;
            }
            ChildFailureAggregator childFailureAggregator = new ChildFailureAggregator(elementPath, failureAggregator);
            childValidator.validate(element, childFailureAggregator);
        }
    }

}
