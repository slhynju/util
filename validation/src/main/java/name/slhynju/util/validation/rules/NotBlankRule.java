package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * Reports a {@code Failure} if the target {@code String} value is a blank one.
 * It does not report {@code Failures} if the target {@code String} value is {@code null}.
 * @author slhynju
 */
public final class NotBlankRule extends StringRuleSupport {

    /**
     * Creates a new instance.
     * @param path a relative path the the root object. It shall not be {@code null}.
     * @param when a condition to enable this rule. It can be {@code null}. A {@code null} or empty value is treated as enabled.
     * @param messageKey an i18n client error message key. It can be {@code null}.
     * @param internalErrorCode an error code to use for internal monitoring. It can be {@code null}.
     * @param clientErrorCode an error code to use for external client reference. It can be {@code null}.
     * @param source a defining source of this rule. It shall not be {@code null}.
     * @param parser a SpEL expression parser. It shall not be {@code null}.
     */
    public NotBlankRule(@NotNull String path, @Nullable String when, @Nullable String messageKey,
                        @Nullable String internalErrorCode, @Nullable String clientErrorCode,
                        @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("not_blank");
    }

    @Override
    protected void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator) {
        String trimmedValue = value.trim();
        if (trimmedValue.isEmpty()) {
            reportFailure(value, failureAggregator);
        }
    }
}
