package name.slhynju.util.validation.rules;

import name.slhynju.util.FreeStringBuilder;
import name.slhynju.util.NumberUtil;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class IntStringRule extends StringRuleSupport {

    private final boolean signAllowed;

    private final int min;

    private final int max;

    public IntStringRule(@NotNull String path, boolean signAllowed, int min, int max, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("value_invalid");
        this.signAllowed = signAllowed;
        this.min = min;
        this.max = max;
        if (min > max) {
            String message = FreeStringBuilder.format("Invalid min value {} and max value {} found in source {}. Min value shall be no more than max value.",
                    String.valueOf(min), String.valueOf(max), source.getName());
            throw new IllegalArgumentException(message);
        }
    }

    @Override
    protected void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator) {
        if (!NumberUtil.isIntegerString(value)) {
            reportFailure(value, failureAggregator);
            return;
        }
        if (!signAllowed) {
            char firstChar = value.charAt(0);
            if (firstChar == '+' || firstChar == '-') {
                reportFailure(value, failureAggregator);
                return;
            }
        }
        try {
            int n = Integer.parseInt(value);
            if (n < min || n > max) {
                reportFailure(value, failureAggregator);
            }
        } catch (NumberFormatException e) {
            reportFailure(value, failureAggregator);
        }
    }

    @Override
    protected void addMessageParams(@NotNull Failure failure) {
        failure.setClientMessageParams(Integer.valueOf(min), Integer.valueOf(max));
    }
}
