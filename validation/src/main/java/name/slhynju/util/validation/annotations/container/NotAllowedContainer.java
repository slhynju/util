package name.slhynju.util.validation.annotations.container;

import name.slhynju.util.validation.annotations.NotAllowed;

import java.lang.annotation.*;

/**
 * @author slhynju
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NotAllowedContainer {

    NotAllowed[] value();

}
