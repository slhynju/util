package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.DecimalStringContainer;

import java.lang.annotation.*;

/**
 * Reports a {@code Failure} if the {@code path} expression is not evaluated as a valid decimal {@code String}.
 * <p>This rule does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}. See {@code @Required} rule instead.</p>
 * <p>This rule uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
 * @author slhynju
 */
@Repeatable(DecimalStringContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DecimalString {

    /**
     * A relative path to the root {@code Object}. Its evaluated value will be reported in the {@code Failure} if any.
     * <p>This rule does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}.</p>
     */
    String path();

    /**
     * Indicates whether the plus and minus signs are allowed. Allowed by default.
     */
    boolean signAllowed() default true;

    /**
     * The maximum fraction digits allowed. It shall be a non-negative integer.
     */
    int maxFractionDigits();

    /**
     * the minimum value allowed. 0.0 by default.
     */
    double min() default 0.0;

    /**
     * the maximum value allowed.
     */
    double max();

    /**
     * a {@code boolean} condition to enable this rule. A {@code null} or empty value is treated as enabled.
     * <p>This rule does not report {@code Failures} if the {@code when} condition is not evaluated as {@code true}.</p>
     */
    String when() default "";

    /**
     * the i18n client error message key.
     * <p>This rule uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
     * <p>This rule sets the {@code min} value and {@code max} value as message parameters, which could be used by the template.</p>
     */
    String messageKey() default "";

    /**
     * the error code to use for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code to use for external client reference.
     */
    String clientErrorCode() default "";
}
