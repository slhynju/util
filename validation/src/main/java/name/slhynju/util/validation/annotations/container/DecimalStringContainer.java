package name.slhynju.util.validation.annotations.container;

import name.slhynju.util.validation.annotations.DecimalString;

import java.lang.annotation.*;

/**
 * @author slhynju
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DecimalStringContainer {

    DecimalString[] value();
}
