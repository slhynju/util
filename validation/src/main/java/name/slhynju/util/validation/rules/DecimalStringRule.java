package name.slhynju.util.validation.rules;

import name.slhynju.util.FreeStringBuilder;
import name.slhynju.util.NumberUtil;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * Reports a {@code Failure} if the target {@code String} is not a valid decimal {@code String}.
 * <p>This {@code Rule} does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}.</p>
 * <p>This {@code Rule} uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
 *
 * @author slhynju
 */
public final class DecimalStringRule extends StringRuleSupport {

    /**
     * Indicates whether the plus and minus signs are allowed.
     */
    private final boolean signAllowed;

    /**
     * The maximum fraction digits allowed.
     */
    private final int maxFractionDigits;

    /**
     * the minimum value allowed.
     */
    private final double min;

    /**
     * the maximum value allowed.
     */
    private final double max;

    /**
     * Creates a new instance.
     *
     * @param path              a relative path the the root object. It shall not be {@code null}.
     * @param signAllowed       whether the plus and minus signs are allowed.
     * @param maxFractionDigits the maximum fraction digits allowed. It shall be a non-negative integer.
     * @param min               the minimum value allowed.
     * @param max               the maximum value allowed.
     * @param when              a condition to enable this {@code Rule}. It can be {@code null}. A {@code null} or empty value is treated as enabled.
     * @param messageKey        an i18n client error message key. It can be {@code null}.
     * @param internalErrorCode an error code to use for internal monitoring. It can be {@code null}.
     * @param clientErrorCode   an error code to use for external client reference. It can be {@code null}.
     * @param source            a defining source of this {@code Rule}. It shall not be {@code null}.
     * @param parser            a SpEL {@code ExpressionParser}. It shall not be {@code null}.
     * @throws IllegalArgumentException if the {@code maxFractionDigits} is negative, or the {@code min} value is greater than the {@code max} value.
     */
    public DecimalStringRule(@NotNull String path, boolean signAllowed, int maxFractionDigits, double min, double max, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        if (maxFractionDigits < 0) {
            String message = FreeStringBuilder.format("Invalid maxFractionDigits {} defined in source {}. It shall be a non-negative integer.",
                    String.valueOf(maxFractionDigits), source.getName());
            throw new IllegalArgumentException(message);
        }
        if (min > max) {
            String message = FreeStringBuilder.format("Invalid min value {} and max value {} defined in source {}. Min value shall be no more than max value.",
                    String.valueOf(min), String.valueOf(max), source.getName());
            throw new IllegalArgumentException(message);
        }
        guardMessageKey("value_invalid");
        this.signAllowed = signAllowed;
        this.maxFractionDigits = maxFractionDigits;
        this.min = min;
        this.max = max;
    }

    @Override
    protected void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator) {
        if (!NumberUtil.isNumberString(value) || reviewSign(value) || reviewFractionDigits(value) || reviewRange(value)) {
            reportFailure(value, failureAggregator);
        }
    }

    /**
     * Reviews whether the first code point is a plus or minus sign and not allowed by this {@code Rule}.
     * @param value a {@code String} value. It shall not be {@code null}.
     * @return {@code true} -- fails validation; {@code false} -- passes validation.
     */
    private boolean reviewSign(@NotNull String value) {
        if (signAllowed) {
            return false;
        }
        int signCodePoint = value.codePointAt(0);
        return NumberUtil.isSign(signCodePoint);
    }

    /**
     * Reviews whether the actual fraction digits exceeds the maximum digits allowed by this {@code Rule}.
     * @param value a {@code String} value. It shall not be {@code null}.
     * @return {@code true} -- fails validation; {@code false} -- passes validation.
     */
    private boolean reviewFractionDigits(@NotNull String value) {
        int separatorIndex = value.indexOf('.');
        if (separatorIndex == -1) {
            return false;
        }
        int fractionDigits = value.length() - separatorIndex - 1;
        return fractionDigits > maxFractionDigits;
    }

    /**
     * Reviews whether the value is out of the range allowed by this {@code Rule}.
     * @param value a {@code String} value. It shall not be {@code null}.
     * @return {@code true} -- fails validation; {@code false} -- passes validation.
     */
    private boolean reviewRange(@NotNull String value) {
        try {
            double d = Double.parseDouble(value);
            return d < min || d > max;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @Override
    protected void addMessageParams(@NotNull Failure failure) {
        failure.setClientMessageParams(Double.valueOf(min), Double.valueOf(max));
    }
}
