package name.slhynju.util.validation.rules;

import name.slhynju.util.FreeStringBuilder;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class DoubleRangeRule extends ObjectRuleSupport {

    private final double min;

    private final double max;

    public DoubleRangeRule(@NotNull String path, double min, double max, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("value_invalid");
        this.min = min;
        this.max = max;
        if (min > max) {
            String message = FreeStringBuilder.format("Invalid min value {} and max value {} found in source {}. Min value shall be no more than max value.",
                    String.valueOf(min), String.valueOf(max), source.getName());
            throw new IllegalArgumentException(message);
        }
    }

    @Override
    protected void reviewValue(@NotNull Object value, @NotNull Consumer<Failure> failureAggregator) {
        if (value instanceof Double) {
            Double d = (Double) value;
            if (d.doubleValue() < min || d.doubleValue() > max) {
                reportFailure(d, failureAggregator);
            }
            return;
        }
        String message = buildClassCastMessage("Double", value);
        throw new ClassCastException(message);
    }

    @Override
    protected void addMessageParams(@NotNull Failure failure) {
        failure.setClientMessageParams(Double.valueOf(min), Double.valueOf(max));
    }
}
