package name.slhynju.util.validation.rules;

import name.slhynju.util.StringUtil;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * Default implementation of {@code Rule}.
 * @author slhynju
 */
public abstract class RuleSupport implements Rule {

    /**
     * a relative path to the root object. Its evaluated value will be reported in the {@code Failure} if any.
     */
    @NonNls
    protected final @NotNull String path;

    /**
     * the parsed SpEL {@code Expression} of {@code path} attribute.
     */
    protected final @NotNull Expression pathExpr;

    /**
     * a {@code boolean} condition to enable this {@code Rule}. A {@code null} or empty value is treated as enabled.
     * <p>This {@code Rule} does not report {@code Failures} if the {@code when} condition is not evaluated as {@code true}.</p>
     */
    protected final @NotNull String when;

    /**
     * the parsed SpEL {@code Expression} of {@code when} attribute.
     */
    protected final @Nullable Expression whenExpr;

    /**
     * the defining source of this rule.
     */
    protected final @NotNull Class<?> source;

    /**
     * the i18n client error message key.
     */
    protected @Nullable String messageKey;

    /**
     * the error code to use for internal monitoring.
     */
    protected final @Nullable String internalErrorCode;

    /**
     * the error code to use for external client reference.
     */
    protected final @Nullable String clientErrorCode;

    /**
     * Creates a new instance.
     * @param path a relative path the the root object. It shall not be {@code null}.
     * @param when a condition to enable this {@code Rule}. It can be {@code null}. A {@code null} or empty value is treated as enabled.
     * @param messageKey an i18n client error message key. It can be {@code null}.
     * @param internalErrorCode an error code to use for internal monitoring. It can be {@code null}.
     * @param clientErrorCode an error code to use for external client reference. It can be {@code null}.
     * @param source a defining source of this {@code Rule}. It shall not be {@code null}.
     * @param parser a SpEL {@code ExpressionParser}. It shall not be {@code null}.
     */
    public RuleSupport(@NotNull String path, @Nullable String when, @Nullable String messageKey,
                       @Nullable String internalErrorCode, @Nullable String clientErrorCode,
                       @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        this.path = path;
        pathExpr = parser.parseExpression(path);
        // the default values in annotations do not support null. Therefore empty Strings are used.
        this.when = StringUtil.emptyToNull(when);
        whenExpr = StringUtil.isEmpty(when) ? null : parser.parseExpression(when);
        this.messageKey = StringUtil.emptyToNull(messageKey);
        this.internalErrorCode = StringUtil.emptyToNull(internalErrorCode);
        this.clientErrorCode = StringUtil.emptyToNull(clientErrorCode);
        this.source = source;
    }

    // apply() is implemented by child classes.

    /**
     * Checks whether this {@code Rule} is disabled against the target {@code Object}.
     * A {@code Rule} is disabled when the {@code when} condition is specified AND is not evaluated as {@code true}.
     * @param target a target {@code Object} to evaluate. It shall not be {@code null}.
     * @param context a SpEL {@code EvaluationContext}. It shall not be {@code null}.
     * @return {@code true} -- this {@code Rule} is disabled; {@code false} -- is enabled.
     */
    protected final boolean isDisabled(@NotNull Object target, @NotNull EvaluationContext context) {
        if (whenExpr == null) {
            return false;
        }
        Object whenResultObj = evaluate(whenExpr, target, context);
        if( whenResultObj instanceof Boolean ) {
            Boolean whenResult = (Boolean)whenResultObj;
            return ! whenResult.booleanValue();
        }
        return true;
    }

    /**
     * Evaluates the value specified by {@code path} attribute against the target {@code Object}.
     * @param target a target {@code Object} to evaluate. It shall not be {@code null}.
     * @param context a SpEL {@code EvaluationContext}. It shall not be {@code null}.
     * @return the evaluated value.
     */
    protected final @Nullable Object evaluatePath(@NotNull Object target, @NotNull EvaluationContext context) {
        return evaluate(pathExpr, target, context);
    }

    /**
     * Evaluates the value specified by a SpEL {@code Expression} against the target {@code Object}.
     * @param expr a SpEL {@code Expression}. It shall not be {@code null}.
     * @param target a target {@code Object} to evaluate. It shall not be {@code null}.
     * @param context a SpEL {@code EvaluationContext}. It shall not be {@code null}.
     * @return the evaluated value.
     */
    protected static @Nullable Object evaluate(@NotNull Expression expr, @NotNull Object target, @NotNull EvaluationContext context) {
        try {
            return expr.getValue(context, target);
        } catch (EvaluationException e) {
            // ignore the exception
            return null;
        }
    }

    /**
     * Reports a {@code Failure}.
     * @param value an evaluated value associated with the {@code path} attribute. It can be {@code null}.
     * @param failureAggregator an action to aggregate all reported {@code Failures}. It shall not be {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    protected final void reportFailure(@Nullable Object value, @NotNull Consumer<Failure> failureAggregator) {
        Failure failure = buildFailure(path, value);
        failureAggregator.accept(failure);
    }

    /**
     * Builds a {@code Failure} instance.
     * @param newPath a {@code path} to use in the {@code Failure} instance. It shall not be {@code null}.
     * @param value an evaluated value associated with the {@code path} attribute. It can be {@code null}.
     * @return a new {@code Failure} instance.
     */
    protected final @NotNull Failure buildFailure(@NotNull String newPath, @Nullable Object value) {
        Failure failure = new Failure(newPath, value, source);
        failure.setInternalErrorCode(internalErrorCode);
        failure.setClientErrorCode(clientErrorCode);
        failure.setClientMessageKey(messageKey);
        addMessageParams(failure);
        return failure;
    }

    /**
     * Adds the parameter values of the i18n client error message to the {@code Failure} instance.
     * @param failure a {@code Failure} to report. It shall not be {@code null}.
     */
    protected void addMessageParams(@NotNull Failure failure) {
        // DO NOTHING
    }

    /**
     * Provides a default i18n client error message key if not specified by annotations.
     * @param defaultSuffix a default suffix to use. It shall not be {@code null}.
     */
    protected final void guardMessageKey(@NotNull String defaultSuffix) {
        if (messageKey == null) {
            messageKey = path + '.' + defaultSuffix;
        }
    }

    /**
     * Builds an error message for {@code ClassCastException} during evaluation.
     * @param expectedType the expected type. It shall not be {@code null}.
     * @param actualValue the actual evaluated value. It shall not be {@code null}.
     * @return an error message.
     */
    /*protected final String buildClassCastMessage(@NotNull String expectedType, @NotNull Object actualValue) {
        return FreeStringBuilder.format("Invalid path {} defined in source {}. {} type is expected but the actual evaluated type is {}.",
                path, source.getName(), expectedType, actualValue.getClass().getName());
    }*/
}
