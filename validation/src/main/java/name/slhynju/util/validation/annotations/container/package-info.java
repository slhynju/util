/**
 * The container annotations required by Java annotation mechanism. Not expected to be used by application code.
 * @author slhynju
 */
package name.slhynju.util.validation.annotations.container;