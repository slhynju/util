package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.ForEachContainer;

import java.lang.annotation.*;

/**
 * @author slhynju
 */
@Repeatable(ForEachContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ForEach {

    /**
     * the relative path to retrieve the target string.
     */
    String path();

    boolean nullElementAllowed() default false;

    Class<?> childValidator();

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";
}
