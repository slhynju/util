/**
 * The actual rules associated with the annotations.
 * @author slhynju
 */
package name.slhynju.util.validation.rules;