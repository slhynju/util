package name.slhynju.util.validation.impl;

import name.slhynju.util.validation.Failure;
import name.slhynju.util.validation.Validator;
import name.slhynju.util.validation.rules.Rule;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.List;
import java.util.function.Consumer;

/**
 * A rule-based validator.
 *
 * @author slhynju
 */
public final class RuleValidator implements Validator {

    /**
     * The validation {@code Rules}.
     */
    private final @NotNull List<Rule> rules;

    /**
     * Creates a new instance with {@code Rules}.
     * <p>This constructor is used by {@code ValidatorFactory}.</p>
     * @param rules a {@code List} of validation {@code Rules}. It shall not be {@code null}.
     */
    @SuppressWarnings("BoundedWildcard")
    public RuleValidator(@NotNull List<Rule> rules) {
        //no copy for performance reason.
        this.rules = rules;
    }

    @Override
    public void validate(@Nullable Object target, @NotNull Consumer<Failure> failureAggregator) {
        EvaluationContext context = SimpleEvaluationContext.forReadOnlyDataBinding()
                .withRootObject(target).build();
        for (Rule rule : rules) {
            rule.apply(target, context, failureAggregator);
        }
    }
}
