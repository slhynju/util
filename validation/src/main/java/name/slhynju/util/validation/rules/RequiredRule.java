package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class RequiredRule extends RuleSupport {

    public RequiredRule(@NotNull String path, @Nullable String when, @Nullable String messageKey,
                        @Nullable String internalErrorCode, @Nullable String clientErrorCode,
                        @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("required");
    }

    @Override
    public void apply(@Nullable Object target, @NotNull EvaluationContext context,
                      @NotNull Consumer<Failure> failureAggregator) {
        if (target == null) {
            reportFailure(null, failureAggregator);
            return;
        }
        if (isDisabled(target, context)) {
            return;
        }
        Object value = evaluatePath(target, context);
        if (value == null) {
            reportFailure(null, failureAggregator);
        }
    }

}
