package name.slhynju.util.validation;

import name.slhynju.util.validation.annotations.*;
import name.slhynju.util.validation.impl.RuleValidator;
import name.slhynju.util.validation.rules.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * A factory of {@code Validators}.
 *
 * @author slhynju
 */
@SuppressWarnings({"OverlyCoupledClass", "BoundedWildcard", "ObjectAllocationInLoop"})
public final class ValidatorFactory {

    /**
     * The cached {@code Validators}. The key is the defining source. The value is the {@code Validator} instance.
     */
    private static final @NotNull ConcurrentMap<Class<?>, Validator> validators = new ConcurrentHashMap<>();

    /**
     * The SpEL {@code ExpressionParser}.
     */
    private static final @NotNull ExpressionParser exprParser = new SpelExpressionParser();

    /**
     * Returns a {@code Validator}. Creates one on the fly if necessary.
     *
     * @param rulesContainer a container {@code Class} annotated with validation rules. It shall not be {@code null}.
     * @return a {@code Validator}.
     */
    @SuppressWarnings("ReuseOfLocalVariable")
    public static @NotNull Validator getInstance(@NotNull Class<?> rulesContainer) {
        Validator validator = validators.get(rulesContainer);
        if (validator == null) {
            validator = create(rulesContainer);
            validators.putIfAbsent(rulesContainer, validator);
            return validators.get(rulesContainer);
        }
        return validator;
    }

    /**
     * Creates a {@code Validator} based on the rule annotations.
     * @param rulesContainer a container {@code Class} annotated with validation rules. It shall not be {@code null}.
     * @return a new {@code Validator}.
     */
    private static @NotNull Validator create(@NotNull Class<?> rulesContainer) {
        List<Rule> rules = discoverRules(rulesContainer);
        return new RuleValidator(rules);
    }

    /**
     * Discovers the validation {@code Rules} based on the rule annotations.
     * @param rulesContainer a container {@code Class} annotated with validation rules. It shall not be {@code null}.
     * @return a {@code List} of discovered {@code Rules}.
     */
    private static @NotNull List<Rule> discoverRules(@NotNull Class<?> rulesContainer) {
        List<Rule> rules = new ArrayList<>();
        discoverNotAllowedRules(rulesContainer, rules);
        discoverRequiredRules(rulesContainer, rules);
        discoverNotBlankRules(rulesContainer, rules);
        discoverLengthRules(rulesContainer, rules);
        discoverValidCharsRules(rulesContainer, rules);
        discoverI18NValidCharsRules(rulesContainer, rules);
        discoverInRules(rulesContainer, rules);
        discoverIntRangeRules(rulesContainer, rules);
        discoverDoubleRangeRules(rulesContainer, rules);
        discoverAssertRules(rulesContainer, rules);
        discoverCountryCodeRules(rulesContainer, rules);
        discoverLocalDateStringRules(rulesContainer, rules);
        discoverIntStringRules(rulesContainer, rules);
        discoverDecimalStringRules(rulesContainer, rules);
        return rules;
    }

    private static void discoverNotAllowedRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        NotAllowed[] annotations = rulesContainer.getAnnotationsByType(NotAllowed.class);
        for (NotAllowed annotation : annotations) {
            Rule rule = new NotAllowedRule(annotation.path(), annotation.when(), annotation.messageKey(),
                    annotation.internalErrorCode(), annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverRequiredRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        Required[] annotations = rulesContainer.getAnnotationsByType(Required.class);
        for (Required annotation : annotations) {
            Rule rule = new RequiredRule(annotation.path(), annotation.when(), annotation.messageKey(),
                    annotation.internalErrorCode(), annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverNotBlankRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        NotBlank[] annotations = rulesContainer.getAnnotationsByType(NotBlank.class);
        for (NotBlank annotation : annotations) {
            Rule rule = new NotBlankRule(annotation.path(), annotation.when(), annotation.messageKey(),
                    annotation.internalErrorCode(), annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverLengthRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        Length[] annotations = rulesContainer.getAnnotationsByType(Length.class);
        for (Length annotation : annotations) {
            Rule rule = new LengthRule(annotation.path(), annotation.minLength(), annotation.maxLength(), annotation.when(),
                    annotation.messageKey(), annotation.internalErrorCode(), annotation.clientErrorCode(),
                    rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverValidCharsRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        ValidChars[] annotations = rulesContainer.getAnnotationsByType(ValidChars.class);
        for (ValidChars annotation : annotations) {
            Rule rule = new ValidCharsRule(annotation.path(), annotation.letters(), annotation.digits(), annotation.space(),
                    annotation.moreValidChars(), annotation.when(), annotation.messageKey(), annotation.internalErrorCode(),
                    annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverI18NValidCharsRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        I18NValidChars[] annotations = rulesContainer.getAnnotationsByType(I18NValidChars.class);
        for (I18NValidChars annotation : annotations) {
            Rule rule = new I18NValidCharsRule(annotation.path(), annotation.letters(), annotation.digits(), annotation.space(),
                    annotation.moreValidChars(), annotation.when(), annotation.messageKey(), annotation.internalErrorCode(),
                    annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverInRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        In[] annotations = rulesContainer.getAnnotationsByType(In.class);
        for (In annotation : annotations) {
            Rule rule = new InRule(annotation.path(), annotation.list(), annotation.when(), annotation.messageKey(),
                    annotation.internalErrorCode(), annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverIntRangeRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        IntRange[] annotations = rulesContainer.getAnnotationsByType(IntRange.class);
        for (IntRange annotation : annotations) {
            Rule rule = new IntRangeRule(annotation.path(), annotation.min(), annotation.max(), annotation.when(),
                    annotation.messageKey(), annotation.internalErrorCode(), annotation.clientErrorCode(),
                    rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverDoubleRangeRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        DoubleRange[] annotations = rulesContainer.getAnnotationsByType(DoubleRange.class);
        for (DoubleRange annotation : annotations) {
            Rule rule = new DoubleRangeRule(annotation.path(), annotation.min(), annotation.max(), annotation.when(),
                    annotation.messageKey(), annotation.internalErrorCode(), annotation.clientErrorCode(),
                    rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverAssertRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        Assert[] annotations = rulesContainer.getAnnotationsByType(Assert.class);
        for (Assert annotation : annotations) {
            Rule rule = new AssertRule(annotation.path(), annotation.test(), annotation.when(),
                    annotation.messageKey(), annotation.internalErrorCode(), annotation.clientErrorCode(),
                    rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverCountryCodeRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        CountryCode[] annotations = rulesContainer.getAnnotationsByType(CountryCode.class);
        for (CountryCode annotation : annotations) {
            Rule rule = new CountryCodeRule(annotation.path(), annotation.length(), annotation.when(),
                    annotation.messageKey(), annotation.internalErrorCode(), annotation.clientErrorCode(),
                    rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverLocalDateStringRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        LocalDateString[] annotations = rulesContainer.getAnnotationsByType(LocalDateString.class);
        for (LocalDateString annotation : annotations) {
            Rule rule = new LocalDateStringRule(annotation.path(), annotation.pattern(), annotation.when(),
                    annotation.messageKey(), annotation.internalErrorCode(), annotation.clientErrorCode(),
                    rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverIntStringRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        IntString[] annotations = rulesContainer.getAnnotationsByType(IntString.class);
        for (IntString annotation : annotations) {
            Rule rule = new IntStringRule(annotation.path(), annotation.signAllowed(), annotation.min(),
                    annotation.max(), annotation.when(), annotation.messageKey(), annotation.internalErrorCode(),
                    annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverDecimalStringRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        DecimalString[] annotations = rulesContainer.getAnnotationsByType(DecimalString.class);
        for (DecimalString annotation : annotations) {
            Rule rule = new DecimalStringRule(annotation.path(), annotation.signAllowed(), annotation.maxFractionDigits(),
                    annotation.min(), annotation.max(), annotation.when(), annotation.messageKey(),
                    annotation.internalErrorCode(), annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }

    private static void discoverForEachRules(@NotNull Class<?> rulesContainer, @NotNull List<Rule> rules) {
        ForEach[] annotations = rulesContainer.getAnnotationsByType(ForEach.class);
        for (ForEach annotation : annotations) {
            //TODO address the circular dependency.
            Class<?> childValidatorContainer = annotation.childValidator();
            Validator childValidator = getInstance(childValidatorContainer);
            Rule rule = new ForEachRule(annotation.path(), annotation.nullElementAllowed(), childValidator,
                    annotation.when(), annotation.messageKey(), annotation.internalErrorCode(),
                    annotation.clientErrorCode(), rulesContainer, exprParser);
            rules.add(rule);
        }
    }
}

