package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.AssertContainer;

import java.lang.annotation.*;

/**
 * Reports a {@code Failure} if the {@code test} condition is not evaluated as {@code true}.
 * <p>This rule does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}. See {@code @Required} rule instead.</p>
 * <p>This rule uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
 * @author slhynju
 */
@Repeatable(AssertContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Assert {

    /**
     * a relative path to the root object. Its evaluated value will be reported in the {@code Failure} if any.
     * <p>This rule does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}.</p>
     */
    String path();

    /**
     * a {@code boolean} condition to check.
     * <p>This rule reports a {@code Failure} if the {@code test} condition is not evaluated as {@code true}.</p>
     */
    String test();

    /**
     * a {@code boolean} condition to enable this rule. A {@code null} or empty value is treated as enabled.
     * <p>This rule does not report {@code Failures} if the {@code when} condition is not evaluated as {@code true}.</p>
     * @return a {@code boolean} condition to enable this rule.
     */
    String when() default "";

    /**
     * the i18n client error message key.
     * <p>This rule uses a default message key of {@code ${path}.value_invalid} if the message key is not specified.</p>
     */
    String messageKey() default "";

    /**
     * the error code to use for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code to use for external client reference.
     */
    String clientErrorCode() default "";

}
