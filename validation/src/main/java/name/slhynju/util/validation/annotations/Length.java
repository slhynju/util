package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.LengthContainer;

import java.lang.annotation.*;

/**
 * Reports failure if the length of target string is invalid.
 * Note this rule does not report failure for null values. Use @Required annotation instead.
 *
 * @author slhynju
 */
@Repeatable(LengthContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Length {

    /**
     * the relative path to retrieve the target string.
     */
    String path();

    /**
     * the minimum length required. any negative value is treated as zero.
     */
    int minLength() default 0;

    /**
     * the maximum length allowed. any negative value is treated as zero.
     */
    int maxLength();

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";

}
