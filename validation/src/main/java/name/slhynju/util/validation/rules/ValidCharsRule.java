package name.slhynju.util.validation.rules;

import name.slhynju.util.StringUtil;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.Set;
import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class ValidCharsRule extends StringRuleSupport {

    private final boolean lettersAllowed;

    private final boolean digitsAllowed;

    private final boolean spaceAllowed;

    private final @NotNull Set<Character> moreValidChars;

    @SuppressWarnings("BooleanParameter")
    public ValidCharsRule(@NotNull String path, boolean lettersAllowed, boolean digitsAllowed, boolean spaceAllowed, @Nullable String moreValidChars, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("chars_invalid");
        this.lettersAllowed = lettersAllowed;
        this.digitsAllowed = digitsAllowed;
        this.spaceAllowed = spaceAllowed;
        this.moreValidChars = StringUtil.splitChars(moreValidChars);
    }

    @Override
    protected void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator) {
        for (int I = 0; I < value.length(); I++) {
            char ch = value.charAt(I);
            if (notValid(ch)) {
                reportFailure(value, failureAggregator);
                return;
            }
        }
    }

    @SuppressWarnings({"SimplifiableIfStatement", "OverlyComplexBooleanExpression", "CharacterComparison"})
    private boolean notValid(char ch) {
        Character chObj = Character.valueOf(ch);
        if (moreValidChars.contains(chObj)) {
            return false;
        }
        if (('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z')) {
            return !lettersAllowed;
        }
        if ('0' <= ch && ch <= '9') {
            return !digitsAllowed;
        }
        if (ch == ' ') {
            return !spaceAllowed;
        }
        return true;
    }
}
