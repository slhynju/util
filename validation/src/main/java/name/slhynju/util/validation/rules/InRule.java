package name.slhynju.util.validation.rules;

import name.slhynju.util.collection.CollectionUtil;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class InRule extends ObjectRuleSupport {

    private final @NotNull Set<Object> validValues;

    public InRule(@NotNull String path, @NotNull String list, @Nullable String when, @Nullable String messageKey,
                  @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source,
                  @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("value_invalid");
        Expression listExpr = parser.parseExpression(list);
        validValues = CollectionUtil.toSet(listExpr.getValue(List.class));
    }

    @Override
    protected void reviewValue(@NotNull Object value, @NotNull Consumer<Failure> failureAggregator) {
        if (validValues.contains(value)) {
            return;
        }
        reportFailure(value, failureAggregator);
    }
}
