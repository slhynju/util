package name.slhynju.util.validation.annotations.container;

import name.slhynju.util.validation.annotations.IntString;

import java.lang.annotation.*;

/**
 * @author slhynju
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IntStringContainer {

    IntString[] value();
}
