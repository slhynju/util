package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.NotBlankContainer;

import java.lang.annotation.*;

/**
 * Reports failure if the target string exists and is blank.
 * Note this rule does not report failure for null values. See @Required annotation instead.
 *
 * @author slhynju
 */
@Repeatable(NotBlankContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NotBlank {

    /**
     * the relative path to retrieve the target string.
     */
    String path();

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";

}
