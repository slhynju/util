package name.slhynju.util.validation.rules;

import name.slhynju.util.FreeStringBuilder;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class LengthRule extends StringRuleSupport {

    private final int minLength;

    private final int maxLength;

    public LengthRule(@NotNull String path, int minLength, int maxLength, @Nullable String when, @Nullable String messageKey, @Nullable String internalErrorCode, @Nullable String clientErrorCode, @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("length_invalid");
        this.minLength = Math.max(minLength, 0);
        this.maxLength = Math.max(maxLength, 0);
        if (this.minLength > this.maxLength) {
            String message = FreeStringBuilder.format("Invalid min length {} and max length {} found in source {}. Min length shall be no more than max length.",
                    String.valueOf(minLength), String.valueOf(maxLength), source.getName());
            throw new IllegalArgumentException(message);
        }
    }

    @Override
    protected void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator) {
        int valueLength = value.length();
        if (valueLength < minLength || valueLength > maxLength) {
            reportFailure(value, failureAggregator);
        }
    }

    @Override
    protected void addMessageParams(@NotNull Failure failure) {
        failure.setClientMessageParams(Integer.valueOf(minLength), Integer.valueOf(maxLength));
    }
}
