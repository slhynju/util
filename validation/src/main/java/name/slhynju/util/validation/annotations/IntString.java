package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.IntStringContainer;

import java.lang.annotation.*;

/**
 * @author slhynju
 */
@Repeatable(IntStringContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IntString {

    /**
     * the relative path to retrieve the target string.
     */
    String path();

    /**
     * Whether the +/- sign prefix allowed.
     */
    boolean signAllowed() default true;

    /**
     * the minimum value allowed.
     */
    int min() default 0;

    /**
     * the maximum value allowed.
     */
    int max() default Integer.MAX_VALUE;

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";
}
