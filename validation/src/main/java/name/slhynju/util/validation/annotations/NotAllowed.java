package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.NotAllowedContainer;

import java.lang.annotation.*;

/**
 * Reports failure if certain prohibited fields are incorrectly filled by clients.
 * This rule is opposite to the @Required rule.
 *
 * @author slhynju
 */
@Repeatable(NotAllowedContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NotAllowed {

    /**
     * the relative path to retrieve the target object.
     */
    String path();

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";
}
