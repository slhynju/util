package name.slhynju.util.validation;

import name.slhynju.util.BeanStringBuilder;
import name.slhynju.util.EqualsUtil;
import name.slhynju.util.HashCodeBuilder;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A single validation failure.
 *
 * @author slhynju
 */
public class Failure {

    /**
     * the relative path to the root object.
     */
    @NonNls
    protected @NotNull String path;

    /**
     * the evaluated value. Note this may contain sensitive information.
     */
    @SuppressWarnings("FieldNotUsedInToString")
    protected final @Nullable Object value;

    /**
     * the error code to use for internal monitoring.
     */
    @NonNls
    protected @Nullable String internalErrorCode = null;

    /**
     * the error code to use for external client reference.
     */
    @NonNls
    protected @Nullable String clientErrorCode = null;

    /**
     * the i18n error message key to use for external client reference. A response builder in application code shall
     * build an i18n error message based on this message key and parameter values provided in this class.
     * @see name.slhynju.util.io.I18NResource#format(String, Object...)
     */
    @NonNls
    protected @Nullable String clientMessageKey = null;

    /**
     * the parameter values of the i18n error message.
     */
    protected @Nullable Object[] clientMessageParams = null;

    /**
     * the validator source that reports this failure.
     */
    protected final @NotNull Class<?> source;

    /**
     * Reports a new validation {@code Failure}.
     * @param path a relative path to the root object. It shall not be {@code null}.
     * @param value an evaluated value. It can be {@code null}.
     * @param source a validator source. It shall not be {@code null}.
     */
    public Failure(@NotNull String path, @Nullable Object value, @NotNull Class<?> source) {
        this.path = path;
        this.value = value;
        this.source = source;
    }

    /**
     * Returns the relative path to the root {@code Object}.
     * @return the relative path to the root {@code Object}.
     */
    public @NotNull String getPath() {
        return path;
    }

    /**
     * Adds a prefix to the {@code path}.
     * @param prefix a prefix to add to the {@code path}. It shall not be {@code null}.
     */
    public void addPathPrefix(@NotNull String prefix) {
        path = prefix + '.' + path;
    }

    /**
     * Returns the evaluated value.
     * @return the evaluated value.
     */
    public @Nullable Object getValue() {
        return value;
    }

    /**
     * Returns the internal error code.
     * @return the internal error code.
     */
    public @Nullable String getInternalErrorCode() {
        return internalErrorCode;
    }

    /**
     * Sets the internal error code.
     * @param internalErrorCode an internal error code. It can be {@code null}.
     */
    public void setInternalErrorCode(@Nullable String internalErrorCode) {
        this.internalErrorCode = internalErrorCode;
    }

    /**
     * Returns the client error code.
     * @return the client error code.
     */
    public @Nullable String getClientErrorCode() {
        return clientErrorCode;
    }

    /**
     * Sets the client error code.
     * @param clientErrorCode a client error code. It can be {@code null}.
     */
    public void setClientErrorCode(@Nullable String clientErrorCode) {
        this.clientErrorCode = clientErrorCode;
    }

    /**
     * Returns the client error message key.
     * @return the client error message key.
     */
    public @Nullable String getClientMessageKey() {
        return clientMessageKey;
    }

    /**
     * Sets the client error message key.
     * @param clientMessageKey a client error message key. It can be {@code null}.
     */
    public void setClientMessageKey(@Nullable String clientMessageKey) {
        this.clientMessageKey = clientMessageKey;
    }

    /**
     * Returns the client error message parameter values.
     * @return the client error message parameter values.
     */
    public @Nullable Object[] getClientMessageParams() {
        return clientMessageParams;
    }

    /**
     * Sets the client error message parameter values.
     * @param clientMessageParams an array of client error message parameter values. It can be {@code null}.
     */
    public void setClientMessageParams(@Nullable Object... clientMessageParams) {
        this.clientMessageParams = clientMessageParams;
    }

    /**
     * Returns the simple name of the validator source that reports this {@code Failure}.
     * @return the simple name of the validator source.
     */
    public @NotNull String getSource() {
        return source.getSimpleName();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Failure) {
            Failure other = (Failure) obj;
            return EqualsUtil.isEquals(path, other.path) && EqualsUtil.isEquals(source, other.source)
                    && EqualsUtil.isEquals(clientMessageKey, other.clientMessageKey);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(path).append(source).append(clientMessageKey).toValue();
    }

    @Override
    public @NotNull String toString() {
        // value is skipped as it may contain sensitive information.
        return new BeanStringBuilder(Failure.class).append("path", path)
                .append("internalErrorCode", internalErrorCode).append("clientErrorCode", clientErrorCode)
                .append("clientMessageKey", clientMessageKey).append("clientMessageParams", clientMessageParams)
                .append("source", source).toS();
    }
}
