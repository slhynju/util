package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * Rule to check a non-null object value.
 *
 * @author slhynju
 */
public abstract class ObjectRuleSupport extends RuleSupport {

    public ObjectRuleSupport(@NotNull String path, @Nullable String when, @Nullable String messageKey,
                             @Nullable String internalErrorCode, @Nullable String clientErrorCode,
                             @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
    }

    @Override
    public void apply(@Nullable Object target, @NotNull EvaluationContext context,
                      @NotNull Consumer<Failure> failureAggregator) {
        if (target == null) {
            return;
        }
        if (isDisabled(target, context)) {
            return;
        }
        Object value = evaluatePath(target, context);
        if (value == null) {
            return;
        }
        reviewValue(value, failureAggregator);
    }

    protected abstract void reviewValue(@NotNull Object value, @NotNull Consumer<Failure> failureAggregator);

}
