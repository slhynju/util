package name.slhynju.util.validation.rules;

import name.slhynju.util.StringUtil;
import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.ExpressionParser;

import java.util.Set;
import java.util.function.Consumer;

/**
 * @author slhynju
 */
public final class I18NValidCharsRule extends StringRuleSupport {

    private final boolean lettersAllowed;

    private final boolean digitsAllowed;

    private final boolean spaceAllowed;

    private final @NotNull Set<Integer> moreValidCodePoints;

    @SuppressWarnings("BooleanParameter")
    public I18NValidCharsRule(@NotNull String path, boolean lettersAllowed, boolean digitsAllowed, boolean spaceAllowed,
                              @Nullable String moreValidChars, @Nullable String when, @Nullable String messageKey,
                              @Nullable String internalErrorCode, @Nullable String clientErrorCode,
                              @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
        guardMessageKey("chars_invalid");
        this.lettersAllowed = lettersAllowed;
        this.digitsAllowed = digitsAllowed;
        this.spaceAllowed = spaceAllowed;
        moreValidCodePoints = StringUtil.splitCodePoints(moreValidChars);
    }

    @Override
    protected void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator) {
        for (int I = 0; I < value.length(); I++) {
            int codePoint = value.codePointAt(I);
            if (notValid(codePoint)) {
                reportFailure(value, failureAggregator);
                return;
            }
        }
    }

    @SuppressWarnings("SimplifiableIfStatement")
    private boolean notValid(int codePoint) {
        Integer codePointObj = Integer.valueOf(codePoint);
        if (moreValidCodePoints.contains(codePointObj)) {
            return false;
        }
        if (Character.isLetter(codePoint)) {
            return !lettersAllowed;
        }
        if (Character.isDigit(codePoint)) {
            return !digitsAllowed;
        }
        if (Character.isSpaceChar(codePoint)) {
            return !spaceAllowed;
        }
        return true;
    }
}
