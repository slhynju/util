package name.slhynju.util.validation.annotations.container;

import name.slhynju.util.validation.annotations.I18NValidChars;

import java.lang.annotation.*;

/**
 * @author slhynju
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface I18NValidCharsContainer {

    I18NValidChars[] value();
}
