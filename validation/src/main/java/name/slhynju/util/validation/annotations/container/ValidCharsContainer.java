package name.slhynju.util.validation.annotations.container;

import name.slhynju.util.validation.annotations.ValidChars;

import java.lang.annotation.*;

/**
 * @author slhynju
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidCharsContainer {

    ValidChars[] value();
}
