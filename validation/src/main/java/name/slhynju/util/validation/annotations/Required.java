package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.RequiredContainer;

import java.lang.annotation.*;

/**
 * Reports failure if some mandatory fields are missing.
 * If message key is not specified, it uses default key of path + ".required".
 *
 * @author slhynju
 */
@Repeatable(RequiredContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Required {

    /**
     * the relative path to retrieve the target object.
     */
    String path();

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";

}
