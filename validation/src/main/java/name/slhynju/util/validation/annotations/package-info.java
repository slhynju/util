/**
 * The annotations of validation rules. They can be selectively used by application codes.
 * @author slhynju
 */
package name.slhynju.util.validation.annotations;