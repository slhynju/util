package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;

import java.util.function.Consumer;

/**
 * A {@code Rule} that checks a non-null {@code String} value.
 * <p>This {@code Rule} does not report {@code Failures} if the {@code path} expression is evaluated as {@code null}.</p>
 * @author slhynju
 */
public abstract class StringRuleSupport extends RuleSupport {

    /**
     * Creates a new instance.
     * @param path a relative path the the root object. It shall not be {@code null}.
     * @param when a condition to enable this {@code Rule}. It can be {@code null}. A {@code null} or empty value is treated as enabled.
     * @param messageKey an i18n client error message key. It can be {@code null}.
     * @param internalErrorCode an error code to use for internal monitoring. It can be {@code null}.
     * @param clientErrorCode an error code to use for external client reference. It can be {@code null}.
     * @param source a defining source of this {@code Rule}. It shall not be {@code null}.
     * @param parser a SpEL {@code ExpressionParser}. It shall not be {@code null}.
     */
    public StringRuleSupport(@NotNull String path, @Nullable String when, @Nullable String messageKey,
                             @Nullable String internalErrorCode, @Nullable String clientErrorCode,
                             @NotNull Class<?> source, @NotNull ExpressionParser parser) {
        super(path, when, messageKey, internalErrorCode, clientErrorCode, source, parser);
    }

    @Override
    public void apply(@Nullable Object target, @NotNull EvaluationContext context,
                      @NotNull Consumer<Failure> failureAggregator) {
        if (target == null) {
            return;
        }
        if (isDisabled(target, context)) {
            return;
        }
        Object value = evaluatePath(target, context);
        if (value == null) {
            return;
        }
        if (value instanceof String) {
            reviewValue((String) value, failureAggregator);
            return;
        }
        reportFailure(value,failureAggregator);
    }

    /**
     * Reviews a non-null {@code String} value and reports {@code Failures} if any.
     * @param value a {@code String} value. It shall not be {@code null}.
     * @param failureAggregator an action to aggregate all reported {@code Failures}. It shall not be {@code null}.
     */
    protected abstract void reviewValue(@NotNull String value, @NotNull Consumer<Failure> failureAggregator);
}
