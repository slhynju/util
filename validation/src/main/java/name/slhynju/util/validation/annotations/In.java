package name.slhynju.util.validation.annotations;

import name.slhynju.util.validation.annotations.container.InContainer;

import java.lang.annotation.*;

/**
 * Reports failure if the object value is not in the list of valid values.
 *
 * @author slhynju
 */
@Repeatable(InContainer.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface In {

    /**
     * the relative path to retrieve the target object.
     */
    String path();

    /**
     * the list of valid values.
     */
    String list();

    /**
     * a condition to enable this rule. A rule is enabled by default.
     */
    String when() default "";

    /**
     * the i18n message key.
     */
    String messageKey() default "";

    /**
     * the error code for internal monitoring.
     */
    String internalErrorCode() default "";

    /**
     * the error code for external client reference.
     */
    String clientErrorCode() default "";

}
