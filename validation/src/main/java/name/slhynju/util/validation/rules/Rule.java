package name.slhynju.util.validation.rules;

import name.slhynju.util.validation.Failure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.expression.EvaluationContext;

import java.util.function.Consumer;

/**
 * A validation rule.
 * TODO: IpRule, PatternRule, ClassRule
 * @author slhynju
 */
@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface Rule {

    /**
     * Validates the target {@code Object} with this {@code Rule}.
     * @param target a target {@code Object} to validate. It can be {@code null}.
     * @param context a SpEL {@code EvaluationContext}. It shall not be {@code null}.
     * @param failureAggregator an action to aggregate all reported {@code Failures}. It shall not be {@code null}.
     * @throws ClassCastException if the evaluated value does not belong to the expected type.
     */
    void apply(@Nullable Object target, @NotNull EvaluationContext context,
               @NotNull Consumer<Failure> failureAggregator);
}
