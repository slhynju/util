/**
 * The internal implementation of {@code Validator}.
 * @author slhynju
 */
package name.slhynju.util.validation.impl;