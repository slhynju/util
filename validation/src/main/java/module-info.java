module name.slhynju.util.validation {
    requires org.jetbrains.annotations;
    requires spring.expression;
    requires name.slhynju.util;

    exports name.slhynju.util.validation;
    exports name.slhynju.util.validation.annotations;
}