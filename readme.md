
# Overview
slhynju-util is a lightweight utility library authored by slhynju. It requires Java 8 to compile and run. 

# Key Classes

## name.slhynju package
Root package of the library.

* ApplicationException. Default Exception of the framework. It is a RuntimeException.
* UnexpectedApplicationFlowException. A defensive exception to indicate flows that shall never happen.

## name.slhynju.util
Utility classes for common data types and java.lang package.

* BeanStringBuilder. To support toString() method for simple POJOs.
* HashCodeBuilder. To support hashCode() method for simple POJOs.
* EqualsUtil. To support equals() method for simple POJOs.
* CompareToBuilder. To support comparison for simple POJOs.
* FreeStringBuilder. To freely build a String.
* StringUtil. To support String.
* DateUtil. To support Date and Calendar.
* NumberUtil. To support number types.
* ClassUtil. To support reflection.
* I18NResource. An alternative solution to ResourceBundle to support internationalization.
* RandomGenerator. To support random data generation.

## name.slhynju.util.collection
Utility classes for java.util package.

* CollectionUtil. To support collection and arrays.

## name.slhynju.util.io
Utility classes for common IO operations.

## name.slhynju.util.nio
Utility classes for common NIO operations.

## name.slhynju.util.sql
Utility classes for SQL operations.
 
# License
This library is licensed under Apache License, Version 2.0. See [license](license).

# Author
You may contact the author at 
new StringBuilder("slhynju").append("@").append("gmail.com").toString().